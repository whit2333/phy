      SUBROUTINE EXRPNT( ISTR, * )
C
      IMPLICIT REAL*8 (A-H,O-Z)

      INTEGER*4 LENVSM, MXEXPR, MXFUN, MXCODE, MXOPER, MXSTR, MXQCOD
      PARAMETER (LENVSM =   32, MXEXPR = 3000, MXFUN = 222, MXQCOD = 20)
      PARAMETER (MXCODE = 2200, MXOPER =   35, MXSTR = 250)

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      INTEGER*4 TVARALC, TVARADR, TVARLEN, TVARNUM
C
C   TVARALC = number of bytes allocated for text variable characteristics
C             (does not include the data)
C   TVARADR = starting address of the text variable table
C   TVARLEN = number of bytes needed for an entry in the table (60 bytes)
C   TVARNUM = number of text variables currently defined
C
C   Allocate the space as bytes. Let BASE = (TVARADR+(I-1)*TVARLEN)/4, for
C   1 <= I <= TVARNUM.  The last byte used for I'th variable at 4*BASE+VARLEN
C   and the start of the next variable is at 4*BASE+VARLEN+1
C                     
C   I4$(BASE+1)  = length of the I'th variable name
C                  always allocate 32 (4*8) bytes for every name
C   L1$((BASE+2)*4+j) for j = 1, I4$(BASE+1)  is the name
C   I4$(BASE+10) = number of dimensions: 0=scalar, 1=vector
C   I4$(BASE+11) = starting address of data
C   I4$(BASE+12) = number of strings if a vector
C   I4$(BASE+13) = total length of data
C   I4$(BASE+14) = starting address for offsets for vector
C                   I4$(I4$(BASE+14)+1) = 0 for every variable
C   I4$(BASE+15) = starting address for lengths for vector
C
C    scalar variable:
C       length = I4$(BASE+13) 
C       line = L1$(I4$(BASE+11)+j) for j = 1 --> I4$(BASE+13) 
C    vector variable:
C     for string k:  1 <= k <= I4$(BASE+12)
C       length = I4$(I4$(BASE+13)+k)
C       line = L1$(I4$(BASE+11)+j) for
C        j = I4$(I4$(BASE+14)+k)+1 --> I4$(I4$(BASE+14)+k)+I4$(I4$(BASE+15)+k)

      COMMON /TEXT_VARIABLES/ TVARALC, TVARADR, TVARLEN, TVARNUM

      INTEGER*4 ISTR

      INTEGER*2 NCODE(MXSTR),ICODE(4,MXCODE,MXSTR),FIRSTC(MXCODE,MXSTR)
      LOGICAL*1 STRCOD(MXSTR), VALID(MXCODE,MXSTR)
      COMMON /EXCODS/ NCODE, ICODE, FIRSTC, STRCOD, VALID

C  FUNCTN  array of NFUN function names
C  NARGUM  specifies the allowed number of arguments for each function,
C          where NARGUM(1,K) = minimum number of arguments allowed
C          where NARGUM(2,K) = maximum number of arguments allowed,
C          normally, NARGUM(1,K) = NARGUM(2,K), with 0 allowed
C  IFNTYP  specifies function type
C          0 is numeric to numeric scalar function
C          1 is numeric to numeric vector function
C          2 is mixed mode to numeric function
C          3 is mixed mode to character function
C          4 is character to character function
C          5 is EXPAND function
C  ARGTYP  specifies function argument type
C         -1 is numeric 
C          1 is character

      CHARACTER*(LENVSM) FUNCTN(MXFUN)
      INTEGER*4          NFUN, NARGUM(2,MXFUN), IFNTYP(MXFUN)
     &                  ,ARGTYP(20,MXFUN)
      COMMON /EXPFUN/ NFUN,NARGUM,IFNTYP,ARGTYP,FUNCTN

C   OPERAT  table of NOPER mathematical and logical operators
C   IPRIOR  table of NOPER*2 operator priorities which give the
C           precedence of the operator, the type of the operator
C           (unary or binary), and the associativity of the operator
C           (left or right)
C   IOPTYP  table of operator types,
C           0 is scalar operator, 1 is vector operator

      CHARACTER*(LENVSM) OPERAT(MXOPER)
      INTEGER*4   NOPER, IPRIOR(2,MXOPER), IOPTYP(MXOPER)
      COMMON /EXPOPR/ NOPER, IPRIOR, IOPTYP, OPERAT

C  error message variables 

      CHARACTER*(MXEXPR) EXPBUF, ERRBUF
      CHARACTER*80       ERRMES
      INTEGER*4          LEXPBUF, ERRPTR 
      COMMON /EXPERR/ ERRPTR, LEXPBUF, ERRMES, EXPBUF, ERRBUF

      CHARACTER*(MXEXPR) QCODE(MXQCOD)
      INTEGER*4      NQCODE, LENQC(MXQCOD)
      COMMON /EXQUOT/ NQCODE, LENQC, QCODE

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

C   local variables

      INTEGER*2 IOUT(4,MXCODE)
      INTEGER*2 ISTACK(4,MXCODE)

cc  used for diagnostic output
cc
cc      CHARACTER*15       SOUT
cc      CHARACTER*(LENVSM) CHCODE(MXCODE)
cc      INTEGER*4          BASE
CCC
      SAVE

C   4th pass
C
C   Loop over the NCODE(ISTR) input codes in ICODE and reorder into
C   Reverse Polish Notation (RPN), storing the final RPN code in ICODE. 
C   NOUT = Current number of RPN codes in ICODE
C   NSTACK = Curr. # of operators stored in operator stack 

 4200 NOUT   = 0
      NSTACK = 0
      DO 5000 I = 1, NCODE(ISTR)  

CC      write(6,1010)icode(1,i,istr),icode(2,i,istr),icode(3,i,istr)

        ERRPTR = FIRSTC(I,ISTR)
        ERRBUF(ERRPTR:ERRPTR) = '^'
        ITABL = ICODE(2,I,ISTR)
        IF( ITABL .EQ. 1 )THEN      ! function in input stream
          NARG = ICODE(1,I,ISTR)    ! get number of function arguments
          IF( IFNTYP(ICODE(3,I,ISTR)) .LT. 3 )THEN
            ERRMES = 'numeric function in character expression'
            RETURN 1
          END IF
          IF( NARG .LE. 0 )THEN     ! the function has no arguments
            NOUT = NOUT+1           ! push directly onto output buffer
            IOUT(1,NOUT) = ICODE(1,I,ISTR)
            IOUT(2,NOUT) = ICODE(2,I,ISTR)
            IOUT(3,NOUT) = ICODE(3,I,ISTR)
            IOUT(4,NOUT) = ICODE(4,I,ISTR)
            GO TO 5000
          END IF
        ELSE IF( ITABL .EQ. 2 )THEN 
          ERRMES = 'numeric variable in character expression'
          RETURN 1
        ELSE IF( ITABL .EQ. 3 )THEN
          ERRMES = 'numeric constant in character expression'
          RETURN 1
        ELSE IF( ITABL .EQ. 4 )THEN ! operator in input stream
          IOPER  = ICODE(3,I,ISTR)  ! operator index in master table
          IOPARG = ICODE(1,I,ISTR)  ! operator type 2=binary, 1=unary
          IF( IOPTYP(IOPER) .NE. 2 )THEN
            ERRMES = 'numeric operator in character expression'
            RETURN 1
          END IF
          IPRIO  = IPRIOR(IOPARG,IOPER)
C                                     |IPRIO| is the operator priority
C                                     or precedence. The sign of IPRIO
C                                     gives associativity, with
C                                     + for left and - for right 
          IF( NSTACK.LE.0 )THEN     ! no codes on stack
            ISAVE = 0
            GO TO 5050
          END IF

C   Pop codes from the stack onto the IOUT output buffer until an
C   operator of lower precedence than that of the current
C   ICODE(3,I,ISTR), or until a ( or , or : is encountered

          DO II = NSTACK, 1, -1      ! scan the stack from the top down
            ISAVE  = II
            ISTABL = ISTACK(2,II)    ! type of code 1=func,2=var,...
            IF( ISTABL .EQ. 1 )THEN  ! function on stack
              NOUT = NOUT+1          ! pop function onto output buffer
              IOUT(1,NOUT) = ISTACK(1,II)
              IOUT(2,NOUT) = ISTACK(2,II)
              IOUT(3,NOUT) = ISTACK(3,II)
              IOUT(4,NOUT) = ISTACK(4,II)
            ELSE IF( ISTABL .EQ. 4 )THEN  ! operator on stack
              IOPERS = ISTACK(3,II)
              IOPARS = ISTACK(1,II)
              IAPRIO = IABS(IPRIOR(IOPARS,IOPERS)) ! operator priority
              IF( IPRIO .LT. 0 )THEN ! input stream op. is right assoc.

C   IAPRIO <= |IPRIO|   the stack operator has a lower precedence than
C   the current operator. If this is so, stop popping the stack and push
C   the current operator onto the stack.  If not, then pop the operator
C   off the stack onto the output buffer

                IF( IAPRIO .LE. IABS(IPRIO) )GO TO 5050
              ELSE                   ! input stream op. is left assoc.

C   IAPRIO < IPRIO   the stack operator has a lower precedence than the
C   current operator. If this is so, stop popping the stack and push the
C   current operator onto the stack.  If not, then pop the operator off
C   the stack onto the output buffer

                IF( IAPRIO .LT. IPRIO )GO TO 5050
              END IF

C   stack operator has a higher precedence than input stream operator

              NOUT = NOUT+1          ! pop operator to the output buffer
              IOUT(1,NOUT) = ISTACK(1,II)
              IOUT(2,NOUT) = ISTACK(2,II)
              IOUT(3,NOUT) = ISTACK(3,II)
              IOUT(4,NOUT) = ISTACK(4,II)
            ELSE IF( ISTABL .EQ. 5 )THEN  ! special character on stack
C                                           this should be a ( [ or {
              GO TO 5050                  ! stop popping the stack
            END IF
          END DO
          ISAVE = 0
 5050     NSTACK = ISAVE             ! ISAVE = number of elements left
C                                      in the stack after previous loop
        ELSE IF( ITABL .EQ. 5 )THEN ! special character in input stream

C   Note: ISPEC= 4  )]}  pop stack up to corresponding ([{, discard )]}
C              = 5  ([{  push onto stack 
C              = 6  ,    pop stack up to corresponding ([{

          ISPEC = ICODE(3,I,ISTR)       ! type of special character
          IF( ISPEC .EQ. 5 )GO TO 5015  ! ([{ pushed onto stack directly
          IF( (ISPEC .EQ. 10) .OR. (ISPEC .EQ. 11) )THEN  ! # *
            ERRMES = 'ERROR in EXRPNT: # or * left on stack'
            RETURN 1
          ELSE IF( (ISPEC.EQ.4) .OR. (ISPEC.EQ.6) )THEN  ! )]},

C   pop codes from the stack onto the output buffer until the
C   corresponding ([{ is encountered

            IF( NSTACK .LE. 0 )THEN  ! no codes on stack
              ISAVE = 0
              GO TO 5150             ! stop popping the stack
            END IF
            DO II = NSTACK, 1, -1    ! scan the stack from the top down
              ISAVE  = II
              ISTABL = ISTACK(2,II)  ! type of code:  1=func,2=var, ...
              IF( ISTABL .EQ. 1 )THEN  ! function on stack
                NOUT = NOUT+1        ! pop function onto output buffer
                IOUT(1,NOUT) = ISTACK(1,II)
                IOUT(2,NOUT) = ISTACK(2,II)
                IOUT(3,NOUT) = ISTACK(3,II)
                IOUT(4,NOUT) = ISTACK(4,II)
              ELSE IF( ISTABL .EQ. 4 )THEN ! operator on stack
                NOUT = NOUT+1        ! pop operator onto output buffer
                IOUT(1,NOUT) = ISTACK(1,II)
                IOUT(2,NOUT) = ISTACK(2,II)
                IOUT(3,NOUT) = ISTACK(3,II)
                IOUT(4,NOUT) = ISTACK(4,II)
              ELSE IF( ISTABL .EQ. 5 )THEN ! special character on stack
C                                            this should be ( [ or {
C   stop popping the stack
C     for an opening bracket, ISTACK(1,II)= 0 unless:
C      ([{ opens a function argument  -->  ISTACK(1,II)= 2
C      ([{ opens a variable subscript -->  ISTACK(1,II)= 3

                IF( ISPEC .EQ. 4 )THEN     ! ) ] or } on input stream
                  ISAVE = II - 1     ! discard opening ([{
                ELSE IF( ISPEC .EQ. 6 )THEN   ! , 
                  ISAVE = II
                END IF
                GO TO 5150           ! stop popping the stack
              END IF
            END DO
            ISAVE = 0
 5150       NSTACK = ISAVE  ! number of elements left in stack after
            GO TO 5000      !  the previous loop is ISAVE
          END IF
        ELSE IF( ITABL .EQ. 9 )THEN  ! immediate value is numeric
          ERRMES = 'numeric value in character expression'
          RETURN 1
        ELSE IF( ITABL .EQ. 10 )THEN
          ERRMES = 'range vector in character expression'
          RETURN 1
        ELSE IF( ITABL .EQ. 11 )THEN
          ERRMES = 'list vector in character expression'
          RETURN 1
        ELSE IF( ITABL .EQ. 12 )THEN ! text variable in input stream
          NOUT = NOUT+1              ! push directly onto output buffer
          IOUT(1,NOUT) = ICODE(1,I,ISTR)
          IOUT(2,NOUT) = ICODE(2,I,ISTR)  
          IOUT(3,NOUT) = ICODE(3,I,ISTR)
          IOUT(4,NOUT) = ICODE(4,I,ISTR)
          GO TO 5000
        ELSE IF( ITABL .EQ. 13 )THEN ! quote string in input stream
          NOUT = NOUT+1              ! push directly onto output buffer
          IOUT(1,NOUT) = ICODE(1,I,ISTR)
          IOUT(2,NOUT) = ICODE(2,I,ISTR)
          IOUT(3,NOUT) = ICODE(3,I,ISTR)
          IOUT(4,NOUT) = ICODE(4,I,ISTR)
          GO TO 5000
        ELSE IF( ITABL .EQ. 15 )THEN ! numeric subvar. in input stream
          NOUT = NOUT+1              ! push directly onto output buffer
          IOUT(1,NOUT) = ICODE(1,I,ISTR)
          IOUT(2,NOUT) = ICODE(2,I,ISTR)
          IOUT(3,NOUT) = ICODE(3,I,ISTR)
          IOUT(4,NOUT) = ICODE(4,I,ISTR)
          GO TO 5000
        ELSE IF( ITABL .EQ. 16 )THEN ! character subvar. in input stream
          NOUT = NOUT+1              ! push directly onto output buffer
          IOUT(1,NOUT) = ICODE(1,I,ISTR)
          IOUT(2,NOUT) = ICODE(2,I,ISTR)
          IOUT(3,NOUT) = ICODE(3,I,ISTR)
          IOUT(4,NOUT) = ICODE(4,I,ISTR)
          GO TO 5000
        END IF      ! end IF-THEN-ELSE over input code types

C   Increment the number of elements in the stack, NSTACK, and check
C   for an overflow. If no overflow, store ICODE in ISTACK.

 5015   NSTACK = NSTACK+1
        IF( NSTACK .GT. MXCODE )THEN
          ERRMES = 'ERROR in EXRPNT: stack overflow'
          RETURN 1
        END IF
        ISTACK(1,NSTACK) = ICODE(1,I,ISTR)
        ISTACK(2,NSTACK) = ICODE(2,I,ISTR)
        ISTACK(3,NSTACK) = ICODE(3,I,ISTR)
        ISTACK(4,NSTACK) = ICODE(4,I,ISTR)
 5000 CONTINUE

C   finished scanning ICODE   pop the remaining elements of the stack

      IF( NSTACK .LE. 0 )GO TO 5250  ! no codes on stack
      DO II = NSTACK, 1, -1          ! scan the stack from the top down
        NOUT = NOUT+1
        IOUT(1,NOUT) = ISTACK(1,II)
        IOUT(2,NOUT) = ISTACK(2,II)
        IOUT(3,NOUT) = ISTACK(3,II)
        IOUT(4,NOUT) = ISTACK(4,II)
      END DO
 5250 NCODE(ISTR) = NOUT   ! the no. of codes in RPN notation of ICODE

cc      write(6,1020)ncode(istr),istr
cc 1020 format(' finished RPN, ncode,istr= ',2i5)

      DO I = 1, NCODE(ISTR)
        ICODE(1,I,ISTR) = IOUT(1,I)
        ICODE(2,I,ISTR) = IOUT(2,I)
        ICODE(3,I,ISTR) = IOUT(3,I)
        ICODE(4,I,ISTR) = IOUT(4,I)

cc      write(6,1010)icode(1,i,istr),icode(2,i,istr),icode(3,i,istr)
cc 1010 format(' icode1,2,3= ',3i5)

      END DO

cc  Write out some diagnostic output for now
cc  Store the symbolic name of each RPN code entry in CHCODE
cc      write(6,1020)ncode(istr),istr
cc      do i = 1, ncode(istr)
cc        write(6,1020)ncode(istr),istr
cc        itabl= icode(2,i,istr)
cc        indx = icode(3,i,istr)
cc        chcode(i) = ' '
cc        if( itabl .eq. 1 )then                 ! function
cc          chcode(i)= functn(indx)
cc        else if( itabl .eq. 4 )then            ! operator
cc          chcode(i)= operat(indx)
cc        else if( itabl .eq. 5 )then            ! special grammar symbol
cc          if( indx .eq. 4 ) then
cc            chcode(i)= ')'
cc          else if( indx .eq. 5 ) then
cc            chcode(i)= '('
cc          else if( indx .eq. 6 ) then
cc            chcode(i)= ','
cc          end if
cc        else if( itabl .eq. 12 ) then           ! text variable
cc          base = (tvaradr+(indx-1)*tvarlen)/4
cc          do j = 1, I4D(base+1)
cc            chcode(i)(j:j) = char(L1D((base+2)*4+j))
cc          end do
cc        else if( itabl .eq. 13 ) then           ! quote string
cc          chcode(i) = qcode(indx)(1:6)
cc        else if( itabl .eq. 15 ) then           ! numeric subvariable
cc          call integer_to_char(indx,.false.,sout,lens)
cc          chcode(i) = '$num'//sout(1:lens)
cc        else if( itabl .eq. 16 ) then           ! char. subvariable
cc          call integer_to_char(indx,.false.,sout,lens)
cc          chcode(i) = '$chr'//sout(1:lens)
cc        end if
cc        write(6,1030)i,itabl,indx,chcode(i)
cc 1030   format(' i,itabl,indx,chcode= ',3i5,' ',a)
cc      end do
cc      write(6,5600) (chcode(i),i=1,ncode(istr))
cc 5600 format(' chcode= ',5x,10a6/14x,10a6)

      RETURN
      END
