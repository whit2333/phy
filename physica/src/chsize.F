      SUBROUTINE CHSIZE( OFF, OLD, NEW, TYPE, * )
C
C  input
C    OFF    The pointer to the existing location (if any)
C    OLD    The length (in "elements") of the array at the old location
C           If OLD=0, we assume this is a NEW array
C    NEW    The length of the new array
C           If NEW=0, then free this space but do not get any more
C    TYPE   The number of bytes/elements (eg TYPE=4 for REAL*4)
C  output
C    OFF    The pointer to a new location
C
      IMPLICIT NONE

      INTEGER*4 OFF, OLD, NEW, TYPE

      REAL*8    R8D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D,R8D,I2D,L1D)

C   MASTER TABLE variables
C     I4ADDR    address of I4D(1)
C     MXTBL     maximum number of table entries allowed
C     NTABLE    highest position in the Master Table that has been filled
C     IEXIST    address of the existence part of the master table
C     IADDR     address of the address part of the master table
C     IBYTES    address of the size part of the master table
C     ITYPES    address of the type part of the master table
C     TBLADR    address of the master table

      INTEGER*4 I4ADDR, MXTBL, NTABLE, IEXIST, IVEC, IADDR, IBYTES
     &         ,ITYPES, TBLADR

#ifdef VMS
      INTEGER*4 LIB$CREATE_VM_ZONE, ISTAT

      INTEGER*4 ZONE_ID
      COMMON /CHSIZE_ZONE/ ZONE_ID
#elif defined (g77) || defined (gfortran)
      INTEGER*4 PHYSICA_MALLOC
      INTEGER*4 I4ADDR8
      EQUIVALENCE (I4ADDR,I4ADDR8)
#endif
C  local variables

      character*90 string
      INTEGER*4 I, K, NEWBYT, NEWADDR, OLDBYT, OLDADR, IDUM
      LOGICAL*4 FIRST
      DATA FIRST /.TRUE./
CCC
      SAVE
      IF( FIRST )THEN
        FIRST = .FALSE.

C  I4ADDR is the base address from which all the assigned arrays
C  will be assigned offset values

#ifdef VMS
        I4ADDR = %LOC(I4D(1))
        ISTAT = LIB$CREATE_VM_ZONE( ZONE_ID )
        IF( .NOT.ISTAT )THEN
          CALL ERROR_MESSAGE('CHSIZE routine','unable to create zone')
          CALL PUT_SYSMSG( ISTAT )
          RETURN 1
        END IF
#elif _AIX
        I4ADDR = LOC(I4D(1))
#elif defined (g77) || defined (gfortran)
        I4ADDR8 = LOC(I4D(1))
#else
        I4ADDR = %LOC(I4D(1))
#endif

C  Get a block of space for the MASTER TABLE, a table which will
C  contain the the addresses of the assigned arrays.
C  We need MXTBL*1 words for the table of logical flags, and
C  4*(MXTBL*4) words for the table of offsets, addresses and lengths;
C  for a total of MXTBL*17 words

        MXTBL  = 100
        NEWBYT = MXTBL*17
#if defined (g77) || defined (gfortran)
        tbladr = PHYSICA_MALLOC( NEWBYT )
c      write(*,*)'tbladr = ',tbladr
        IF( TBLADR .eq. 0 )THEN
          WRITE(STRING,10)NEWBYT,TBLADR
   10     FORMAT('NBYTES=',I10,', ADDR=',I10)
          CALL ERROR_MESSAGE('PHYSICA_MALLOC routine',STRING(1:34))
          GO TO 900
        END IF
        IEXIST = TBLADR-I4ADDR8
        IVEC   = (TBLADR+MXTBL-I4ADDR8)/4
        IADDR  = (TBLADR+5*MXTBL-I4ADDR8)/4
        IBYTES = (TBLADR+9*MXTBL-I4ADDR8)/4
        ITYPES = (TBLADR+13*MXTBL-I4ADDR8)/4
#else
        CALL GET_SPACE( NEWBYT, TBLADR, *900 )
        IEXIST = TBLADR-I4ADDR
        IVEC   = (TBLADR+MXTBL-I4ADDR)/4
        IADDR  = (TBLADR+5*MXTBL-I4ADDR)/4
        IBYTES = (TBLADR+9*MXTBL-I4ADDR)/4
        ITYPES = (TBLADR+13*MXTBL-I4ADDR)/4
#endif
        NTABLE = 0
        DO I = 1, MXTBL   ! initialize the "Exist" Table to .FALSE.
          L1D(IEXIST+I) = .FALSE.
        END DO
      END IF
      IF( TYPE.NE.1 .AND. TYPE.NE.2 .AND.
     &    TYPE.NE.4 .AND. TYPE.NE.8 )THEN
        CALL ERROR_MESSAGE('CHSIZE routine','invalid TYPE')
        GO TO 900
      END IF
      IF( NEW .LT. 0 )THEN
        CALL ERROR_MESSAGE('CHSIZE routine','NEW < 0')
        GO TO 900
      END IF
      IF( OLD .LT. 0 )THEN
        CALL ERROR_MESSAGE('CHSIZE routine','OLD < 0')
        GO TO 900
      ELSE IF( OLD .GT. 0 )THEN ! verify that a consistent table entry exists
        DO I = 1, NTABLE
          IF( L1D(IEXIST+I) )THEN
            IF( OFF .EQ. I4D(IVEC+I) )THEN             ! found entry
              IF( TYPE .NE. I4D(ITYPES+I) )THEN 
                WRITE(STRING,100)
     &           I,TYPE,I4D(ITYPES+I),I4D(IBYTES+I),I4D(IVEC+I)
  100           FORMAT(I6,'th table entry=',I3,', specified type=',I3
     &           ,', bytes=',I8,', address=',I8)
                CALL ERROR_MESSAGE('CHSIZE routine',STRING(1:78)) 
                GO TO 900
              END IF
              IF( OLD*TYPE .NE. I4D(IBYTES+I) )THEN 
                WRITE(STRING,101)
     &           I,OLD*TYPE,I4D(IBYTES+I),I4D(ITYPES+I),I4D(IVEC+I)
  101           FORMAT(I6,'th table entry=',I8,', specified size=',I8
     &           ,', type=',I3,', address=',I8)
                CALL ERROR_MESSAGE('CHSIZE routine',STRING(1:83))
                GO TO 900
              END IF
              K = I
              OLDBYT = I4D(IBYTES+K)
              OLDADR = I4D(IADDR+K)
              GO TO 20
            END IF
          END IF
        END DO
        CALL ERROR_MESSAGE('CHSIZE routine'
     &   ,'attempt to delete an array that is not in the table')
        RETURN 1
      ELSE IF( OLD .EQ. 0 )THEN    ! a new array is being created
        IF( NEW .EQ. 0 )THEN
          CALL ERROR_MESSAGE('CHSIZE routine','OLD=0 and NEW=0')
          GO TO 900
        END IF
        DO I = 1, NTABLE           ! see if any empty "holes"
          K = I
          IF( .NOT.L1D(IEXIST+K) )GO TO 20
        END DO
        IF( NTABLE .GE. MXTBL )THEN
          OLDBYT = MXTBL*17
          OLDADR = TBLADR
          MXTBL  = MXTBL+100
          NEWBYT = MXTBL*17
#if defined (g77) || defined (gfortran)
          tbladr = PHYSICA_MALLOC( NEWBYT )
c      write(*,*)'tbladr = ',tbladr
          IF( TBLADR .eq. 0 )THEN
            WRITE(STRING,10)NEWBYT,TBLADR
            CALL ERROR_MESSAGE('PHYSICA_MALLOC routine',STRING(1:34))
            GO TO 900
          END IF
          DO I = 1, NTABLE
            IDUM = TBLADR-I4ADDR8
            L1D(IDUM+I) = L1D(IEXIST+I)
            IDUM = (TBLADR+MXTBL-I4ADDR8)/4
            I4D(IDUM+I) = I4D(IVEC+I)
            IDUM = (TBLADR+5*MXTBL-I4ADDR8)/4
            I4D(IDUM+I) = I4D(IADDR+I)
            IDUM = (TBLADR+9*MXTBL-I4ADDR8)/4
            I4D(IDUM+I) = I4D(IBYTES+I)
            IDUM = (TBLADR+13*MXTBL-I4ADDR8)/4
            I4D(IDUM+I) = I4D(ITYPES+I)
          END DO
          IEXIST = TBLADR-I4ADDR8
          IVEC   = (TBLADR+MXTBL-I4ADDR8)/4
          IADDR  = (TBLADR+5*MXTBL-I4ADDR8)/4
          IBYTES = (TBLADR+9*MXTBL-I4ADDR8)/4
          ITYPES = (TBLADR+13*MXTBL-I4ADDR8)/4
c      write(*,*)'oldadr = ',oldadr
          CALL PHYSICA_FREE( OLDADR )
#else
          CALL GET_SPACE( NEWBYT, TBLADR, *900 )
          DO I = 1, NTABLE
            IDUM = TBLADR-I4ADDR
            L1D(IDUM+I) = L1D(IEXIST+I)
            IDUM = (TBLADR+MXTBL-I4ADDR)/4
            I4D(IDUM+I) = I4D(IVEC+I)
            IDUM = (TBLADR+5*MXTBL-I4ADDR)/4
            I4D(IDUM+I) = I4D(IADDR+I)
            IDUM = (TBLADR+9*MXTBL-I4ADDR)/4
            I4D(IDUM+I) = I4D(IBYTES+I)
            IDUM = (TBLADR+13*MXTBL-I4ADDR)/4
            I4D(IDUM+I) = I4D(ITYPES+I)
          END DO
          IEXIST = TBLADR-I4ADDR
          IVEC   = (TBLADR+MXTBL-I4ADDR)/4
          IADDR  = (TBLADR+5*MXTBL-I4ADDR)/4
          IBYTES = (TBLADR+9*MXTBL-I4ADDR)/4
          ITYPES = (TBLADR+13*MXTBL-I4ADDR)/4
          CALL FREE_SPACE( OLDBYT, OLDADR, *900 )
#endif
        END IF
        NTABLE = NTABLE+1                  ! no "holes", add a new entry
        K = NTABLE
      END IF
   20 NEWBYT = TYPE*NEW                    ! requested space in bytes 
      IF( OLD .LT. NEW )THEN
        IF( OLD .EQ. 0 )THEN               ! add new entry to the master table
#if defined (g77) || defined (gfortran)
          newaddr = PHYSICA_MALLOC( NEWBYT )
c      write(*,*)'newaddr = ',newaddr
          IF( NEWADDR .eq. 0 )THEN
            WRITE(STRING,10)NEWBYT,NEWADDR
            CALL ERROR_MESSAGE('PHYSICA_MALLOC routine',STRING(1:34))
            GO TO 900
          END IF
          L1D(IEXIST+K) = .TRUE.
          OFF = (NEWADDR-I4ADDR8)/TYPE
          I4D(IVEC+K) = OFF
          I4D(IADDR+K) = NEWADDR
          I4D(IBYTES+K) = NEWBYT
          I4D(ITYPES+K) = TYPE
        ELSE          ! OLD > 0:  destroy the old and create a new
          newaddr = PHYSICA_MALLOC( NEWBYT )
c      write(*,*)'newaddr = ',newaddr
          IF( NEWADDR .eq. 0 )THEN
            WRITE(STRING,10)NEWBYT,NEWADDR
            CALL ERROR_MESSAGE('PHYSICA_MALLOC routine',STRING(1:34))
            GO TO 900
          END IF
          OFF = (NEWADDR-I4ADDR8)/TYPE
          I4D(IVEC+K) = OFF
          I4D(IADDR+K) = NEWADDR
          I4D(IBYTES+K) = NEWBYT
          IF( TYPE .EQ. 1 )THEN
            DO I = 1, OLD
              L1D(NEWADDR-I4ADDR8+I) = L1D(OLDADR-I4ADDR8+I)
            END DO
          ELSE IF( TYPE .EQ. 2 )THEN
            DO I = 1, OLD
              I2D((NEWADDR-I4ADDR8)/2+I) = I2D((OLDADR-I4ADDR8)/2+I)
            END DO
          ELSE IF( TYPE .EQ. 4 )THEN
            DO I = 1, OLD
              I4D((NEWADDR-I4ADDR8)/4+I) = I4D((OLDADR-I4ADDR8)/4+I)
            END DO
          ELSE IF( TYPE .EQ. 8 )THEN
            DO I = 1, OLD
              R8D((NEWADDR-I4ADDR8)/8+I) = R8D((OLDADR-I4ADDR8)/8+I)
            END DO
          END IF
c      write(*,*)'oldadr = ',oldadr
          CALL PHYSICA_FREE( OLDADR )
#else
          CALL GET_SPACE( NEWBYT, NEWADDR, *900 )
          L1D(IEXIST+K) = .TRUE.
          OFF = (NEWADDR-I4ADDR)/TYPE
          I4D(IVEC+K) = OFF
          I4D(IADDR+K) = NEWADDR
          I4D(IBYTES+K) = NEWBYT
          I4D(ITYPES+K) = TYPE
        ELSE          ! OLD > 0:  destroy the old and create a new
          CALL GET_SPACE( NEWBYT, NEWADDR, *900 )
          OFF = (NEWADDR-I4ADDR)/TYPE
          I4D(IVEC+K) = OFF
          I4D(IADDR+K) = NEWADDR
          I4D(IBYTES+K) = NEWBYT
          IF( TYPE .EQ. 1 )THEN
            DO I = 1, OLD
              L1D(NEWADDR-I4ADDR+I) = L1D(OLDADR-I4ADDR+I)
            END DO
          ELSE IF( TYPE .EQ. 2 )THEN
            DO I = 1, OLD
              I2D((NEWADDR-I4ADDR)/2+I) = I2D((OLDADR-I4ADDR)/2+I)
            END DO
          ELSE IF( TYPE .EQ. 4 )THEN
            DO I = 1, OLD
              I4D((NEWADDR-I4ADDR)/4+I) = I4D((OLDADR-I4ADDR)/4+I)
            END DO
          ELSE IF( TYPE .EQ. 8 )THEN
            DO I = 1, OLD
              R8D((NEWADDR-I4ADDR)/8+I) = R8D((OLDADR-I4ADDR)/8+I)
            END DO
          END IF
          CALL FREE_SPACE( OLDBYT, OLDADR, *900 )
#endif
        END IF
      ELSE IF( OLD .GT. NEW )THEN
        IF( NEW .EQ. 0 )THEN                ! just destroy the old
#if defined (g77) || defined (gfortran)
c      write(*,*)'oldadr = ',oldadr
          CALL PHYSICA_FREE( OLDADR )
          L1D(IEXIST+K) = .FALSE.           ! empty this slot in Master Table
        ELSE                       ! NEW > 0: destroy and create
          newaddr = PHYSICA_MALLOC( NEWBYT )
c      write(*,*)'newaddr = ',newaddr
          IF( NEWADDR .eq. 0 )THEN
            WRITE(STRING,10)NEWBYT,NEWADDR
            CALL ERROR_MESSAGE('PHYSICA_MALLOC routine',STRING(1:34))
            GO TO 900
          END IF
          OFF = (NEWADDR-I4ADDR8)/TYPE
          I4D(IVEC+K) = OFF
          I4D(IADDR+K) = NEWADDR
          I4D(IBYTES+K) = NEWBYT
          IF( TYPE .EQ. 1 )THEN
            DO I = 1, NEW
              L1D(NEWADDR-I4ADDR8+I) = L1D(OLDADR-I4ADDR8+I)
            END DO
          ELSE IF( TYPE .EQ. 2 )THEN
            DO I = 1, NEW
              I2D((NEWADDR-I4ADDR8)/2+I) = I2D((OLDADR-I4ADDR8)/2+I)
            END DO
          ELSE IF( TYPE .EQ. 4 )THEN
            DO I = 1, NEW
              I4D((NEWADDR-I4ADDR8)/4+I) = I4D((OLDADR-I4ADDR8)/4+I)
            END DO
          ELSE IF( TYPE .EQ. 8 )THEN
            DO I = 1, NEW
              R8D((NEWADDR-I4ADDR8)/8+I) = R8D((OLDADR-I4ADDR8)/8+I)
            END DO
          END IF
c      write(*,*)'oldadr = ',oldadr
          CALL PHYSICA_FREE( OLDADR )
#else
          CALL FREE_SPACE( OLDBYT, OLDADR, *900 )
          L1D(IEXIST+K) = .FALSE.           ! empty this slot in Master Table
        ELSE                       ! NEW > 0: destroy and create
          CALL GET_SPACE( NEWBYT, NEWADDR, *900 )
          OFF = (NEWADDR-I4ADDR)/TYPE
          I4D(IVEC+K) = OFF
          I4D(IADDR+K) = NEWADDR
          I4D(IBYTES+K) = NEWBYT
          IF( TYPE .EQ. 1 )THEN
            DO I = 1, NEW
              L1D(NEWADDR-I4ADDR+I) = L1D(OLDADR-I4ADDR+I)
            END DO
          ELSE IF( TYPE .EQ. 2 )THEN
            DO I = 1, NEW
              I2D((NEWADDR-I4ADDR)/2+I) = I2D((OLDADR-I4ADDR)/2+I)
            END DO
          ELSE IF( TYPE .EQ. 4 )THEN
            DO I = 1, NEW
              I4D((NEWADDR-I4ADDR)/4+I) = I4D((OLDADR-I4ADDR)/4+I)
            END DO
          ELSE IF( TYPE .EQ. 8 )THEN
            DO I = 1, NEW
              R8D((NEWADDR-I4ADDR)/8+I) = R8D((OLDADR-I4ADDR)/8+I)
            END DO
          END IF
          CALL FREE_SPACE( OLDBYT, OLDADR, *900 )
#endif
        END IF
      END IF
      RETURN
  900 RETURN 1
      END
