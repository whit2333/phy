      SUBROUTINE PSANGL( NEWVAL, OLDVAL, * )

      REAL*4 NEWVAL, OLDVAL

C  PSANGL changes the value of the angle for the current character set
C
C  NEWVAL is the new value to be assigned to the angle (REAL*4)
C  OLDVAL is set on return to the previous angle value (REAL*4)
C  alternate return taken if angle is <= -90 or >= 90

      BYTE      LINE1(56), LINE3(25000)
      INTEGER*2 LINE2(256)
      INTEGER*4 MGRID
      LOGICAL*4 GRID16, LISPON
      COMMON /FONT/ LINE1, LINE2, LINE3, GRID16, LISPON, MGRID

      INTEGER*2 SIZE, GRID, HSPAC, VSPAC, PSUPX, PSUPY, PSUBX, PSUBY
     &         ,REFX, REFY, SWITC, LSPAC
      REAL*4    SHGT, SCAL, SSCAL, ANGL
      BYTE      NAME(16)
      EQUIVALENCE (LINE1( 1),    SIZE),
     &            (LINE1( 3),    GRID),
     &            (LINE1( 5),   HSPAC),
     &            (LINE1( 7),   VSPAC),
     &            (LINE1( 9),   PSUPX),
     &            (LINE1(11),   PSUPY),
     &            (LINE1(13),   PSUBX),
     &            (LINE1(15),   PSUBY),
     &            (LINE1(17),    SHGT),
     &            (LINE1(21),    SCAL),
     &            (LINE1(25),   SSCAL),
     &            (LINE1(29),    ANGL),
     &            (LINE1(33),    REFX),
     &            (LINE1(35),    REFY),
     &            (LINE1(37),   SWITC),
     &            (LINE1(39),   LSPAC),
     &            (LINE1(41), NAME(1))
CCC
      IF ( (NEWVAL.LE.-90.) .OR. (NEWVAL.GE.90.) )RETURN 1
      OLDVAL = ANGL
      ANGL = NEWVAL
      CALL LOCTRA  ! update the local transformation
      RETURN
      END      
