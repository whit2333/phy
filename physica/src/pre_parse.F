      SUBROUTINE PRE_PARSE_LINE( LINE, LENL, * )

C   This routine cuts off everything after encountering an exclamation
C   mark, !, if not in quotes.  It also replaces a tab with a blank

      CHARACTER*1 COMENT   ! comment delimiter
      LOGICAL*4   IGNORE_COMENT
      COMMON /COMENTC/ IGNORE_COMENT, COMENT
      
      CHARACTER*1024 LINE, CLINE
      INTEGER*4     LENL, LEN2

      INTEGER*2 ISTATE, NEW_STATE, CLASSES(0:127), STATE_TABLE(4,6)
      DATA STATE_TABLE
C          `    '  othr   ! 
     1 /   2,   1,   1, 100,
     2     3,   5,   4,   4,  ! opening quote 
     3     3,   5,   4,   4,  ! opening quote
     4     3,   5,   4,   4,  ! inside quotes
     5     3,   5,   4,   4,  ! closing quote
     6     3,   1,   1, 100 / ! closing final quote
CCC
C      DATA CLASSES
C     &/  3,  3,  3,  3,  3,  3,  3,  3,  3,  5,  3,  3,  3,  3,  3,  3,
C     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
C     &   3,  4,  3,  3,  3,  3,  3,  2,  3,  3,  3,  3,  3,  3,  3,  3,
C     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
C     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
C     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
C     &   1,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
C     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3 /
      DO I = 0, 127
        CLASSES(I) = 3
      ENDDO
      CLASSES(9) = 5
      CLASSES(39) = 2
      CLASSES(96) = 1
      CLASSES(ICHAR(COMENT)) = 4

      NEW_STATE = 1
      ICHL      = 0
      IQUOTE    = 0
      LEN2      = 0
      CLINE     = ' '
      I = 0
 10   I = I + 1
      IF( I .GT. LENL )THEN
        LINE = CLINE
        LENL = LEN2
        RETURN
      END IF
      ICHL = ICHAR(LINE(I:I))
      IF( ICHL .EQ. 9 )THEN
        LINE(I:I) = ' '  ! Replace a tab with a blank
        ICHL = 32
      END IF
      IF( (LEN2.EQ.0) .AND. (ICHL.EQ.32) )GO TO 10
      LEN2 = LEN2+1
      CLINE(LEN2:LEN2) = LINE(I:I)
      ISTATE = NEW_STATE
      NEW_STATE = STATE_TABLE(CLASSES(ICHL),ISTATE)
      IF( NEW_STATE .EQ. 2 )THEN
        IQUOTE = IQUOTE+1
      ELSE IF( NEW_STATE .EQ. 3 )THEN
        IQUOTE = IQUOTE+1
      ELSE IF( NEW_STATE .EQ. 5 )THEN
        IQUOTE = IQUOTE-1
        IF( IQUOTE.EQ.0 )NEW_STATE = 6
      ELSE IF( NEW_STATE .EQ. 100 )THEN
        LINE = CLINE
        LENL = LEN2-1
        DO J = I-1, 1, -1
          IF( LINE(J:J) .EQ. ' ' )THEN
            LENL = LENL-1
          ELSE 
            LENL = MAX(0,LENL)
            RETURN
          END IF
        END DO
        LENL = MAX(0,LENL)
        RETURN
      END IF
      GO TO 10
      END
