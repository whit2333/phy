#include <signal.h>
#include <X11/Xlib.h>

#ifdef VMS
#define x_resize_window_ x_resize_window
#define x_get_window_dimensions_ x_get_window_dimensions
#define sig_off_ sig_off
#define sig_on_ sig_on
#endif
 
 /* Global variables */

 Display *dpy;
 Window g_window, t_window;
 int gwidth, gheight;
 
 void x_resize_window_( int *w, int *h )
  {
    unsigned int width, height;
    width = *w;
    height = *h;
    XResizeWindow( dpy, g_window, width, height );
    gwidth = width;
    gheight = height;
    xvst_replay_(0);
    XSetInputFocus( dpy, t_window, RevertToPointerRoot, CurrentTime );
    XSelectInput( dpy, t_window, 0 );
    XFlush( dpy );
  }

 void x_get_window_dimensions_( int *w, int *h )
  {
    *w = gwidth;
    *h = gheight;
  }
 
