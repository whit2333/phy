      CHARACTER*1 FUNCTION READ_KEY( OPTIONS, PROMPT, ALTKEY, TIMEOUT )

      IMPLICIT INTEGER*4 (A-Z)

      CHARACTER*(*) OPTIONS, PROMPT
      CHARACTER*1   ALTKEY
      INTEGER*4     TIMEOUT
C                                       Henry Baragar
C                                       TRIUMF, UBC
C                                       May, 1984
C
C This routine gets a single key stroke from the terminal immediately.
C If an arrow key was hit, then READ_KEY returns as <null> with with
C ALTKEY set, where:
C              '^'  - means the up-arrow was hit
C              'v'  - means the down-arrow was hit (NB. v not V)
C              '<'  - means the left-arrow was hit
C              '>'  - means the right-arrow was hit
C
C If the 'KEYPAD' option is set, then READ_KEY returns as <null> and ALTKEY
C is set to the character that you see on the KEYPAD key; and the PF keys are
C returned as:
C              'P'  - for PF1
C              'Q'  - for PF2
C              'R'  - for PF3
C              'S'  - for PF4
C              'n'  - for F14
C
C Also, if an <ESC> sequence could not be interpreted, then READ_KEY is
C returned set to <ESC> and ALTKEY is returned with the character at which
C interpretation was lost (I know this could cause problems
C -- but if you don't like it, use the 'NOESC' option and interpret the
C <ESC> sequence yourself!!)
C
C If the 'TIMEOUT' option is set, then if there is no character typed within
C the TIMEOUT period then both READ_KEY and ALTKEY return as <null>
C
C N.B. To get the opposite effect of an option, don't mention it.
C      i.e. the defaults are the opposite of the options available.
C      e.g. the default is that READ_KEY tries to interperate <ESC>
C           sequences, and if it recognizes it, READ_KEY is returned
C           as null, and ALTKEY contains a single character symbolic
C           representation of the key (eg. for down-arrow ALTKEY='v')
C INPUT:
C      OPTIONS - a list of options where:
C             option / abbrev  - description
C              'ECHO' /'EC'    - echo the character read in
C              'KEYPAD' /'K'   - means turn the keypad on
C              'NOKEYPAD' /'NOK' - means turn the keypad off
C              'LC2UC' /'LC'   - translate all lower to upper case
C              'NOCTRL' /'NOC' - echo all alphanumerics
C              'NOESC' /'NOE'  - means don't try to interpret ESC sequences
C              'PROMPT' /'PR'  - write PROMPT before reading
C              'PURGE' /'PU'   - purge the type ahead buffer before reading
C              'FAST' /'FAST'  - maintains terminal settings between
C                                keystrokes (to avoid character loss)
C                                no changes should be done until reset
C              'TIMEOUT' /'TI' - if TIMEOUT seconds have elapsed
C                                without the user hitting a key:
C                                READ_KEY=<null>, ALTKEY=<null>
C      ALTKEY  if READ_KEY is returned as <null>, then ALTKEY
C              returns with a symbolic representation of the key hit:
C              see above for symbolic values returned
C      PROMPT  the prompt to be written before reading the key
C              (required even if 'PROMPT' is not mentioned)
C      TIMEOUT the number of seconds to wait before returning
C              to the caller if the user has not hit a key
C
C OUTPUT:
C      READ_KEY  the key that was hit, unless an <ESC> sequence was invoked
C                and interpretted in which case it is <null>
C      ALTKEY    if READ_KEY=<null>, then ALTKEY returns with the symboic
C                key (see above); else if READ_KEY=<ESC> and 'NOESC' was not
C                invoked then ALTKEY returns with the character on which
C                this routine choked; else ALTKEY returns as <null>
C EXAMPLES:
C              CHARACTER*1 KEY,ALTKEY,READ_KEY
C              KEY = READ_KEY (' ',' ',ALTKEY)
C         doesn't echo, translates the arrow keys, but does not
C         translate the keypad keys (except for the PF keys)
C              KEY = READ_KEY ('PROMPT KEYPAD','Enter: ',ALTKEY)
C         doesn't echo, translate both arrows and keypad keys, and
C         displays the prompt "Enter: " before reading
C              KEY = READ_KEY ('PR K','Enter: ',ALTKEY)
C         same as previous example
C              KEY = READ_KEY ('TIMEOUT',' ',ALTKEY,45)
C         doesn't echo, translates only arrows and PFs, and returns
C         after 45 seconds if there has been no imput
C NOTES:
C  1)  For more detailed, though less lucid, information, refer to the
C      "VAX/VMS I/O user's Guide (Volume 1)", section 9.4
C
C  Modified Apr 18/88 by F.W. Jones: channel assigned to terminal
C   placed in common block for use by VST_CROSSHAIR.
C  Modified Nov 21/90 by J.Chuma: allow F14 key (same as CTRL-A)
C  Modified 29-APR-91 by F. Jones: added signal handler for RISC/ULTRIX
C   version, to clean up when resuming after a CTRL-Z interrupt
C  Modified 27-May-91 to fix lost characters on fast input and allow
C   purge of type ahead buffer
C  Mofidied 2-Nov-91 by C. Kost: Posix compliant version
C   See the section TERMIO(4) of the ULTRIX manual or type "man 4 termio"
C   for details on forwarding single characters and ECHO flag values
C  Modified 03-JUL-93 by FWJ for OSF/1 compatibility.  The ECHO_MASK
C   and CANON_MASK are now provided by TCGETA
CZZ
      CHARACTER*1 NULL, CR, ESC
      PARAMETER (LENRK = 1)

#ifdef VMS
      COMMON /READ_KEY/ CHAN

      PARAMETER IO$_READPROMPT=X'37'
      PARAMETER IO$M_NOECHO=X'40',  IO$M_NOFILTR=X'200',
     &          IO$M_CVTLOW=X'100', IO$M_PURGE=X'800',
     &          IO$M_TIMED=X'80'
      EXTERNAL SS$_NORMAL, SS$_TIMEOUT

C  Make sure that we have a channel (maybe one from a previous call)

      NULL = CHAR(0)
      CR = CHAR(13)
      ESC = CHAR(27)
      IF( CHAN .EQ. 0 )THEN
        ISTAT = SYS$ASSIGN ('TT:',CHAN,,)
        IF( ISTAT.NE.%LOC(SS$_NORMAL) )THEN
          IDUM = NARGSI(1)
          CALL PUT_SYSMSG(ISTAT)
          RETURN
        END IF
      END IF

C  Set up the options

      OPTS = IOR(IO$_READPROMPT,IO$M_NOFILTR)
      IF( INDEX(OPTIONS,'EC')  .EQ. 0 )OPTS = IOR(OPTS,IO$M_NOECHO)
      IF( INDEX(OPTIONS,'K')   .NE. 0 )WRITE(*,62)ESC   !Turn keypad on
   62 FORMAT('+',A,'=',$)
      IF( INDEX(OPTIONS,'NOK') .NE. 0 )WRITE(*,61)ESC   !Turn keypad off
   61 FORMAT('+',A,'>',$)
      IF( INDEX(OPTIONS,'LC')  .NE. 0 )OPTS = IOR(OPTS,IO$M_CVTLOW)
      IF( INDEX(OPTIONS,'PU')  .NE. 0 )OPTS = IOR(OPTS,IO$M_PURGE)
      IF( INDEX(OPTIONS,'TI')  .EQ. 0 )THEN
        WAIT = 0
      ELSE
        OPTS = IOR(OPTS,IO$M_TIMED)
        WAIT = TIMEOUT
      END IF

C  Read the key; with or without a prompt
  
      READ_KEY = NULL
      ALTKEY = NULL
      IF( INDEX(OPTIONS,'PR') .EQ. 0 )THEN
        LENPR = 0
        ISTAT = SYS$QIOW (, %VAL(CHAN), %VAL(OPTS), , , , 
     &   %REF(READ_KEY),%VAL(LENRK),%VAL(WAIT),,%REF(' '),%VAL(LENPR))
      ELSE
        LENPR = LEN(PROMPT)
        ISTAT = SYS$QIOW (, %VAL(CHAN), %VAL(OPTS), , , , 
     &   %REF(READ_KEY),%VAL(LENRK),%VAL(WAIT),,%REF(PROMPT)
     &  ,%VAL(LENPR))
      END IF
      IF( ISTAT.NE.%LOC(SS$_NORMAL) )THEN
        IDUM = NARGSI(1)
        CALL PUT_SYSMSG(ISTAT)
        RETURN
      END IF
      IF( ISTAT .EQ. %LOC(SS$_TIMEOUT) )RETURN

C  Now echo if 'NOCTRL' was set, and try to interpret the <ESC> sequence
C  in 'NOESC' was not set.

      IF( INDEX(OPTIONS,'NOC').NE.0 .AND.
     &    ICHAR(READ_KEY).GE.ICHAR(' ') )WRITE(*,60)READ_KEY
   60 FORMAT('+',A,$)
      IF( INDEX(OPTIONS,'NOE') .EQ. 0 )THEN
        OPTS = IAND(OPTS,NOT(IO$M_PURGE))
        OPTS = IAND(OPTS,NOT(IO$M_CVTLOW))
        IF( READ_KEY.EQ.ESC )THEN
          LENPR = 0
          ISTAT = SYS$QIOW(, %VAL(CHAN), %VAL(OPTS), , , , 
     &     %REF(ALTKEY),%VAL(LENRK),%VAL(WAIT),,%REF(' '),%VAL(LENPR))
          IF( ALTKEY.EQ.'[' )THEN
            ISTAT = SYS$QIOW(, %VAL(CHAN), %VAL(OPTS), , , , 
     &       %REF(ALTKEY),%VAL(LENRK),%VAL(WAIT),,%REF(' '),%VAL(LENPR))

C  We think an arrow key has been hit

            IF( ALTKEY.EQ.'A' )THEN
              ALTKEY = '^'
              READ_KEY = NULL
            ELSE IF( ALTKEY.EQ.'B' )THEN
              ALTKEY = 'v'
              READ_KEY = NULL
            ELSE IF( ALTKEY.EQ.'C' )THEN
              ALTKEY = '>'
              READ_KEY = NULL
            ELSE IF( ALTKEY.EQ.'D' )THEN
              ALTKEY = '<'
              READ_KEY = NULL
            ELSE IF( ALTKEY.EQ.'2' )THEN
              ISTAT = SYS$QIOW(, %VAL(CHAN), %VAL(OPTS), , , , 
     &         %REF(ALTKEY),%VAL(LENRK),%VAL(WAIT),,%REF(' ')
     &         ,%VAL(LENPR))
              IF( ALTKEY.EQ.'6' )THEN
                ISTAT = SYS$QIOW (,%VAL(CHAN),%VAL(OPTS), , , , 
     &           %REF(ALTKEY),%VAL(LENRK),%VAL(WAIT),,%REF(' ')
     &           ,%VAL(LENPR))
                IF( ALTKEY.EQ.'~' )THEN
                  ALTKEY = 'n'              ! found F14 key
                  READ_KEY = NULL
                END IF
              END IF
            END IF
          ELSE IF( ALTKEY.EQ.'O' )THEN
            ISTAT = SYS$QIOW (, %VAL(CHAN), %VAL(OPTS), , , , 
     &       %REF(ALTKEY),%VAL(LENRK),%VAL(WAIT),,%REF(' '),%VAL(LENPR))

C  We think a keypad key has been hit

            IF( ICHAR(ALTKEY).GE.ICHAR('p') .AND. 
     &          ICHAR(ALTKEY).LE.ICHAR('y') )THEN
              ALTKEY = CHAR(ICHAR(ALTKEY)-ICHAR('p')+ICHAR('0'))
              READ_KEY = NULL
            ELSE IF( ALTKEY.EQ.'l' )THEN
              ALTKEY = ','
              READ_KEY = NULL
            ELSE IF( ALTKEY.EQ.'m' )THEN
              ALTKEY = '-'
              READ_KEY = NULL
            ELSE IF( ALTKEY.EQ.'n' )THEN
              ALTKEY = '.'
              READ_KEY = NULL
            ELSE IF( ALTKEY.EQ.'M' )THEN
              ALTKEY = CR
              READ_KEY = NULL
            ELSE IF( ICHAR(ALTKEY).GE.ICHAR('P') .AND. 
     &               ICHAR(ALTKEY).LE.ICHAR('S') )THEN
              READ_KEY = NULL
            END IF
          END IF
        ELSE IF( READ_KEY.NE.ESC )THEN
          ALTKEY = NULL
        END IF
      ELSE    ! we're not supposed to know about <ESC> sequences
        ALTKEY = NULL
      END IF   
      RETURN
      END
#else
      LOGICAL*4 LCTOUC, FIRST, INIT, PURGE, FAST
      DATA FIRST /.TRUE./, INIT /.FALSE./

      INTEGER*4 OPTS(4)

      INTEGER*4 NORMAL(40)
      COMMON /READ_KEY_IOCTL/ NORMAL

      NULL = CHAR(0)
      CR = CHAR(13)
      ESC = CHAR(27)

      IF( FIRST )THEN

C Get normal characteristics on first application use of READ_KEY
C This call also initializes ECHO_MASK and CANON_MASK  FWJ 05-JUL-93

        CALL TCGETA(0,NORMAL,ECHO_MASK,CANON_MASK)
        DO I = 1, 4
          OPTS(I) = NORMAL(I)
        END DO
        NOECHO_MASK = NOT(ECHO_MASK)
        NOCANON_MASK = NOT(CANON_MASK)

C Establish signal handler to restore normal characteristics when 
C application terminates

        CALL READ_KEY_TRAP

C By default, set for no echo and single character immediate forward

        OPTS(1) = IAND(OPTS(1),NOECHO_MASK)

C Set CBREAK option (wait for single character, then immediate forward) 

        OPTS(1) = IAND(OPTS(1),NOCANON_MASK)     ! Disable Canonical procesg
        OPTS(2) = 1                              ! MIN=1
        OPTS(3) = 0                              ! TIME=0
#ifdef __osf__
        CALL TCSETA(0,OPTS)                      ! set options
#else
        CALL TCSETAF(0,OPTS)                     ! set options and flush
#endif
        FIRST=.FALSE.
      END IF

C Always set for : wait for single character, then immediate forward. 

      OPTS(1) = IAND(OPTS(1),NOCANON_MASK)     ! Disable Canonical procesg
      OPTS(2) = 1                              ! MIN=1
      OPTS(3) = 0                              ! TIME=0
      LCTOUC  = .FALSE.
      PURGE   = .FALSE.
      FAST    = .FALSE.

C Set for default of no echo

      OPTS(1) = IAND(OPTS(1),NOECHO_MASK)
      IF( INDEX(OPTIONS,'EC') .NE. 0 )OPTS(1) = IOR(OPTS(1),ECHO_MASK)
      IF( INDEX(OPTIONS,'K' ) .NE. 0 )WRITE(*,62)ESC   !Turn keypad on
   62 FORMAT(A,'=',$)
      IF( INDEX(OPTIONS,'NOK') .NE. 0 )WRITE(*,61)ESC   !Turn keypad off
   61 FORMAT(A,'>',$)
      IF( INDEX(OPTIONS,'LC') .NE. 0 )LCTOUC = .TRUE.
      IF( INDEX(OPTIONS,'PU')    .NE. 0 )PURGE = .TRUE.
      IF( INDEX(OPTIONS,'FAST')  .NE. 0 )FAST = .TRUE.
      IF( INDEX(OPTIONS,'RESET') .NE. 0 )THEN  ! Reset back to NORMAL mode 
        IF( .NOT.PURGE )THEN                   ! so regular READ will work
          CALL TCSETA(0,NORMAL)
        ELSE
          CALL TCSETAF(0,NORMAL)
        END IF
        INIT = .TRUE.
        READ_KEY = ' '
        RETURN
      END IF
      IF( INIT )THEN           ! This is first use after a "reset".
        IF( .NOT.PURGE )THEN   ! Now we need to set things to current options
          CALL TCSETA(0,OPTS)
        ELSE
          CALL TCSETAF(0,OPTS)
        END IF
        INIT = .FALSE.
      END IF
      IF( .NOT.FAST )THEN   ! Set i/o characteristics, Non-FAST call
        IF( .NOT.PURGE )THEN
          CALL TCSETA(0,OPTS)
        ELSE
          CALL TCSETAF(0,OPTS)
        END IF
      END IF

C  Read the key; with or without a prompt.

      READ_KEY = NULL
      IF( INDEX(OPTIONS,'PR').NE.0 )WRITE(*,10)PROMPT
   10 FORMAT(A,$)
      IDUM = GETC(READ_KEY)

C  Convert lower case to upper case if applicable

      IF( LCTOUC .AND. ICHAR(READ_KEY).GT.96 .AND.
     & ICHAR(READ_KEY).LT.123 )READ_KEY = CHAR(ICHAR(READ_KEY)-32)
      IF( READ_KEY.EQ.ESC )THEN  ! ESC encountered 
        IDUM = GETC(ALTKEY)      ! Handle special cases: next character [ or O
        IF( ALTKEY.EQ.'[' )THEN  ! We think an arrow key has been hit
          IDUM = GETC(ALTKEY)
          IF( ALTKEY .EQ. 'A' )THEN
            ALTKEY = '^'
            READ_KEY = NULL
          ELSE IF( ALTKEY .EQ. 'B' )THEN
            ALTKEY = 'v'
            READ_KEY = NULL
          ELSE IF( ALTKEY .EQ. 'C' )THEN
            ALTKEY = '>'
            READ_KEY = NULL
          ELSE IF( ALTKEY .EQ. 'D' )THEN
            ALTKEY = '<'
            READ_KEY = NULL
          END IF
        ELSE IF( ALTKEY .EQ. 'O' )THEN  ! We think a keypad key has been hit
          IDUM = GETC(ALTKEY)
          IF( ICHAR(ALTKEY) .GE. ICHAR('p') .AND.  
     &        ICHAR(ALTKEY) .LE. ICHAR('y') )THEN
            ALTKEY = CHAR(ICHAR(ALTKEY)-ICHAR('p')+ICHAR('0'))
            READ_KEY = NULL
          ELSE IF( ALTKEY .EQ. 'l' )THEN
            ALTKEY = ','
            READ_KEY = NULL
          ELSE IF( ALTKEY .EQ. 'm' )THEN
            ALTKEY = '-'
            READ_KEY = NULL
          ELSE IF( ALTKEY .EQ. 'n' )THEN
            ALTKEY = '.'
            READ_KEY = NULL
          ELSE IF( ALTKEY .EQ. 'M' )THEN
            ALTKEY = CR
            READ_KEY = NULL
          ELSE IF( ICHAR(ALTKEY) .GE. ICHAR('P') .AND. 
     &             ICHAR(ALTKEY) .LE. ICHAR('S') )THEN
            READ_KEY = NULL
          END IF
        ELSE
          ALTKEY = NULL
        END IF
      ELSE       ! we're not supposed to know about <ESC> sequences
        ALTKEY = NULL
      END IF
      IF( FAST )RETURN

C   Restore NORMAL terminal input state (if not FAST).

      IF( .NOT.PURGE )THEN
        CALL TCSETA(0,NORMAL)
      ELSE
        CALL TCSETAF(0,NORMAL)
      END IF
      RETURN
      END
#endif
