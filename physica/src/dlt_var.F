      SUBROUTINE DELETE_VARIABLE( K )

C   If K > 0 then just destroy one variable with index K
C   If K < 0 then 
C     K  = -1 --> destroy all vectors
C     K  = -2 --> destroy all scalars
C     K  = -4 --> destroy all matrices
C     K  = -8 --> destroy all tensors
      
      IMPLICIT NONE

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      INTEGER*4 VARALC, VARADR, VARLEN, VARNUM, VARMAX
     &         ,TSCAL, NSCAL, ASCAL, ESCAL

C   VARALC = number of bytes allocated for numeric variable
C            characteristics (does not include the data)
C            VARALC should be a multiple of 512 
C   VARADR = starting address of the numeric variable table
C   VARLEN = # of bytes needed for an entry in the table
C   VARNUM = # of numeric variables currently defined
C   VARMAX = # of variables (including scalars) for which space is allocated 
C
C   TSCAL  = # of scalars for which space is allocated 
C   NSCAL  = # of current scalars
C   ASCAL  = starting address for scalar data
C   ESCAL  = starting address of the scalars existence list
C
C   Allocate the space as bytes. Let BASE = (VARADR+(I-1)*VARLEN)/4, for
C   1 <= I <= VARNUM.  The last byte used for I'th variable at 4*BASE+VARLEN
C   and the start of the next variable is at 4*BASE+VARLEN+1
C
C   I4$(BASE+1)   = length of the I'th variable name
C                   always allocate 32 (4*8) bytes for every name
C                   name is in L1$((BASE+2)*4+j) for j = 1, length
C   I4$(BASE+10)  = -1 if scalar is a dummy index
C                    0 if normal scalar
C                   +1 if scalar is allowed to vary in fit
C   I4$(BASE+11)  = number of dimensions: 0=scalar, 1=vector, 2=matrix, 3=tensor
C   I4$(BASE+12)  = starting address of data
C   I4$(BASE+13)  = number of elements if a vector
C                   or number of rows if a matrix (1st dimension)
C   I4$(BASE+14)  = number of columns (2nd dimension)
C   I4$(BASE+15)  = number of planes (3rd dimension)
C   I4$(BASE+16)  = initial index of 1st dimension
C   I4$(BASE+17)  = initial index of 2nd dimension
C   I4$(BASE+18)  = initial index of 3rd dimension
C   I4$(BASE+19)  = final index of 1st dimension
C   I4$(BASE+20)  = final index of 2nd dimension
C   I4$(BASE+21)  = final index of 3rd dimension
C   I4$(BASE+22)  = number of lines of history for variable I
C   I4$(BASE+23)  = total allocation for history for variable I
C   I4$(BASE+24)  = address for history line lengths
C   I4$(BASE+25)  = address for history line characters
C    for history line 1:
C       length = I4$(I4$(BASE+24)+1) 
C       line = L1$(I4$(BASE+25)+j) for j = 1 --> I4$(I4$(BASE+24)+1) 
C    for history line k > 1:
C       length = I4$(I4$(BASE+24)+k) 
C         prev_len = sum(I4$(I4$(BASE+24)+j),j,1:k-1)
C       line = L1$(I4$(BASE+25)+prev_len+j) for j = 1 --> I4$(I4$(BASE+24)+k)

      COMMON /NUMERIC_VARIABLES/ VARALC, VARADR, VARLEN, VARNUM, VARMAX
     &                          ,TSCAL, NSCAL, ASCAL, ESCAL

      INTEGER*4 K

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      BYTE      L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

C   local variables

      INTEGER*4 I, J, BASE, NPTS
CCC
      IF( K .GT. 0 )THEN
        BASE = (VARADR+(K-1)*VARLEN)/4
        IF( I4D(BASE+11) .EQ. 0 )THEN
          L1D(ESCAL+I4D(BASE+12)) = 0
          NSCAL = NSCAL-1
        ELSE
          IF( I4D(BASE+11) .EQ. 1 )THEN
            NPTS = I4D(BASE+13)
          ELSE IF( I4D(BASE+11) .EQ. 2 )THEN
            NPTS = I4D(BASE+13)*I4D(BASE+14)
          ELSE IF( I4D(BASE+11) .EQ. 3 )THEN
            NPTS = I4D(BASE+13)*I4D(BASE+14)*I4D(BASE+15)
          END IF
          CALL CHSIZE(I4D(BASE+12),NPTS,0,8,*9001)
        END IF
        CALL CHSIZE(I4D(BASE+25),I4D(BASE+23),0,1,*9002)
        CALL CHSIZE(I4D(BASE+24),I4D(BASE+22),0,4,*9003)

C   zero out this section in the table

        DO J = VARADR+(K-1)*VARLEN+1, VARADR+K*VARLEN
          L1D(J) = 0
        END DO
        VARNUM = VARNUM-1
      ELSE IF( K .EQ. -1 )THEN   ! destroy all vectors
        DO I = 1, VARMAX
          BASE = (VARADR+(I-1)*VARLEN)/4
          IF( (I4D(BASE+1).GT.0) .AND. (I4D(BASE+11).EQ.1) )THEN
            CALL CHSIZE(I4D(BASE+12),I4D(BASE+13),0,8,*9001)   ! data 
            CALL CHSIZE(I4D(BASE+25),I4D(BASE+23),0,1,*9002)   ! history
            CALL CHSIZE(I4D(BASE+24),I4D(BASE+22),0,4,*9003)

C   zero out this section in the table

            DO J = VARADR+(I-1)*VARLEN+1, VARADR+I*VARLEN
              L1D(J) = 0
            END DO
            VARNUM = VARNUM-1
          END IF
        END DO
      ELSE IF( K .EQ. -2 )THEN   ! destroy all scalars
        DO I = 1, VARMAX
          BASE = (VARADR+(I-1)*VARLEN)/4
          IF( (I4D(BASE+1).GT.0) .AND. (I4D(BASE+11).EQ.0) )THEN
            L1D(ESCAL+I4D(BASE+12)) = 0
            CALL CHSIZE(I4D(BASE+25),I4D(BASE+23),0,1,*9002)   ! history
            CALL CHSIZE(I4D(BASE+24),I4D(BASE+22),0,4,*9003)
            NSCAL = NSCAL-1

C   zero out this section in the table

            DO J = VARADR+(I-1)*VARLEN+1, VARADR+I*VARLEN
              L1D(J) = 0
            END DO
            VARNUM = VARNUM-1
          END IF
        END DO
      ELSE IF( K .EQ. -4 )THEN   ! destroy all matrices
        DO I = 1, VARMAX
          BASE = (VARADR+(I-1)*VARLEN)/4
          IF( (I4D(BASE+1).GT.0) .AND. (I4D(BASE+11).EQ.2) )THEN
            NPTS = I4D(BASE+13)*I4D(BASE+14)
            CALL CHSIZE(I4D(BASE+12),NPTS,0,8,*9001)
            CALL CHSIZE(I4D(BASE+25),I4D(BASE+23),0,1,*9002)   ! history
            CALL CHSIZE(I4D(BASE+24),I4D(BASE+22),0,4,*9003)

C   zero out this section in the table

            DO J = VARADR+(I-1)*VARLEN+1, VARADR+I*VARLEN
              L1D(J) = 0
            END DO
            VARNUM = VARNUM-1
          END IF
        END DO
      ELSE IF( K .EQ. -8 )THEN   ! destroy all tensors
        DO I = 1, VARMAX
          BASE = (VARADR+(I-1)*VARLEN)/4
          IF( (I4D(BASE+1).GT.0) .AND. (I4D(BASE+11).EQ.3) )THEN
            NPTS = I4D(BASE+13)*I4D(BASE+14)*I4D(BASE+15)
            CALL CHSIZE(I4D(BASE+12),NPTS,0,8,*9001)
            CALL CHSIZE(I4D(BASE+25),I4D(BASE+23),0,1,*9002)   ! history
            CALL CHSIZE(I4D(BASE+24),I4D(BASE+22),0,4,*9003)

C   zero out this section in the table

            DO J = VARADR+(I-1)*VARLEN+1, VARADR+I*VARLEN
              L1D(J) = 0
            END DO
            VARNUM = VARNUM-1
          END IF
        END DO
      END IF
      RETURN
 9001 CALL ERROR_MESSAGE('??'
     & ,'deallocating space for data: DELETE_VARIABLE routine')
      RETURN
 9002 CALL ERROR_MESSAGE('??'
     & ,'deallocating space for history lines: DELETE_VARIABLE routine')
      RETURN
 9003 CALL ERROR_MESSAGE('??'
     & ,'deallocating space for history line lengths:'//
     &  ' DELETE_VARIABLE routine')
      RETURN
      END      
