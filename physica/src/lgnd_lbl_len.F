      SUBROUTINE LEGEND_LABEL_LENGTH( PLEN, HEIGHT, HITMAX, LINEIN
     &                               ,LENIN, *)

      CHARACTER*1024 LINEIN
      REAL*4        PLEN, HEIGHT, HITMAX
      INTEGER*4     LENIN

      REAL*4    ANGL, ANGL_OLD
      LOGICAL*4 ITALICS
      COMMON /GPLOT_PSALPH/ ITALICS, ANGL, ANGL_OLD

      LOGICAL*4 ASIS
      COMMON /MODE/ ASIS

      REAL*4    XMINHP, XMAXHP, YMINHP, YMAXHP
      INTEGER*4 IORIENTHP
      COMMON /HARDCOPYRANGE/ XMINHP, XMAXHP, YMINHP, YMAXHP, IORIENTHP

      CHARACTER*2 CTRL
      COMMON /GPLOT_CONTROLS/ CTRL

      LOGICAL*4 CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED

C  local variables

      CHARACTER*256 LINE
      CHARACTER*2   CTRL_OLD
      CHARACTER*20  FONT, FONT_OLD, GETLAB
      CHARACTER*1   ESC, DUM(16)
      REAL*4        ADUM
      INTEGER*4     LENGTH
      INTEGER*2     STATE_TABLE(4,5), CLASSES(0:127), ISTATE
     &             ,NEW_STATE, ICLASS
      LOGICAL*4     ASIS_OLD, ITALICS_OLD
      BYTE          TEXT(1024)

      DATA CLASSES
     &/  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  4,  3,  3,  3,  3,
     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  1,  3,  2,  3,
     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3 /

      DATA STATE_TABLE
C          <    >  othr <ESC>
     1 /   2,   1,   1,  100,
     2     2,   1,   3,  100,
     3     2,   5,   4,  100,
     4     2,   5,   4,  100,
     5     2,   1,   1,  100  /

      DATA DUM /'0','1','2','3','4','5','6','7'
     &         ,'8','9','A','B','C','D','E','F'/
CCC
      ESC = CHAR(27)

      IF( CTRLC_CALLED )RETURN 1

      IF( LENIN .EQ. 0 )RETURN

      FONT     = GETLAB('FONT')
      LENFONT  = LENSIG(FONT)
      XUWIND   = GETNAM('XUWIND')
      XLWIND   = GETNAM('XLWIND')

      CALL PSANGL(ANGL,ANGL_OLD)
      CALL PSANGL(ANGL_OLD,ADUM)
      FONT_OLD    = FONT
      LENFONT_OLD = ABS(LENFONT)
      ASIS_OLD    = ASIS
      ITALICS_OLD = ITALICS
      HEIGHT_OLD  = HEIGHT
      HITMAX      = HEIGHT
      CTRL_OLD    = CTRL
      IDCNT       = 0
      IUCNT       = 0

      LINE = ' '
      LINE = LINEIN
      LENGTH = LENIN+1
      LINE(LENGTH:LENGTH) = ESC
      PLEN = 0.0
      I = 0

      NEW_STATE = 1
      IONE      = 1
      I = 0
   10 I = I + 1
      IF( CTRLC_CALLED )GO TO 100
      ISTATE     = NEW_STATE
      ICLASS     = CLASSES(ICHAR(LINE(I:I)))
      NEW_STATE  = STATE_TABLE(ICLASS,ISTATE)
      ITWO       = I - 1
      IF( NEW_STATE .EQ. 100 )THEN  ! End of line reached
        IF( ITWO .GE. IONE )THEN
          IF( ASIS )THEN
            II = 2
            DO I = IONE+1, ITWO, 2
              DO J = 5, 16          ! first hex digit goes from 4 to F
                IF( LINE(I-1:I-1) .EQ. DUM(J) )THEN
                  II1 = J-1
                  GO TO 20
                END IF
              END DO
              CALL TRANSPARENT_MODE(0)
              CALL ERROR_MESSAGE('GRAPH legend'
     &         ,'hex code error '//LINE(I-1:I))
              TEXT(II/2) = 0
              GO TO 40
   20         DO J = 1, 16
                IF( LINE(I:I) .EQ. DUM(J) )THEN
                  II2 = J-1
                  GO TO 30
                END IF
              END DO
              CALL TRANSPARENT_MODE(0)
              CALL ERROR_MESSAGE('GRAPH legend'
     &         ,'hex code error '//LINE(I-1:I))
              TEXT(II/2) = 0
              GO TO 40
   30         ISS = 16*II1+II2
              IF( ISS .GT. 127 )ISS = ISS-256
              TEXT(II/2) = ISS
   40         II = II+2
            END DO
            LEN = (ITWO-IONE+1) / 2
            PLEN = PLEN + PSMLEN(TEXT,LEN,HEIGHT)
          ELSE
            LEN = ITWO-IONE+1
#ifdef VMS
            PLEN = PLEN + PSMLEN(%REF(LINE(IONE:ITWO)),LEN,HEIGHT)
#else
            PLEN = PLEN + PSMLEN(LINE(IONE:ITWO),LEN,HEIGHT)
#endif
          END IF
        END IF
        ASIS = ASIS_OLD
        CALL PFONT(FONT_OLD(1:LENFONT_OLD),0)
        HEIGHT = HEIGHT_OLD
        CLASSES(ICHAR(CTRL(1:1))) = 3
        CLASSES(ICHAR(CTRL(2:2))) = 3
        CTRL = CTRL_OLD
        CLASSES(ICHAR(CTRL(1:1))) = 1
        CLASSES(ICHAR(CTRL(2:2))) = 2
        ITALICS = ITALICS_OLD
        ANGL = ANGL_OLD
        CALL PSANGL(ANGL,ADUM)
        IDCNT = 0
        IUCNT = 0
        RETURN
      ELSE IF( NEW_STATE .EQ. 3 )THEN  ! Leading command delimiter found
        J1 = I
        J2 = I
      ELSE IF( NEW_STATE .EQ. 4 )THEN  ! Keep track of characters found
        J2 = I                         !  after leading command delimiter
      ELSE IF( NEW_STATE .EQ. 5 )THEN  ! Trailing command delimiter found
        ITWO = J1 - 2
        IF( ITWO .GE. IONE )THEN
          IF( ASIS )THEN
            II = 2
            DO I = IONE+1, ITWO, 2
              DO J = 5, 16          ! first hex digit goes from 4 to F
                IF( LINE(I-1:I-1) .EQ. DUM(J) )THEN
                  II1 = J-1
                  GO TO 50
                END IF
              END DO
              CALL TRANSPARENT_MODE(0)
              CALL ERROR_MESSAGE('GRAPH legend'
     &         ,'hex code error '//LINE(I-1:I))
              TEXT(II/2) = 0
              GO TO 70
   50         DO J = 1, 16
                IF( LINE(I:I) .EQ. DUM(J) )THEN
                  II2 = J-1
                  GO TO 60
                END IF
              END DO
              CALL TRANSPARENT_MODE(0)
              CALL ERROR_MESSAGE('GRAPH legend'
     &         ,'hex code error '//LINE(I-1:I))
              TEXT(II/2) = 0
              GO TO 70
   60         ISS = 16*II1+II2
              IF( ISS .GT. 127 )ISS = ISS-256
              TEXT(II/2) = ISS
   70         II = II+2
            END DO
            LEN = (ITWO-IONE+1) / 2
            PLEN = PLEN + PSMLEN(TEXT,LEN,HEIGHT)
          ELSE
            LEN = ITWO-IONE+1
#ifdef VMS
            PLEN = PLEN + PSMLEN(%REF(LINE(IONE:ITWO)),LEN,HEIGHT)
#else
            PLEN = PLEN + PSMLEN(LINE(IONE:ITWO),LEN,HEIGHT)
#endif
          END IF
        END IF
        CALL LEGEND_DO_COMMAND(HEIGHT,LINE(J1:J2),J2-J1+1,IDCNT,IUCNT
     &   ,PLEN,FONT,LENFONT,HITMAX,*100)
        IONE = J2 + 2
      END IF
      GO TO 10
  100 CALL PSANGL(ANGL_OLD,ADUM)
      IF( CTRLC_CALLED )RETURN 1
      RETURN
      END

      SUBROUTINE LEGEND_DO_COMMAND(HEIGHT,COMM,LENT,IDCNT,IUCNT
     &                            ,PLEN,FONT,LENFONTIN,HITMAX,*)

      CHARACTER*(*) COMM
      CHARACTER*20  FONT
      REAL*4        HEIGHT, PLEN, HITMAX
      INTEGER*4     LENT, IDCNT, IUCNT, LENFONTIN

      INTEGER*4 ILEV
      COMMON /PLOT_LEVEL/ ILEV

      LOGICAL*4 ASIS
      COMMON /MODE/ ASIS

      REAL*4    ANGL, ANGL_OLD
      LOGICAL*4 ITALICS
      COMMON /GPLOT_PSALPH/ ITALICS, ANGL, ANGL_OLD

      CHARACTER*2 CTRL
      COMMON /GPLOT_CONTROLS/ CTRL

      LOGICAL*4 DEFAULTS
      COMMON /GPLOT_TEXT/ DEFAULTS

      INTEGER*4 IMONITOR, IOUTM
      COMMON /PLOTMONITOR/ IMONITOR, IOUTM

      INTEGER*4 IMONITOR2, IOUTM2
      COMMON /PLOTMONITOR2/ IMONITOR2, IOUTM2

C   local variables

      CHARACTER*1024 COMM2, MSSG
      CHARACTER*14  FONT2
      CHARACTER*2   HEXCODE
      CHARACTER*1   COMM1, DUM(16)
      REAL*8        V8
      REAL*4        DH, GETNAM, YLWIND, YUWIND, XLWIND, XUWIND, ADUM
      LOGICAL*4     ASIS_SAVE
      BYTE          TEXT

      DATA DUM /'0','1','2','3','4','5','6','7'
     &         ,'8','9','A','B','C','D','E','F'/
      DATA DH /0.6/
CCC
      LENFONT = ABS(LENFONTIN)
      YLWIND  = GETNAM('YLWIND')
      YUWIND  = GETNAM('YUWIND')
      XLWIND  = GETNAM('XLWIND')
      XUWIND  = GETNAM('XUWIND')
      I1  = 1
    2 IND = INDEX(COMM(I1:LENT),',')
      IF( IND .GT. 0 )THEN
        LEN  = IND - 1
      ELSE
        LEN = LENT - I1 + 1
      END IF
      I2 = I1 + LEN - 1

C   First check to see if the command is a special name

      CALL UPRCASE(COMM(I1:I2),COMM2(1:LEN))
      COMM1 = COMM2(1:1)
      CALL NAME_IN_HEXCODE(COMM(I1:I2),HEXCODE,FONT2,LF)
      IF( HEXCODE .NE. '  ' )THEN
        IF( FONT2(1:LF) .NE. FONT(1:LENFONT) )CALL PFONT(FONT2(1:LF),0)
        DO J = 5, 16          ! first hex digit goes from 4 to F
          IF( HEXCODE(1:1) .EQ. DUM(J) )THEN
            II1 = J-1
            GO TO 4
          END IF
        END DO
        GO TO 50
    4   DO J = 1, 16
          IF( HEXCODE(2:2) .EQ. DUM(J) )THEN
            II2 = J-1
            GO TO 6
          END IF
        END DO
        GO TO 50
    6   ISS = 16*II1+II2
        IF( ISS .GT. 127 )ISS = ISS-256
        TEXT = ISS
    8   ASIS_SAVE = ASIS
        ASIS = .TRUE.
        PLEN = PLEN + PSMLEN(TEXT,1,HEIGHT)
        ASIS = ASIS_SAVE 
        IF(FONT2(1:LF).NE.FONT(1:LENFONT))CALL PFONT(FONT(1:LENFONT),0)
        GO TO 10
      END IF
      IF( INDEX(COMM2(1:LEN),'DEF') .EQ. 1 )THEN
        GO TO 10
      ELSE IF( INDEX(COMM2(1:LEN),'NOD') .EQ. 1 )THEN
        GO TO 10
      ELSE IF( COMM1 .EQ. 'X' )THEN  ! Hexadecimal input flag
        ASIS = .NOT.ASIS
      ELSE IF( (COMM1.EQ.'I') .OR. (COMM2(1:LEN).EQ.'EM') )THEN ! Italics flag
        IF( ITALICS )THEN
          ITALICS = .FALSE.
          ANGL = ANGL_OLD
        ELSE
          ITALICS = .TRUE.
          ANGL = 20.0
        END IF
        CALL PSANGL(ANGL,ADUM,*30)
      ELSE IF(COMM1 .EQ. 'F')THEN  ! Set the font
        FONT(1:LEN-1) = COMM2(2:LEN)
        LENFONT       = LEN - 1
        CALL PFONT(FONT(1:LENFONT),0)
        CALL PSANGL(ANGL,ADUM,*30)
      ELSE IF(COMM1 .EQ. 'H')THEN  ! Set the height
        IF( COMM2(LEN:LEN) .EQ. '%' )THEN
          CALL CH_REAL8(COMM2(2:LEN-1),V8,*20)
          HEIGHT = V8 * (YUWIND - YLWIND) / 100.
        ELSE
          CALL CH_REAL8(COMM2(2:LEN),V8,*20)
          HEIGHT = SNGL( V8 )
        END IF
        IF( HEIGHT .GT. HITMAX )HITMAX = HEIGHT
      ELSE IF(COMM1 .EQ. 'B')THEN  ! Bolding flag
        GO TO 10
      ELSE IF(COMM1 .EQ. 'Z')THEN  ! Include a horizontal space
        IF( COMM2(LEN:LEN) .EQ. '%' )THEN
          CALL CH_REAL8(COMM2(2:LEN-1),V8,*20)
          HORIZ_SPACE = V8 * (XUWIND - XLWIND) / 100.
        ELSE
          CALL CH_REAL8(COMM2(2:LEN),V8,*20)
          HORIZ_SPACE = SNGL( V8 )
        END IF
        PLEN = PLEN + HORIZ_SPACE
      ELSE IF(COMM1 .EQ. 'V')THEN  ! Include a vertical space
        GO TO 10
      ELSE IF( (COMM1.EQ.'U') .OR. (COMM1.EQ.'^') )THEN ! Super-script flag
        IF( IDCNT .GT. 0 )THEN
          IDCNT  = IDCNT - 1
          HEIGHT = HEIGHT / DH
        ELSE
          IUCNT  = IUCNT + 1
          HEIGHT = HEIGHT * DH
          HITMAX = HITMAX+HEIGHT
        END IF
      ELSE IF( (COMM1.EQ.'D') .OR. (COMM1.EQ.'_') )THEN ! Sub-script flag
        IF(IUCNT .GT. 0)THEN
          IUCNT  = IUCNT - 1
          HEIGHT = HEIGHT / DH
        ELSE
          IDCNT  = IDCNT + 1
          HEIGHT = HEIGHT * DH
        END IF
      ELSE IF(COMM1 .EQ. 'C')THEN  ! Set the terminal colour and the pen number
        GO TO 10
      ELSE IF(COMM1 .EQ. 'M')THEN  ! Insert a plotting symbol into the string
        CHARSZ = GETNAM('CHARSZ')
        PLEN   = PLEN + CHARSZ
      ELSE IF(COMM1 .EQ. '?')THEN
        GO TO 10
      ELSE 
        GO TO 40
      END IF
   10 IF( IND .GT. 0 )THEN
        I1 = I1 + IND
        GO TO 2
      END IF
      RETURN
   20 CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      MSSG = 'converting '//CTRL(1:1)//COMM(I1:I2)//CTRL(2:2)//
     &  ' to real number(s)'
      CALL ERROR_MESSAGE('GRAPH legend',MSSG(1:11+1+I2-I1+1+1+18))
      RETURN 1
   30 CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      MSSG = 'calling PSANGL with '//CTRL(1:1)//COMM(I1:I2)//CTRL(2:2)
      CALL ERROR_MESSAGE('GRAPH legend',MSSG(1:20+1+I2-I1+1+1))
      RETURN 1
   40 CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      MSSG = 'invalid text-format command '//CTRL(1:1)//COMM(I1:I2)//
     &  CTRL(2:2)
      CALL ERROR_MESSAGE('GRAPH legend',MSSG(1:28+1+I2-I1+1+1))
      RETURN 1
   50 CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      MSSG = 'invalid hex code '//HEXCODE
      CALL ERROR_MESSAGE('GRAPH legend',MSSG(1:17+2))
      RETURN 1
      END
