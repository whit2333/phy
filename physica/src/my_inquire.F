      SUBROUTINE MY_INQUIRE( * )

C INQUIRE `message prompt' p1 { p2 ... }
C  pI can be a scalar or a text variable

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT, MXCHAR
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20,   MXCHAR=1024)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      CHARACTER*(LENVSM) NAMEG
      CHARACTER*256      NAMEV, NAMET
      COMMON /GETVARIABLEERROR/ NAMEG, LNG, NAMEV, LNV, NAMET, LNT

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      BYTE      L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

      LOGICAL*4 CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED

      CHARACTER*1024 JFILE
      INTEGER*4     LNJ
      LOGICAL*1     JOURNAL, JOURNAL_MACRO
      COMMON /JOURNAL/ IJRNL, LNJ, JOURNAL, JOURNAL_MACRO, JFILE

      INTEGER*4 IOUTS
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS

      INTEGER*4 IINS
      COMMON /PLOT_INPUT_UNIT/ IINS

C   local variables

      CHARACTER*1024      MESSAGE, LINE, STRING
      CHARACTER*(LENVSM) NAMES(NTOTP)
      CHARACTER*15       SOUT
      REAL*8             XNC
      INTEGER*4          INDX, NDIM(NTOTP)
     &                  ,IDX1(4,NTOTP), IDX2(4,NTOTP), IDX3(4,NTOTP)
     &                  ,LN(NTOTP), IVARTYPE(NTOTP), SADDR, ISTRT, N1
     &                  ,N2, N3
     &                  ,IFNAL, LENGTH, NF, IFLD, I, J, NLNE, LENM
CCC
      IF( NQUAL(1) .GE. 1 )
     & CALL WARNING_MESSAGE('INQUIRE','there are no valid qualifiers')

      IF( ITYPE(2) .NE. 2 )THEN
        CALL ERROR_MESSAGE('INQUIRE','expecting a prompt string')
        GO TO 92
      END IF

      LINE = 'INQUIRE '//STRINGS(2)(1:LENST(2))
      NLNE = 8+LENST(2)
      MESSAGE = STRINGS(2)
      CALL GET_TEXT_VARIABLE( MESSAGE, LENST(2), MESSAGE, LENM, *92 )

      IFLD = 3
      DO I = IFLD, NFLD
        IF( ITYPE(I) .NE. 2 )THEN
          CALL INTEGER_TO_CHAR(I,.FALSE.,SOUT,LENS)
          CALL ERROR_MESSAGE('INQUIRE'
     &     ,'field #'//SOUT(1:LENS)//' is invalid')
          GO TO 92
        END IF
        CALL PARSE_NAME( STRINGS(I)(1:LENST(I)),INDX,NDIM(I),N1,N2,N3 )
        LINE = LINE(1:NLNE)//' '//STRINGS(I)(1:LENST(I))
        NLNE = NLNE+1+LENST(I)
        IVARTYPE(I) = NDIM(I)
        IF( NDIM(I) .EQ. -1 )THEN
          CALL GET_OUTPUT_TEXT_VARIABLE( STRINGS(I)(1:LENST(I)), INDX
     &     ,NDIM(I), IDX1(1,I), IDX2(1,I), IER )
        ELSE 
          CALL GET_OUTPUT_VARIABLE( STRINGS(I)(1:LENST(I)), INDX
     &     ,NDIM(I), IDX1(1,I), IDX2(1,I), IDX3(1,I), IER )
        END IF
        IF( IER .NE. 0 )THEN
          CALL GET_VARIABLE_ERROR('INQUIRE',IER)
          GO TO 92
        END IF
        NAMES(I) = NAMEG
        LN(I) = LNG
      END DO
   20 WRITE(IOUTS,22)MESSAGE(1:LENM)
   22 FORMAT(' ',A,' ',$)
      READ(IINS,24,ERR=20,END=90)STRING
   24 FORMAT(A)
      if( ctrlc_called )goto 92
      LST = LENSIG(STRING)
      IF( LST .EQ. 0 )THEN
        STRING = ' '
        LST = 1
      END IF
      IF( JOURNAL )WRITE(IJRNL,10)MESSAGE(1:LENM)//' '//STRING(1:LST)
   10 FORMAT(' ',A)

      NF = NFLD
      IF( (IFLD.EQ.NFLD) .AND. (IVARTYPE(IFLD).EQ.-1) )THEN

C   only one variable requested and it is a text variable
C   so, combine all responses into one string

        STRINGS(1) = STRING
        LENST(1) = LST
        ITYPE(1) = 2
        IF( STRINGS(1) .EQ. ' ' )ITYPE(1) = 0
      ELSE
        NFLD = NFLD - IFLD + 1
        NFLD_START = 0
        CALL PARSE_INPUT_LINE( STRING(1:LST), *92 )
      END IF

      DO 100 I = IFLD, NF
        IF( IVARTYPE(I) .EQ. -1 )THEN
          IF( ITYPE(I-IFLD+1) .EQ. 0 )GO TO 100
          ISTRT = 1
          IFNAL = LENST(I-IFLD+1)
          LENGTH = LENST(I-IFLD+1)
          IF( (STRINGS(I-IFLD+1)(1:1).EQ.CHAR(96)) .AND.
     &        (STRINGS(I-IFLD+1)(LENGTH:LENGTH).EQ.CHAR(39)) )THEN

C         char(96)= `  and  char(39)= '
C         assume there are no internal operators in the string

            ISTRT  = 2
            IFNAL  = LENST(I-IFLD+1)-1
            LENGTH = LENGTH-2
          END IF
          CALL GET_TEMP_SPACE(SADDR,LENGTH,1,*99)
          DO J = ISTRT, IFNAL
            L1D(SADDR+J-ISTRT+1) = ICHAR(STRINGS(I-IFLD+1)(J:J))
          END DO
          IF( NDIM(I) .EQ. 2 )THEN        ! two sets of indices used
            IF( IDX1(1,I) .EQ. 0 )THEN       ! vector array index not used
              JJ = IDX1(2,I)
              IF( JJ .LE. 0 )THEN
                CALL GET_VARIABLE_ERROR('INQUIRE',39)
                GO TO 92
              END IF
              CALL PUT_TEXT_VARIABLE(NAMES(I)(1:LN(I)),JJ,1
     &         ,IDX2(1,I),SADDR,LENGTH,'INQUIRE',*92)
            ELSE                           ! vector array index used
              DO J = 1, IDX1(4,I)
                JJ = I4D(IDX1(3,I)+J)
                IF( JJ .LE. 0 )THEN
                  CALL GET_VARIABLE_ERROR('INQUIRE',39)
                  GO TO 92
                END IF
                CALL PUT_TEXT_VARIABLE(NAMES(I)(1:LN(I)),JJ,1
     &           ,IDX2(1,I),SADDR,LENGTH,'INQUIRE',*92)
              END DO
            END IF
          ELSE IF( NDIM(I) .EQ. 1 )THEN   ! one set of indices used
            IF( IDX1(1,I) .EQ. 0 )THEN       ! assume array index
              JJ = IDX1(2,I)
              IF( JJ .LE. 0 )THEN
                CALL GET_VARIABLE_ERROR('INQUIRE',39)
                GO TO 92
              END IF
              CALL PUT_TEXT_VARIABLE(NAMES(I)(1:LN(I)),JJ,0
     &         ,IDX1(1,I),SADDR,LENGTH,'INQUIRE',*92)
            ELSE                    ! assume character index
              CALL PUT_TEXT_VARIABLE(NAMES(I)(1:LN(I)),0,1
     &         ,IDX1(1,I),SADDR,LENGTH,'INQUIRE',*92)
            END IF
          ELSE IF( NDIM(I) .EQ. 0 )THEN    !  no indices used
            CALL PUT_TEXT_VARIABLE(NAMES(I)(1:LN(I)),0,0
     &       ,IDX1(1,I),SADDR,LENGTH,'INQUIRE',*92)
          END IF
        ELSE IF( IVARTYPE(I) .EQ. 0 )THEN
          IF( ITYPE(I-IFLD+1) .EQ. 2 )THEN
            CALL GET_VARIABLE(STRINGS(I-IFLD+1)(1:LENST(I-IFLD+1))
     &       ,NDM,XNC,N1,N2,N3,N4,*20)
            IF( NDM .NE. 0 )THEN
              CALL INTEGER_TO_CHAR((I-IFLD+1),.FALSE.,SOUT,LENS)
              CALL RITE('expecting scalar or constant in field #'//
     &         SOUT(1:LENS))
              GO TO 20
            END IF
          ELSE IF( ITYPE(I-IFLD+1) .EQ. 1 )THEN
            XNC = REALS(I-IFLD+1)
          ELSE 
            GO TO 100
          END IF
          CALL PUT_VARIABLE( NAMES(I)(1:LN(I)), NDIM(I)
     &     ,IDX1(1,I), IDX2(1,I), IDX3(1,I)
     &     ,0,XNC,0,0,0,0,LINE(1:NLNE),'INQUIRE',*92)
        ELSE IF( IVARTYPE(I) .EQ. 1 )THEN
          IF( ITYPE(I-IFLD+1) .EQ. 2 )THEN
            CALL GET_VARIABLE(STRINGS(I-IFLD+1)(1:LENST(I-IFLD+1))
     &       ,NDM,XNC,IOUT,NOUT,N3,N4,*20 )
            IF( NDM .NE. 1 )THEN
              CALL INTEGER_TO_CHAR((I-IFLD+1),.FALSE.,SOUT,LENS)
              CALL RITE('expecting vector in field #'//SOUT(1:LENS))
              GO TO 20
            END IF
          ELSE IF( ITYPE(I-IFLD+1) .EQ. 1 )THEN
            CALL INTEGER_TO_CHAR((I-IFLD+1),.FALSE.,SOUT,LENS)
            CALL RITE('expecting vector in field #'//SOUT(1:LENS))
            GO TO 20
          ELSE 
            GO TO 100
          END IF
          CALL PUT_VARIABLE( NAMES(I)(1:LN(I)), NDIM(I)
     &     ,IDX1(1,I), IDX2(1,I), IDX3(1,I)
     &     ,1,XNC,IOUT,NOUT,0,0,LINE(1:NLNE),'INQUIRE',*92)
        ELSE IF( IVARTYPE(I) .EQ. 2 )THEN
          IF( ITYPE(I-IFLD+1) .EQ. 2 )THEN
            CALL GET_VARIABLE(STRINGS(I-IFLD+1)(1:LENST(I-IFLD+1))
     &       ,NDM,XNC,IOUT,NR,NC,N4,*20 )
            IF( NDM .NE. 2 )THEN
              CALL INTEGER_TO_CHAR((I-IFLD+1),.FALSE.,SOUT,LENS)
              CALL RITE('expecting matrix in field #'//SOUT(1:LENS))
              GO TO 20
            END IF
          ELSE IF( ITYPE(I-IFLD+1) .EQ. 2 )THEN
            CALL INTEGER_TO_CHAR((I-IFLD+1),.FALSE.,SOUT,LENS)
            CALL RITE('expecting matrix in field #'//SOUT(1:LENS))
            GO TO 20
          ELSE 
            GO TO 100
          END IF
          CALL PUT_VARIABLE( NAMES(I)(1:LN(I)), NDIM(I)
     &     ,IDX1(1,I), IDX2(1,I), IDX3(1,I)
     &     ,2,XNC,IOUT,NR,NC,0,LINE(1:NLNE),'INQUIRE',*92)
        END IF
  100 CONTINUE
      IF( STACK )WRITE(IOUT_UNIT,10)LINE(1:NLNE)
   90 CALL DELETE_TEMP_SPACE
      RETURN
   92 CALL DELETE_TEMP_SPACE
      RETURN 1
   99 CALL ERROR_MESSAGE('INQUIRE','allocating dynamic array space')
      RETURN 1
      END
