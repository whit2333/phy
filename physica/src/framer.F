      SUBROUTINE FRAMER( IHCOR, VERTEX, MASK )
C
C     AUXILIARY-LIBRARY-ROUTINE FOR HST2D
C
C                                                05/AUG/1980
C                                                C.J. KOST SIN
C   from ACM Algorithm 483
C
C Routine to plot a frame on the projection of a 3-dimensional figure
C as drawn by plot3d.
C
C input parameters -
C     IHCOR    number of the vertex of the figure which appears to be 
C              furthest in the background (minus z direction)
C     VERTEX   array containing the coordinates of the vertices of this
C              figure as returned from plot3d on the last call         
C     MASK     array containing the mask for this figure as returned by
C              plot3d on the last call
C              
C The vertices of the frame are numbered (1-4) in the same order as
C their coordinates appear in vertex. the mask array is altered by this 
C routine, but the plotter origin is not moved.
C
      IMPLICIT REAL*8 (A-H,O-Z)

      REAL*8    VERTEX(1)
      INTEGER*4 IHCOR, MASK(1)

      REAL*8    ARRAY(14), CMETRC
      INTEGER*4 I
CCC
      I = 2*IHCOR
      CMETRC = 1.0D0
      IF( I .LT. 2 )I = 2
      IF( I .GT. 8 )I = 8

C the vertices which may be hidden are drawn by a call to PLOT3D

      ARRAY(1) = VERTEX(I-1)
      ARRAY(8) = VERTEX(I)
      ARRAY(2) = VERTEX(I+7)
      ARRAY(9) = VERTEX(I+8)
      ARRAY(4) = ARRAY(2)
      ARRAY(11) = ARRAY(9)
      ARRAY(6) = ARRAY(2)
      ARRAY(13) = ARRAY(9)
      ARRAY(7) = ARRAY(1)
      ARRAY(14) = ARRAY(8)
      I = I - 2
      IF( I .EQ. 0 )I = 8
      ARRAY(3) = VERTEX(I+7)
      ARRAY(10) = VERTEX(I+8)
      I = I + 4
      IF( I .GT. 8 )I = I - 8
      ARRAY(5) = VERTEX(I+7)
      ARRAY(12) = VERTEX(I+8)
      CALL PLOT3D( 110, ARRAY, ARRAY(8), 0.0D0, 1.0D0, 1.0D0, 0.0D0
     & ,2, 7, 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0, MASK, 0.0D0 )

C the remaining vertices are drawn by calls to HPLOT

      CALL HPLOT( VERTEX(I-1), VERTEX(I), 3 )
      I = I - 2
      DO J = 1, 3
        I = I + 2
        IF( I .EQ. 10 )I = 2
        CALL HPLOT( VERTEX(I+7), VERTEX(I+8), 2 )
      END DO
      CALL HPLOT( VERTEX(I-1), VERTEX(I), 2 )
      I = I - 2
      IF( I .EQ. 0 )I = 8
      CALL HPLOT( VERTEX(I-1), VERTEX(I), 3 )
      CALL HPLOT( VERTEX(I+7), VERTEX(I+8), 2 )
      RETURN
      END
