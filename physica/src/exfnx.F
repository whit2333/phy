       SUBROUTINE EXFNX

C   Sets up a table of NFUN standard mathematical REAL*8 functions
C
C  FUNCTN  array of NFUN function names
C  NARGUM  specifies the allowed number of arguments for each function,
C          where NARGUM(1,K) = minimum number of arguments allowed
C          where NARGUM(2,K) = maximum number of arguments allowed,
C          normally, NARGUM(1,K) = NARGUM(2,K), with 0 allowed
C  IFNTYP  specifies function type
C          0 is numeric to numeric scalar function
C          1 is numeric to numeric vector function
C          2 is mixed mode to numeric function
C          3 is mixed mode to character function
C          4 is character to character function
C          5 is EXPAND function
C  ARGTYP  specifies function argument type
C         -1 is numeric 
C          1 is character

      IMPLICIT REAL*8 (A-H,O-Z)

      INTEGER*4 LENVSM, MXFUN
      PARAMETER (LENVSM=32, MXFUN=222)

      CHARACTER*(LENVSM) FUNCTN(MXFUN)
      INTEGER*4          NFUN, NARGUM(2,MXFUN), IFNTYP(MXFUN)
     &                  ,ARGTYP(20,MXFUN)
      COMMON /EXPFUN/ NFUN,NARGUM,IFNTYP,ARGTYP,FUNCTN
CCC
      NFUN = 216

      DO I = 1, NFUN
        IFNTYP(I) = 0
        DO J = 1, 20
          ARGTYP(J,I) = -1   ! default argument type is numeric
        END DO
      END DO
C   flag vector functions
      IFNTYP( 57)= 1   ! MAX
      IFNTYP( 61)= 1   ! MIN
      IFNTYP(158)= 1   ! LOOP
      IFNTYP(159)= 1   ! SUM
      IFNTYP(160)= 1   ! PROD
      IFNTYP(161)= 3   ! RCHAR
      IFNTYP(162)= 3   ! CHAR
      IFNTYP(163)= 2   ! ICHAR
      IFNTYP(164)= 2   ! EVAL
      IFNTYP(165)= 5   ! EXPAND
      IFNTYP(166)= 4   ! DATE
      IFNTYP(167)= 4   ! TIME
      IFNTYP(168)= 4   ! UCASE
      IFNTYP(169)= 4   ! LCASE
      IFNTYP(170)= 4   ! TCASE
      IFNTYP(171)= 2   ! INDEX
      IFNTYP(172)= 2   ! CLEN
      IFNTYP(173)= 1   ! VLEN
      IFNTYP(174)= 1   ! LEN
      IFNTYP(175)= 1   ! WHERE
      IFNTYP(176)= 1   ! RSUM
      IFNTYP(177)= 1   ! RPROD
      IFNTYP(178)= 2   ! EQS
      IFNTYP(179)= 2   ! NES
      IFNTYP(180)= 2   ! SUB
      IFNTYP(181)= 2   ! SUP
      IFNTYP(182)= 1   ! ROLL
      IFNTYP(183)= 1   ! STEP
      IFNTYP(184)= 1   ! WRAP
      IFNTYP(186)= 1   ! FOLD
      IFNTYP(187)= 1   ! UNFOLD
      IFNTYP(188)= 1   ! CONVOL
      IFNTYP(189)= 2   ! INTEGRAL
      IFNTYP(190)= 2   ! DERIV
      IFNTYP(191)= 2   ! INTERP
      IFNTYP(192)= 1   ! SMOOTH
      IFNTYP(193)= 2   ! FFT
      IFNTYP(194)= 1   ! SAVGOL
      IFNTYP(195)= 1   ! SPLINTERP
      IFNTYP(196)= 1   ! SPLSMOOTH
      IFNTYP(197)= 1   ! FIRST
      IFNTYP(198)= 1   ! LAST
      IFNTYP(199)= 2   ! IFFT
      IFNTYP(200)= 2   ! EXIST
      IFNTYP(201)= 3   ! VARNAME
      IFNTYP(202)= 1   ! ICLOSE
      IFNTYP(203)= 1   ! GAUSSJ
      IFNTYP(204)= 1   ! AREA
      IFNTYP(205)= 4   ! TRANSLATE
      IFNTYP(206)= 1   ! PFACTORS
      IFNTYP(207)= 1   ! INVERSE
      IFNTYP(208)= 1   ! DET
      IFNTYP(209)= 1   ! IDENTITY
      IFNTYP(210)= 1   ! EIGEN
      IFNTYP(211)= 1   ! IEQUAL
      IFNTYP(212)= 4   ! STRING
      IFNTYP(213)= 1   ! JOIN
      IFNTYP(214)= 3   ! VARTYPE
      IFNTYP(215)= 2   ! BIN
      IFNTYP(216)= 1   ! SSUM (simple sum)

C  Define the function name table 

      FUNCTN(1) = 'LOG'
      FUNCTN(2) = 'LN'
      FUNCTN(3) = 'DIGAMMA'
      FUNCTN(4) = 'PSI'
      FUNCTN(5) = 'LOG10'
      FUNCTN(6) = 'AIRY'
      FUNCTN(7) = 'EXP'
      FUNCTN(8) = 'LOGAM'
      FUNCTN(9) = 'SQRT'
      FUNCTN(10)= 'BESI0'
      FUNCTN(11)= 'SIN'
      FUNCTN(12)= 'SINE'
      FUNCTN(13)= 'BESI1'
      FUNCTN(14)= 'BESK0'
      FUNCTN(15)= 'COS'
      FUNCTN(16)= 'COSINE'
      FUNCTN(17)= 'BESK1'
      FUNCTN(18)= 'BESJ0'
      FUNCTN(19)= 'TAN'
      FUNCTN(20)= 'TANGENT'
      FUNCTN(21)= 'BESJ1'
      FUNCTN(22)= 'BESY0'
      FUNCTN(23)= 'ASIN'
      FUNCTN(24)= 'ARSIN'
      FUNCTN(25)= 'BESY1'
      FUNCTN(26)= 'COSINT'
      FUNCTN(27)= 'DAWSON'
      FUNCTN(28)= 'ELLICK'
      FUNCTN(29)= 'ACOS'
      FUNCTN(30)= 'ARCOS'
      FUNCTN(31)= 'ELLICE'
      FUNCTN(32)= 'ERF'
      FUNCTN(33)= 'EXPINT'
      FUNCTN(34)= 'FREQ'
      FUNCTN(35)= 'ATAN'
      FUNCTN(36)= 'ARTAN'
      FUNCTN(37)= 'ARCTAN'
      FUNCTN(38)= 'GAUSSIN'
      FUNCTN(39)= 'SININT'
      FUNCTN(40)= 'GAMMA'
      FUNCTN(41)= 'ATAN2'
      FUNCTN(42)= 'ARTAN2'
      FUNCTN(43)= 'ARCTAN2'
      FUNCTN(44)= 'DILOG'
      FUNCTN(45)= 'STRUVE0'
      FUNCTN(46)= 'STRUVE1'
      FUNCTN(47)= 'SINH'
      FUNCTN(48)= 'COSH'
      FUNCTN(49)= 'TANH'
      FUNCTN(50)= 'CSCH'
      FUNCTN(51)= 'SECH'
      FUNCTN(52)= 'COTH'
      FUNCTN(53)= 'ABS'
      FUNCTN(54)= 'ASINH'
      FUNCTN(55)= 'ARSINH'
      FUNCTN(56)= 'ARCOSH'
      FUNCTN(57)= 'MAX'
      FUNCTN(58)= 'ACOSH'
      FUNCTN(59)= 'ACOTH'
      FUNCTN(60)= 'ARCOTH'
      FUNCTN(61)= 'MIN'
      FUNCTN(62)= 'ASECH'
      FUNCTN(63)= 'ARSECH'
      FUNCTN(64)= 'ACSCH'
      FUNCTN(65)= 'ARCSCH'
      FUNCTN(66)= 'COT'
      FUNCTN(67)= 'SEC'
      FUNCTN(68)= 'CSC'
      FUNCTN(69)= 'INT'
      FUNCTN(70)= 'IFIX'
      FUNCTN(71)= 'MOD'
      FUNCTN(72)= 'ARCOT'
      FUNCTN(73)= 'ACOT'
      FUNCTN(74)= 'ARSEC'
      FUNCTN(75)= 'SIGN'
      FUNCTN(76)= 'ASEC'
      FUNCTN(77)= 'DIM'
      FUNCTN(78)= 'ARCSC'
      FUNCTN(79)= 'RAN'
      FUNCTN(80)= 'VOIGT'
      FUNCTN(81)= 'ACSC'
      FUNCTN(82)= 'FREC1'
      FUNCTN(83)= 'FREC2'
      FUNCTN(84)= 'FRES1'
      FUNCTN(85)= 'FRES2'
      FUNCTN(86)= 'PLM'
      FUNCTN(87)= 'PLMN'
      FUNCTN(88)= 'PLMU'
      FUNCTN(89)= 'FERDIRAC'
      FUNCTN(90)= 'ERFC'
      FUNCTN(91)= 'BIRY'
      FUNCTN(92)= 'EI'
      FUNCTN(93)= 'FACTORIAL'
      FUNCTN(94)= 'BER'
      FUNCTN(95)= 'BEI'
      FUNCTN(96)= 'AERF'
      FUNCTN(97)= 'EXPN'
      FUNCTN(98)= 'RADMAC'
      FUNCTN(99)= 'WALSH'
      FUNCTN(100)= 'BETA'
      FUNCTN(101)= 'BINOM'
      FUNCTN(102)= 'CHEBY'
      FUNCTN(103)= 'CHISQ'
      FUNCTN(104)= 'CHISQINV'
      FUNCTN(105)= 'FINELLIC'
      FUNCTN(106)= 'EINELLIC'
      FUNCTN(107)= 'GAMMAIN'
      FUNCTN(108)= 'GAMMACIN'
      FUNCTN(109)= 'GAMMATIN'
      FUNCTN(110)= 'HERMITE'
      FUNCTN(111)= 'LAGUERRE'
      FUNCTN(112)= 'LEGENDRE'
      FUNCTN(113)= 'STUDENT'
      FUNCTN(114)= 'STUDENTI'
      FUNCTN(115)= 'BETAIN'
      FUNCTN(116)= 'BIVARNOR'
      FUNCTN(117)= 'CHLOGU'
      FUNCTN(118)= 'FISHER'
      FUNCTN(119)= 'POICA'
      FUNCTN(120)= 'JACOBI'
      FUNCTN(121)= 'HYPGEO'
      FUNCTN(122)= 'KER'
      FUNCTN(123)= 'KEI'
      FUNCTN(124)= 'SIND'
      FUNCTN(125)= 'COSD'
      FUNCTN(126)= 'TAND'
      FUNCTN(127)= 'ASIND'
      FUNCTN(128)= 'ACOSD'
      FUNCTN(129)= 'ATAND'
      FUNCTN(130)= 'ATAN2D'
      FUNCTN(131)= 'NINT'
      FUNCTN(132)= 'USER1'
      FUNCTN(133)= 'USER2'
      FUNCTN(134)= 'USER3'
      FUNCTN(135)= 'USER4'
      FUNCTN(136)= 'USER5'
      FUNCTN(137)= 'USER6'
      FUNCTN(138)= 'USER7'
      FUNCTN(139)= 'USER8'
      FUNCTN(140)= 'USERN'
      FUNCTN(141)= 'AERFC'
      FUNCTN(142)= 'GAUSS'
      FUNCTN(143)= 'AGAUSS'
      FUNCTN(144)= 'PROB'
      FUNCTN(145)= 'ATANH'
      FUNCTN(146)= 'ARTANH'
      FUNCTN(147)= 'GAMMLN'
      FUNCTN(148)= 'GAMMQ'
      FUNCTN(149)= 'CLEBSG'
      FUNCTN(150)= 'WIGN3J'
      FUNCTN(151)= 'WIGN6J'
      FUNCTN(152)= 'WIGN9J'
      FUNCTN(153)= 'RACAHC'
      FUNCTN(154)= 'JAHNUF'
      FUNCTN(155)= 'NORMAL'
      FUNCTN(156)= 'TINA'
      FUNCTN(157)= 'ELTIME'
      FUNCTN(158)= 'LOOP'
      FUNCTN(159)= 'SUM'
      FUNCTN(160)= 'PROD'
      FUNCTN(161)= 'RCHAR'
      FUNCTN(162)= 'CHAR'
      FUNCTN(163)= 'ICHAR'
      FUNCTN(164)= 'EVAL'
      FUNCTN(165)= 'EXPAND'
      FUNCTN(166)= 'DATE'
      FUNCTN(167)= 'TIME'
      FUNCTN(168)= 'UCASE'
      FUNCTN(169)= 'LCASE'
      FUNCTN(170)= 'TCASE'
      FUNCTN(171)= 'INDEX'
      FUNCTN(172)= 'CLEN'
      FUNCTN(173)= 'VLEN'
      FUNCTN(174)= 'LEN'
      FUNCTN(175)= 'WHERE'
      FUNCTN(176)= 'RSUM'
      FUNCTN(177)= 'RPROD'
      FUNCTN(178)= 'EQS'
      FUNCTN(179)= 'NES'
      FUNCTN(180)= 'SUB'
      FUNCTN(181)= 'SUP'
      FUNCTN(182)= 'ROLL'
      FUNCTN(183)= 'STEP'
      FUNCTN(184)= 'WRAP'
      FUNCTN(185)= 'NULL'
      FUNCTN(186)= 'FOLD'
      FUNCTN(187)= 'UNFOLD'
      FUNCTN(188)= 'CONVOL'
      FUNCTN(189)= 'INTEGRAL'
      FUNCTN(190)= 'DERIV'
      FUNCTN(191)= 'INTERP'
      FUNCTN(192)= 'SMOOTH'
      FUNCTN(193)= 'FFT'
      FUNCTN(194)= 'SAVGOL'
      FUNCTN(195)= 'SPLINTERP'
      FUNCTN(196)= 'SPLSMOOTH'
      FUNCTN(197)= 'FIRST'
      FUNCTN(198)= 'LAST'
      FUNCTN(199)= 'IFFT'
      FUNCTN(200)= 'EXIST'
      FUNCTN(201)= 'VARNAME'
      FUNCTN(202)= 'ICLOSE'
      FUNCTN(203)= 'GAUSSJ'
      FUNCTN(204)= 'AREA'
      FUNCTN(205)= 'TRANSLATE'
      FUNCTN(206)= 'PFACTORS'
      FUNCTN(207)= 'INVERSE'
      FUNCTN(208)= 'DET'
      FUNCTN(209)= 'IDENTITY'
      FUNCTN(210)= 'EIGEN'
      FUNCTN(211)= 'IEQUAL'
      FUNCTN(212)= 'STRING'
      FUNCTN(213)= 'JOIN'
      FUNCTN(214)= 'VARTYPE'
      FUNCTN(215)= 'BIN'
      FUNCTN(216)= 'SSUM'

C Specify the number of acceptable arguments that may be passed
C to the subroutine
C        NARGUM(1,I) specifies the mininum number for routine I
C        NARGUM(2,I) specifies the maximum number for routine I
C
C The most common number of arguments allowed is exactly 1, 
C and so initially fill the NARGUM array with this value

      DO K = 1, NFUN
        NARGUM(1,K) = 1
        NARGUM(2,K) = 1
      END DO

C Now specify the exceptions, i.e. those functions which may
C take more than a single argument

      NARGUM(1,41)= 2      ! ATAN2
      NARGUM(2,41)= 2
      NARGUM(1,42)= 2      ! ARTAN2
      NARGUM(2,42)= 2
      NARGUM(1,43)= 2      ! ARCTAN2
      NARGUM(2,43)= 2
      NARGUM(1,57)= 1      ! MAX
      NARGUM(2,57)= 20
      NARGUM(1,61)= 1      ! MIN
      NARGUM(2,61)= 20
      NARGUM(1,71)= 2      ! MOD
      NARGUM(2,71)= 2
      NARGUM(1,75)= 2      ! SIGN
      NARGUM(2,75)= 2
      NARGUM(1,77)= 2      ! DIM
      NARGUM(2,77)= 2
      NARGUM(1,80)= 4      ! VOIGT
      NARGUM(2,80)= 4      !
      NARGUM(1,86)= 3      ! PLM
      NARGUM(2,86)= 3      !
      NARGUM(1,87)= 3      ! PLMN
      NARGUM(2,87)= 3      !
      NARGUM(1,88)= 3      ! PLMU
      NARGUM(2,88)= 3      !
      NARGUM(1,89)= 2      ! FERDIRAC
      NARGUM(2,89)= 2      !
      DO K = 97, 114       ! EXPN to STUDENTI
        NARGUM(1,K)= 2
        NARGUM(2,K)= 2
      END DO
      DO K = 115, 119      ! BETAIN to POICA
        NARGUM(1,K)= 3
        NARGUM(2,K)= 3
      END DO               
      NARGUM(1,120)= 4     ! JACOBI
      NARGUM(2,120)= 4     !
      NARGUM(1,121)= 4     ! HYPGEO
      NARGUM(2,121)= 4     !
      NARGUM(1,130)= 2     ! ATAN2D
      NARGUM(2,130)= 2     !
      DO K = 1, 8          ! USER1 to USER8
        NARGUM(1,131+K)= 1
        NARGUM(2,131+K)= 20
      END DO
      NARGUM(1,140)= 1     ! USERN
      NARGUM(2,140)= 20    !
      NARGUM(1,144)= 2     ! PROB
      NARGUM(2,144)= 2     !
      NARGUM(1,148)= 2     ! GAMMQ
      NARGUM(2,148)= 2     !
      NARGUM(1,149)= 3     ! CLEBSG(A,B,C,X,Y,Z)
      NARGUM(2,149)= 6     !
      NARGUM(1,150)= 3     ! WIGN3J(A,B,C,X,Y,Z)
      NARGUM(2,150)= 6     !
      NARGUM(1,151)= 6     ! WIGN6J(U,V,W,X,Y,Z)
      NARGUM(2,151)= 6     !
      NARGUM(1,152)= 9     ! WIGN9J(A1,A2,A3,A4,A5,A6,A7,A8,A9)
      NARGUM(2,152)= 9     !
      NARGUM(1,153)= 6     ! RACAHC(A,B,C,D,E,F)
      NARGUM(2,153)= 6     !
      NARGUM(1,154)= 6     ! JAHNUF(A,B,C,D,E,F)
      NARGUM(2,154)= 6     !
      NARGUM(1,155)= 3     ! NORMAL(X,A,B)
      NARGUM(2,155)= 3     !
      NARGUM(1,156)= 4     ! TINA(X,B,C,D)
      NARGUM(2,156)= 4     !
      NARGUM(1,158)= 3     ! LOOP array function
      NARGUM(2,158)= 3     !
      NARGUM(1,159)= 3     ! SUM array function
      NARGUM(2,159)= 3     !
      NARGUM(1,160)= 3     ! PROD array function
      NARGUM(2,160)= 3     !
C
      NARGUM(1,161)= 1     ! RCHAR 
      NARGUM(2,161)= 2     !
      ARGTYP(1,161)=-1     ! numeric
      ARGTYP(2,161)= 1     ! character

      NARGUM(1,162)= 1     ! CHAR
      NARGUM(2,162)= 1     !
      ARGTYP(1,162)=-1     ! numeric

      NARGUM(1,163)= 1     ! ICHAR
      NARGUM(2,163)= 1     !
      ARGTYP(1,163)= 1     ! character

      NARGUM(1,164)= 1     ! EVAL
      NARGUM(2,164)= 1     !
      ARGTYP(1,164)= 2     ! expandable character (to be evaluated)

      NARGUM(1,165)= 1     ! EXPAND
      NARGUM(2,165)= 1     !
      ARGTYP(1,165)= 3     ! character to be expanded

      NARGUM(1,166)= 0     ! DATE
      NARGUM(2,166)= 0     !

      NARGUM(1,167)= 0     ! TIME
      NARGUM(2,167)= 0     !

      NARGUM(1,168)= 1     ! UCASE
      NARGUM(2,168)= 1     !
      ARGTYP(1,168)= 1     ! character

      NARGUM(1,169)= 1     ! LCASE
      NARGUM(2,169)= 1     !
      ARGTYP(1,169)= 1     ! character

      NARGUM(1,170)= 1     ! TCASE
      NARGUM(2,170)= 1     !
      ARGTYP(1,170)= 1     ! character

      NARGUM(1,171)= 2     ! INDEX
      NARGUM(2,171)= 2     !
      ARGTYP(1,171)= 1     ! character
      ARGTYP(2,171)= 1     ! character

      NARGUM(1,172)= 1     ! CLEN
      NARGUM(2,172)= 1     ! 
      ARGTYP(1,172)= 1     ! character

      NARGUM(1,173)= 1     ! VLEN( vector ) or VLEN( matrix )
      NARGUM(2,173)= 1     !
      ARGTYP(1,173)=-1     ! numeric

      NARGUM(1,174)= 1     ! LEN( vector )
      NARGUM(2,174)= 1     !
      ARGTYP(1,174)=-1     ! numeric

      NARGUM(1,175)= 1     ! WHERE( vector )
      NARGUM(2,175)= 1     !
      ARGTYP(1,175)=-1     ! numeric

      NARGUM(1,176)= 3     ! RSUM array function
      NARGUM(2,176)= 3     !

      NARGUM(1,177)= 3     ! RPROD array function
      NARGUM(2,177)= 3     !

      NARGUM(1,178)= 2     ! EQS
      NARGUM(2,178)= 2     !
      ARGTYP(1,178)= 1     ! character
      ARGTYP(2,178)= 1     ! character

      NARGUM(1,179)= 2     ! NES
      NARGUM(2,179)= 2     !
      ARGTYP(1,179)= 1     ! character
      ARGTYP(2,179)= 1     ! character

      NARGUM(1,180)= 2     ! SUB
      NARGUM(2,180)= 2     !
      ARGTYP(1,180)= 1     ! character
      ARGTYP(2,180)= 1     ! character

      NARGUM(1,181)= 2     ! SUP
      NARGUM(2,181)= 2     !
      ARGTYP(1,181)= 1     ! character
      ARGTYP(2,181)= 1     ! character

      NARGUM(1,182)= 2     ! ROLL
      NARGUM(2,182)= 2     !
      ARGTYP(1,182)=-1     ! numeric
      ARGTYP(2,182)=-1     ! numeric

      NARGUM(1,183)= 2     ! STEP
      NARGUM(2,183)= 2     !
      ARGTYP(1,183)=-1     ! numeric
      ARGTYP(2,183)=-1     ! numeric

      NARGUM(1,184)= 2     ! WRAP
      NARGUM(2,184)= 2     !
      ARGTYP(1,184)=-1     ! numeric
      ARGTYP(2,184)=-1     ! numeric

      NARGUM(1,186)= 2     ! FOLD( vector, scalar )
      NARGUM(2,186)= 2     !
      ARGTYP(1,186)=-1     ! numeric
      ARGTYP(2,186)=-1     ! numeric

      NARGUM(1,187)= 1     ! UNFOLD( matrix )
      NARGUM(2,187)= 1     !
      ARGTYP(1,187)=-1     ! numeric

      NARGUM(1,188)= 3     ! CONVOL( x, blur, flag )
      NARGUM(2,188)= 3     !
      ARGTYP(1,188)=-1     ! numeric
      ARGTYP(2,188)=-1     ! numeric
      ARGTYP(3,188)=-1     ! numeric

      NARGUM(1,189)= 2     ! INTEGRAL( x, y, {`keyword'} )
      NARGUM(2,189)= 3     !
      ARGTYP(1,189)= -1    ! numeric
      ARGTYP(2,189)= -1    ! numeric
      ARGTYP(3,189)= 1     ! character

      NARGUM(1,190)= 2     ! DERIV( x, y, {`keyword'} )
      NARGUM(2,190)= 3     !
      ARGTYP(1,190)= -1    ! numeric
      ARGTYP(2,190)= -1    ! numeric
      ARGTYP(3,190)= 1     ! character

      NARGUM(1,191)= 3     ! INTERP( x, y, xout, {`keyword'} )
      NARGUM(2,191)= 4     !
      ARGTYP(1,191)= -1    ! numeric
      ARGTYP(2,191)= -1    ! numeric
      ARGTYP(3,191)= -1    ! numeric
      ARGTYP(4,191)= 1     ! character

      NARGUM(1,192)= 3     ! SMOOTH( x, y, xout, {weights} )
      NARGUM(2,192)= 4     !
      ARGTYP(1,192)= -1    ! numeric
      ARGTYP(2,192)= -1    ! numeric
      ARGTYP(3,192)= -1    ! numeric
      ARGTYP(4,192)= -1    ! numeric

      NARGUM(1,193)= 1     ! FFT( x, {`keyword'} )
      NARGUM(2,193)= 2     !
      ARGTYP(1,193)= -1    ! numeric
      ARGTYP(2,193)= 1     ! character

      NARGUM(1,194)= 3     ! SAVGOL( order, filterwidth, y )
      NARGUM(2,194)= 3     !
      ARGTYP(1,194)= -1    ! numeric
      ARGTYP(2,194)= -1    ! numeric
      ARGTYP(3,194)= -1    ! numeric

      NARGUM(1,195)= 3     ! SPLINTERP( x, y, npts )
      NARGUM(2,195)= 3     !
      ARGTYP(1,195)= -1    ! numeric
      ARGTYP(2,195)= -1    ! numeric
      ARGTYP(3,195)= -1    ! numeric

      NARGUM(1,196)= 3     ! SPLSMOOTH( x, y, npts, {weights} )
      NARGUM(2,196)= 4     !
      ARGTYP(1,196)= -1    ! numeric
      ARGTYP(2,196)= -1    ! numeric
      ARGTYP(3,196)= -1    ! numeric
      ARGTYP(4,196)= -1    ! numeric

      NARGUM(1,197)= 1     ! FIRST( vector )
      NARGUM(2,197)= 1     !
      ARGTYP(1,197)= -1    ! numeric

      NARGUM(1,198)= 1     ! LAST( vector )
      NARGUM(2,198)= 1     !
      ARGTYP(1,198)= -1    ! numeric

      NARGUM(1,199)= 1     ! IFFT( m, {`keyword'} )
      NARGUM(2,199)= 2     !
      ARGTYP(1,199)= -1    ! numeric
      ARGTYP(2,199)= 1     ! character

      NARGUM(1,200)= 1     ! EXIST( variable )
      NARGUM(2,200)= 1     ! 
      ARGTYP(1,200)= 1     ! character

      NARGUM(1,201)= 1     ! VARNAME
      NARGUM(2,201)= 1     ! 
      ARGTYP(1,201)= -1    ! numeric

      NARGUM(1,202)= 2     ! ICLOSE( vector, value )
      NARGUM(2,202)= 2     !
      ARGTYP(1,202)= -1    ! numeric
      ARGTYP(2,202)= -1    ! numeric

      NARGUM(1,203)= 2     ! GAUSSJ( matrix, vector )
      NARGUM(2,203)= 2     !
      ARGTYP(1,203)=-1     ! numeric
      ARGTYP(2,203)=-1     ! numeric

      NARGUM(1,204)= 2     ! AREA( xvector, yvector )
      NARGUM(2,204)= 2     !
      ARGTYP(1,204)=-1     ! numeric
      ARGTYP(2,204)=-1     ! numeric

      NARGUM(1,205)= 1     ! TRANSLATE( string )
      NARGUM(2,205)= 1     !
      ARGTYP(1,205)= 1     ! character

      NARGUM(1,206)= 1     ! PFACTORS( scalar )
      NARGUM(2,206)= 1     !
      ARGTYP(1,206)=-1     ! numeric

      NARGUM(1,207)= 1     ! INVERSE( matrix )
      NARGUM(2,207)= 1     !
      ARGTYP(1,207)=-1     ! numeric

      NARGUM(1,208)= 1     ! DET( matrix )
      NARGUM(2,208)= 1     !
      ARGTYP(1,208)=-1     ! numeric

      NARGUM(1,209)= 1     ! IDENTITY( scalar )
      NARGUM(2,209)= 1     !
      ARGTYP(1,209)=-1     ! numeric

      NARGUM(1,210)= 1     ! EIGEN( matrix )
      NARGUM(2,210)= 1     !
      ARGTYP(1,210)=-1     ! numeric

      NARGUM(1,211)= 2     ! IEQUAL( vector, value )
      NARGUM(2,211)= 2     !
      ARGTYP(1,211)= -1    ! numeric
      ARGTYP(2,211)= -1    ! numeric

      NARGUM(1,212)= 1     ! STRING
      NARGUM(2,212)= 1     !
      ARGTYP(1,212)= 1     ! character

      NARGUM(1,213)= 2     ! JOIN( vector, vector )
      NARGUM(2,213)= 2     !
      ARGTYP(1,213)= -1    ! numeric
      ARGTYP(2,213)= -1    ! numeric

      NARGUM(1,214)= 1     ! VARTYPE( variable )
      NARGUM(2,214)= 1     !
      ARGTYP(1,214)= 1     ! character

      NARGUM(1,215)= 4     ! BIN( x, nbins, xmin, xmax, {`keyword'} )
      NARGUM(2,215)= 5     !
      ARGTYP(1,215)= -1    ! numeric
      ARGTYP(2,215)= -1    ! numeric
      ARGTYP(3,215)= -1    ! numeric
      ARGTYP(4,215)= -1    ! numeric
      ARGTYP(5,215)= 1     ! character

      NARGUM(1,216)= 1     ! SSUM( vector )
      NARGUM(2,216)= 1     !
      ARGTYP(1,216)=-1     ! numeric

      RETURN
      END
