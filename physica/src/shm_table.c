/* find the names of shared memory */

#include <sys/table.h>
#include <sys/shm.h>

void shm_table()
/* char *names; */
 {
  int id;      /* id of the system table                                  */
  int index;   /* index of an element within the table                    */
  char *addr;  /* address of a struct of the appropriate type to copy     */
               /* the element values to                                   */
  int nel;     /* signed number which specifies how many elements to copy */
  u_int lel;   /* expected size of a single element                       */

  id = TBL_PROCINFO;  /* shared memory region ID table */
  lel = 0;
  nel = 1000;
  table( id, index, addr, nel, lel )
  printf("nel= %d, lel= %d\n",nel,lel);
 }
/*  id = TBL_SHMDS;  shared memory region ID table */
/*  table( id, index, addr, nel, lel ); */
