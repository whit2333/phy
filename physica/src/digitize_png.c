 
 /* Include the most common X11 header files. */

#include <stdio.h>
#include <memory.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/cursorfont.h>
#include <X11/keysym.h>
#include <X11/keysymdef.h>
#include <X11/Xatom.h>

 /* default big string size */
 
#ifndef BUFSIZE
#define BUFSIZE 256
#endif

 /*
   common event mask for top-level windows.
 */
#define EVENT_MASK (long) ( ButtonPressMask | KeyPressMask | KeyReleaseMask )

#define LEFT_BUTTON  1
#define RIGHT_BUTTON 3
 
 Window t_window, png_window;
 Display *dpy;
 int png_width, png_height;
 
 void digitize_png_( int *, int *, double *, double *, int *,
                     double *, double *, double *, double *,
                     int *, int * );
 int GetGeometry( Window, int *, int *, int *, int * );
 void PrintWindowInfo( Window );
 void GetWindowInfo( Window, char * );
 void GetWindowName( Window, char * );
 void GetClassHintAsText( Window, char * );
 int EventLoop( int *, int *, int * );
 int ProcessKeyPress( XKeyEvent * );
 void CloseAndQuit( Cursor );
 int CoordinateTransform( double *, double *, double *, double *,
                          double *, double *, double *, double *, double *, double * );
 
 void digitize_png_( int *result, int *num, double xa[], double ya[], int ba[],
                     double x1[], double y1[], double x2[], double y2[],
                     int *width, int *height )
  {
    /*
      This routine allows the user to digitize points from a graph in a png window.
      
      num initially is the max number of points one can pick
      after return, it is the number of points which were picked
    */
    int screen;
    XEvent event;
    Cursor cursor;
    XWindowAttributes window_attributes;
    int status, nEvent, flag;
    
    int cntr, x, y, button;
    /*double x1[3], y1[3], x2[3], y2[3];*/
    double a, b, c, d, e, f;
    int revert_to;
    int dummy;
    
    int initN = *num;

    *width = png_width;
    *height = png_height;

    /*printf( "width = %d, height = %d\n", *width, *height );*/
    
    /* PrintWindowInfo( png_window ); */
    /*
      flush all the previous events received and wait for all events
      to be processed by the server.  True means all events in the
      input queue are discarded.
    */
    XSync( dpy, True );
    
    screen = DefaultScreen( dpy );
    cursor = XCreateFontCursor( dpy, XC_crosshair );
    XDefineCursor( dpy, png_window, cursor );

    XSelectInput( dpy, png_window, EVENT_MASK );
    XSelectInput( dpy, t_window, KeyPressMask );
    XSync( dpy, True );
    
    printf( "\nPosition the crosshair on a point on the graph\n" );
    printf( "that you know the coordinates of and press the left mouse button\n" );

    XRaiseWindow( dpy, png_window );
    rpng_x_display_image_();
    XSync( dpy, True );
    do
    {
      nEvent = EventLoop( &button, &x, &y );
      if( nEvent == 0 )
      {
        CloseAndQuit( cursor );
        *result = 0;
        return;
      }
    }
    while( nEvent == 1 );
    /*printf( "x = %d, y = %d\n", x, y );*/
    x1[0] = (double)x;
    y1[0] = (double)y;
    printf( "\nEnter the x and y graph coordinates of this point\n" );
    while( scanf( "%lf %lf", &x2[0], &y2[0] ) < 2 );

    printf( "\nPosition the crosshair on another point on the graph\n" );
    printf( "that you know the coordinates of and press the left mouse button\n" );

    XRaiseWindow( dpy, png_window );
    rpng_x_display_image_();
    XSync( dpy, True );
    do
    {
      nEvent = EventLoop( &button, &x, &y );
      if( nEvent == 0 )
      {
        CloseAndQuit( cursor );
        *result = 0;
        return;
      }
    }
    while( nEvent == 1 );
    /*printf( "x = %d, y = %d\n", x, y );*/
    x1[1] = (double)x;
    y1[1] = (double)y;
    printf( "\nEnter the x and y graph coordinates of this point\n" );
    while( scanf( "%lf %lf", &x2[1], &y2[1] ) < 2 );

    printf( "\nPosition the crosshair on a non-colinear point on the graph\n" );
    printf( "that you know the coordinates of and press the left mouse button\n" );

    XRaiseWindow( dpy, png_window );
    rpng_x_display_image_();
    XSync( dpy, True );
    do
    {
      nEvent = EventLoop( &button, &x, &y );
      if( nEvent == 0 )
      {
        CloseAndQuit( cursor );
        *result = 0;
        return;
      }
    }
    while( nEvent == 1 );
    /*printf( "x = %d, y = %d\n", x, y );*/
    x1[2] = (double)x;
    y1[2] = (double)y;
    printf( "\nEnter the x and y graph coordinates of this point\n" );
    while( scanf( "%lf %lf", &x2[2], &y2[2] ) < 2 );
    
    printf( "\nNow pick points on the graph\n" );
    printf( "Click left mouse button for pen down, right button for pen up\n" );
    printf( "Type a Q to quit\n\n" );
    /*
      set the keyboard focus to the png window
    */
    XSetInputFocus( dpy, png_window, RevertToPointerRoot, CurrentTime );
    XNextEvent( dpy, &event );

    XRaiseWindow( dpy, png_window );
    rpng_x_display_image_();
    XSync( dpy, True );
    XFlush( dpy );
    
    if( !CoordinateTransform( x1, y1, x2, y2, &a, &b, &c, &d, &e, &f ) )
    {
      CloseAndQuit( cursor );
      printf( "\nError: division by zero in coordinate transform\n" );
      *result = 1;
      return;
    }
    cntr = 0;
    flag = 1; /* True */
    while( nEvent = EventLoop( &button, &x, &y ) )
    {
      if( nEvent == 1 ) /* keypress other than q */
      {
        flag = 0; /* False */
      }
      else /* mouse button click */
      {
        if( flag )
        {
          xa[cntr] = a + b*x + c*y;
          ya[cntr] = d + e*x + f*y;
          ba[cntr] = button;
          if( button == 1 )
          {
            printf( "(x,y) = (%lf,%lf), pen down\n", xa[cntr], ya[cntr] );
          }
          else
          {
            printf( "(x,y) = (%lf,%lf), pen up\n", xa[cntr], ya[cntr] );
          }
          if( ++cntr == initN )break;
        }
        flag = 1;
      }
    }
    *num = cntr;
    CloseAndQuit( cursor );
    *result = 0;
    return;
  }

 int CoordinateTransform( double x1[], double y1[], double x2[], double y2[],
                          double *a, double *b, double *c, double *d, double *e, double *f )
  {
    double denom;
    double aa, bb, cc, dd, ee, ff;
    
    denom = x1[0]*y1[1] - x1[1]*y1[0] - x1[2]*(y1[1]-y1[0]) + y1[2]*(x1[1]-x1[0]);
    if( denom == 0.0 )return( 0 );
    
    aa = x1[2]*(y1[0]*x2[1] - y1[1]*x2[0]) -
        y1[2]*(x1[0]*x2[1] - x1[1]*x2[0]) +
        x2[2]*(x1[0]*y1[1] - x1[1]*y1[0]);
    aa /= denom;
    *a = aa;
    
    bb = -y1[0]*x2[1] + y1[1]*x2[0] +
        y1[2]*(x2[1]-x2[0]) - x2[2]*(y1[1]-y1[0]);
    bb /= denom;
    *b = bb;
    
    cc = x1[0]*x2[1] - x1[1]*x2[0] -
        x1[2]*(x2[1]-x2[0]) + x2[2]*(x1[1]-x1[0]);
    cc /= denom;
    *c = cc;
    
    dd = x1[2]*(y1[0]*y2[1]-y1[1]*y2[0]) -
        y1[2]*(x1[0]*y2[1]-x1[1]*y2[0]) +
        y2[2]*(x1[0]*y1[1]-x1[1]*y1[0]);
    dd /= denom;
    *d = dd;
    
    ee = -y1[0]*y2[1] + y1[1]*y2[0] +
        y1[2]*(y2[1]-y2[0]) - y2[2]*(y1[1]-y1[0]);
    ee /= denom;
    *e = ee;
    
    ff = x1[0]*y2[1] - x1[1]*y2[0] -
        x1[2]*(y2[1]-y2[0]) + y2[2]*(x1[1]-x1[0]);
    ff /= denom;
    *f = ff;
  }
 
 void CloseAndQuit( Cursor cursor )
  {
    XSelectInput( dpy, png_window, StructureNotifyMask );
    XSelectInput( dpy, t_window, 0 );
    XFreeCursor( dpy, cursor );
    XFlush( dpy );
  }
 
 int EventLoop( int *button, int *x, int *y )
  {
    /*
     *   This routine blocks awaiting an X event.
     *   When an event comes in, it is decoded and processed.
     *   When the user types a "q" or "Q", the routine returns 0, otherwise 1.
     *   Return a 0 to quit, a 1 to keep going
     */
    XEvent theEvent;
    KeySym theKeySym;
    XKeyEvent *theKeyEvent;
    /*
     *   block on input, awaiting an event from X
     */
    /*XAllowEvents( dpy, SyncBoth, CurrentTime );*/
    XNextEvent( dpy, &theEvent );
    /*
     *   decode the event and call a specific routine to handle it
     */
    switch( theEvent.type )
    {
     case ButtonPress:
       if( theEvent.xbutton.button == LEFT_BUTTON )
       {
         /*printf( "\nLeft mouse button (%d,%d)\n",
           theEvent.xbutton.x, theEvent.xbutton.y );*/
         *x = theEvent.xbutton.x;
         *y = theEvent.xbutton.y;
         *button = 1;
       }
       else if( theEvent.xbutton.button == RIGHT_BUTTON )
       {
         /*printf( "\nRight mouse button (%d,%d)\n",
           theEvent.xbutton.x, theEvent.xbutton.y );*/
         *x = theEvent.xbutton.x;
         *y = theEvent.xbutton.y;
         *button = -1;
       }
       break;
     case KeyPress:
       theKeyEvent = (XKeyEvent *) &theEvent;
       return( ProcessKeyPress( theKeyEvent ) );
    }
    /*
     *   0 means q key was hit, 1 means another key was hit,
     *   2 means a mouse button was hit
     */
    return 2;
  }

 int ProcessKeyPress( XKeyEvent *theEvent )
  {
    /*
     *   note that we pass a pointer to an XKeyEvent structure, even
     *   though the event was originally placed in an XEvent union
     *   structure. X overlays the various structure types into a
     *   union of structures.  By using the XKeyEvent, we can access
     *   the keyboard events more easily.
     */
    int length;
    int theKeyBufferMaxLen = 64;
    char theKeyBuffer[65];
    KeySym theKeySym;
    XComposeStatus theComposeStatus;

    length = XLookupString( theEvent, theKeyBuffer, theKeyBufferMaxLen,
                            &theKeySym, &theComposeStatus );
    if( ( theKeySym >= ' ' ) && ( theKeySym <= '~' ) && ( length > 0 ) )
    {
      /*printf( "\nASCII key was hit: [%c]\n", theKeyBuffer[0] );*/
      if( ( theKeyBuffer[0] == 'q' ) || ( theKeyBuffer[0] == 'Q' ) )
        return 0;
    }
    return 1;
  }
 
 void PrintWindowInfo( Window window )
  {
    /* this routine prints out information on a window to stdout */

    char string[BUFSIZE+1];
    GetWindowInfo( window, string );
    printf( "following is information on the window you have chosen\n" );
    printf( "%s\n", string );
  }

 void GetWindowInfo( Window window, char string[] )
  {
    /*   this routine builds up a character string with information
     *   about a given window:
     *       name
     *       class
     *       local geometry
     */

    int x, y, width, height;
    char name[BUFSIZE+1];
    int status;
    char st[BUFSIZE+1];

    /* get info on the window */

    GetWindowName( window, name );
    if( name[0] != '\0' )
    {
      sprintf( string, "(%s)[0x%lx] ", name, window );
    }
    else
    {
      sprintf( string, "[0x%lx] ", window );
    }
    GetClassHintAsText( window, st );
    strcat( string, st );
    status = GetGeometry( window, &x, &y, &width, &height );
    if( status != 0 )
    {
      sprintf( st, "at local %d, %d; %d by %d", x, y, width, height );
      strcat( string, st );
    }
  }

 int GetGeometry( Window window, int *x, int *y, int *width, int *height )
  {
    /*   this routine call XGetGeometry() and returns the x, y, width and height
     *   for the window.
     */

    unsigned int borderWidth, depth;
    Window rootWindow;
    int status;

    status = XGetGeometry( dpy, window, &rootWindow, x, y, width, height,
                           &borderWidth, &depth );
    return( status );
  }

 void GetWindowName( Window window, char name[] )
  {
    /*   this routine returns the window's name.  It uses the old
     *   Release 3 routine, XFetchName() rather than XGetWMName()
     *   for compatibility reasons.
     */

    char *windowName;
    XFetchName( dpy, window, &windowName );
    if( windowName != (char *) NULL )
    {
      strcpy( name, windowName );
      XFree( windowName );
    }
    else
    {
      name[0] = '\0';
    }
  }

 void GetClassHintAsText( Window window, char string[] )
  {
    /*   This routine gets the window's class hint, if one is available,
     *   and places it into the given string, as
     *   "CLASS: class name / class type"
     *
     *   This routine depends on the symbol X11R4
     */

    XClassHint *classHint;
    int status;

    /* initialize string */

    string[0] = '\0';
    classHint = XAllocClassHint();
    if( classHint != (XClassHint *) NULL )
    {
      status = XGetClassHint( dpy, window, classHint );
      if( status != 0 )
      {
        sprintf( string, "CLASS %s/%s ", classHint->res_name,
                        classHint->res_class );
        XFree( classHint->res_name );
        XFree( classHint->res_class );
      }

      /* free memory for whole hint structure */

      XFree( classHint );
    }
  }
 
 /* end of file */
 
