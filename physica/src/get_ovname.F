      SUBROUTINE GET_OUTPUT_VARIABLE_NAME( NAME_IN, IER )
C
C   Input:  NAME_IN  is the input name 
C   Output: IER      error number ( =0 if no error )
C                    GET_VARIABLE_ERROR  writes the error message
C
      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

      CHARACTER*(*) NAME_IN
      INTEGER*4     IER

      CHARACTER*(LENVSM)  NAMEG
      CHARACTER*256       NAMEV, NAMET
      COMMON /GETVARIABLEERROR/ NAMEG, LNG, NAMEV, LNV, NAMET, LNT

C  local variables

      CHARACTER*1 EON
      INTEGER*2  ISTATE, NSTATE, CLASSES(0:127)
      DATA CLASSES
C        0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
     &/  6,  6,  6,  6,  6,  6,  6,  6,  6,  1,  6,  6,  6,  6,  6,  6,
     &   6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  7,  6,  6,  6,  6,
     &   1,  6,  6,  6,  3,  6,  6,  6,  4,  5,  6,  6,  6,  6,  6,  6,
     &   3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  6,  6,  6,  6,  6,  6,
     &   6,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,
     &   2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  4,  6,  5,  6,  3,
     &   6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,
     &   6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  4,  6,  5,  6,  6 /

      INTEGER*2  STABLE(7,3)
      DATA STABLE
C         1    2      3     4     5       6      7
C       blnk  A-Z  $_0-9   {([   ])}   other    EON
     1 /  1,   2,   -10,  -20,  -20,      2,   -20
     2 ,100,   3,     3,  100,  -30,      3,   100 
     3 ,100,   3,     3,  100,  -30,      3,   100 /
CC     1 /  1,   2,   -10,  -20,  -20,    -10,   -20
CC     2 ,100,   3,     3,  100,  -30,    -30,   100 
CC     3 ,100,   3,     3,  100,  -30,    -30,   100 /
CCC
      EON = CHAR(27)

      IER   = 0
      NAMEG = ' '
      NAMEV = ' '
      LNV   = LEN(NAME_IN)
      CALL UPRCASE(NAME_IN(1:LNV),NAMEV(1:LNV))
      NAMEV(LNV+1:LNV+1) = EON

      NSTATE = 1
      I      = 0      ! character count
  10  I      = I + 1
      ISTATE = NSTATE
      NSTATE = STABLE(CLASSES(ICHAR(NAMEV(I:I))),ISTATE)
      IF( NSTATE .EQ. 2 )THEN          ! start variable name
        ISTART = I
      ELSE IF( NSTATE .EQ. 100 )THEN   ! variable with no indices
        LNG = I - ISTART
        IF( LNG .GT. LENVSM )THEN
          IER = 33
          RETURN
        END IF
        NAMEG(1:LNG) = NAMEV(ISTART:I-1)
        CALL CHECK_RES_NAME(NAMEG,IER)
        RETURN
      ELSE IF( NSTATE .EQ. -10 )THEN   ! invalid first character
        IER = 34
        RETURN
      ELSE IF( NSTATE .EQ. -20 )THEN   ! no name
        IER = 14
        RETURN
      ELSE IF( NSTATE .EQ. -30 )THEN   ! invalid character in name
        IER = 35
        RETURN
      END IF
      GO TO 10
      END
