      SUBROUTINE FZERO( * )

      PARAMETER (MXCODE=2200)

      INTEGER*4 NTOTP, NTOTQ, MXCHAR, MXEXPR
      PARAMETER (NTOTP=50, NTOTQ=10, MXCHAR=1024, MXEXPR=3000)

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      INTEGER*4 VARALC, VARADR, VARLEN, VARNUM, VARMAX
     &         ,TSCAL, NSCAL, ASCAL, ESCAL

C   VARALC = number of bytes allocated for numeric variable
C            characteristics (does not include the data)
C            VARALC should be a multiple of 512 
C   VARADR = starting address of the numeric variable table
C   VARLEN = # of bytes needed for an entry in the table
C   VARNUM = # of numeric variables currently defined
C   VARMAX = # of variables (including scalars) for which space is allocated 
C
C   TSCAL  = # of scalars for which space is allocated 
C   NSCAL  = # of current scalars
C   ASCAL  = starting address for scalar data
C   ESCAL  = starting address of the scalars existence list
C
C   Allocate the space as bytes. Let BASE = (VARADR+(I-1)*VARLEN)/4, for
C   1 <= I <= VARNUM.  The last byte used for I'th variable at 4*BASE+VARLEN
C   and the start of the next variable is at 4*BASE+VARLEN+1
C
C   I4$(BASE+1)   = length of the I'th variable name
C                   always allocate 32 (4*8) bytes for every name
C                   name is in L1$((BASE+2)*4+j) for j = 1, length
C   I4$(BASE+10)  = -1 if scalar is a dummy index
C                    0 if normal scalar
C                   +1 if scalar is allowed to vary in fit
C   I4$(BASE+11)  = number of dimensions: 0=scalar, 1=vector, 2=matrix, 3=tensor
C   I4$(BASE+12)  = starting address of data
C   I4$(BASE+13)  = number of elements if a vector
C                   or number of rows if a matrix (1st dimension)
C   I4$(BASE+14)  = number of columns (2nd dimension)
C   I4$(BASE+15)  = number of planes (3rd dimension)
C   I4$(BASE+16)  = initial index of 1st dimension
C   I4$(BASE+17)  = initial index of 2nd dimension
C   I4$(BASE+18)  = initial index of 3rd dimension
C   I4$(BASE+19)  = final index of 1st dimension
C   I4$(BASE+20)  = final index of 2nd dimension
C   I4$(BASE+21)  = final index of 3rd dimension
C   I4$(BASE+22)  = number of lines of history for variable I
C   I4$(BASE+23)  = total allocation for history for variable I
C   I4$(BASE+24)  = address for history line lengths
C   I4$(BASE+25)  = address for history line characters
C    for history line 1:
C       length = I4$(I4$(BASE+24)+1) 
C       line = L1$(I4$(BASE+25)+j) for j = 1 --> I4$(I4$(BASE+24)+1) 
C    for history line k > 1:
C       length = I4$(I4$(BASE+24)+k) 
C         prev_len = sum(I4$(I4$(BASE+24)+j),j,1:k-1)
C       line = L1$(I4$(BASE+25)+prev_len+j) for j = 1 --> I4$(I4$(BASE+24)+k)

      COMMON /NUMERIC_VARIABLES/ VARALC, VARADR, VARLEN, VARNUM, VARMAX
     &                          ,TSCAL, NSCAL, ASCAL, ESCAL

C  FZERO X f(X)
C  X must be a vector.  Suppose the length of X is N.
C  FZERO returns up to N zeroes of f(X), a function of the dummy vector X.
C  The zeroes of the function will be put into an updated X vector.

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

      INTEGER*4 NVUSED, IVUSED(MXCODE)
      COMMON /VARS_USED/ NVUSED, IVUSED

C  EXVALX error message variables

      CHARACTER*(MXEXPR) EXPBUF, ERRBUF
      CHARACTER*80       ERRMES
      INTEGER*4          LEXPBUF, ERRPTR
      COMMON /EXPERR/ ERRPTR, LEXPBUF, ERRMES, EXPBUF, ERRBUF

      LOGICAL*4 CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED

C  local variables

      CHARACTER*1 BS
      INTEGER*4   EXPTYP, BASE, IDX1(4), IDX2(4), IDX3(4)
      LOGICAL*4   MESSAGES, OUTPUT
CCC
      BS = CHAR(92) ! backslash

      STRINGS(1) = 'FZERO'
      LENST(1) = 5
      MESSAGES = .TRUE.
      DO I = 1, NQUAL(1)
        IF( QUALIFIERS(1,I)(1:1) .EQ. '-' )THEN
          II = 2
        ELSE
          II = 1
        END IF
        L = LENQL(1,I)
        IF( INDEX('MESSAGES',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            MESSAGES = .TRUE.
          ELSE
            MESSAGES = .FALSE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('NOMESSAGES',QUALIFIERS(1,I)(II:L)).EQ.1)THEN
          IF( II .EQ. 1 )THEN
            MESSAGES = .FALSE.
          ELSE
            MESSAGES = .TRUE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE
          CALL WARNING_MESSAGE('FZERO'
     &     ,'unknown qualifier: '//QUALIFIERS(1,I)(1:L))
        END IF
      END DO
      OUTPUT = MESSAGES .AND. ((NUNIT.EQ.1) .OR. ECHO )

      CALL GET_VECTOR( STRINGS(2), LENST(2), ITYPE(2), 'FZERO'
     & ,'dummy vector', INITX2, NR, *92, *92 )
      STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(2)(1:LENST(2))
      LENST(1) = LENST(1)+1+LENST(2)

      CALL GET_TEMP_SPACE( INITX, NR, 8, *300 )
      DO I = 1, NR                    ! save initial values
        R8D(INITX+I) = R8D(INITX2+I)
      END DO

      CALL GET_VARIABLE_INDEX( STRINGS(2)(1:LENST(2)),INDX,ND,*93,*92 )
      CALL DELETE_VARIABLE( INDX )
      CALL PUT_VARIABLE( STRINGS(2)(1:LENST(2)), 0, IDX1,IDX2,IDX3
     &  ,0, 0.0D0, 0, 0,0,0, STRINGS(1)(1:LENST(1)), 'FZERO', *92)
      CALL GET_VARIABLE_INDEX( STRINGS(2)(1:LENST(2)),INDX,ND,*93,*92 )

      IF( ITYPE(3) .EQ. 0 )THEN
        CALL ERROR_MESSAGE('FZERO','expecting an expression')
        CALL DELETE_VARIABLE( INDX )
        GO TO 92
      ELSE IF( ITYPE(3) .EQ. 1 )THEN
        CALL ERROR_MESSAGE('FZERO','expecting an expression')
        CALL DELETE_VARIABLE( INDX )
        GO TO 92
      ELSE IF( ITYPE(3) .EQ. 3 )THEN
        CALL ERROR_MESSAGE('FZERO'
     &   ,'equal sign encountered; expecting an expression')
        CALL DELETE_VARIABLE( INDX )
        GO TO 92
      END IF
      STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(3)(1:LENST(3))
      LENST(1) = LENST(1)+1+LENST(3)

C  decode the expression

      IF( TRAP )CALL LOK_E_TRAP( *290, ISTAT )
      EXPTYP = -1
      CALL EXVALX( STRINGS(3)(1:LENST(3)), EXPTYP, *190 )
      IF( NVUSED .EQ. 0 )THEN
        CALL ERROR_MESSAGE('FZERO','no variables in the expression')
        CALL DELETE_VARIABLE( INDX )
        GO TO 92
      END IF

C  Check that the dummy variable occurs in the expression

      DO I = 1, NVUSED
        IF( INDX .EQ. IVUSED(I) )GO TO 12
      END DO
      CALL ERROR_MESSAGE('FZERO'
     & ,'dummy variable not found in the expression')
      CALL DELETE_VARIABLE( INDX )
      GO TO 92

C Check that no other independent variables occur in expression

   12 DO I = 1, NVUSED
        BASE = (VARADR+(IVUSED(I)-1)*VARLEN)/4
        IF( I4D(BASE+11).NE.0 )THEN
          CALL ERROR_MESSAGE('FZERO'
     &     ,'scalar variables only are allowed in the expression')
          CALL DELETE_VARIABLE( INDX )
          GO TO 92
        END IF
      END DO

      ISTR = 1
      CALL EXRPN( ISTR, *190 )

C FROM UBC NLE (Zeros of Nonlinear Equations)
C Finds one or more real zeros (roots) of a real non-linear
C function of one variable (may allow more in future)

      CALL DRZFUN( NR, R8D(INITX+1), INDX, OUTPUT, *92 )
      CALL DELETE_VARIABLE( INDX )

      IF( CTRLC_CALLED )GO TO 92

      IF( NR .GT. 0 )
     & CALL PUT_VARIABLE( STRINGS(2)(1:LENST(2)), 0, IDX1,IDX2,IDX3
     &  ,1, 0.0D0, INITX, NR,0,0, STRINGS(1)(1:LENST(1)), 'FZERO', *92)

      CALL DELETE_TEMP_SPACE
      IF( TRAP )CALL LOK_E_UNTRAP
      RETURN
   92 CALL DELETE_TEMP_SPACE
      IF( TRAP )CALL LOK_E_UNTRAP
      RETURN 1
   93 CALL ERROR_MESSAGE('FZERO'
     & ,'indices not allowed on independent variable')
      CALL DELETE_TEMP_SPACE
      IF( TRAP )CALL LOK_E_UNTRAP
      RETURN 1

C Error messages
C Expression evaluator errors

  190 CALL ERROR_MESSAGE('FZERO',ERRMES(1:LENSIG(ERRMES)))
      CALL RITE(STRINGS(1)(1:LENST(1)))
      IF( ERRPTR .GT. 0 )THEN
        CALL RITE(EXPBUF(1:LEXPBUF))
        CALL RITE(ERRBUF(1:ERRPTR))
      END IF
      CALL DELETE_VARIABLE( INDX )
      CALL DELETE_TEMP_SPACE
      IF( TRAP )CALL LOK_E_UNTRAP
      RETURN 1

C   Arithmetic exception during evaluation

  290 CALL ERROR_MESSAGE('FZERO','arithmetic exception')
      CALL PUT_SYSMSG(ISTAT)
      CALL RITE(STRINGS(1)(1:LENST(1)))
      CALL DELETE_VARIABLE( INDX )
      CALL DELETE_TEMP_SPACE
      IF( TRAP )CALL LOK_E_UNTRAP
      RETURN 1
  300 CALL ERROR_MESSAGE('FZERO'
     & ,'allocating temporary dynamic array space')
      CALL DELETE_VARIABLE( INDX )
      CALL DELETE_TEMP_SPACE
      IF( TRAP )CALL LOK_E_UNTRAP
      RETURN 1
      END
