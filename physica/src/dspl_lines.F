      SUBROUTINE DISPLAY_LINES
      CHARACTER*2 STRING

      XL = GETNAM('XLWIND')
      XU = GETNAM('XUWIND')
      YL = GETNAM('YLWIND')
      YU = GETNAM('YUWIND')
      DX = XU - XL
      DY = YU - YL

      TX = GETNAM('TXTHIT')
      XC = GETNAM('XLOC')
      YC = GETNAM('YLOC')
      CU = GETNAM('CURSOR')
      TX = TX/DY*100.
      XC = (XC-XL)/DX*100.
      YC = (YC-YL)/DY*100.

      X1 = XL+.1*DX
      X2 = XL+.9*DX
      Y1 = YL+.1*DY
      Y2 = YL+.9*DY

      XTXT = X1+0.1*(X2-X1)
      XL1 = XTXT+0.05*(X2-X1)
      XL2 = X2-0.1*(X2-X1)
      TXTHIT = (Y2-Y1)/15.5
      DY2 = (Y2-Y1)/10.
      CALL SETNAM('CURSOR',-3.)
      CALL SETNAM('TXTHIT',TXTHIT)
      CALL SETNAM('XLOC',XTXT)
      YTXT = Y2
      DO I = 1, 10
        YTXT = YTXT-DY2
        CALL SETNAM('YLOC',YTXT)
        J = IFIX(LOG10(FLOAT(I)))+1
cc   10   format(i<j>)
        WRITE(STRING,10)I
   10   FORMAT(I2)
        CALL SETLAB('TEXT',STRING(1:J))
        YY = YTXT+0.5*TXTHIT
        CALL PLOT_R(XL1,YY,3)
        CALL DLINE(XL2,YY,I,1)
      END DO
      CALL FLUSH_PLOT
      CALL SETNAM('%TXTHIT',TX)
      CALL SETNAM('%XLOC',XC)
      CALL SETNAM('%YLOC',YC)
      CALL SETNAM('CURSOR',CU)
      RETURN
      END
