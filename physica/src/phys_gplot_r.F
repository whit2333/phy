      SUBROUTINE PHYSICA_GPLOT_R(XI,YI,IPEN
     &                          ,NXYPOL,IXPOL,IYPOL,IFD,FLAG,*)
C
      REAL*4    XI, YI, XDRAW(2), YDRAW(2)
      INTEGER*4 IPEN, NXYPOL, IXPOL, IYPOL, IFD
      LOGICAL*4 FLAG
C
      REAL*4    R4D(1)
      REAL*8    R8D(1)
      INTEGER*4 I4D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R4D, R8D)
C
      LOGICAL*4 FIRST
      COMMON /GPLOT_R_FIRST/ FIRST

      LOGICAL*4 CALLED_FROM_GPLOT
      COMMON /GPLOT_CALLED/  CALLED_FROM_GPLOT

      REAL*4 XPREV, YPREV
      INTEGER*4 IPENP
      COMMON /PLOT_PREVIOUS/ XPREV, YPREV, IPENP

C  Modified by J.L.Chuma, 09-Apr-1997 to elimate SIND, COSD for g77
      REAL*4 DTOR /0.017453292519943/

      XPL(X,Y) = XLAXIS + COSX*(AX * X + BX) + COSY*(AY * Y + BY)
      YPL(X,Y) = YLAXIS + SINX*(AX * X + BX) + SINY*(AY * Y + BY)
CCC
      IF( FIRST )THEN
        XMIN   = GETNAM('XMIN')
        XMAX   = GETNAM('XMAX')
        YMIN   = GETNAM('YMIN')
        YMAX   = GETNAM('YMAX')
        XLOG   = ABS(GETNAM('XLOG'))
        YLOG   = ABS(GETNAM('YLOG'))
        IF( XLOG .GT. 1. )ALOGX = ALOG(XLOG)
        IF( YLOG .GT. 1. )ALOGY = ALOG(YLOG)
        XAXISA = GETNAM('XAXISA')
        YAXISA = GETNAM('YAXISA')
        XLAXIS = GETNAM('XLAXIS')
        YLAXIS = GETNAM('YLAXIS')
        XUAXIS = GETNAM('XUAXIS')
        YUAXIS = GETNAM('YUAXIS')
        COSX   = COS(XAXISA*DTOR)
        SINX   = SIN(XAXISA*DTOR)
        COSY   = COS(YAXISA*DTOR)
        SINY   = SIN(YAXISA*DTOR)
        IF( ABS(COSX) .LE. 1.E-7 )COSX = 0.0
        IF( ABS(SINX) .LE. 1.E-7 )SINX = 0.0
        IF( ABS(COSY) .LE. 1.E-7 )COSY = 0.0
        IF( ABS(SINY) .LE. 1.E-7 )SINY = 0.0
        AX     = (XUAXIS - XLAXIS) / (XMAX - XMIN)
        BX     = -AX * XMIN
        AY     = (YUAXIS - YLAXIS) / (YMAX - YMIN)
        BY     = -AY * YMIN
        LINTYP = GETNAM('LINTYP')
        ICLIP  = IFIX(GETNAM('CLIP'))
      END IF
C
      IF( .NOT.CALLED_FROM_GPLOT )LINTYP = GETNAM('LINTYP')
      X = XI
      Y = YI
      IF( XLOG .GT. 1. )THEN 
        IF( X .LE. 0. )X = 1.E-36
        X = ALOG(X) / ALOGX
      END IF
      IF( YLOG .GT. 1. )THEN 
        IF( Y .LE. 0. )Y = 1.E-36
        Y = ALOG(Y) / ALOGY
      END IF
      IF( FIRST )THEN
        XP = X
        YP = Y
        FIRST = .FALSE.
      END IF
      IF( ABS(IPEN) .EQ. 3 )THEN
        IF( (((X - XMIN)*(XMAX - X) .GE. 0.) .AND.
     &       ((Y - YMIN)*(YMAX - Y) .GE. 0.)) .OR. (ICLIP.LE.0) )THEN
          XX = XPL(X,Y)
          YY = YPL(X,Y)
          CALL PLOT_R(XX,YY,3)
          IF( FLAG )THEN
            IFD = IFD+1
            IF( IFD .GT. NXYPOL )THEN
              CALL GET_MORE_SPACE(IXPOL,NXYPOL,NXYPOL+10,4,*99)
              CALL GET_MORE_SPACE(IYPOL,NXYPOL,NXYPOL+10,4,*99)
              NXYPOL = NXYPOL+10
            END IF
            R4D(IXPOL+IFD) = XX
            R4D(IYPOL+IFD) = YY
          END IF
        END IF
        XP = X
        YP = Y
      ELSE IF( ABS(IPEN) .EQ. 2 )THEN
        IF( IPEN .EQ. 2 )THEN
          IEND = 0
        ELSE
          IEND = 1
        END IF
        IF( ICLIP .GT. 0 )THEN
          CALL WINDOW_CLIP(XP,YP,X,Y,XMIN,XMAX,YMIN,YMAX
     &     ,XDRAW,YDRAW,NDRAW)
          IF( NDRAW .EQ. 1 )THEN
            XX = XPL(XDRAW(1),YDRAW(1))
            YY = YPL(XDRAW(1),YDRAW(1))
            CALL DLINE(XX,YY,LINTYP,IEND)
            IF( FLAG )THEN
              IFD = IFD+1
              IF( IFD .GT. NXYPOL )THEN
                CALL GET_MORE_SPACE(IXPOL,NXYPOL,NXYPOL+10,4,*99)
                CALL GET_MORE_SPACE(IYPOL,NXYPOL,NXYPOL+10,4,*99)
                NXYPOL = NXYPOL+10
              END IF
              R4D(IXPOL+IFD) = XX
              R4D(IYPOL+IFD) = YY
            END IF
          ELSE IF( NDRAW .EQ. 2 )THEN
            XX = XPL(XDRAW(1),YDRAW(1))
            YY = YPL(XDRAW(1),YDRAW(1))
            CALL PLOT_R(XX,YY,3)
            IF( FLAG )THEN
              IFD = IFD+1
              IF( IFD .GT. NXYPOL )THEN
                CALL GET_MORE_SPACE(IXPOL,NXYPOL,NXYPOL+10,4,*99)
                CALL GET_MORE_SPACE(IYPOL,NXYPOL,NXYPOL+10,4,*99)
                NXYPOL = NXYPOL+10
              END IF
              R4D(IXPOL+IFD) = XX
              R4D(IYPOL+IFD) = YY
            END IF
            XX = XPL(XDRAW(2),YDRAW(2))
            YY = YPL(XDRAW(2),YDRAW(2))
            CALL DLINE(XX,YY,LINTYP,IEND)
            IF( FLAG )THEN
              IFD = IFD+1
              IF( IFD .GT. NXYPOL )THEN
                CALL GET_MORE_SPACE(IXPOL,NXYPOL,NXYPOL+10,4,*99)
                CALL GET_MORE_SPACE(IYPOL,NXYPOL,NXYPOL+10,4,*99)
                NXYPOL = NXYPOL+10
              END IF
              R4D(IXPOL+IFD) = XX
              R4D(IYPOL+IFD) = YY
            END IF
          END IF
        ELSE
          XX = XPL(X,Y)
          YY = YPL(X,Y)
          CALL DLINE(XX,YY,LINTYP,IEND)
          IF( FLAG )THEN
            IFD = IFD+1
            IF( IFD .GT. NXYPOL )THEN
              CALL GET_MORE_SPACE(IXPOL,NXYPOL,NXYPOL+10,4,*99)
              CALL GET_MORE_SPACE(IYPOL,NXYPOL,NXYPOL+10,4,*99)
              NXYPOL = NXYPOL+10
            END IF
            R4D(IXPOL+IFD) = XX
            R4D(IYPOL+IFD) = YY
          END IF
        END IF
        XP = X
        YP = Y
      ELSE IF( IPEN .EQ. -1 )THEN
        XI = XPL(X,Y)
        YI = YPL(X,Y)
      END IF
      IF( (((X - XMIN)*(XMAX - X) .LT. 0.) .OR.
     &     ((Y - YMIN)*(YMAX - Y) .LT. 0.)) .AND. (ICLIP.GT.0) )RETURN 1
      RETURN
   99 CALL ERROR_MESSAGE('PHYSICA_GPLOT_R routine'
     & ,'allocating dynamic array space')
      RETURN
      END
