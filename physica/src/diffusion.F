      SUBROUTINE DIFFUSION(A,M,N,XMN,XMX,YMN,YMX,FMINI,FMAXI
     &        ,DERIV,PARTIAL
     &        ,PROFILE,POLAR,IXCOL,NX,IYROW,NY)
C
C           FMINI ---> FMAXI specify the range of print densities
C           desired on a scale from 0 --> 1 
C
C           When logical DERIV=.TRUE. a density plot of the derivative
C           of the array A is produced ( (x'**2+y'**2)**.5)
C
      CHARACTER*132 XLABEL, YLABEL, GETLAB
      REAL*8        A(N,M), XMN, XMX, YMN, YMX, FMINI, FMAXI
     &             ,ALOW, AHI, AHIMALOW, DIJ, FIJ, FI, FMIN, FMAX
     &             ,FJ, EI, EJ, ERR, DOUT, RNLO, RNHI, RMLO, RMHI
     &             ,RADMIN, RADMAX, THETAMIN, THETAMAX, AMIN, AMAX, AVAL
     &             ,AX, AY, ARG, XRMIN, XRMAX, XCMIN, XCMAX, AMEAN
     &             ,XLOC, YLOC, RAD, THETA
      INTEGER*4     PROFILE
      LOGICAL*4     DERIV, PARTIAL, POLAR, WELL_SAVE

      INTEGER*2 ILARR1
      LOGICAL*1 LARR1
      EQUIVALENCE (LARR1,ILARR1)

      REAL*8    R8D(1)
      INTEGER*4 I4D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, L1D)

      INTEGER*4 IOUTS
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS

      INTEGER*4 IMONITOR, IOUTM
      COMMON /PLOTMONITOR/ IMONITOR, IOUTM

      INTEGER*4 IMONITOR2, IOUTM2
      COMMON /PLOTMONITOR2/ IMONITOR2, IOUTM2

      REAL*4 XMINMP, XMAXMP, YMINMP, YMAXMP
     &      ,XMINM,  XMAXM,  YMINM,  YMAXM
      COMMON /MONITORRANGE/ XMINMP, XMAXMP, YMINMP, YMAXMP
     &                     ,XMINM,  XMAXM,  YMINM,  YMAXM

      INTEGER*4 IORIENTH
      REAL*4 XMINHP, XMAXHP, YMINHP, YMAXHP
      COMMON /HARDCOPYRANGE/ XMINHP, XMAXHP, YMINHP, YMAXHP, IORIENTH

      REAL*4 XMINH2, XMAXH2, YMINH2, YMAXH2
      COMMON /HARDCOPYRANGE2/ XMINH2, XMAXH2, YMINH2, YMAXH2

      LOGICAL*4 WELL
      COMMON /TO_BIT_OR_NOT/ WELL

      LOGICAL*4 CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED
CCC                                     
      WELL_SAVE = WELL
      IMONITOR_SAVE = IMONITOR
      IMONITOR2_SAVE = IMONITOR2
      NSAVE = 0

      FMIN = MAX(0.0D0,FMINI)
      FMAX = FMAXI

C   Convert GPLOT axis units to world units

      XMIN = GETNAM('XMIN')
      XMAX = GETNAM('XMAX')
      YMIN = GETNAM('YMIN')
      YMAX = GETNAM('YMAX')
      XLOG = ABS(GETNAM('XLOG'))
      YLOG = ABS(GETNAM('YLOG'))
      IF( XLOG .GT. 1. )THEN
        XMIN = XLOG ** XMIN
        XMAX = XLOG ** XMAX
      END IF
      IF( YLOG .GT. 1. )THEN
        YMIN = YLOG ** YMIN
        YMAX = YLOG ** YMAX
      END IF
      XMN2 = MAX(SNGL(XMN),XMIN)
      XMX2 = MIN(SNGL(XMX),XMAX)
      YMN2 = MAX(SNGL(YMN),YMIN)
      YMX2 = MIN(SNGL(YMX),YMAX)
      CALL GPLOT_CONVERT(XMN2,YMN2,XMNW,YMNW,1)
      CALL GPLOT_CONVERT(XMX2,YMX2,XMXW,YMXW,1)

C   Determine the pixel increments and numbers

      CALL PIXEL_EXTENT(XMNW,XMXW,YMNW,YMXW
     & ,XINC, YINC, NXMIN, NXMAX, NYMIN, NYMAX
     & ,XINCP,YINCP,NXMINP,NXMAXP,NYMINP,NYMAXP
     & ,XINCB,YINCB,NXMINB,NXMAXB,NYMINB,NYMAXB)

C   NCOLS is the number of pixels horizontally
C   NROWS is the number of pixels vertically

      NCOLS = NXMAX - NXMIN + 1
      NROWS = NYMAX - NYMIN + 1

C   NCOLSB is the number of bitmap units horizontally
C   NROWSB is the number of bitmap units vertically

      NCOLSB = NXMAXB - NXMINB + 1
      NROWSB = NYMAXB - NYMINB + 1

C   NCOLSP is the number of imonitor2 units horizontally
C   NROWSP is the number of imonitor2 units vertically

      NCOLSP = NXMAXP - NXMINP + 1
      NROWSP = NYMAXP - NYMINP + 1

C   Get the window parameters 

      XUWIND  = GETNAM('XUWIND')
      XLWIND  = GETNAM('XLWIND')
      YUWIND  = GETNAM('YUWIND')
      YLWIND  = GETNAM('YLWIND')

C   Get the axis locations

      YLAXIS = GETNAM('YLAXIS')
      XLAXIS = GETNAM('XLAXIS')
      YUAXIS = GETNAM('YUAXIS')
      XUAXIS = GETNAM('XUAXIS')

C   Use the following for zooming in

      IF( POLAR )THEN   ! must be a matrix (not scattered points)
        MLO  = 1
        MHI  = M
        NLO  = 1
        NHI  = N
        RMLO = 1.0D0
        RMHI = M
        RNLO = 1.0D0
        RNHI = N
        RADMAX = R8D(IXCOL+MHI)
        RADMIN = R8D(IXCOL+MLO)
        THETAMAX = R8D(IYROW+NHI)
        THETAMIN = R8D(IYROW+NLO)
      ELSE
        MLO  = MAX(0,  INT((XMIN-XMN)/(XMX-XMN)*(M-1))) + 1
        MHI  = MIN(M-1,INT((XMAX-XMN)/(XMX-XMN)*(M-1))) + 1
        NLO  = MAX(0,  INT((YMIN-YMN)/(YMX-YMN)*(N-1))) + 1
        NHI  = MIN(N-1,INT((YMAX-YMN)/(YMX-YMN)*(N-1))) + 1
        RMLO = MAX(0.0D0,  (XMIN-XMN)/(XMX-XMN)*(M-1)) + 1
        RMHI = MIN(M-1.0D0,(XMAX-XMN)/(XMX-XMN)*(M-1)) + 1
        RNLO = MAX(0.0D0,  (YMIN-YMN)/(YMX-YMN)*(N-1)) + 1
        RNHI = MIN(N-1.0D0,(YMAX-YMN)/(YMX-YMN)*(N-1)) + 1
      END IF

C   Determine min and max values in array A. The minimum value
C   will correspond to zero (blank) print density, the maximum
C   to solid black print

      AMIN =  1.D30
      AMAX = -1.D30
      IF( PARTIAL )THEN
        J1 = NLO
        J2 = NHI
        I1 = MLO
        I2 = MHI
      ELSE
        J1 = 1
        J2 = N
        I1 = 1
        I2 = M
      END IF
      IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 2) )THEN
        CALL GET_TEMP_SPACE(IXR,N,8,*99)
        CALL ZERO_ARRAY(R8D(IXR+1),8*N)
      END IF
      IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 1) )THEN
        CALL GET_TEMP_SPACE(IXC,M,8,*99)
        CALL ZERO_ARRAY(R8D(IXC+1),8*M)
      END IF
      IF( .NOT.DERIV )THEN
        DO I = I1, I2
          DO J = J1, J2
            AVAL = A(J,I)
            IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 2) )THEN
              R8D(IXR+J) = R8D(IXR+J) + AVAL
            END IF
            IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 1) )THEN
              R8D(IXC+I) = R8D(IXC+I) + AVAL
            END IF
            IF( (AVAL .LT. AMIN) .AND. (AVAL .GT. -1.D30) )AMIN = AVAL
            IF( AVAL .GT. AMAX )AMAX = AVAL
          END DO
        END DO
      ELSE         ! DERIV = .TRUE. so use derivative contrast scale
        NM1 = J2-1
        MM1 = I2-1
        NUM = M * N
        CALL GET_TEMP_SPACE(INITAXY,NUM,8,*99)
        DO I = I1, MM1
          DO J = J1, NM1
            AX  = A(J,I+1)-A(J,I)
            AY  = A(J+1,I)-A(J,I)
            ARG = SQRT(AX**2+AY**2)
            R8D(INITAXY+J+(I-1)*N) = ARG
            IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 2) )
     &       R8D(IXR+J) = R8D(IXR+J) + ARG
            IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 1) )
     &       R8D(IXC+I) = R8D(IXC+I) + ARG
            IF( ARG .LT. AMIN )AMIN = ARG
            IF( ARG .GT. AMAX )AMAX = ARG
            IF( J .EQ. NM1 )R8D(INITAXY+J+I*N) = ARG
            IF( I .EQ. MM1 )R8D(INITAXY+(J+1)+(I-1)*N) = ARG
          END DO
        END DO
        R8D(INITAXY+J2+(I2-1)*N) = R8D(INITAXY+(J2-1)+(I2-2)*N)
      END IF
      ALOW  = AMIN+FMIN*(AMAX-AMIN)
      AHI   = AMIN+FMAX*(AMAX-AMIN)
      AHIMALOW = AHI - ALOW
      IF( AHIMALOW .EQ. 0.D0 )THEN
        CALL ERROR_MESSAGE('DENSITY','Region is flat')
        GO TO 90
      END IF
      NLEVELS = 20
      IF( PROFILE .GT. 0 )THEN
        XLABEL = GETLAB('XLABEL')
        YLABEL = GETLAB('YLABEL')
        CALL SETLAB('XLABEL',' ')
        CALL SETLAB('YLABEL',' ')
        XMIN   = GETNAM('XMIN')
        XMAX   = GETNAM('XMAX')
        YMIN   = GETNAM('YMIN')
        YMAX   = GETNAM('YMAX')
        HISTYP = GETNAM('HISTYP')
        YAXIS  = GETNAM('YAXIS')
        XAXIS  = GETNAM('XAXIS')
        XLINC  = GETNAM('NLXINC')
        YLINC  = GETNAM('NLYINC')
        XSINC  = GETNAM('NSXINC')
        YSINC  = GETNAM('NSYINC')
        BOX    = GETNAM('BOX')
        XMASK  = GETNAM('MASK')
        XNUMSZ = GETNAM('XNUMSZ')
        YNUMSZ = GETNAM('YNUMSZ')
        IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 2) )THEN
          CALL SETNAM('HISTYP',3.)
          CALL SETNAM('YAXIS',0.)
          CALL SETNAM('XMIN',0.)
          CALL SETNAM('XMAX',1.)
          CALL SETNAM('NLXINC',1.)
          CALL SETNAM('BOX',0.)
          CALL SETNAM('XNUMSZ',0.)
          CALL SETNAM('XLAXIS',XUAXIS)
          CALL SETNAM('XUAXIS',XUAXIS+0.1*(XUWIND-XLWIND))
          CALL SETNAM('MASK',-1.)
          XRMIN = R8D(IXR+J1)
          XRMAX = XRMIN
          DO J = J1, J2
            IF( XRMAX .LT. R8D(IXR+J) )XRMAX = R8D(IXR+J)
            IF( XRMIN .GT. R8D(IXR+J) )XRMIN = R8D(IXR+J)
          END DO
          IF( XRMIN .EQ. XRMAX )THEN
            DO J = J1, J2
              R8D(IXR+J) = 1.0D0
            END DO
          ELSE
            DO J = J1, J2
              R8D(IXR+J) = (R8D(IXR+J) - XRMIN) / (XRMAX - XRMIN)
            END DO
          END IF
          CALL PHYSICA_GPLOT(IXR+J1-1,0,0,IYROW+J1-1,0,0,J2-J1+1,1)
          IF( CTRLC_CALLED )GO TO 20
        END IF
        IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 1) )THEN
          CALL SETNAM('HISTYP',1.)
          CALL SETNAM('BOX',0.)
          CALL SETNAM('YAXIS',1.)
          CALL SETNAM('XAXIS',0.)
          CALL SETNAM('NLYINC',1.)
          CALL SETNAM('YMIN',0.)
          CALL SETNAM('YMAX',1.)
          CALL SETNAM('XMIN',XMIN)
          CALL SETNAM('XMAX',XMAX)
          CALL SETNAM('YNUMSZ',0.)
          CALL SETNAM('XLAXIS',XLAXIS)
          CALL SETNAM('XUAXIS',XUAXIS)
          CALL SETNAM('YLAXIS',YUAXIS)
          CALL SETNAM('YUAXIS',YUAXIS+0.1*(YUWIND-YLWIND))
          CALL SETNAM('MASK',-1.)
          XCMIN = R8D(IXC+I1)
          XCMAX = XCMIN
          DO I = I1, I2
            IF( XCMIN .GT. R8D(IXC+I) )XCMIN = R8D(IXC+I)
            IF( XCMAX .LT. R8D(IXC+I) )XCMAX = R8D(IXC+I)
          END DO
          IF( XCMAX .EQ. XCMIN )THEN
            DO I = I1, I2
              R8D(IXC+I) = 1.0D0
            END DO
          ELSE
            DO I = I1, I2
              R8D(IXC+I) = (R8D(IXC+I) - XCMIN) / (XCMAX - XCMIN)
            END DO
          END IF
          CALL PHYSICA_GPLOT(IXCOL+I1-1,0,0,IXC+I1-1,0,0,I2-I1+1,1)
        END IF
  20    CALL SETNAM('XMIN',XMIN)
        CALL SETNAM('XMAX',XMAX)
        CALL SETNAM('YMIN',YMIN)
        CALL SETNAM('YMAX',YMAX)
        CALL SETNAM('HISTYP',HISTYP)
        CALL SETNAM('YAXIS',YAXIS)
        CALL SETNAM('XAXIS',XAXIS)
        CALL SETNAM('NLXINC',XLINC)
        CALL SETNAM('NLYINC',YLINC)
        CALL SETNAM('NSXINC',XSINC)
        CALL SETNAM('NSYINC',YSINC)
        CALL SETNAM('BOX',BOX)
        CALL SETNAM('MASK',XMASK)
        CALL SETNAM('%XNUMSZ',100.*XNUMSZ/(YUWIND-YLWIND))
        CALL SETNAM('%YNUMSZ',100.*YNUMSZ/(YUWIND-YLWIND))
        CALL SETNAM('%YLAXIS',100.*(YLAXIS-YLWIND)/(YUWIND-YLWIND))
        CALL SETNAM('%YUAXIS',100.*(YUAXIS-YLWIND)/(YUWIND-YLWIND))
        CALL SETNAM('%XLAXIS',100.*(XLAXIS-XLWIND)/(XUWIND-XLWIND))
        CALL SETNAM('%XUAXIS',100.*(XUAXIS-XLWIND)/(XUWIND-XLWIND))
        CALL SETLAB('XLABEL',XLABEL)
        CALL SETLAB('YLABEL',YLABEL)
      END IF
      IF( CTRLC_CALLED )GO TO 90

C   Linear density scale
C   Handle special case of constant density separately

      AMEAN = (AMIN + AMAX) / 2.0D0
      IF( AMEAN .EQ. 0.D0 )AMEAN = 1.D-10
      IF( ABS((AMAX-AMIN)/AMEAN) .LT. 1.0D-5 )THEN
        CALL ERROR_MESSAGE('DENSITY','Region is flat')
        GO TO 90
      END IF

C   Now, for each print dot location, determine the corresponding
C   effective index of the A(M,N) array and evaluate the
C   appropriate density by 2-d linear (4 pt.) interpolation

C   First do the screen picture

      WELL = .FALSE.
      IMONITOR2 = 0
      IF( IMONITOR .EQ. 0 )GO TO 1010
      CALL CHSIZE(INITD1,0,NCOLS+4,8,*99)
      CALL CHSIZE(INITD2,0,NCOLS+4,8,*99)
      CALL CHSIZE(INITD3,0,NCOLS+4,8,*99)
      NSAVE = NCOLS+4
      CALL ZERO_ARRAY(R8D(INITD1+1),8*NSAVE)
      CALL ZERO_ARRAY(R8D(INITD2+1),8*NSAVE)
      CALL ZERO_ARRAY(R8D(INITD3+1),8*NSAVE)
      NY = NYMIN
      YP = YMINHP+(NY-1)*YINC

C   Calculate first
C   Can't skip pixels and draw while calculating because it needs
C   previous values

      DO I = 1, NROWS
        NX = NXMIN
        EI = RNLO + (RNHI-RNLO)*(I-1)/(NROWS-1)
        IL = EI
        IF( IL .LT. NLO )IL = NLO
        IF( IL .GT. NHI-1 )IL = NHI-1
        IH = IL+1
        FI = EI-IL
        DO 800 J = 1, NCOLS
          IF( CTRLC_CALLED )THEN
            CALL CHSIZE(INITD1,NSAVE,0,8,*99)
            CALL CHSIZE(INITD2,NSAVE,0,8,*99)
            CALL CHSIZE(INITD3,NSAVE,0,8,*99)
            GO TO 90
          END IF
          IF( POLAR )THEN
            XLOC = XMN2 + ((XMX2-XMN2)*(J-1))/(NCOLS-1)
            YLOC = YMN2 + ((YMX2-YMN2)*(I-1))/(NROWS-1)
            RAD  = SQRT(XLOC*XLOC+YLOC*YLOC)
            IF( RAD .EQ. 0.0D0 )THEN
              THETA = 0.D0
            ELSE
              THETA = ATAN2D(YLOC,XLOC)
              IF( THETA .LT. 0.D0 )THETA = THETA+360.0D0
            END IF
            EJ = RMLO+(RMHI-RMLO)*(RAD-RADMIN)/(RADMAX-RADMIN)
            EI = RNLO+(RNHI-RNLO)*(THETA-THETAMIN)/(THETAMAX-THETAMIN)
            IL = EI
            JL = EJ
            IF( IL .LT. NLO   )GO TO 800
            IF( IL .GT. NHI-1 )GO TO 800
            IH = IL+1
            FI = MIN(1.D0,MAX(0.D0,EI-IL))
            IF( JL .LT. MLO   )GO TO 800
            IF( JL .GT. MHI-1 )GO TO 800
            JH = JL+1
            FJ = MIN(1.D0,MAX(0.D0,EJ-JL))
          ELSE
            EJ = RMLO + (RMHI-RMLO)*(J-1)/(NCOLS-1)
            JL = EJ
            IF( JL .LT. MLO   )JL = MLO
            IF( JL .GT. MHI-1 )JL = MHI-1
            JH = JL+1
            FJ = EJ-JL
          END IF

C   Interpolate array A to find value at (EI,EJ) location

          IF( DERIV )THEN
            FIJ = (1.D0-FI) * (1.D0-FJ) * R8D(INITAXY+IL+(JL-1)*N)
     &             + FI * (1.D0-FJ) *R8D(INITAXY+IH+(JL-1)*N)
     &             + (1.D0-FI) * FJ * R8D(INITAXY+IL+(JH-1)*N)
     &             + FI * FJ * R8D(INITAXY+IH+(JH-1)*N)
          ELSE 
            FIJ = (1.D0-FI) * (1.D0-FJ) * A(IL,JL)
     &               + FI * (1.D0-FJ) * A(IH,JL)
     &               + (1.D0-FI) * FJ * A(IL,JH) 
     &               + FI * FJ * A(IH,JH)
          END IF
          IF( FIJ .GT. AHI )FIJ = AHI
          DIJ = (FIJ - ALOW) / AHIMALOW
          IF( DIJ .LT. 0.D0 )DIJ = 0.D0
          IF( DIJ .GT. 1.D0 )DIJ = 1.D0
          DIJ = DIJ + R8D(INITD1+J+2)
          XP = XMINHP+(NX-1)*XINC
          IF( DIJ .GE. 0.5D0 )THEN
            IF( POLAR )THEN
              IF( (RAD.GE.RADMIN) .AND. (RAD.LE.RADMAX) )THEN
                IF( (THETA.GE.THETAMIN) .AND. (THETA.LE.THETAMAX) )THEN
                  CALL PLOT_R(XP,YP,20)
                  DOUT = 1.D0
                END IF
              END IF
            ELSE
              CALL PLOT_R(XP,YP,20)
              DOUT = 1.D0
            END IF
          ELSE
            DOUT = 0.D0
          END IF
          NX = NX+1
          ERR = DIJ-DOUT
          R8D(INITD1+J+3) = R8D(INITD1+J+3) + ERR*7.D0/48.D0
          R8D(INITD1+J+4) = R8D(INITD1+J+4) + ERR*5.D0/48.D0
          R8D(INITD2+J)   = R8D(INITD2+J)   + ERR*3.D0/48.D0
          R8D(INITD2+J+1) = R8D(INITD2+J+1) + ERR*5.D0/48.D0
          R8D(INITD2+J+2) = R8D(INITD2+J+2) + ERR*7.D0/48.D0
          R8D(INITD2+J+3) = R8D(INITD2+J+3) + ERR*5.D0/48.D0
          R8D(INITD2+J+4) = R8D(INITD2+J+4) + ERR*3.D0/48.D0
          R8D(INITD3+J)   = R8D(INITD3+J)   + ERR*1.D0/48.D0
          R8D(INITD3+J+1) = R8D(INITD3+J+1) + ERR*3.D0/48.D0
          R8D(INITD3+J+2) = R8D(INITD3+J+2) + ERR*5.D0/48.D0
          R8D(INITD3+J+3) = R8D(INITD3+J+3) + ERR*3.D0/48.D0
          R8D(INITD3+J+4) = R8D(INITD3+J+4) + ERR*1.D0/48.D0
 800    CONTINUE
        DO J = 1, NCOLS+4
          R8D(INITD1+J) = R8D(INITD2+J)
          R8D(INITD2+J) = R8D(INITD3+J)
        END DO
        CALL ZERO_ARRAY(R8D(INITD3+1),8*(NSAVE))
        NY = NY+1
        YP = YMINHP+(NY-1)*YINC
      END DO
      CALL FLUSH_PLOT

C  Now do imonitor2 if it is on

 1010 IMONITOR2 = IMONITOR2_SAVE
      CALL DWG_OUTPUT('OFF')
      IF( IMONITOR2 .EQ. 0 )GO TO 2010
      CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      WRITE(IOUTS,*)'Making plotter display... Please wait...'
      IMONITOR = 0
      CALL CHSIZE(INITD1,NSAVE,NCOLSP+4,8,*99)
      CALL CHSIZE(INITD2,NSAVE,NCOLSP+4,8,*99)
      CALL CHSIZE(INITD3,NSAVE,NCOLSP+4,8,*99)
      NSAVE = NCOLSP+4
      CALL ZERO_ARRAY(R8D(INITD1+1),8*NSAVE)
      CALL ZERO_ARRAY(R8D(INITD2+1),8*NSAVE)
      CALL ZERO_ARRAY(R8D(INITD3+1),8*NSAVE)
      NY = NYMINP
      YP = YMINHP+(NY-1)*YINCP
      DO I = 1, NROWSP
        NX = NXMINP
        EI = RNLO + (RNHI-RNLO)*(I-1)/(NROWSP-1)
        IL = EI
        IF( IL .LT. NLO )IL = NLO
        IF( IL .GT. NHI-1 )IL = NHI-1
        IH = IL+1
        FI = EI-IL
        DO 900 J = 1, NCOLSP
          IF( CTRLC_CALLED )THEN
            CALL CHSIZE(INITD1,NSAVE,0,8,*99)
            CALL CHSIZE(INITD2,NSAVE,0,8,*99)
            CALL CHSIZE(INITD3,NSAVE,0,8,*99)
            GO TO 90
          END IF
          IF( POLAR )THEN
            XLOC = XMN2 + (XMX2-XMN2)*(J-1.0)/(NCOLSP-1.0)
            YLOC = YMN2 + (YMX2-YMN2)*(I-1.0)/(NROWSP-1.0)
            RAD  = SQRT(XLOC*XLOC+YLOC*YLOC)
            IF( RAD .EQ. 0.0D0 )THEN
              THETA = 0.0D0
            ELSE
              THETA = ATAN2D(YLOC,XLOC)
              IF( THETA .LT. 0.0D0 )THETA = THETA+360.0D0
            END IF
            EJ = RMLO+(RMHI-RMLO)*(RAD-RADMIN)/(RADMAX-RADMIN)
            EI = RNLO+(RNHI-RNLO)*(THETA-THETAMIN)/(THETAMAX-THETAMIN)
            IL = EI
            JL = EJ
            IF( IL .LT. NLO   )GO TO 900
            IF( IL .GT. NHI-1 )GO TO 900
            IH = IL+1
            FI = EI-IL
            IF( JL .LT. MLO   )GO TO 900
            IF( JL .GT. MHI-1 )GO TO 900
            JH = JL+1
            FJ = EJ-JL
          ELSE
            EJ = RMLO + (RMHI-RMLO)*(J-1)/(NCOLSP-1)
            JL = EJ
            IF( JL .LT. MLO   )JL = MLO
            IF( JL .GT. MHI-1 )JL = MHI-1
            JH = JL+1
            FJ = EJ-JL
          END IF

C   Interpolate array A to find value at (EI,EJ) location

          IF( DERIV )THEN
            FIJ = (1.D0-FI) * (1.D0-FJ) * R8D(INITAXY+IL+(JL-1)*N)
     &             + FI * (1.D0-FJ) *R8D(INITAXY+IH+(JL-1)*N)
     &             + (1.D0-FI) * FJ * R8D(INITAXY+IL+(JH-1)*N)
     &             + FI * FJ * R8D(INITAXY+IH+(JH-1)*N)
          ELSE 
            FIJ = (1.D0-FI) * (1.D0-FJ) * A(IL,JL)
     &               + FI * (1.D0-FJ) * A(IH,JL)
     &               + (1.D0-FI) * FJ * A(IL,JH) 
     &               + FI * FJ * A(IH,JH)
          END IF
          IF( FIJ .GT. AHI )FIJ = AHI
          DIJ = (FIJ - ALOW) / AHIMALOW
          IF( DIJ .LT. 0.D0 )DIJ = 0.D0
          IF( DIJ .GT. 1.D0 )DIJ = 1.D0
          DIJ = DIJ + R8D(INITD1+J+2)
          XP = XMINHP+(NX-1)*XINCP
          IF( DIJ .GE. 0.5D0 )THEN
            IF( POLAR )THEN
              IF( (RAD.GE.RADMIN) .AND. (RAD.LE.RADMAX) )THEN
                IF( (THETA.GE.THETAMIN) .AND. (THETA.LE.THETAMAX) )THEN
                  CALL PLOT_R(XP,YP,20)
                  DOUT = 1.D0
                END IF
              END IF
            ELSE
              CALL PLOT_R(XP,YP,20)
              DOUT = 1.D0
            END IF
          ELSE
            DOUT = 0.D0
          END IF
          NX = NX+1
          ERR = DIJ-DOUT
          R8D(INITD1+J+3) = R8D(INITD1+J+3) + ERR*7.D0/48.D0
          R8D(INITD1+J+4) = R8D(INITD1+J+4) + ERR*5.D0/48.D0
          R8D(INITD2+J)   = R8D(INITD2+J)   + ERR*3.D0/48.D0
          R8D(INITD2+J+1) = R8D(INITD2+J+1) + ERR*5.D0/48.D0
          R8D(INITD2+J+2) = R8D(INITD2+J+2) + ERR*7.D0/48.D0
          R8D(INITD2+J+3) = R8D(INITD2+J+3) + ERR*5.D0/48.D0
          R8D(INITD2+J+4) = R8D(INITD2+J+4) + ERR*3.D0/48.D0
          R8D(INITD3+J)   = R8D(INITD3+J)   + ERR*1.D0/48.D0
          R8D(INITD3+J+1) = R8D(INITD3+J+1) + ERR*3.D0/48.D0
          R8D(INITD3+J+2) = R8D(INITD3+J+2) + ERR*5.D0/48.D0
          R8D(INITD3+J+3) = R8D(INITD3+J+3) + ERR*3.D0/48.D0
          R8D(INITD3+J+4) = R8D(INITD3+J+4) + ERR*1.D0/48.D0
 900    CONTINUE
        DO J = 1, NCOLSP+4
          R8D(INITD1+J) = R8D(INITD2+J)
          R8D(INITD2+J) = R8D(INITD3+J)
        END DO
        CALL ZERO_ARRAY(R8D(INITD3+1),8*NSAVE)
        NY = NY+1
        YP = YMINHP+(NY-1)*YINCP
      END DO

C  Now do the bitmap if the bitmap is on

 2010 WELL = WELL_SAVE
      IMONITOR2 = 0
      IF( .NOT.WELL )THEN
        CALL CHSIZE(INITD1,NSAVE,0,8,*99)
        CALL CHSIZE(INITD2,NSAVE,0,8,*99)
        CALL CHSIZE(INITD3,NSAVE,0,8,*99)
        GO TO 90
      END IF
      IMONITOR = IMONITOR_SAVE
      CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      WRITE(IOUTS,*)'Making bitmap... Please wait...'
      IMONITOR = 0
      CALL CHSIZE(INITD1,NSAVE,NCOLSB+4,8,*99)
      CALL CHSIZE(INITD2,NSAVE,NCOLSB+4,8,*99)
      CALL CHSIZE(INITD3,NSAVE,NCOLSB+4,8,*99)
      NSAVE = NCOLSB+4
      CALL ZERO_ARRAY(R8D(INITD1+1),8*NSAVE)
      CALL ZERO_ARRAY(R8D(INITD2+1),8*NSAVE)
      CALL ZERO_ARRAY(R8D(INITD3+1),8*NSAVE)
      NY = NYMINB
      YP = YMINHP+(NY-1)*YINCB
      DO I = 1, NROWSB
        NX = NXMINB
        EI = RNLO + (RNHI-RNLO)*(I-1)/(NROWSB-1)
        IL = EI
        IF( IL .LT. NLO )IL = NLO
        IF( IL .GT. NHI-1 )IL = NHI-1
        IH = IL+1
        FI = EI-IL
        DO 1900 J = 1, NCOLSB
          IF( CTRLC_CALLED )THEN
            CALL CHSIZE(INITD1,NSAVE,0,8,*99)
            CALL CHSIZE(INITD2,NSAVE,0,8,*99)
            CALL CHSIZE(INITD3,NSAVE,0,8,*99)
            GO TO 90
          END IF
          IF( POLAR )THEN
            XLOC = XMN2 + (XMX2-XMN2)*(J-1.0)/(NCOLSB-1.0)
            YLOC = YMN2 + (YMX2-YMN2)*(I-1.0)/(NROWSB-1.0)
            RAD  = SQRT(XLOC*XLOC+YLOC*YLOC)
            IF( RAD .EQ. 0.0D0 )THEN
              THETA = 0.0D0
            ELSE
              THETA = ATAN2D(YLOC,XLOC)
              IF( THETA .LT. 0.0D0 )THETA = THETA+360.0D0
            END IF
            EJ = RMLO+(RMHI-RMLO)*(RAD-RADMIN)/(RADMAX-RADMIN)
            EI = RNLO+(RNHI-RNLO)*(THETA-THETAMIN)/(THETAMAX-THETAMIN)
            IL = EI
            JL = EJ
            IF( IL .LT. NLO   )GO TO 1900
            IF( IL .GT. NHI-1 )GO TO 1900
            IH = IL+1
            FI = EI-IL
            IF( JL .LT. MLO   )GO TO 1900
            IF( JL .GT. MHI-1 )GO TO 1900
            JH = JL+1
            FJ = EJ-JL
          ELSE
            EJ = RMLO + (RMHI-RMLO)*(J-1)/(NCOLSB-1)
            JL = EJ
            IF( JL .LT. MLO   )JL = MLO
            IF( JL .GT. MHI-1 )JL = MHI-1
            JH = JL+1
            FJ = EJ-JL
          END IF

C   Interpolate array A to find value at (EI,EJ) location

          IF( DERIV )THEN
            FIJ = (1.D0-FI) * (1.D0-FJ) * R8D(INITAXY+IL+(JL-1)*N)
     &             + FI * (1.D0-FJ) *R8D(INITAXY+IH+(JL-1)*N)
     &             + (1.D0-FI) * FJ * R8D(INITAXY+IL+(JH-1)*N)
     &             + FI * FJ * R8D(INITAXY+IH+(JH-1)*N)
          ELSE 
            FIJ = (1.D0-FI) * (1.D0-FJ) * A(IL,JL)
     &               + FI * (1.D0-FJ) * A(IH,JL)
     &               + (1.D0-FI) * FJ * A(IL,JH) 
     &               + FI * FJ * A(IH,JH)
          END IF
          IF( FIJ .GT. AHI )FIJ = AHI
          DIJ = (FIJ - ALOW) / AHIMALOW
          IF( DIJ .LT. 0.D0 )DIJ = 0.D0
          IF( DIJ .GT. 1.D0 )DIJ = 1.D0
          DIJ = DIJ + R8D(INITD1+J+2)
          XP = XMINHP+(NX-1)*XINCB
          IF( DIJ .GE. 0.5D0 )THEN
            IF( POLAR )THEN
              IF( (RAD.GE.RADMIN) .AND. (RAD.LE.RADMAX) )THEN
                IF( (THETA.GE.THETAMIN) .AND. (THETA.LE.THETAMAX) )THEN
                  CALL PLOT_R(XP,YP,20)
                  DOUT = 1.D0
                END IF
              END IF
            ELSE
              CALL PLOT_R(XP,YP,20)
              DOUT = 1.D0
            END IF
          ELSE
            DOUT = 0.D0
          END IF
          NX = NX+1
          ERR = DIJ-DOUT
          R8D(INITD1+J+3) = R8D(INITD1+J+3) + ERR*7.D0/48.D0
          R8D(INITD1+J+4) = R8D(INITD1+J+4) + ERR*5.D0/48.D0
          R8D(INITD2+J)   = R8D(INITD2+J)   + ERR*3.D0/48.D0
          R8D(INITD2+J+1) = R8D(INITD2+J+1) + ERR*5.D0/48.D0
          R8D(INITD2+J+2) = R8D(INITD2+J+2) + ERR*7.D0/48.D0
          R8D(INITD2+J+3) = R8D(INITD2+J+3) + ERR*5.D0/48.D0
          R8D(INITD2+J+4) = R8D(INITD2+J+4) + ERR*3.D0/48.D0
          R8D(INITD3+J)   = R8D(INITD3+J)   + ERR*1.D0/48.D0
          R8D(INITD3+J+1) = R8D(INITD3+J+1) + ERR*3.D0/48.D0
          R8D(INITD3+J+2) = R8D(INITD3+J+2) + ERR*5.D0/48.D0
          R8D(INITD3+J+3) = R8D(INITD3+J+3) + ERR*3.D0/48.D0
          R8D(INITD3+J+4) = R8D(INITD3+J+4) + ERR*1.D0/48.D0
1900    CONTINUE
        DO J = 1, NCOLSB+4
          R8D(INITD1+J) = R8D(INITD2+J)
          R8D(INITD2+J) = R8D(INITD3+J)
        END DO
        CALL ZERO_ARRAY(R8D(INITD3+1),8*NSAVE)
        NY = NY+1
        YP = YMINHP+(NY-1)*YINCB
      END DO
      CALL CHSIZE(INITD1,NSAVE,0,8,*99)
      CALL CHSIZE(INITD2,NSAVE,0,8,*99)
      CALL CHSIZE(INITD3,NSAVE,0,8,*99)
  90  WELL = WELL_SAVE
      IMONITOR = IMONITOR_SAVE
      IMONITOR2 = IMONITOR2_SAVE
      CALL DWG_OUTPUT('ON')
      RETURN            
  99  CALL ERROR_MESSAGE('DENSITY','modifying dynamic array space')
      WELL = WELL_SAVE
      IMONITOR = IMONITOR_SAVE
      IMONITOR2 = IMONITOR2_SAVE
      CALL DWG_OUTPUT('ON')
      RETURN
      END
