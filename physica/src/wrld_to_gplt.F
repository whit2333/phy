      SUBROUTINE WORLD_TO_GPLOT(XIN,YIN,XOUT,YOUT)

      LOGICAL FIRST
      COMMON / WORLDTOGPLOT / FIRST
      DATA FIRST / .TRUE. /

C  Modified by J.L.Chuma, 08-Apr-1997 to elimate SIND, COSD for g77
      REAL*4 DTOR /0.017453292519943/

      XPL(X,Y) = AX*((COSY*(Y - YLAXIS) - SINY*(X - XLAXIS))/SC - BX)
      YPL(X,Y) = AY*((SINX*(X - XLAXIS) - COSX*(Y - YLAXIS))/SC - BY)
CCC
      IF( FIRST )THEN
        XMIN   = GETNAM('XMIN')
        XMAX   = GETNAM('XMAX')
        YMIN   = GETNAM('YMIN')
        YMAX   = GETNAM('YMAX')
        XLOG   = ABS(GETNAM('XLOG'))
        YLOG   = ABS(GETNAM('YLOG'))
        IF( XLOG .GT. 1. )ALOGX = ALOG(XLOG)
        IF( YLOG .GT. 1. )ALOGY = ALOG(YLOG)
C
C    This is to handle non-integral log scale limits
C
        IF( XLOG .GT. 1. )THEN
          IF( XMIN .GE. 0. )THEN
            XMIN = FLOAT(IFIX(XMIN))
          ELSE
            IF( XMIN .NE. FLOAT(IFIX(XMIN)) )XMIN = IFIX(XMIN) - 1.
          END IF
          CALL SETNAM('XMIN',XMIN)
          IF( XMAX .LE. 0. )THEN
            XMAX = FLOAT(IFIX(XMAX))
          ELSE
            IF( XMAX .NE. FLOAT(IFIX(XMAX)) )XMAX = IFIX(XMAX) + 1.
          END IF
          CALL SETNAM('XMAX',XMAX)
        END IF
        IF( YLOG .GT. 1. )THEN
          IF( YMIN .GE. 0. )THEN
            YMIN = FLOAT(IFIX(YMIN))
          ELSE
            IF( YMIN .NE. FLOAT(IFIX(YMIN)) )YMIN = IFIX(YMIN) - 1.
          END IF
          CALL SETNAM('YMIN',YMIN)
          IF( YMAX .LE. 0. )THEN
            YMAX = FLOAT(IFIX(YMAX))
          ELSE
            IF( YMAX .NE. FLOAT(IFIX(YMAX)) )YMAX = IFIX(YMAX) + 1.
          END IF
          CALL SETNAM('YMAX',YMAX)
        END IF
        XAXISA = GETNAM('XAXISA')
        YAXISA = GETNAM('YAXISA')
        XLAXIS = GETNAM('XLAXIS')
        YLAXIS = GETNAM('YLAXIS')
        XUAXIS = GETNAM('XUAXIS')
        YUAXIS = GETNAM('YUAXIS')
        COSX   = COS(XAXISA*DTOR)
        SINX   = SIN(XAXISA*DTOR)
        COSY   = COS(YAXISA*DTOR)
        SINY   = SIN(YAXISA*DTOR)
        IF( ABS(COSX) .LE. 1.E-7 )COSX = 0.0
        IF( ABS(SINX) .LE. 1.E-7 )SINX = 0.0
        IF( ABS(COSY) .LE. 1.E-7 )COSY = 0.0
        IF( ABS(SINY) .LE. 1.E-7 )SINY = 0.0
        SC     = SINX * COSY - COSX * SINY
        AX     = (XMAX - XMIN) / (XUAXIS - XLAXIS)
        BX     = - XMIN / AX
        AY     = (YMAX - YMIN) / (YUAXIS - YLAXIS)
        BY     = - YMIN / AY
        FIRST  = .FALSE.
      END IF
      XOUT = XPL(XIN,YIN)
      YOUT = YPL(XIN,YIN)
      IF( XLOG .GT. 1. )THEN 
        IF( XOUT .LE. 0. )XOUT = 1.E-36
        XOUT = ALOG(XOUT) / ALOGX
      END IF
      IF( YLOG .GT. 1. )THEN 
        IF( YOUT .LE. 0. )YOUT = 1.E-36
        YOUT = ALOG(YOUT) / ALOGY
      END IF
      RETURN
      END
