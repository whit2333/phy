      DOUBLE PRECISION FUNCTION ERFC(X)
      IMPLICIT REAL*8 (A-H,O-Z)
C
C     ******************************************************************
C     ERFC(X)=(2/sqrt(pi))*INTEGRAL(exp(-t**2))dt, limits X to infinity
C     ERFC(X) is sometimes denoted as Q(X)
C     REFERENCE: CERN library routine C300, see also ERF(X)
C     ******************************************************************
C     THE ERFC  FUNCTION IS COMPUTED FROM THE RATIONAL APPROXIMAT-
C     IONS OF W.J.CODY, MATHEMATICS OF COMPUTATION, VOLUME 22 (1969),
C     PAGES 631-637.
C
C     THE ERFC FUNCTION IS RELATED TO THE ERF FUNCTION BY:
C             ERFC(X) = 1 - ERF(X)
C     ******************************************************************
C     ACCURACY : TO 1.0E-7
C     ******************************************************************
      DIMENSION P1(3),Q1(3), P2(5),Q2(5), P3(3),Q3(3)
C
      DATA CONST/ 0.56418 9584 D0 /
C     ( CONST=SQRT(1/PI) . )
C
      DATA XMAX/ 8.9 /
C     ( XMAX=SQRT(-ALOG(RMIN)-10.0), WHERE RMIN IS THE SMALLEST NORMAL-
C     IZED REPRESENTABLE NUMBER.  ERFC(XMAX) IS CLOSE TO THE UNDERFLOW
C     THRESHOLD. )
C
      DATA XUNIT/ 4.4 /
C     ( XUNIT=SQRT(-ALOG(RELPR)+1.0), WHERE RELPR IS THE SMALLEST NUMBER
C     FOR WHICH 1.0+RELPR DIFFERS FROM 1.0.  ERF(XUNIT) IS INDISTIN-
C     GUISHABLE FROM 1.0. )
C
      DATA P1/2.13853 322 D1,
     *        1.72227 577 D0,
     *        3.16652 891 D-1/
      DATA Q1/1.89522 572 D1,
     *        7.84374 571 D0,
     *        1.00000 000 D0/
      DATA P2/7.37388 831 D0,
     *        6.86501 848 D0,
     *        3.03179 934 D0,
     *        5.63169 619 D-1,
     *        4.31877 874 D-5/
      DATA Q2/7.37396 089 D0,
     *        1.51849 082 D1,
     *        1.27955 295 D1,
     *        5.35421 679 D0,
     *        1.00000 000 D0/
      DATA P3/-4.25799 644 D-2,
     *        -1.96068 974 D-1,
     *        -5.16882 262 D-2/
      DATA Q3/1.50942 071 D-1,
     *        9.21452 412 D-1,
     *        1.00000 000 D0/
C
C     ******************************************************************
C
C  START.
C
      T=X
      A=ABS(T)
      IF(T.GE.-XUNIT) GO TO 1
         ERFC=2.0
         RETURN
    1 IF(T.LE.XMAX) GO TO 2
         ERFC=0.0
         RETURN
C
    2 S=T**2
      IF(A.GT.0.47) GO TO 4
C
      Y=T*(P1(1)+S*(P1(2)+S*P1(3) ))
     *   /(Q1(1)+S*(Q1(2)+S*Q1(3) ))
    3 ERFC=1.0-Y
      RETURN
C
C  SET Y=ERFC(A), THEN TERMINATE.
C
    4 IF(A.GT.4.) GO TO 5
C
      Y=EXP(-S)*(P2(1)+A*(P2(2)+A*(P2(3)+A*(P2(4)+A*P2(5) ))))
     *         /(Q2(1)+A*(Q2(2)+A*(Q2(3)+A*(Q2(4)+A*Q2(5) ))))
      GO TO 7
C
    5 R=1.0/A
      U=R**2
      Y=R*EXP(-S)*( CONST + U*(P3(1)+U*P3(2))/(Q3(1)+U*Q3(2)) )
C
    7 IF(T.GE.0.) ERFC=Y
      IF(T.LT.0.) ERFC=2.0-Y
      RETURN
C
      END
 
 
 
