      REAL*8 FUNCTION  FRES12(XX)
C
C     ****************************************************************
C     FRES12 RETURNS THE FRESNEL AND THE ASSOCIATED FRESNEL INTEGRALS 
C        S(X) [FRES1] AND S2(X) [FRES2]
C        S(X) = INTEGRAL(sin(pi*(t**2)/2))dt, limits 0 to X
C        S2(X)= 1/sqrt(2*pi)*INTEGRAL(sin(t)/sqrt(t))dt, limits 0 to X 
C      NB. FRES1(X) = FRES2(pi*(X**2)/2)
C     REFERENCE: CERN library routine C309
C     ****************************************************************
C     ACCURACY: TO AT LEAST 1.0E-6 IN TESTED REGION
C       REGION TESTED: 0.0 <= XX <= 5.00           for FRES1         
C                      0.0 <= XX <= 6.28 (2* pi)   for FRES2
C     ****************************************************************
      IMPLICIT REAL*8 (A-H, O-Z)
C
C START
      ENTRY FRES2(XX)
      X=0.79788 45608 02865*SQRT(ABS(XX))
      IF(XX .LT. 0.0) X=-X
      GO TO 5
      ENTRY FRES1(XX)
      X=XX
 5    T=X**2
      IF(T .GE. 9.0) GO TO 1
      Z=T/9.0
      Y=4.0*Z**2-2.0
      A=     -0.00000 00000 00001
      B=Y*A  +0.00000 00000 00030
      A=Y*B-A-0.00000 00000 00794
      B=Y*A-B+0.00000 00000 18654
      A=Y*B-A-0.00000 00003 86931
      B=Y*A-B+0.00000 00070 25677
      A=Y*B-A-0.00000 01105 48467
      B=Y*A-B+0.00000 14897 19660
      A=Y*B-A-0.00001 69541 78157
      B=Y*A-B+0.00016 02434 43651
      A=Y*B-A-0.00123 23144 20465
      B=Y*A-B+0.00751 77634 79240
      A=Y*B-A-0.03524 82880 29314
      B=Y*A-B+0.12219 10666 02012
      A=Y*B-A-0.30039 87868 77130
      B=Y*A-B+0.51546 16065 59411
      A=Y*B-A-0.68888 16952 98469
      B=Y*A-B+0.92649 39769 89515
      A=Y*B-A-1.24769 75072 91387
      A=Y*A-B+1.73417 43390 31447
      FRES1=X*Z*0.5*(A-B)
      FRES2=FRES1
      RETURN
    1 Z=9.0/T
      Y=4.0*Z**2-2.0
      A=     -0.00000 00000 00001
      B=Y*A  +0.00000 00000 00008
      A=Y*B-A-0.00000 00000 00053
      B=Y*A-B+0.00000 00000 00381
      A=Y*B-A-0.00000 00000 03145
      B=Y*A-B+0.00000 00000 30716
      A=Y*B-A-0.00000 00003 70990
      B=Y*A-B+0.00000 00059 08966
      A=Y*B-A-0.00000 01373 98906
      B=Y*A-B+0.00000 55718 91859
      A=Y*B-A-0.00057 36213 72272
      A=Y*A-B+0.63546 10984 12986
      AA=0.5*(A-B)/X
      A=     -0.00000 00000 00002
      B=Y*A  +0.00000 00000 00008
      A=Y*B-A-0.00000 00000 00050
      B=Y*A-B+0.00000 00000 00330
      A=Y*B-A-0.00000 00000 02478
      B=Y*A-B+0.00000 00000 21678
      A=Y*B-A-0.00000 00002 28879
      B=Y*A-B+0.00000 00030 74598
      A=Y*B-A-0.00000 00569 63997
      B=Y*A-B+0.00000 16631 69852
      A=Y*B-A-0.00009 83959 02454
      A=Y*A-B+0.02231 55798 58535
      BB=0.5*(A-B)*Z/X
      Z=1.57079 63267 94897*T
      T=SIN(Z)
      Z=COS(Z)
      A=0.5
      IF(X .LT. 0.0) A=-0.5
      FRES1=A-T*BB-Z*AA
      FRES2=FRES1
      RETURN
      END
