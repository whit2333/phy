      REAL*8 FUNCTION FERDIR(X,P)
C
C     *****************************************************************
C     FERDIR CALCILATES THE FERMI-DIRAC FUNCTION
C     FERDIR(X,P)= INTEGRAL(t**P/(exp(t-X)+1))dt, limits 0 to infinity
C     REFERENCE: CERN library routine C338
C     *****************************************************************
C     ARGUMENT RESTRICTION :
C       X ----- REAL
C       P ----- ONE OF 3 VALUES ONLY: -1/2, 1/2, 3/2
C               if P is not any of the above, FERDIR is set to 0
C     *****************************************************************
C     ACCURACY: { yet unchecked }
C     *****************************************************************
C
      IMPLICIT REAL*8 (A-H, O-Z)
      DIMENSION P1(5),P2(5),P3(5),P4(5),P5(5),P6(5),P7(5),P8(5),P9(5)
      DIMENSION Q1(5),Q2(5),Q3(5),Q4(5),Q5(5),Q6(5),Q7(5),Q8(5),Q9(5)
      DATA P1
     1/-1.25331 41288 20E+0, -1.72366 35577 01E+0, -6.55904 57292 58E-1,
     2 -6.34228 31976 82E-2, -1.48838 31061 16E-5/
      DATA Q1
     1/+1.00000 00000 00E+0, +2.19178 09259 80E+0, +1.60581 29554 06E+0,
     2 +4.44366 95274 81E-1, +3.62423 22881 12E-2/
      DATA P2
     1/-3.13328 53055 70E-1, -4.16187 38522 93E-1, -1.50220 84005 88E-1,
     2 -1.33957 93751 73E-2, -1.51335 07001 38E-5/
      DATA Q2
     1/+1.00000 00000 00E+0, +1.87260 86759 02E+0, +1.14520 44465 78E+0,
     2 +2.57022 55875 73E-1, +1.63990 25435 68E-2/
      DATA P3
     1/-2.34996 39854 06E-1, -2.92737 36375 47E-1, -9.88309 75887 38E-2,
     2 -8.25138 63795 51E-3, -1.87438 41532 23E-5/
      DATA Q3
     1/+1.00000 00000 00E+0, +1.60859 71091 46E+0, +8.27528 95308 80E-1,
     2 +1.52232 23828 50E-1, +7.69512 04750 64E-3/
      DATA P4
     1/+1.07381 27694E+0, +5.60033 03660E+0, +3.68822 11270E+0,
     2 +1.17433 92816E+0, +2.36419 35527E-1/
      DATA Q4
     1/+1.00000 00000E+0, +4.60318 40667E+0, +4.30759 10674E-1,
     2 +4.21511 32145E-1, +1.18326 01601E-2/
      DATA P5
     1/+6.78176 62666 0E-1, +6.33124 01791 0E-1, +2.94479 65177 2E-1,
     2 +8.01320 71141 9E-2, +1.33918 21294 0E-2/
      DATA Q5
     1/+1.00000 00000 0E+0, +1.43740 40039 7E-1, +7.08662 14845 0E-2,
     2 +2.34579 49473 5E-3, -1.29449 92883 5E-5/
      DATA P6
     1/+1.15302 13402E+0, +1.05915 58972E+0, +4.68988 03095E-1,
     2 +1.18829 08784E-1, +1.94387 55787E-2/
      DATA Q6
     1/+1.00000 00000E+0, +3.73489 53841E-2, +2.32484 58137E-2,
     2 -1.37667 70874E-3, +4.64663 92781E-5/
      DATA P7
     1/-8.22255 9330E-1, -3.62036 9345E+1, -3.01538 5410E+3,
     2 -7.04987 1579E+4, -5.69814 5924E+4/
      DATA Q7
     1/+1.00000 0000E+0, +3.93568 9841E+1, +3.56875 6266E+3,
     2 +4.18189 3625E+4, +3.38513 8907E+5/
      DATA P8
     1/+8.22449 97626E-1, +2.00463 03393E+1, +1.82680 93446E+3,
     2 +1.22265 30374E+4, +1.40407 50092E+5/
      DATA Q8
     1/+1.00000 00000E+0, +2.34862 07659E+1, +2.20134 83743E+3,
     1 +1.14426 73596E+4, +1.65847 15900E+5/
      DATA P9
     1/+2.46740 02368 4E+0, +2.19167 58236 8E+2, +1.23829 37907 5E+4,
     2 +2.20667 72496 8E+5, +8.49442 92003 4E+5/
      DATA Q9
     1/+1.00000 00000 0E+0, +8.91125 14061 9E+1, +5.04575 66966 7E+3,
     2 +9.09075 94630 4E+4, +3.89960 91564 1E+5/
C
C  START
      IF(P .EQ. 1.5) GO TO 30
      IF(P .EQ. 0.5) GO TO 20
      IF(P .EQ.-0.5) GO TO 10
      FERDIR=0.
      PRINT 100,P
  100 FORMAT(1X,23HFERDIR ... ILLEGAL P =  ,F10.6)
      RETURN
C
   10 IF(X .GT. 4.0) GO TO 11
      IF(X .GT. 1.0) GO TO 12
      Y=EXP(X)
      FERDIR=Y*(1.772453850905516+Y*
     1       (P1(1)+Y*(P1(2)+Y*(P1(3)+Y*(P1(4)+Y*P1(5)))))/
     2       (Q1(1)+Y*(Q1(2)+Y*(Q1(3)+Y*(Q1(4)+Y*Q1(5))))))
      RETURN
C
   12 FERDIR=(P4(1)+X*(P4(2)+X*(P4(3)+X*(P4(4)+X*P4(5)))))/
     1       (Q4(1)+X*(Q4(2)+X*(Q4(3)+X*(Q4(4)+X*Q4(5)))))
      RETURN
C
   11 Y=1.0/X**2
      FERDIR=SQRT(X)*(2.0+Y*
     1       (P7(1)+Y*(P7(2)+Y*(P7(3)+Y*(P7(4)+Y*P7(5)))))/
     2       (Q7(1)+Y*(Q7(2)+Y*(Q7(3)+Y*(Q7(4)+Y*Q7(5))))))
      RETURN
C
   20 IF(X .GT. 4.0) GO TO 21
      IF(X .GT. 1.0) GO TO 22
      Y=EXP(X)
      FERDIR=Y*(0.886226925452758+Y*
     1       (P2(1)+Y*(P2(2)+Y*(P2(3)+Y*(P2(4)+Y*P2(5)))))/
     2       (Q2(1)+Y*(Q2(2)+Y*(Q2(3)+Y*(Q2(4)+Y*Q2(5))))))
      RETURN
C
   22 FERDIR=(P5(1)+X*(P5(2)+X*(P5(3)+X*(P5(4)+X*P5(5)))))/
     1       (Q5(1)+X*(Q5(2)+X*(Q5(3)+X*(Q5(4)+X*Q5(5)))))
      RETURN
C
   21 Y=1.0/X**2
      FERDIR=X*SQRT(X)*(0.666666666666667+Y*
     1       (P8(1)+Y*(P8(2)+Y*(P8(3)+Y*(P8(4)+Y*P8(5)))))/
     2       (Q8(1)+Y*(Q8(2)+Y*(Q8(3)+Y*(Q8(4)+Y*Q8(5))))))
      RETURN
C
   30 IF(X .GT. 4.0) GO TO 31
      IF(X .GT. 1.0) GO TO 32
      Y=EXP(X)
      FERDIR=Y*(1.329340388179137+Y*
     1       (P3(1)+Y*(P3(2)+Y*(P3(3)+Y*(P3(4)+Y*P3(5)))))/
     2       (Q3(1)+Y*(Q3(2)+Y*(Q3(3)+Y*(Q3(4)+Y*Q3(5))))))
      RETURN
C
   32 FERDIR=(P6(1)+X*(P6(2)+X*(P6(3)+X*(P6(4)+X*P6(5)))))/
     1       (Q6(1)+X*(Q6(2)+X*(Q6(3)+X*(Q6(4)+X*Q6(5)))))
      RETURN
C
   31 XS=X**2
      Y=1.0/XS
      FERDIR=XS*SQRT(X)*(0.4+Y*
     1       (P9(1)+Y*(P9(2)+Y*(P9(3)+Y*(P9(4)+Y*P9(5)))))/
     2       (Q9(1)+Y*(Q9(2)+Y*(Q9(3)+Y*(Q9(4)+Y*Q9(5))))))
      RETURN
 
      END
 
