      DOUBLE PRECISION FUNCTION BETAI (X, PIN, QIN)
C**********************************************************************
C   REFERENCE: SLATEC LIBRARY; CATEGORY B5F
C              BASED ON BOSTEN & BATTISTE, REMARK ON ALGORITHM 179,
C                COMM. ACM, vol. 17, p.153, (1974)
C***PURPOSE
C   CALCULATES THE  INCOMPLETE BETA FUNCTION, I<X>(P,Q)
C      I<X>(P,Q) = B<X>(P,Q) / B<1>(P,Q)
C       where B<X>(P,Q)= INTEGRAL from 0 to X of
C                         (t**(P-1) * (1-t)**(Q-1)) dt; P,Q>0; 0<=X<=1
C             B<1>(P,Q)= complete beta function
C
C   INPUT ARGUMENTS --
C  X --   UPPER LIMIT OF INTEGRATION.  X MUST BE IN (0,1) INCLUSIVE.
C  P --   FIRST BETA DISTRIBUTION PARAMETER.  P MUST BE GT 0.0.
C  Q --   SECOND BETA DISTRIBUTION PARAMETER.  Q MUST BE GT 0.0.
C  BETAI -- THE INCOMPLETE BETA FUNCTION RATIO IS THE PROBABILITY THAT A
C           RANDOM VARIABLE FROM A BETA DISTRIBUTION HAVING PARAMETERS
C           P AND Q WILL BE LESS THAN OR EQUAL TO X.
C
C***EXTERNAL ROUTINES CALLED: D1MACH,DLBETA
C***********************************************************************
      IMPLICIT REAL*8 (A-H,O-Z)
      DATA EPS, ALNEPS, SML, ALNSML / 4*0.0D0 /
C
      BETAI = 0.0D0
      IF (EPS.NE.0.0D0) GO TO 10
      EPS = D1MACH(3)
      ALNEPS = DLOG (EPS)
      SML = D1MACH(1)
      ALNSML = DLOG (SML)
C
 10   IF (X.GE.0.D0 .AND. X.LE.1.D0) GO TO 100
        WRITE(6,*) '  BETAI-- X IS NOT IN THE RANGE [0,1]'
        RETURN
100   IF (PIN.GT.0.D0 .AND. QIN.GT.0.D0) GO TO 110
        WRITE(6,*) '  BETAI-- P AND/OR Q IS <= ZERO' 
        RETURN
C
110   Y = X
      P = PIN
      Q = QIN
      IF (Q.LE.P .AND. X.LT.0.8D0) GO TO 20
      IF (X.LT.0.2D0) GO TO 20
      Y = 1.0D0 - Y
      P = QIN
      Q = PIN
C
 20   IF ((P+Q)*Y/(P+1.D0).LT.EPS) GO TO 80
C
C EVALUATE THE INFINITE SUM FIRST.  TERM WILL EQUAL
C Y**P/BETA(PS,P) * (1.-PS)-SUB-I * Y**I / FAC(I) .
C
      PS = Q - DINT(Q)
      IF (PS.EQ.0.D0) PS = 1.0D0
      XB = P*DLOG(Y) - DLBETA(PS,P) - DLOG(P)
      BETAI = 0.0D0
      IF (XB.LT.ALNSML) GO TO 40
C
      BETAI = DEXP (XB)
      TERM = BETAI*P
      IF (PS.EQ.1.0D0) GO TO 40
      N = MAX1 (SNGL(ALNEPS/DLOG(Y)), 4.0)
      DO 30 I=1,N
        XI = I
        TERM = TERM * (XI-PS)*Y/XI
        BETAI = BETAI + TERM/(P+XI)
 30   CONTINUE
C
C NOW EVALUATE THE FINITE SUM, MAYBE.
C
 40   IF (Q.LE.1.0D0) GO TO 70
C
      XB = P*DLOG(Y) + Q*DLOG(1.0D0-Y) - DLBETA(P,Q) - DLOG(Q)
      IB = MAX1 (SNGL(XB/ALNSML), 0.0)
      TERM = DEXP (XB - DBLE(FLOAT(IB))*ALNSML )
      C = 1.0D0/(1.D0-Y)
      P1 = Q*C/(P+Q-1.D0)
C
      FINSUM = 0.0D0
      N = Q
      IF (Q.EQ.DBLE(FLOAT(N))) N = N - 1
      DO 50 I=1,N
        IF (P1.LE.1.0D0 .AND. TERM/EPS.LE.FINSUM) GO TO 60
        XI = I
        TERM = (Q-XI+1.0D0)*C*TERM/(P+Q-XI)
C
        IF (TERM.GT.1.0D0) IB = IB - 1
        IF (TERM.GT.1.0D0) TERM = TERM*SML
C
        IF (IB.EQ.0) FINSUM = FINSUM + TERM
 50   CONTINUE
C
 60   BETAI = BETAI + FINSUM
 70   IF (Y.NE.X .OR. P.NE.PIN) BETAI = 1.0D0 - BETAI
      BETAI = DMAX1 (DMIN1 (BETAI, 1.0D0), 0.0D0)
      RETURN
C
 80   BETAI = 0.0D0
      XB = P*DLOG(DMAX1(Y,SML)) - DLOG(P) - DLBETA(P,Q)
      IF (XB.GT.ALNSML .AND. Y.NE.0.0D0) BETAI = DEXP(XB)
      IF (Y.NE.X .OR. P.NE.PIN) BETAI = 1.0D0 - BETAI
C
      RETURN
      END
