       REAL*8 FUNCTION FISHER (M,N,X)
C**********************************************************************
C   COMPUTES FISHER'S F-DISTRIBUTION WITH M AND N DEGREES OF FREEDOM
C    i.e., THE PROBILITY
C    FISHER(M,N,X) = GAMMA((M+N)/2) / (GAMMA(M/2)*GAMMA(N/2)) *
C        INTEGRAL from 0 to w of t**(M/2-1)/(t+1)**((M+N)/2) dt
C        where w=(M/N)*X
C
C THE F-DISTRIBUTION IS ALSO KNOWN AS THE VARIANCE-RATIO DISTRIBUTION
C   OR THE SNEDECOR'S DISTRIBUTION
C
C   REFERENCE: CACM ALGORITHM #322
C***********************************************************************
C   ACCURACY: TO AT LEAST 1.0E-3 for 1<=M<=60, 1<=N<=30
c***********************************************************************
      IMPLICIT REAL*8 (C-H,O-Z)
      INTEGER A,B
C
      A=2*(M/2)-M+2
      B=2*(N/2)-N+2
      W=X*M/N
      Z=1.D0/(1+W)
      IF (A .NE. 1) GO TO 20
        IF (B .NE. 1) GO TO 10
          P=SQRT(W)
          Y=0.31830988618379067154                  ! 1/PI
          D=Y*Z/P
          P=2*Y*ATAN(P)
          GO TO 50
 10       P=SQRT(W*Z)
          D=P*Z/W*0.5
          GO TO 50
 20     IF (B .NE. 1) GO TO 30
          P=SQRT(Z)
          D=0.5*Z*P
          P=1.D0-P
          GO TO 50
 30       D=Z*Z
          P=W*Z
 50   Y=2*W/Z
      IF (A .NE. 1) GO TO 70
        DO 60 J=B+2,N,2
          D=(1+A/(J-2.D0))*D*Z
          P=P+D*Y/(J-1)
 60     CONTINUE
        GO TO 80
 70     ZK=Z**((N-1.D0)/2)
        D=D*ZK*N/B
        P=P*ZK + W*Z*(ZK-1)/(Z-1)
 80   Y=W*Z
      Z=2/Z
      B=N-2
      DO 90 I=A+2,M,2
        J=I+B
        D=Y*D*J/(I-2.D0)
        P=P-Z*D/J
 90   CONTINUE
      FISHER=P
      RETURN
      END

