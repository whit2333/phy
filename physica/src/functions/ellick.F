      REAL*8 FUNCTION ELLICK(X)
C     ****************************************************************
C     ELLICK is the complete elliptic integral of the first kind
C     ELLICK(X)= INTEGRAL(1/sqrt(1-(X*sin(t))**2))dt, limits 0 to pi/2
C
C     ELLICK is a special case of the INCOMPLETE ELLIPTIC INTEGRAL
C        ELLICK(X) = FINELL(X,pi/2)
C     REFERENCE: CERN library routine C308
C     ****************************************************************
C     RESTRICITION ON ARGUMENT X: ABS(X) < 1,
C       if ABS(X) >=1, ELLICK is set to zero with an error message
C     ****************************************************************
C     ACCURACY: IN GENERAL TO AT LEAST 1.0E-6
C       REGION TESTED : 0.0 <= X < 1.00
C     ****************************************************************
C
      IMPLICIT REAL*8 (A-H, O-Z)
      DIMENSION A(4),B(4)
      DATA N /4/
      DATA C /1.38629 44/
      DATA A
     1/1.45133 86E-2, 3.74253 96E-2, 3.58998 01E-2, 9.66633 84E-2/
      DATA B
     1/4.41839 82E-3, 3.32852 10E-2, 6.88029 55E-2, 1.24985 95E-1/
      DATA IF/0/, NF/10/
C
C  START
      IF (ABS(X) .GE. 1.0) GO TO 2
      ETA = 1.0 - X**2
      PA= A(1)
      PB= B(1)
      DO 1 I = 2,N
        PA= PA*ETA + A(I)
    1   PB=PB*ETA + B(I)
      ELLICK= C+ PA*ETA - LOG(ETA)*(0.5+PB*ETA)
      RETURN
C
    2 ELLICK=0.0
      IF=IF+1
      IF (IF .LE. NF) PRINT 100, X
      RETURN
C
  100 FORMAT(1X,27HELLICK ... ILLEGAL ARGUMENT,E20.10)
      END
 
 
 
