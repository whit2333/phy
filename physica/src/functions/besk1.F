      DOUBLE PRECISION FUNCTION BESK1 (X)
C***********************************************************************
C   REFERENCE: SLATEC LIBRARY; CATEGORY B5J
C***PURPOSE
C  BESK1 COMPUTES THE MODIFIED (HYPERBOLIC) BESSEL FUNCTION OF THIRD
C    KIND OF ORDER ONE AT X, WHERE X .GT. 0.
C
C   SERIES FOR BK1     ON THE INTERVAL  0.          TO  4.00000E+00
C                                 WITH WEIGHTED ERROR   9.16E-32
C                                  LOG WEIGHTED ERROR  31.04
C                        SIGNIFICANT FIGURES REQUIRED  30.61
C                             DECIMAL PLACES REQUIRED  31.64
C
C***EXTERNAL ROUTINES CALLED : INITDS,EBESK1,BESI1,DCSEVL,D1MACH
C***********************************************************************
C     ARGUMENT:  overflow @ X ~< 0.594E-38
C               underflow @ X ~> 86.492
C***********************************************************************
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION BK1CS(16)
C
      DATA BK1 CS(  1) / +.2530022733 8947770532 5311208685 33 D-1     /
      DATA BK1 CS(  2) / -.3531559607 7654487566 7238316918 01 D+0     /
      DATA BK1 CS(  3) / -.1226111808 2265714823 4790679300 42 D+0     /
      DATA BK1 CS(  4) / -.6975723859 6398643501 8129202960 83 D-2     /
      DATA BK1 CS(  5) / -.1730288957 5130520630 1765073689 79 D-3     /
      DATA BK1 CS(  6) / -.2433406141 5659682349 6007350301 64 D-5     /
      DATA BK1 CS(  7) / -.2213387630 7347258558 3152525451 26 D-7     /
      DATA BK1 CS(  8) / -.1411488392 6335277610 9583302126 08 D-9     /
      DATA BK1 CS(  9) / -.6666901694 1993290060 8537512643 73 D-12    /
      DATA BK1 CS( 10) / -.2427449850 5193659339 2631968648 53 D-14    /
      DATA BK1 CS( 11) / -.7023863479 3862875971 7837971200 00 D-17    /
      DATA BK1 CS( 12) / -.1654327515 5100994675 4910293333 33 D-19    /
      DATA BK1 CS( 13) / -.3233834745 9944491991 8933333333 33 D-22    /
      DATA BK1 CS( 14) / -.5331275052 9265274999 4666666666 66 D-25    /
      DATA BK1 CS( 15) / -.7513040716 2157226666 6666666666 66 D-28    /
      DATA BK1 CS( 16) / -.9155085717 6541866666 6666666666 66 D-31    /
      DATA NTK1, XMIN, XSML, XMAX / 0, 3*0.D0 /
C
      BESK1 = 0.0D0
      IF (NTK1.NE.0) GO TO 10
      NTK1 = INITDS (BK1CS, 16, 0.1D0*D1MACH(3))
      XMIN = DEXP (DMAX1(DLOG(D1MACH(1)), -DLOG(D1MACH(2))) + 0.01D0)
      XSML = DSQRT (4.0D0*D1MACH(3))
      XMAX = -DLOG(D1MACH(1))
      XMAX = XMAX - 0.5D0*XMAX*DLOG(XMAX)/(XMAX+0.5D0)
C
 10   IF (X.GT.0.D0) GO TO 100
        WRITE(6,*) '  BESK1 -- X IS ZERO OR NEGATIVE'
        RETURN
100   IF (X.GT.2.0D0) GO TO 20
C
      IF (X.GE.XMIN) GO TO 110
        WRITE(6,*) '  BESK1 -- X SO SMALL K1 OVERFLOWS'
        RETURN
110   Y = 0.D0
      IF (X.GT.XSML) Y = X*X
      BESK1 = DLOG(0.5D0*X)*BESI1(X) + (0.75D0 + DCSEVL (.5D0*Y-1.D0,
     1  BK1CS, NTK1))/X
      RETURN
C
 20   BESK1 = 0.D0
      IF (X.LE.XMAX) GO TO 120
        WRITE(6,*) '  BESK1 -- X SO BIG K1 UNDERFLOWS'
        RETURN
120   IF (X.GT.XMAX) RETURN
C
      BESK1 = DEXP(-X) * EBESK1(X)
      RETURN
      END
