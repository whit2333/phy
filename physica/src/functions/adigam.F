      REAL*8 FUNCTION ADIGAM(X)
C
C     ****************************************************************
C     ADIGAM IS THE DIGAMMA FUNCTION (PSI FUNCTION)
C       PSI(X) = d(log GAM(X))/dx , where GAM(X) is the GAMMA FUNCTION
C
C     REFERENCE : CERN library routine C317
C     *****************************************************************
C     RESTRICTION ON X : X IS REAL BUT X # -N (N = 0,1,2,...)
C       if X = -N, ADIGAM is set to 0 with error message
C
C     ACCURACY : to at least 1E-6.
C       Accuracy is lost near zeros of PSI, these zeros are all negative
C         except one at x=1.461633...
C       REGION TESTED: 1.000 <= X <= 2.000 for real X,
C                      2 <= X <= 101 for integer X.
C     ******************************************************************
C
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION B(6)
      DATA PI /3.14159 26535 89793/
      DATA B /+8.33333 33333E-2, -8.33333 33333E-3,
     1        +3.96825 39683E-3, -4.16666 66667E-3,
     2        +7.57575 75758E-3, -2.10927 96093E-2/
C
C   START
      A=ABS(X)
      IF(-A .EQ.AINT(X)) GO TO 4
      V=A
      H=0.
      IF(A .GE. 15.0) GO TO 3
      N=14-AINT(A)
      H=1.0/V
      IF(N .EQ. 0) GO TO 2
      DO 1 I = 1,N
      V=V+1.0
    1 H=H+1.0/V
    2 V=V+1.0
C
C   FOR X >=15, ASYMPTOTIC EXPANSION IS USED
    3 R=1.0/V**2
      ADIGAM=LOG(V)-0.5/V-R*(B(1)+R*(B(2)+R*(B(3)+R*(B(4)+R*(B(5)+R*
     1 (B(6)+R*B(1)))))))-H
      IF(X .GE. 0.0) RETURN
      H=PI*A
      ADIGAM=ADIGAM+1.0/A+PI*COS(H)/SIN(H)
      RETURN
    4 PRINT 100,X
      ADIGAM=0.
      RETURN
  100 FORMAT(1X,46HADIGAM ... ARGUMENT IS NON-POSITIVE INTEGER = ,F20.2)
C
      END
 
