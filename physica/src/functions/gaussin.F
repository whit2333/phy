      DOUBLE PRECISION FUNCTION GAUSSIN(P)
C    *************************************************************
C    THIS ROUTINE COMPUTES THE INVERSE OF THE DISTRIBUTION
C    FUNCTION OF THE STANDARD NORMAL DISTRIBUTION.
C     THE ROUTINE WAS WRITTEN BY S.W. CUNNINGHAM
C
C    REFRENCE: CERN library routine C301
C      also: S.W.CUNNINGHAM Algorithm AS24-from APPLIED STATISTICS,
C            NORMAL INTEGRAL TO DEVIATE.
C    *************************************************************
C
C    RESTRICTION ON P(the argument): 0<= P <=1
C      otherwise an error message will result
C      N.B. If P=0, GAUSSIN is set arbitrarily to -1E10 (-infinity)
C           If P=1,   "      "        "          +1E10 (+infinity)
C    *************************************************************
C    ACCURACY : TO AT LEAST 1.0D-8
C    *************************************************************
C
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION A(5),CONNOR(17),HSINGS(6)
      DATA RTHFPI,RRT2PI,TERMIN/1.2533141373,0.3989422804,1.D-11/
      DATA CONNOR
     1/8.0327350124D-17, 1.4483264644D-15, 2.4668270103D-14,
     23.9554295164D-13,  5.9477940136D-12, 8.3507027951D-11,
     31.0892221037D-9,   1.3122532964D-8,  1.4503852223D-7,
     41.4589169001D-6,   1.3227513228D-5,  1.0683760684D-4,
     57.5757575758D-4,   4.6296296296D-3,  2.3809523810D-2,
     60.1,               0.333333333333/
      DATA HSINGS
     1/2.515517, 0.802853, 0.010328, 1.432788, 0.189269, 0.001308/
C
C    START
C
      GAUSSIN = 0.0D0

      IF(P .LT. 0 .OR. P .GT. 1) GO TO 8
      IF (P .NE. 0.) GO TO 10
        GAUSSIN=-1.0E10
        RETURN
  10  IF (P .NE. 1.) GO TO 11
        GAUSSIN=1.0E10
        RETURN
  11  CONTINUE
C
C    GET FIRST APPROXIMATION BY HASTINGS FORMULA
      B=P
      IF(B .GT. 0.5) B=1.-B
      F=-LOG(B)
      E=SQRT(F+F)
      XO=-E+((HSINGS(3)*E+HSINGS(2))*E+HSINGS(1))/(((HSINGS(6)*E+HSINGS
     1(5))*E+HSINGS(4))*E+1.)
      IF(XO .LT. 0.) GO TO 1
      XO=0.
      PO=0.5
      X1=-RTHFPI
      GO TO 7
C
C    FIND THE AREA CORRESPONDING TO X0
    1 Y=XO*XO
      IF(XO .LE. -1.9) GO TO 3
      Y=-0.5*Y
C
C    SERIES APPROXIMATION
      PO=CONNOR(1)
      DO 2 L=2,17
    2 PO=PO*Y+CONNOR(L)
      PO=(PO*Y+1.)*XO
      X1=-(PO+RTHFPI)*EXP(-Y)
      PO=PO*RRT2PI+0.5
      GO TO 7
C
C    CONTINUED FRACTION APPROXIMATION
    3 Z=1./Y
      A(2)=1.
      A(3)=1.
      A(4)=Z+1.
      A(5)=1.
      W=2.
    4 CONTINUE
      DO 6 L=1,3,2
      DO 5 J=1,2
      K=L+J
      KA=7-K
    5 A(K)=A(KA)+A(K)*W*Z
    6 W=W+1.
      APPRXU=A(2)/A(3)
      APPRXL=A(5)/A(4)
      C=APPRXU-APPRXL
      IF(C .GE. TERMIN) GO TO 4
      X1=APPRXL/XO
      PO=-X1*RRT2PI*EXP(-0.5*Y)
C
C    GET ACCURATE VALUE BY TAYLOR EXPANSION
    7 D=F+DLOG(PO)
      X2=XO*X1*X1-X1
      X3=(X1*X1+2.*XO*X2)*X1-X2
      X=((X3/3.*D+X2)*D*0.5+X1)*D+XO
      IF(P .GT. 0.5) X=-X
      GAUSSIN=X
      RETURN
    8 WRITE(*,9)P
      RETURN
    9 FORMAT(10X,'ARGUMENT P=',E15.5,' IN GAUSSIN ILLEGAL')
      END
