      DOUBLE PRECISION FUNCTION D9LGIC (A, X, ALX)
C*********************************************************************
C   REFERENCE: SLATEC LIBRARY; CATEGORY B5F
C***PURPOSE
C   COMPUTE THE LOG COMPLEMENTARY INCOMPLETE GAMMA FUNCTION FOR LARGE X
C      AND FOR A .LE. X.
C
C***EXTERNAL ROUTINES CALLED : D1MACH
C**********************************************************************
      IMPLICIT REAL*8 (A-H,O-Z)
      DATA EPS / 0.D0 /
C
      D9LGIC = 0.0D0

      IF (X.GT.0.D0 .AND. A.LE.X) GO TO 100
        WRITE(6,*) '  D9LGIC -- X SHOULD > 0 AND >= A'
        RETURN
100   IF (EPS.EQ.0.D0)    EPS = 0.5D0*D1MACH(3)
      XPA = X + 1.0D0 - A
      XMA = X - 1.D0 - A
      R = 0.D0
      P = 1.D0
      S = P
      DO 10 K=1,300
        FK = K
        T = FK*(A-FK)*(1.D0+R)
        R = -T/((XMA+2.D0*FK)*(XPA+2.D0*FK)+T)
        P = R*P
        S = S + P
        IF (DABS(P).LT.EPS*S) GO TO 20
 10   CONTINUE
      WRITE(6,*)
     # ' D9LGIC -- NO CONVERGENCE IN 300 TERMS OF CONTINUED FRACTION'
      RETURN
C
 20   D9LGIC = A*ALX - X + DLOG(S/XPA)
      RETURN
      END

