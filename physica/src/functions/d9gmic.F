      DOUBLE PRECISION FUNCTION D9GMIC (A, X, ALX)
C**********************************************************************
C   REFERENCE: SLATEC LIBRARY; CATEGORY B5F
C***PURPOSE
C   COMPUTE THE COMPLEMENTARY INCOMPLETE GAMMA FUNCTION FOR A NEAR
C     A NEGATIVE INTEGER AND FOR SMALL X.
C
C***EXTERNAL ROUTINES CALLED: DLNGAM,D1MACH
C**********************************************************************
      IMPLICIT REAL*8 (A-H,O-Z)
      DATA EULER / 0.5772156649 0153286060 6512090082 40 D0 /
      DATA EPS, BOT / 2*0.D0 /
C
      D9GMIC = 0.0D0

      IF (EPS.NE.0.D0) GO TO 10
      EPS = 0.5D0*D1MACH(3)
      BOT = DLOG (D1MACH(1))
C
 10   IF (A.LE.0.D0) GO TO 100
        WRITE(6,*) '  D9GMIC -- A MUST BE NEAR A NEGATIVE INTEGER'
        RETURN
100   IF (X.GT.0.D0) GO TO 110
        WRITE(6,*) '  D9GMIC -- X MUST BE > ZERO'
        RETURN
C
110   M = -(A - 0.5D0)
      FM = M
      TE = 1.0D0
      T = 1.0D0
      S = T
      DO 20 K=1,200
        FKP1 = K + 1
        TE = -X*TE/(FM+FKP1)
        T = TE/FKP1
        S = S + T
        IF (DABS(T).LT.EPS*S) GO TO 30
 20   CONTINUE
      WRITE(6,*)
     # ' D9GMIC -- NO CONVERGENCE IN 200 TERMS OF CONTINUED FRACTION'
      RETURN
C
 30   D9GMIC = -ALX - EULER + X*S/(FM+1.0D0)
      IF (M.EQ.0) RETURN
C
      IF (M.EQ.1) D9GMIC = -D9GMIC - 1.D0 + 1.D0/X
      IF (M.EQ.1) RETURN
C
      TE = FM
      T = 1.D0
      S = T
      MM1 = M - 1
      DO 40 K=1,MM1
        FK = K
        TE = -X*TE/FK
        T = TE/(FM-FK)
        S = S + T
        IF (DABS(T).LT.EPS*DABS(S)) GO TO 50
 40   CONTINUE
C
 50   DO 60 K=1,M
        D9GMIC = D9GMIC + 1.0D0/DBLE(FLOAT(K))
 60   CONTINUE
C
      SGNG = 1.0D0
      IF (MOD(M,2).EQ.1) SGNG = -1.0D0
      ALNG = DLOG(D9GMIC) - DLNGAM(FM+1.D0)
C
      D9GMIC = 0.D0
      IF (ALNG.GT.BOT) D9GMIC = SGNG * DEXP(ALNG)
      IF (S.NE.0.D0) D9GMIC = D9GMIC +
     1  DSIGN (DEXP(-FM*ALX+DLOG(DABS(S)/FM)), S)
C
      IF (D9GMIC.NE.0.D0 .OR. S.NE.0.D0) GO TO 120
        WRITE(6,*) '  D9GMIC -- RESULT UNDERFLOWS'
        RETURN
120   END

