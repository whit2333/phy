      SUBROUTINE RESTORE_YBOS( IUN, FNAME, IFLAG, * )

C  This routine is intended only for ALPHA-OSF1
C  IFLAG = 0  means restore histograms
C  IFLAG = 1  means restore scatterer points (dots)

C Here is the new include file......everything else stays the same. Can you let
C me know when you have completed the compilations ?
C Oh, I am not sure how you map to the shared memory segment or
C wether you call malloc, but the new structure size is
C      parameter (ybs_structure_size =  3230617)
C  Gertjan
c
c Addition Jan'94(GJH)  added another equivalence to YBS.JW, YBS.RW to
c        access individual bytes. This should not affect anything.
c      
c Addition: 26-Oct-93 (KJR) the /off_tests/ structure used by dotio and
c           programs that call that subroutine (dot, ...) was included
c           at the end of the YBS structure and called /dot_extra_st/.
c           This change necessitated an increase in the ybs_structure_size.

c NOTE: (KJR). The size of the YBS structure is specified as the parameter
c    "ybs_structure_size". Any additions to the structure must be reflected
c    in a corresponding change to this parameter.
c
      integer ybs_structure_size
c     parameter (ybs_structure_size =  2290149)
c     parameter (ybs_structure_size =  2530945)
c     parameter (ybs_structure_size =  2531745)
c     parameter (ybs_structure_size =  2788613)
      parameter (ybs_structure_size =  3230617)
c
c NOTE: (GJH).   Note that any new additions to the
c    structure must be declared between the variables
c    begin_of_ybs_strucuture   and  end_of_ybs_strucuture
c    You can run the program ybs_size to find out the size of the
c    structure. Basically, in fortran it is given by
c    start= %LOC(ybs.begin_of_ybs_structure)
c    end  = %LOC(ybs.end_of_ybs_structure)
c    size = end-start+1          in BYTES
c
c    Revised  25-Oct-94 to allow more scalers and more dots.
      
      integer*4 ybos_size
      parameter (ybos_size=24000)        ! size of basic ybos array
c      
      integer test_size
      parameter (test_size = 1000)        ! number of different tests
c
      integer test_room
      parameter (test_room = 7500)  ! size of array containing tests defs.
c      
      integer test_counters
      parameter (test_counters = 1000) ! size of the array containing test 
c                                      ! counters
      integer number_blocks
      parameter (number_blocks = 20)   ! Number of test blocks
c      
      integer data_size
      parameter (data_size = 17000)    ! Number of different data symbols
c
      integer num_banks
      parameter (num_banks = 200)      ! Number of different banks
c
      integer n_scal,n_bins,n_histos
      parameter (n_scal = 300)         ! Number of scalers
      parameter (n_bins = 160000)      ! Number of bins available for histos
      parameter (n_histos = 2000)      ! Number of histograms
c
      integer user_size
      parameter (user_size = 2000)     ! size of user arrays
c
      integer calib_size                
      parameter (calib_size = 10000)   ! size of calib and cons arrays
c
      integer n_dots
      parameter (n_dots = 100)         ! number of dot plots
c
      integer n_dot_bins
      parameter (n_dot_bins = 150000)  ! total number of dots plotted
c
      integer n_dplot_cols
      parameter (n_dplot_cols = 4)     ! number of dotplot colours
c     
c  Here are the components of the /test_stuff/ data structure.
c     
      structure /test_stuff/
      character*16 tname               ! variable containing the test name
      integer*4 res_tests              ! Number of tests reserved in this test
      integer*4 num_tests              ! Actual number of true tests
      integer*4 test_pos               ! position in true_test array of results
      integer*4 count_pos              ! position in counters array of counter
      integer*4 test_info              ! position in test_defs array of def.
      integer*4 low_test               ! smallest identifier in a vector test
      end structure
c
c  Here are the components of the /data_symbol/ data structure
c      
      structure /data_symbol/
      character*16 dname             ! Variable containing data symbol name
      character*4 bankname           ! Bank name of this data symbol
      character*1 data_typ           ! Data type of this symbol
      integer*4 raw_ident            ! Identifier of this symbol
      integer*4 loc_in_bank          ! location in the bank of this symbol
      integer*4 loc_in_data          ! location in ybos array of this symbol
      integer*4 bank_loc         ! location in bank structure of symbols' bank
      end structure
c
c  Bank structure
c
      structure /bank_stuff/
      integer*4 bank_names          ! Bank name
      integer*4 bank_size           ! Size of this bank
      integer*4 bank_mask           ! Mask on this bank
      integer*4 bank_shift          ! shift on this bank
      integer*4 bank_pointers   !location in symbol structure of first element 
      integer*4 ybos_pointer        ! location in ybos array of this bank
      integer*4 dyn_size            ! actual size of the ybos bank
      end structure
c
c Calibration data structure
c
      structure /cal_stuff/
      character*16 cal_name       ! name of block of calibration data
      integer*4 cons_pointer      ! pointer to the start of data in cons array
      integer*4 cal_size          ! size of block of calibration data
      end structure
c
c Histogram structure      
c
      structure /histo_stuff/
      real*4 xlow               ! low cut of this histogram
      real*4 xhi                ! high cut of this histogram
      real*4 xunder             ! underflow counter
      real*4 xover              ! overflow counter
      integer*4 test_pointer    ! location in test structure of test on histo
      integer*4 data_pointer    ! location in symbol structure of data in histo
      integer*4 n_bins          ! number of bins in the histo
      integer*4 start_address   ! first bin in histo array
      integer*4 weight_pointer !location in symbol structure of weight of histo
      character*16 title_name   ! title of histo
      character*16 title_desc   ! 
      character*16 test_name    ! test name on histo
      character*16 data_name    ! data name of histo
      character*16 histo_type   ! histogram type
      end structure
c
c  dot plot colour substructure
c
      structure /col_stuff/
      integer*4 col_test         ! index of test for this colour
      integer*4 col_def          ! number of bins reserevd for this colour
      integer*4 col_pointer      ! pointer to first bin of this colour
      integer*4 col_off          ! offset from pointer of next free bin
      end structure
c
c dot plot main structure
c      
      structure /dot_stuff/
      character*16 dot_name      ! name of the dotplot
      character*4 dot_type       ! defines the type of variables of the dotplot
      integer*4 x_pointer        ! pointer to x-variable
      integer*4 y_pointer        ! pointer to y-variable
      real*4 x_low               ! minimum value of x-variable
      real*4 x_high              ! maximum value of x-variable
      real*4 y_low               ! minimum value of y-variable
      real*4 y_high              ! maximum value of y-variable
      integer*4 over_all         ! pointer to the overall_test of the dotplot
      integer*4 n_colours        ! number of colours defined in this dotplot
      record /col_stuff/ col_st(n_dplot_cols)        ! colour subsructure
      end structure
c
c names of tests and x and y variables of each dot plot    
c
      structure /dot_extra_st/
        character*16 x_symbol
        character*16 y_symbol
        character*16 stest_all
        character*16 sdot_name
        character*16 sctest(n_dplot_cols)
      end structure
c     
c  This is the code for the full SUSIYBOS data structure
c
      structure /ybos_all/
      
c dummy variables to indicate size

      integer*4 begin_of_ybs_structure
      
      union     ! YBOS structures
         map
            integer*4 jw(ybos_size)       ! YBOS array
         end map
         map
            integer*2 jw2(2*ybos_size)    ! integer*2 YBOS ayyray
         end map
         map
            real*4  rw(ybos_size)         ! array for access to real*4
         end map
         map
            character*1 cw(4*ybos_size)   ! array for access to bytes.
         end map
      end union
c
      record /test_stuff/ test_st(test_size)   ! test structure
      integer*4 true_tests(test_room)          ! array containing true tests
      integer*4 number_true                    ! number of true tests
      integer*4 counters(test_counters)        ! array containing counters
      integer*4 number_counters                ! number of counters
      integer*4 test_defs(test_room)           ! array containing test defs.
      integer*4 number_defs                    !next free position in test_defs
      integer*4 order_tests(test_size)         !Array giving alphabetic order
      integer*4 blocks(number_blocks) !loc. in test struc. of 1st test of block
      integer*4 num_blocks                     ! number of blocks defined
c
      record /data_symbol/ data_sym(data_size) ! data symbol structure
      integer*4 order(data_size)               ! array giving alphabetic order
      integer*4 hasha(128)                     ! hash array
      integer*4 num_order                      ! number of entries in order
      integer*4 tot_symbols                    ! number of symbols defined
      integer*4 sym_pointers(data_size)        !
      integer*4 num_pointers                   !
c
      record /bank_stuff/ bank_st(num_banks)   ! Bank structure
      integer*4 number_banks                   ! number of defined banks
c     
      record /cal_stuff/ cal_st(num_banks)     ! Calibration data structure
      integer*4 number_blocks                  ! number of calibration blocks
c
      record /histo_stuff/ histo_st(n_histos)  ! histo structure
      real*4  	scl(n_scal)                    ! accumlated scaler array
      real*4  	cur_scl(n_scal)                ! current scaler array
      real*4    num_scaler                     ! number of defined scalers
      logical*1	new_scaler_f                   ! flag if scalers are present
      real*4	histo(n_bins)                  ! histograms
      integer*4 number_bins                    ! number of defined bins
      integer*4 number_histos                  ! number of defined histos
c
      record /dot_stuff/ dot_st(n_dots)    ! dotplot structure
      real*4 xdot(n_dot_bins)              ! x-values of dotplots
      real*4 ydot(n_dot_bins)              ! y-values of dotplots
      integer*4 number_dplots              ! number of defined dotplots
      integer*4 number_dbins               ! number of defined dotplot bins
c     
c  Here are the process control data
c
      
      integer*4 anal_pid          ! process id number
      character*16 expt_name      ! experiment name
      character*16 batch          ! variable for foreground/background
      character*16 geant_mode     ! geant_map
      character*16 isuspend       ! variable to suspend data analysis
      character*16 norm_test      ! normalization test
      character*80 data_input     ! present data source file
      character*80 data_input_list(20) !  list of data source files to analyze
      character*80 data_output1   ! output file 1
      character*80 data_output2   ! output file 2
      character*80 data_output3   ! output file 3
      character*80 data_output4   ! output file 4
      character*80 targ_mat       ! target material
      real*4 targ_pol             ! target polarization
      real*4 beam_mom             ! beam momentum
      real*4 beam_mom_width       ! beam momentum spread
      character*80 beam_part      ! beam particle
      real*4 chaos_field          ! central magnetic field of CHAOS
      real*4 chaos_ang            ! rotation angle of CHAOS
      real*4 chaos_dist           ! CHAOS displacement from zero
      real*4 cpu_time             ! CPU time used by analyzer
      real*4 start_time           !
      character*80 trigger1       ! 1st level trigger description
      character*80 trigger2       ! 2nd level trigger description
      character*80 trigger3       ! 3rd level trigger description
      character*80 comment        ! comment
      integer*4 evnt_no           ! event number of current event
      integer*4 evnt_typ          ! event type of current event
      integer*4 experiment        ! experiment number
      integer*4 on_off_status     ! variable for online/offline switch
      integer*4 i_runno           ! present run number.
      integer*4 i_runno_list(20)  ! list of run numbers to analyze
      integer*4 i_analyzed        ! number of events seen by the analyzer
      integer*4 no_files_to_ana   ! no of runs to analyze in one go
      integer*4 inp_file_cnt      ! counter pointing to run no to analzye
      integer*4 n_evt_max         ! max. number of events to process
      integer*4 n_scl_rec         ! number of scaler events
      integer*4 n_scl_max         ! max. number of scaler events to process
      integer*4 bad_evt_type      ! number of bad events
      integer*4 llunit            ! ybos input unit number
      integer*4 out_unit1         ! ybos unit number for output channel 1
      integer*4 out_unit2         ! ybos unit number for output channel 2
      integer*4 out_unit3         ! ybos unit number for output channel 3
      integer*4 out_unit4         ! ybos unit number for output channel 4
      integer*4 bfield_map        ! number corrosponding to choice of maps
      logical*1 clhis             ! flag to clear histos at beginning of run
      logical*1 cldot             ! flag to clear dotplots at beginning of run
      logical*1 clscl             ! flag to clear scalers at beginning of run
      logical*1 cltst             ! flag to clear tests at beginning of run
      logical*1 clhit             ! flag to clear history at beginning of run
      logical*1 svhis             ! flag to save histos at end of run
      logical*1 svdot             ! flag to save dotplots at end of run
      logical*1 svscl             ! flag to save scalers at end of run
      logical*1 svtst             ! flag to save tests at end of run
      logical*1 svhit             ! flag to save history during run
      
c
c  Now user areas to interchange data through shared memory
c
      integer*4 user_i4(user_size)  
      real*4 user_r4(user_size)
      logical*1 user_l1(user_size)
c
c  Next some user arrays for variables not in YBOS structures
c
      real*4 cons(calib_size)
      real*4 calib(calib_size)
c
c  The extra dot stuff (KJR 26-Oct-93)
c
      record /dot_extra_st/ dot_extra(n_dots)
c  End of mamoth stucture
      logical*1 end_of_ybs_structure
        
      end structure
        record /ybos_all/ ybs

      CHARACTER*(*) FNAME
      INTEGER*4     IUN, IFLAG

      CHARACTER*15 SOUT
      CHARACTER*12 DAT
      CHARACTER*8  TIM
      CHARACTER*1  BS
      INTEGER*4    IDRAVER, LN, LENS, I, J
      REAL*4       FRED
      DATA BS /92/
CCC 
      READ(IUN,ERR=91,END=991)IDRAVER
      IF( IDRAVER .NE. 222 )THEN
        CALL INTEGER_TO_CHAR(IDRAVER,.FALSE.,SOUT,LENS)
        CALL ERROR_MESSAGE('RESTORE'//BS//'YBOS'
     &   ,'file: '//FNAME//' has wrong version number: '//SOUT(1:LENS))
        RETURN 1
      END IF
      IF( IFLAG .EQ. 1 )GO TO 10

C  restoring histograms

      READ(IUN,ERR=91,END=991)TIM                     ! time
      READ(IUN,ERR=91,END=991)DAT                     ! date
      READ(IUN,ERR=91,END=991)YBS.I_RUNNO             ! run number
      READ(IUN,ERR=91,END=991)YBS.I_ANALYZED          ! events analyzed
      READ(IUN,ERR=91,END=991)FRED
      READ(IUN,ERR=91,END=991)YBS.NUMBER_HISTOS,YBS.NUMBER_BINS 
      READ(IUN,ERR=91,END=991)(YBS.HISTO_ST(I).XLOW,
     &          YBS.HISTO_ST(I).XHI,
     &          YBS.HISTO_ST(I).XUNDER,
     &          YBS.HISTO_ST(I).XOVER,
     &          YBS.HISTO_ST(I).TEST_POINTER,
     &          YBS.HISTO_ST(I).DATA_POINTER,
     &          YBS.HISTO_ST(I).N_BINS,
     &          YBS.HISTO_ST(I).START_ADDRESS,I=1,YBS.NUMBER_HISTOS)
      READ(IUN,ERR=91,END=991)(YBS.HISTO_ST(I).TITLE_NAME,
     &          YBS.HISTO_ST(I).TITLE_DESC,
     &          YBS.HISTO_ST(I).TEST_NAME,
     &          YBS.HISTO_ST(I).DATA_NAME,
     &          YBS.HISTO_ST(I).HISTO_TYPE,
     &          YBS.HISTO_ST(I).WEIGHT_POINTER,I=1,YBS.NUMBER_HISTOS)
      READ(IUN,ERR=91,END=991)(YBS.HISTO(I),I=1,YBS.NUMBER_BINS)
      CALL PUT_HYBOS( 'RESTORE', YBS, *92 )
      RETURN

C  restoring scattered points (dots)

   10 READ(IUN,ERR=91,END=991)YBS.I_RUNNO             ! run number
      READ(IUN,ERR=91,END=991)YBS.I_ANALYZED          ! events analyzed
      READ(IUN,ERR=91,END=991)YBS.NUMBER_DPLOTS,YBS.NUMBER_DBINS
      DO J = 1, YBS.NUMBER_DPLOTS
        READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).DOT_NAME
        READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).DOT_TYPE
        READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).X_POINTER
        READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).Y_POINTER
        READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).X_LOW
        READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).X_HIGH
        READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).Y_LOW
        READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).Y_HIGH
        READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).OVER_ALL
        READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).N_COLOURS
        DO K = 1, 4
          READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).COL_ST(K).COL_TEST
          READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).COL_ST(K).COL_DEF
          READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).COL_ST(K).COL_POINTER
          READ(IUN,ERR=91,END=991)YBS.DOT_ST(J).COL_ST(K).COL_OFF
        END DO
        READ(IUN,ERR=91,END=991)YBS.DOT_EXTRA(J).X_SYMBOL
        READ(IUN,ERR=91,END=991)YBS.DOT_EXTRA(J).Y_SYMBOL
        READ(IUN,ERR=91,END=991)YBS.DOT_EXTRA(J).STEST_ALL
        DO K = 1, 4
          READ(IUN,ERR=91,END=991)YBS.DOT_EXTRA(J).SCTEST(K)
        END DO
      END DO
      READ(IUN,ERR=91,END=991)
     & (YBS.XDOT(J),YBS.YDOT(J),J=1,YBS.NUMBER_DBINS)
      CALL PUT_DYBOS( 'RESTORE', YBS, *92 )
      RETURN 

   91 CALL ERROR_MESSAGE('RESTORE'//BS//'YBOS',
     & 'error reading file: '//FNAME)
   92 RETURN 1
  991 CALL ERROR_MESSAGE('RESTORE'//BS//'YBOS'
     & ,'end of file while reading: '//FNAME)
      RETURN 1
      END
