/* detach_shm.c -- detaches current process from existing shared memory *
 *              -- returns 0 on success, -1 on error.                   *
 *              -- also sets shared memory pointer to NULL on success   *
 *              -- and prints error message on error.                   */

#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>

detach_shm_(memptr)
     char **memptr;
{
  int istat;
  
  if( *memptr == NULL){
    printf("%%detach_shm-E, memory pointer is NULL\n");
    return -1;
  }
  if( (istat = shmdt(*memptr)) == -1)
    perror("%detach_shm-E, unable to detach from shared memory");
  else
    *memptr = NULL;
  return istat;
}
