/* map_to_shm.c -- returns the shared memory id and sets the passed *
 *                 pointer to the top of the shared memory.         */

#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ERR (char *) -1

char *map_to_shm_(n)
     int *n;
{
  int shmid;
  char *memaddr;
  key_t shmkey, ipckey(int segment_number);

  shmkey = ipckey(*n);

  /* get shared memory id */

  if( (shmid = shmget(shmkey, 0, 0)) < 0)
    printf("%%map_to_shm-E, shared memory does not exist.");

  /* attach to the shared memory segment */

  else if( (memaddr = shmat(shmid, 0, 0)) == ERR)
    printf("%%map_to_shm-E, unable to attach to shared memory.");
  else
    return memaddr;
  printf(" [Segment %d]\n", *n);
  exit(1);
}
