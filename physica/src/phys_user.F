c
c  this is a copy of physica_user_functions.for for use by
c  unix systems
c
      REAL*8 FUNCTION USER1(A)
      IMPLICIT REAL*8 (A-H,O-Z)
      USER1=A
      RETURN
      END

      REAL*8 FUNCTION USER2(A,B)
      IMPLICIT REAL*8 (A-H,O-Z)
      USER2=A+B
      RETURN
      END

      REAL*8 FUNCTION USER3(A,B,C)
      IMPLICIT REAL*8 (A-H,O-Z)
      USER3=A+B+C
      RETURN
      END

      REAL*8 FUNCTION USER4(A,B,C,D)
      IMPLICIT REAL*8 (A-H,O-Z)
      USER4=A+B+C+D
      RETURN
      END

      REAL*8 FUNCTION USER5(A)
      IMPLICIT REAL*8 (A-H,O-Z)
      USER5=5.0
      RETURN
      END

      REAL*8 FUNCTION USER6(A)
      IMPLICIT REAL*8 (A-H,O-Z)
      USER6=6.0
      RETURN
      END

      REAL*8 FUNCTION USER7(A,B)
      IMPLICIT REAL*8 (A-H,O-Z)
      SUM = 0.0
      DO I = 1, INT(A)
        SUM = SUM+B
      END DO
      USER7 = SUM
      RETURN
      END

      REAL*8 FUNCTION USER8(A)
      IMPLICIT REAL*8 (A-H,O-Z)
      USER8=8.0
      RETURN
      END

      SUBROUTINE SUB1(IATYPE,ICODE,IUPDATE,IER,X1,X2,ADIFF)
      INTEGER*4 IATYPE(15),ICODE(3,15),IUPDATE(15),IER
      REAL*8    ADIFF, X1(1), X2(1)

C   Determine if vector X1 is different than vector X2
C   If there is a difference set ADIFF = 1, otherwise set ADIFF = 0
CCC
      IF( IATYPE(1) .NE. 1 )GO TO 991
      IF( IATYPE(2) .NE. 1 )GO TO 992
      IF( IATYPE(3) .NE. 0 )GO TO 993
      IF( ICODE(1,1) .NE. ICODE(1,2) )GO TO 994
      IUPDATE(3) = 1     ! third argument to be updated
      ADIFF = 0.0D0
      DO I = 1, ICODE(1,1)
        IF( X1(I) .NE. X2(I) )THEN
          ADIFF = 1.0D0
          RETURN
        END IF
      END DO
      RETURN
 991  WRITE(*,*)' error in SUB1:  first argument is not a vector'
      IER = -1
      RETURN
 992  WRITE(*,*)' error in SUB1:  second argument is not a vector'
      IER = -1
      RETURN
 993  WRITE(*,*)' error in SUB1:  third argument is not a scalar'
      IER = -1
      RETURN
 994  WRITE(*,*)' error in SUB1:  input vectors not the same length'
      IER = -1
      RETURN

C  Setting IER to -1 indicates to PHYSICA that an error has occured in
C  the subroutine.  This is like an alternate RETURN. If you call this
C  subroutine in a macro command file, and the error occurs, the macro
C  will stop execution

      END

      SUBROUTINE SUB2(IATYPE,ICODE,IUPDATE,IER,XIN,XOUT,A)
      INTEGER*4 IATYPE(15),ICODE(3,15),IUPDATE(15),IER
      REAL*8    A, XIN(1), XOUT(1)

C   Divide a vector by a scalar and put the output in another vector
C   e.g., CALL SUB2 XIN XOUT A  and  XOUT will be XIN/A
CCC
      IF( IATYPE(1) .NE. 1 )GO TO 991
      IF( IATYPE(2) .NE. 1 )GO TO 992
      IF( IATYPE(3) .NE. 0 )GO TO 993
      IUPDATE(2) = 1     ! second argument to be updated
      NPTS = MIN(ICODE(1,1),ICODE(1,2))  ! use minimum length
      DO I = 1, NPTS
        IF( XIN(I) .GT. 100.0D0 )GO TO 994
        XOUT(I) = XIN(I)/A
      END DO
      RETURN
 991  WRITE(*,*)' error in SUB2:  first argument is not a vector'
      IER = -1
      RETURN
 992  WRITE(*,*)' error in SUB2:  second argument is not a vector'
      IER = -1
      RETURN
 993  WRITE(*,*)' error in SUB2:  third argument is not a scalar'
      IER = -1
      RETURN
 994  WRITE(*,*)' error in SUB2:  XIN(I) > 100'
      IER = -1
      RETURN
      END

      SUBROUTINE SUB3(IATYPE,ICODE,IUPDATE,IER,MATRIX,DIAGONAL)
      INTEGER*4 IATYPE(15),ICODE(3,15),IUPDATE(15),IER
      REAL*8 MATRIX(1), DIAGONAL(1)

C   Extract the diagonal from a square matrix and put it into a vector
C   e.g., CALL SUB3 MATRIX DIAGONAL
C        and  DIAGONAL will be MATRIX[J,J] for J = 1, #
CCC
      IF( IATYPE(1) .NE. 2 )GO TO 991
      IF( IATYPE(2) .NE. 1 )GO TO 992
      IUPDATE(2) = 1     ! second argument to be updated
      NROWS = ICODE(1,1)
      NCOLS = ICODE(2,1)
      IF( NCOLS .NE. NROWS )GO TO 993
      DO J = 1, NCOLS
        DIAGONAL(J) = MATRIX(J+(J-1)*NCOLS)
      END DO
      RETURN
 991  WRITE(*,*)' error in SUB3:  first argument is not a matrix'
      IER = -1
      RETURN
 992  WRITE(*,*)' error in SUB3:  second argument is not a vector'
      IER = -1
      RETURN
 993  WRITE(*,*)' Use a square matrix for SUB3'
      IER = -1
      RETURN
      END

      SUBROUTINE SUB4(IATYPE,ICODE,IUPDATE,IER,X,M,XOUT)
      INTEGER*4 IATYPE(15),ICODE(3,15),IUPDATE(15),IER
      REAL*8    M(1), X(1), XOUT(1)

C   Calculate XOUT = X<>M  (inner product)
C   where X is a vector, M is a matrix, XOUT is the output vector
CCC
      IF( IATYPE(1) .NE. 1 )GO TO 991
      IF( IATYPE(2) .NE. 2 )GO TO 992
      IF( IATYPE(3) .NE. 1 )GO TO 993
      IUPDATE(3) = 1     ! third argument to be updated
      NROWS = ICODE(1,2)
      NCOLS = ICODE(2,2)
      NUMX  = ICODE(1,1)
      NUMXO = ICODE(1,3)
      IF( NUMX .NE. NROWS )GO TO 994
      IF( NUMXO .NE. NCOLS )GO TO 995
      DO I = 1, NCOLS
        XOUT(I) = 0.0D0
        DO J = 1, NROWS
          XOUT(I) = XOUT(I) + X(J)*M(I+(J-1)*NCOLS)
        END DO
      END DO
      RETURN
 991  WRITE(*,*)' *** error in SUB4'
      WRITE(*,*)'     first argument is not a vector'
      IER = -1
      RETURN
 992  WRITE(*,*)' *** error in SUB4'
      WRITE(*,*)'     second argument is not a matrix'
      IER = -1
      RETURN
 993  WRITE(*,*)' *** error in SUB4'
      WRITE(*,*)'     third argument is not a vector'
      IER = -1
      RETURN
 994  WRITE(*,*)' *** error in SUB4'
      WRITE(*,*)'     vector length not equal to row dimension'//
     & ' of matrix'
      IER = -1
      RETURN
 995  WRITE(*,*)' *** error in SUB4'
      WRITE(*,*)'     output vector length not equal to column'//
     & ' dimension of matrix'
      IER = -1
      RETURN
      END

      SUBROUTINE SUB5(IATYPE,ICODE,IUPDATE,IER,LFILE,X,Y)
      INTEGER*4    IATYPE(15),ICODE(3,15),IUPDATE(15),IER
      INTEGER*4    LENF, IUNIT, I, NPTS
      REAL*8       X(1), Y(1), XDUM
      LOGICAL*4    LUOPEN
      BYTE         LFILE(1)
      CHARACTER*80 AFILE

C   Read a vector X from a file, multiply it by 5 and add it to vector Y
C   with the result put into X
C   e.g., CALL SUB5 `FILE.DAT' X Y
C         and X will be X*5+Y
CCC
      IF( IATYPE(1) .NE.-1 )GO TO 991
      IF( IATYPE(2) .NE. 1 )GO TO 992
      IF( IATYPE(3) .NE. 1 )GO TO 993
      IF( ICODE(1,1) .GT. 80 )GO TO 994
      IUPDATE(2) = 1     ! second argument to be updated
      LENF = ICODE(1,1)
      DO I = 1, LENF
        AFILE(I:I) = CHAR(LFILE(I))
      END DO
      DO 2 I = 30, 99
        INQUIRE(UNIT=I,OPENED=LUOPEN,ERR=2)
        IF( .NOT.LUOPEN )THEN
          IUNIT = I
          GO TO 4
        END IF
   2  CONTINUE
   4  OPEN(FILE=AFILE(1:LENF),UNIT=IUNIT,STATUS='OLD',ERR=995)
      I = 1
  10  READ(IUNIT,*,ERR=996,END=20)XDUM
      IF( I .GT. ICODE(1,2) )THEN
        WRITE(*,*)' message from SUB5'
        WRITE(*,*)' max. number of elements from the file has been read'
        WRITE(*,*)' but there is more that could be read'
        GO TO 20
      END IF
      X(I) = XDUM
      I = I+1
      GO TO 10
  20  NPTS = MIN(ICODE(1,2),ICODE(1,3))
      DO I = 1, NPTS
        X(I) = X(I)*5.0D0+Y(I)
      END DO
      ICODE(1,2) = NPTS  ! update the length of X
      RETURN
 991  WRITE(*,*)' *** error in SUB5'
      WRITE(*,*)'     first argument is not a text string'
      IER = -1
      RETURN
 992  WRITE(*,*)' *** error in SUB5'
      WRITE(*,*)'     second argument is not a vector'
      IER = -1
      RETURN
 993  WRITE(*,*)' *** error in SUB5'
      WRITE(*,*)'     third argument is not a vector'
      IER = -1
      RETURN
 994  WRITE(*,*)' *** error in SUB5'
      WRITE(*,*)'     text string is longer than 80 characters'
      IER = -1
      RETURN
 995  WRITE(*,*)' *** error in SUB5'
      WRITE(*,*)'     unable to open file: '//AFILE(1:LENF)
      IER = -1
      RETURN
 996  WRITE(*,*)' *** error in SUB5'
      WRITE(*,9961)I,AFILE(1:LENF)
9961  FORMAT('     reading line#',I3,' from file: ',A)
      IER = -1
      RETURN
      END

      SUBROUTINE SUB6(IATYPE,ICODE,IUPDATE,IER,X,Y)
      INTEGER*4 IATYPE(15),ICODE(3,15),IUPDATE(15),IER
      REAL*8 X(1), Y(1)

C   Multiply a vector by 6 and add it to another vector
C   e.g., CALL SUB6 X Y
C         and X will be X+6*Y
CCC
      IF( IATYPE(1) .NE. 1 )GO TO 991
      IF( IATYPE(2) .NE. 1 )GO TO 992
      IUPDATE(2) = 1     ! second argument to be updated
      NPTS = MIN(ICODE(1,1),ICODE(1,2))
      DO I = 1, NPTS
        X(I) = X(I)+Y(I)*6.0D0
      END DO
      RETURN
 991  WRITE(*,*)' *** error in SUB6'
      WRITE(*,*)'     first argument is not a vector'
      IER = -1
      RETURN
 992  WRITE(*,*)' *** error in SUB6'
      WRITE(*,*)'     second argument is not a vector'
      IER = -1
      RETURN
      END

      SUBROUTINE SUB7(IATYPE,ICODE,IUPDATE,IER,X,Y)
      INTEGER*4 IATYPE(15),ICODE(3,15),IUPDATE(15),IER
      REAL*8 X(1), Y(1)

C   Multiply a vector by 7 and add it to another vector
C   e.g., CALL SUB7 X Y
C         and X will be X+7*Y
CCC
      IF( IATYPE(1) .NE. 1 )GO TO 991
      IF( IATYPE(2) .NE. 1 )GO TO 992
      IUPDATE(2) = 1     ! second argument to be updated
      NPTS = MIN(ICODE(1,1),ICODE(1,2))
      DO I = 1, NPTS
        X(I) = X(I)+Y(I)*7.0D0
      END DO
      RETURN
 991  WRITE(*,*)' *** error in SUB7'
      WRITE(*,*)'     first argument is not a vector'
      IER = -1
      RETURN
 992  WRITE(*,*)' *** error in SUB7'
      WRITE(*,*)'     second argument is not a vector'
      IER = -1
      RETURN
      END

      SUBROUTINE SUB8(IATYPE,ICODE,IUPDATE,IER,X,Y)
      INTEGER*4 IATYPE(15),ICODE(3,15),IUPDATE(15),IER
      REAL*8 X(1), Y(1)

C   Multiply a vector by 8 and add it to another vector
C   e.g., CALL SUB8 X Y
C         and X will be X+8*Y
CCC
      IF( IATYPE(1) .NE. 1 )GO TO 991
      IF( IATYPE(2) .NE. 1 )GO TO 992
      IUPDATE(2) = 1     ! second argument to be updated
      NPTS = MIN(ICODE(1,1),ICODE(1,2))
      DO I = 1, NPTS
        X(I) = X(I)+Y(I)*8.0D0
      END DO
      RETURN
 991  WRITE(*,*)' *** error in SUB8'
      WRITE(*,*)'     first argument is not a vector'
      IER = -1
      RETURN
 992  WRITE(*,*)' *** error in SUB8'
      WRITE(*,*)'     second argument is not a vector'
      IER = -1
      RETURN
      END
