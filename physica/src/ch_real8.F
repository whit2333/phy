      SUBROUTINE CH_REAL8( INPUT, RESULT, * )
C
C  Converts character INPUT to REAL*8 number RESULT
C
      IMPLICIT NONE

      CHARACTER*(*) INPUT
      REAL*8        RESULT

C  local variables

      REAL*8       V8, DIV
      INTEGER*4    EXP, SIGN, EXPSGN, IFIRST, ILAST, NINPUT
     &            ,ICLASS, ICH, ISTATE
      LOGICAL*4    EXPRD 

C   Lexical scanner definitions (and actions) follow:
C
C  The class values used to define CTABLE are as follows:
C       Class       Characters
C         1         end_of_line (off end of input record)
C         2         invalid characters
C         3         blank, tab
C         4         D,E,d,e
C         5         0-9
C         6         .
C         7         +-
C         8         , (non-blank delimiter)
C
C   The following STABLE states are recognized:
C     -2: Successful read, ILAST left alone
C     -1: Successful read, ILAST= ILAST-1
C      0: Conversion error on read
C
C   The following ACTION states are recognized:
C     0: No action taken
C     1: Accumulate integer portion of number
C     3: Set sign
C     5: Accumulate decimal portion of number
C     7: Accumulate integer exponent
C     9: Set exponent sign

      INTEGER*4 CTABLE(0:127), STABLE(8,8), ACTION(8,8)
      DATA CTABLE
     &/  2,  2,  2,  2,  2,  2,  2,  2,  2,  3,  2,  2,  2,  2,  2,  2,
     &   2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,
     &   3,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  7,  8,  7,  6,  2,
     &   5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  2,  2,  2,  2,  2,  2,
     &   2,  2,  2,  2,  4,  4,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,
     &   2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,
     &   2,  2,  2,  2,  4,  4,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,
     &   2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2 /
      DATA STABLE
     & /-1,  0, -1,  0,  0, -1, -1,  0,   !End of line (EOL)
     &   0,  0,  0,  0,  0,  0,  0,  0,   !Invalid characters
     &   1,  0, -1,  0,  0, -1, -1,  0,   !Blank, tab
     &   0,  0,  5,  0,  0,  5,  0,  0,   !d,e,D,E
     &   3,  3,  3,  6,  7,  6,  7,  7,   !0-9
     &   4,  4,  6,  0,  0,  0,  0,  0,   !. (decimal point)
     &   2,  0,  0,  0,  8,  0,  0,  0,   !+-
     &  -2,  0, -2,  0,  0, -2, -2,  0/   !, (non-blank delimiter)
      DATA ACTION
     & / 0,  0,  0,  0,  0,  0,  0,  0,   !End of line (EOL)
     &   0,  0,  0,  0,  0,  0,  0,  0,   !Invalid characters
     &   0,  0,  0,  0,  0,  0,  0,  0,   !Blank
     &   0,  0,  0,  0,  0,  0,  0,  0,   !d,e,D,E
     &   1,  1,  1,  5,  7,  5,  7,  7,   !0-9
     &   0,  0,  0,  0,  0,  0,  0,  0,   !. (decimal point)
     &   3,  0,  0,  0,  9,  0,  0,  0,   !+-
     &   0,  0,  0,  0,  0,  0,  0,  0/   !,TAB (non-blank deli.)
C
C        S   S   I   D   S   R   R   E
C        t   i   n   e   t   e   e   x
C        a   g   t   c   a   a   a   p
C        r   n   e   i   r   l   l   o
C        t       g   m   t       +   n
C                e   a       n   e   e
C                r   l   e   u   x   n
C                        x   m   p   t
C                    p   p   b   o
C                    o   o   e   n   s
C                    i   n   r   e   i
C                    n   e       n   g
C                    t   n       t   n
C                        t
C
C   This lexical scanner scans a string containing an ASCII coded
C   arithmetic value.  ILAST will be updated to reflect the actual last
C   character scanned, from 1...LEN(INPUT). In the event of error, ILAST
C   will point to the location of the error.
C
C   Initially written by Arthur Haynes, TRIUMF U.B.C., January 3, 1980
C   Modified by Phil Bennett and Joe Chuma, August 7, 1984, to efficiently
C   handle real number I/O conversion.
C   Rewritten by Phil Bennett, April, 1989, to fully utilize CHARACTER
C   variables and expressions, as specified by the FORTRAN77 ANSI standard
C
C   Parameter Definitions:
C   --------- -----------
C
C   INPUT:  A CHARACTER variable containing an ASCII coded numerical value
C           to be interpreted.
C
C   ILAST:  The last character of INPUT actually scanned, which lies in the
C           range 1 to LEN(CH). In the event of error, ILAST will point to the
C           location of the character at fault.
C
C  ISTATE: The output state returned by CH_REAL8_SCAN. The possibilities are:
C          -2: Successful read
C          -1: Successful read, but decrement ILAST= ILAST-1
C           0: Conversion error
C
C  CTABLE: This is an INTEGER*4 table of codes specifying the class of each
C          ASCII character, where the table index is given by the ASCII code
C          decimal value. For example, the ASCII code for '2' is decimal
C          50, and since '2' belongs to the class of numeric digits (class 5),
C          the value of CTABLE(50) = 5.
C
C  STABLE: This is a two dimensional array containing the particular lexical
C          scanner, in this case, to decode the character string
C          representation of a numeric value.
C
C  ACTION: This is a matching INTEGER*4 array to STABLE, and should be
C          supplied if actions are to be taken corresponding to certain
C          transition states in the main scanner STABLE. This is useful for
C          CHREAL_SCAN since the decoding of a character numeric value
C          requires the accumulation of several counters, which are
C          conveniently done as the appropriate transition states are
C          encountered. If no action is desired upon a particular transition
C          (the usual situation) then a zero value should appear in the
C          corresponding position in the ACTION table. Actions to be taken
C          should be indexed by positive ODD numbers in the ACTION table
C          (i.e. 1,3,5,7...).
C
C  RESULT: The REAL*8 number returned as the converted value of the character
C          string INPUT.
C
C   Initialize the various accumulators needed for I/O conversion
CCC
      RESULT = 0.0D0
      V8 = 0.0D0
      DIV = 1.0D0
      EXP = 0
      SIGN = 1
      EXPSGN = 1
      EXPRD = .FALSE.

C   Initialize end of field pointer ILAST and state flag ISTATE

      IFIRST = 1
      ILAST  = 0
      NINPUT = LEN(INPUT)
      ISTATE = 1

C   Main loop: check the current state and if this is a transition state 
C   (i.e. the current field is not yet finished) then proceed with the next
C   input character; else end the field and return.
C   If this is an output state, then the conversion of the number is
C   complete and so the final evaluation is done.

  10  IF( ISTATE .LT. 0 )THEN          ! Output state: conversion completed 
        IF( ISTATE .EQ. -1 )ILAST = ILAST-1
        IF( EXPRD )THEN
          RESULT = FLOAT(SIGN)*V8*10.D0**(EXPSGN*EXP)
        ELSE
          RESULT = FLOAT(SIGN)*V8
        END IF
        RETURN
      ELSE IF( ISTATE .EQ. 0 )THEN     ! Error in converting number
        RETURN 1
      END IF

C   If 1 <= ISTATE < 8, then ISTATE is a transition state, and so the
C   scan pointer ILAST is incremented by 1.

      ILAST = ILAST+1

C   If we've exceeded the length of the input line , i.e. ILAST > NINPUT,
C   then ICLASS=1 (EOL), otherwise deduce the class of the current input
C   character from the CTABLE array.

      IF( ILAST .GT. NINPUT )THEN
        ICLASS = 1
        ISTATE = STABLE(ISTATE,ICLASS)
        GO TO 10
      END IF
      ICH = ICHAR( INPUT(ILAST:ILAST) )
      ICLASS = CTABLE(ICH)

C   Use the action table to flag necessary arithmetic actions during the
C   real number conversion. The appropriate actions necessary for the I/O
c   conversion of the real number follow.
      
      IF( ACTION(ISTATE,ICLASS) .EQ. 1 )THEN
        V8 = V8*10.0D0 + FLOAT(ICH-48)
      ELSE IF( ACTION(ISTATE,ICLASS) .EQ. 3 )THEN
        IF( ICH .EQ. 45 )SIGN = -1    !change sign if "-"(ascii 45) found
      ELSE IF( ACTION(ISTATE,ICLASS) .EQ. 5 )THEN
        DIV = DIV*0.1D0
        V8 = V8 + FLOAT(ICH-48)*DIV
      ELSE IF( ACTION(ISTATE,ICLASS) .EQ. 7 )THEN
        EXP = EXP*10 + ICH - 48
        EXPRD = .TRUE.
      ELSE IF( ACTION(ISTATE,ICLASS) .EQ. 9 )THEN
        IF( ICH .EQ. 45 )EXPSGN = -1    !change sign if "-"
        EXPRD = .TRUE.
      END IF

C   Use the transition table STABLE to evaluate the new state given the
C   present state ISTATE and class ICLASS.

      ISTATE = STABLE(ISTATE,ICLASS)
      GO TO 10
      END
