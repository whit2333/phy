      SUBROUTINE RESTORE_MSR(LFDAT,FNAME,LENF,*)

      IMPLICIT INTEGER*2 (M) 
      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

C   Subroutine that reads MSR arrays from logical unit LFDAT
C   This unit should be assigned by the calling program

      CHARACTER*1024 FNAME
      CHARACTER*6   NAME
      CHARACTER*1   BS
      REAL*8        BININC
      INTEGER*4     SADDR

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      BYTE      L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

C...Designed for MSR Standard data architecture,  June 1978...   
C...Header information for histograms

      INTEGER NBINS, NEVTOT, NTPBIN, NT0, NT1, NT2
      INTEGER*2 MASKEV(2)
C     INTEGER IHIST

C...Raw Histogram Data converted to full INTEGER*4 for user: 

CC      COMMON /HIST/ IH(1)    

      INTEGER*4 ITEMP
      INTEGER*2 MTEMP(2), MDUM(16)
      EQUIVALENCE (ITEMP,MTEMP(1)) 
      CHARACTER*40 STRING
      BYTE         LTTL(40), LHTTL(12)
      INTEGER*4 IDX1(4), IDX2(4), IDX3(4)
      DATA IDX1 /0,0,0,0/, IDX2 /0,0,0,0/, IDX3 /0,0,0,0/

      INTEGER*2 MBUFF(256)   
      INTEGER*2 MRUN,MHIST,MSCLR,MSUPD,MMIN,MSEC,MDATE1(6),MDATE2(6)
     &         ,MLSTON(4),MCMCSC,MLOCSC(12),MRSTA,MLOGF(5),MUIC
     &         ,MSPARE(12)
      INTEGER   JTSC(18),JDSC(18),ICTASK,ISCLBL(18),KOMENT(36)
CC    INTEGER   ITTL(10)
CC      COMMON /INBUF/ MRUN,MHIST,MSCLR,MSUPD,JTSC(18),JDSC(18),   
CC     1 MMIN,MSEC,MDATE1(6),MDATE2(6),MLSTON(4),   
CC     2 MCMCSC,MLOCSC(12),MRSTA,ICTASK,MLOGF(5),   
CC     3 MUIC,MSPARE(12),   
CC     4 ITTL(10),ISCLBL(18),KOMENT(36)    
CC      COMMON /HEADER/ IHIST, NBINS, NEVTOT, NTPBIN, MASKEV(2),  
CC     1 NT0, NT1, NT2,   HTITLE(3)  
CCC
      BS = CHAR(92)  ! backslash

      READ(LFDAT,ERR=91,END=9999)
     & MRUN,MHIST,MSCLR,MSUPD,(JTSC(I),I=1,18)
     & ,(JDSC(I),I=1,18),MMIN,MSEC,(MDATE1(I),I=1,6),(MDATE2(I),I=1,6)
     & ,(MLSTON(I),I=1,4),MCMCSC,(MLOCSC(I),I=1,12),MRSTA,ICTASK
     & ,(MLOGF(I),I=1,5),MUIC,(MSPARE(I),I=1,12),(LTTL(I),I=1,40)
     & ,(ISCLBL(I),I=1,18),(KOMENT(I),I=1,36)    

      IRUN  = MRUN     ! run number
      NHIST = MHIST    ! number of histograms

C   Get global title
C   Substitute blanks for question marks in title

      NAME = 'TITLE'
      LN = 5
      LENT = 0
      DO I = 40, 1, -1
        IF( LTTL(I) .EQ. 63 )LTTL(I) = 32
      END DO
      DO I = 40, 1, -1
        IF( LTTL(I) .NE. 32 )THEN
          LENT = I
          GO TO 120
        END IF
      END DO
  120 DO I = 1, LENT
        STRING(I:I) = CHAR(LTTL(I))
      END DO
      NOLDS = 0
      IF( LENT .GT. 0 )THEN
        CALL CHSIZE(SADDR,NOLDS,LENT,1,*99)
        DO I = 1, LENT
          L1D(SADDR+I) = ICHAR(STRING(I:I))
        END DO
        CALL PUT_TEXT_VARIABLE( NAME(1:LN), 0, 0, IDX1
     &   ,SADDR, LENT, 'RESTORE'//BS//'MSR '//FNAME(1:LENF), *92 )
      END IF
      if( nhist .eq. 0 )then
        CALL ERROR_MESSAGE('RESTORE'//BS//'MSR'
     &   ,'number of histograms is zero')
        GO TO 92
      end if
      if( nhist .lt. 0 )then
        CALL ERROR_MESSAGE('RESTORE'//BS//'MSR'
     &   ,'number of histograms is less than zero')
        GO TO 92
      end if
      CALL CHSIZE(ITOTEV,0,NHIST,8,*99)
      CALL CHSIZE(IT0,0,NHIST,8,*99)
      CALL CHSIZE(IT1,0,NHIST,8,*99)
      CALL CHSIZE(IT2,0,NHIST,8,*99)
      DO 300 II = 1, NHIST
        READ(LFDAT,ERR=91,END=9999)
     &   MHSTI,MLNGTH,NEVTOT,MTPBIN,(MASKEV(I),I=1,2)
     &  ,MT0,MT1,MT2,(LHTTL(I),I=1,12),(MDUM(I),I=1,16)
     &  ,(MBUFF(I),I=1,224)

C        IHIST = MHSTI            ! IHIST = histogram number
        NBINS1 = MLNGTH
        if( nbins1 .lt. 0 )nbins1 = nbins1+65536 ! 27-may-94  allow #'s > 32767
        IF( II .EQ. 1 )THEN
          CALL CHSIZE(INITH,0,NHIST*NBINS1,8,*99)
C
C   NTPBIN = 1  -->  0.15625 ns/bin
C          = 2  -->  0.3125  ns/bin
C          = 3  -->  0.625   ns/bin
C          = 4  -->  1.25    ns/bin
C          = 5  -->  2.5     ns/bin
C          = 6  -->  5.0     ns/bin
C          = 7  --> 10.0     ns/bin
C
          NTPBIN = MTPBIN                          ! timing resolution code
          IF( NTPBIN .LT. 0 )NTPBIN = -10*NTPBIN   ! big bins are coded
          if( ntpbin .lt. 16 )then
            BININC = 0.078125d0*2**NTPBIN            ! binning increment
          else if( ntpbin .lt. 32 )then
            bininc = 0.048828125d0*2**(ntpbin-16)
          else
            bininc = 0.001d0*ntpbin
          endif
        ELSE
          IF( NBINS1 .NE. NBINS )THEN       ! NBINS = number of bins
            CALL ERROR_MESSAGE('RESTORE'//BS//'MSR'
     &       ,'histograms do not all have the same length')
            GO TO 92
          END IF
        END IF
        NBINS = NBINS1
        R8D(ITOTEV+II) = NEVTOT   ! total # of events

        NT0 = MT0                ! start of background
        if( nt0 .lt. 0 )nt0 = nt0+65536          ! 30-may-94  allow #'s > 32767
        NT1 = MT1                ! start of data
        if( nt1 .lt. 0 )nt1 = nt1+65536          ! 30-may-94  allow #'s > 32767
        NT2 = MT2                ! end of data
        if( nt2 .lt. 0 )nt2 = nt2+65536          ! 30-may-94  allow #'s > 32767
        R8D(IT0+II) = NT0
        R8D(IT1+II) = NT1
        R8D(IT2+II) = NT2

C...Substitute blanks for underscores in histogram label

        DO I = 1, 12 
          IF( LHTTL(I) .EQ. 95 )LHTTL(I) = 32
          IF( LHTTL(I) .LT. 32 )LHTTL(I) = 32
          IF( LHTTL(I) .GT. 126 )LHTTL(I) = 32
        END DO
        LENT = 0
        DO I = 12, 1, -1
          IF( LHTTL(I) .NE. 32 )THEN
            LENT = I
            GO TO 140
          END IF
        END DO
  140   DO I = 1, LENT
          STRING(I:I) = CHAR(LHTTL(I))
        END DO
        IF( LENT .LE. 0 )THEN
          STRING(1:1) = ' '
          LENT = 1
        END IF
        NAME = 'HTITLE'
        LN = 6

CC      write(*,*)' lent = ',lent
CC      write(*,*)' string(1:lent) = ',string(1:lent)

        IF( LENT .GT. NOLDS )THEN
          CALL CHSIZE(SADDR,NOLDS,LENT,1,*99)
          NOLDS = LENT
        END IF
        DO I = 1, LENT
          L1D(SADDR+I) = ICHAR(STRING(I:I))
        END DO
        CALL PUT_TEXT_VARIABLE( NAME(1:LN), II, 0, IDX1
     &   ,SADDR, LENT, 'RESTORE'//BS//'MSR '//FNAME(1:LENF), *92 )

C...Got histogram header, now get the bin counts

        DO I = 1, 224
          MTEMP(2) = 0
          MTEMP(1) = MBUFF(I)
          IF( I .GT. NBINS )GO TO 300
          R8D(INITH+II+(I-1)*NHIST) = ITEMP

C    FLOAT(ITEMP) = actual counts in each bin

        END DO
        LOC = 224   
        IF( LOC .GE. NBINS )GO TO 300
        BLKS = (NBINS-224)/256	
        NBLK = BLKS 
        LGTH1 = NBINS - 224 - NBLK*256    
        IF( LGTH1 .GT. 0 )NBLK = NBLK + 1  
        DO I = 1, NBLK
          READ(LFDAT,ERR=91,END=9999)MBUFF
          DO J = 1, 256
            MTEMP(2) = 0
            MTEMP(1) = MBUFF(J)
            IF( LOC+J .GT. NBINS )GO TO 300
            R8D(INITH+II+(LOC+J-1)*NHIST) = ITEMP
          END DO
          LOC = LOC + 256  
        END DO
  300 CONTINUE

C   Finished reading file

      NAME = 'IRUN'            ! run number
      LN = 4
      CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,0,1.0D0*IRUN,0,0,0,0
     & ,'RESTORE'//BS//'MSR '//FNAME(1:LENF),'RESTORE',*92)

      NAME = 'NHISTS'          ! number of histograms
      LN = 6
      CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,0,1.0D0*NHIST
     & ,0,0,0,0
     & ,'RESTORE'//BS//'MSR '//FNAME(1:LENF),'RESTORE',*92)

      NAME = 'NBINS'           ! number of bins
      LN = 5
      CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,0,1.0D0*NBINS
     & ,0,0,0,0
     & ,'RESTORE'//BS//'MSR '//FNAME(1:LENF),'RESTORE',*92)

      NAME = 'NS_BIN'          ! binning increment
      LN = 6
      CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,0,BININC,0,0,0,0
     & ,'RESTORE'//BS//'MSR '//FNAME(1:LENF),'RESTORE',*92)

      NAME = 'IH'              ! the actual histograms
      LN = 2
      CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,2,0.0D0,INITH
     & ,NBINS,NHIST,0
     & ,'RESTORE'//BS//'MSR '//FNAME(1:LENF),'RESTORE',*92)

      NAME = 'TOTEV'            ! total number of events
      LN = 5
      CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,1,0.0D0,ITOTEV
     & ,NHIST,0,0
     & ,'RESTORE'//BS//'MSR '//FNAME(1:LENF),'RESTORE',*92)

      NAME = 'IT0'              ! start of background
      LN = 3
      CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,1,0.0D0,IT0
     & ,NHIST,0,0
     & ,'RESTORE'//BS//'MSR '//FNAME(1:LENF),'RESTORE',*92)

      NAME = 'IT1'              ! start of data
      LN = 3
      CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,1,0.0D0,IT1
     & ,NHIST,0,0
     & ,'RESTORE'//BS//'MSR '//FNAME(1:LENF),'RESTORE',*92)

      NAME = 'IT2'              ! end of data
      LN = 3
      CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,1,0.0D0,IT2
     & ,NHIST,0,0
     & ,'RESTORE'//BS//'MSR '//FNAME(1:LENF),'RESTORE',*92)

      CALL CHSIZE(ITOTEV,NHIST,0,8,*99)
      CALL CHSIZE(IT0,NHIST,0,8,*99)
      CALL CHSIZE(IT1,NHIST,0,8,*99)
      CALL CHSIZE(IT2,NHIST,0,8,*99)
      IF( NBINS .GT. 0 )CALL CHSIZE(INITH,NHIST*NBINS,0,8,*99)
      IF( NOLDS .GT. 0 )CALL CHSIZE(SADDR,NOLDS,0,1,*99)
      RETURN

C...ERROR detected on read.  Return with error message... 

   91 CALL ERROR_MESSAGE('RESTORE'//BS//'MSR'
     &   ,'error reading file '//FNAME(1:LENF))
   92 IF( NHIST .GT. 0 )THEN
        CALL CHSIZE(ITOTEV,NHIST,0,8,*99)
        CALL CHSIZE(IT0,NHIST,0,8,*99)
        CALL CHSIZE(IT1,NHIST,0,8,*99)
        CALL CHSIZE(IT2,NHIST,0,8,*99)
        IF( NBINS1 .GT. 0 )CALL CHSIZE(INITH,NHIST*NBINS1,0,8,*99)
      END IF
      IF( NOLDS .GT. 0 )CALL CHSIZE(SADDR,NOLDS,0,1,*99)
      RETURN 1
   99 CALL ERROR_MESSAGE('RESTORE'//BS//'MSR'
     & ,'modifying dynamic array space')
      RETURN 1

C...EOF detected on read.  Return with error message... 

 9999 CALL ERROR_MESSAGE('RESTORE'//BS//'MSR'
     & ,'end of file while reading '//FNAME(1:LENF))
      IF( NHIST .GT. 0 )THEN
        CALL CHSIZE(ITOTEV,NHIST,0,8,*99)
        CALL CHSIZE(IT0,NHIST,0,8,*99)
        CALL CHSIZE(IT1,NHIST,0,8,*99)
        CALL CHSIZE(IT2,NHIST,0,8,*99)
      END IF
      CALL CHSIZE(INITH,NHIST*NBINS,0,8,*99)
      IF( NOLDS .GT. 0 )CALL CHSIZE(SADDR,NOLDS,0,1,*99)
      RETURN 1
      END   
