      SUBROUTINE AUTO_AXES

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
CCC
C  Autoscale variable
C  IAUTO = 0  means OFF
C        = -1 means COMMENSURATE
C        = 1  means ON,  = 4  means ON /VIRTUAL
C        = 2  means X,   = 5  means X /VIRTUAL
C        = 3  means Y,   = 6  means Y /VIRTUAL
      IF( (IAUTO .EQ. 1) .OR. (IAUTO .EQ. 2) )THEN
        CALL SETNAM('XAUTO',2.)
        CALL SETNAM('NXDIG',5.)
        CALL SETNAM('NXDEC',-1.)
        IF( IAUTO.EQ.2 )CALL SETNAM('YAUTO',0.0)
      ENDIF
      IF( (IAUTO .EQ. 1) .OR. (IAUTO .EQ. 3) )THEN
        CALL SETNAM('YAUTO',2.)
        CALL SETNAM('NYDIG',5.)
        CALL SETNAM('NYDEC',-1.)
        IF( IAUTO.EQ.3 )CALL SETNAM('XAUTO',0.0)
      ENDIF
      IF( (IAUTO .EQ. 4) .OR. (IAUTO .EQ. 5) )THEN
        CALL SETNAM('XAUTO',3.)
        CALL SETNAM('NXDIG',5.)
        CALL SETNAM('NXDEC',-1.)
        IF( IAUTO.EQ.5 )CALL SETNAM('YAUTO',0.0)
      ENDIF
      IF( (IAUTO .EQ. 4) .OR. (IAUTO .EQ. 6) )THEN
        CALL SETNAM('YAUTO',3.)
        CALL SETNAM('NYDIG',5.)
        CALL SETNAM('NYDEC',-1.)
        IF( IAUTO.EQ.6 )CALL SETNAM('XAUTO',0.0)
      ENDIF
      RETURN
      END
