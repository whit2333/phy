      SUBROUTINE RESTORE_CHAOS(IUN,FNAME,LENF,LIST,*)

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

C   Subroutine that reads CHAOS arrays from logical unit IUN

      CHARACTER*1024 FNAME
      INTEGER*4     IUN, LENF
      LOGICAL*1     LIST

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      BYTE      L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

C    STAR_H1D include file 

      PARAMETER (NOMBRE_H = 400)
      PARAMETER (NXSCL    = 200)
      REAL*4    ICUT(0:NOMBRE_H,6), TIM(2), DAT(3)
      CHARACTER*32 CUT_TIT(NOMBRE_H,6)

C   local variables

      CHARACTER*132 STRING
      CHARACTER*15  SOUT
      CHARACTER*11  NAME
      CHARACTER*1   BS
      INTEGER*4     SADDR, IDX1(4), IDX2(4), IDX3(4)
      DATA IDX1 /0,0,0,0/, IDX2 /0,0,0,0/, IDX3 /0,0,0,0/
CCC
      BS = CHAR(92)  ! backslash 

      READ(IUN,ERR=9998,END=9999)IDRAVER
      IF( IDRAVER .NE. 111 )THEN
        CALL INTEGER_TO_CHAR(IDRAVER,.FALSE.,SOUT,LENS)
        CALL ERROR_MESSAGE('RESTORE'
     &   ,'wrong version number ('//SOUT(1:LENS)//') in file '//
     &    FNAME(1:LENF))
        RETURN 1
      END IF
      READ(IUN,ERR=9998,END=9999)TIM
      READ(IUN,ERR=9998,END=9999)DAT

C    run number

      READ(IUN,ERR=9998,END=9999)III
      IF( LIST )THEN
        WRITE(STRING,*)'Run number = ',III
        CALL RITE(STRING(1:LENSIG(STRING)))
      ELSE
        NAME = 'RUN_NUMBER'
        LN = 10
        CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,0,1.0D0*III
     &   ,0,0,0,0,'RESTORE'//BS//'CHAOS '//FNAME(1:LENF),'RESTORE',*92)
      END IF

      READ(IUN,ERR=9998,END=9999)I_ANALYZED
      IF( LIST )THEN
        WRITE(STRING,*)'I_ANALYZED = ',I_ANALYZED
        CALL RITE(STRING(1:LENSIG(STRING)))
      ELSE
        NAME = 'I_ANALYZED'
        LN = 11
        CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,0,1.0D0*I_ANALYZED
     &   ,0,0,0,0,'RESTORE'//BS//'CHAOS '//FNAME(1:LENF),'RESTORE',*92)
      END IF

      NOLD4 = 0
      NOLD8 = 0
      CALL CHSIZE(ITMP4,NOLD4,NXSCL,4,*99)
      NOLD4 = NXSCL
      READ(IUN,ERR=9998,END=9999)(R4D(ITMP4+I),I=1,NXSCL)
      IF( .NOT.LIST )THEN
        CALL CHSIZE(ITMP8,NOLD8,NXSCL,8,*99)
        NOLD8 = NXSCL
        DO I = 1, NXSCL
          R8D(ITMP8+I) = R4D(ITMP4+I)
        END DO
        NAME = 'XSCL'
        LN = 4
        CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,1,0.0D0,ITMP8
     &   ,NXSCL,0,0,'RESTORE'//BS//'CHAOS '//FNAME(1:LENF),'RESTORE'
     &   ,*92)
      END IF

C    number of histograms, number of channels

      READ(IUN,ERR=9998,END=9999)NHIST,NCHAN
      IF( LIST )THEN
        WRITE(STRING,*)'Number of histograms = ',NHIST
        CALL RITE(STRING(1:LENSIG(STRING)))
        WRITE(STRING,*)'Number of channels   = ',NCHAN
        CALL RITE(STRING(1:LENSIG(STRING)))
      ELSE
        NAME = 'NHIST'
        LN = 5
        CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,0,1.0D0*NHIST
     &   ,0,0,0,0,'RESTORE'//BS//'CHAOS '//FNAME(1:LENF),'RESTORE',*92)
        NAME = 'NCHAN'
        LN = 5
        CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,0,1.0D0*NCHAN
     &   ,0,0,0,0,'RESTORE'//BS//'CHAOS '//FNAME(1:LENF),'RESTORE',*92)
      END IF

C    xlow, xhi, test#, data quantity to hist, #bins, start address

      READ(IUN,ERR=9998,END=9999)((ICUT(J,JJ),JJ=1,6),J=1,NHIST)
      IF( .NOT.LIST )THEN
        NAME = 'XLO'
        LN = 3
        IF( NHIST .GT. NOLD8 )THEN
          CALL CHSIZE(ITMP8,NOLD8,NHIST,8,*99)
          NOLD8 = NHIST
        END IF
        DO I = 1, NHIST
          R8D(ITMP8+I) = ICUT(I,1)
        END DO
        CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,1,0.0D0,ITMP8
     &   ,NHIST,0,0,'RESTORE'//BS//'CHAOS '//FNAME(1:LENF),'RESTORE'
     &   ,*92)
        NAME = 'XHI'
        LN = 3
        DO I = 1, NHIST
          R8D(ITMP8+I) = ICUT(I,2)
        END DO
        CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,1,0.0D0,ITMP8
     &   ,NHIST,0,0,'RESTORE'//BS//'CHAOS '//FNAME(1:LENF),'RESTORE'
     &   ,*92)
        NAME = 'NBINS'
        LN = 5
        DO I = 1, NHIST
          R8D(ITMP8+I) = ICUT(I,5)
        END DO
        CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,1,0.0D0,ITMP8
     &   ,NHIST,0,0,'RESTORE'//BS//'CHAOS '//FNAME(1:LENF),'RESTORE'
     &   ,*92)
        NAME = 'HSTART'
        LN = 6
        DO I = 1, NHIST
          R8D(ITMP8+I) = ICUT(I,6)
        END DO
        CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,1,0.0D0,ITMP8
     &   ,NHIST,0,0,'RESTORE'//BS//'CHAOS '//FNAME(1:LENF),'RESTORE'
     &   ,*92)
      END IF

C    histo name, title name, test name, data name, event/calib flag
C    title==data name//test name

C    histo name
      READ(IUN,ERR=9998,END=9999)((CUT_TIT(J,JJ),JJ=1,6),J=1,NHIST)
      IF( LIST )THEN
        DO I = 1, NHIST
          CALL RITE(' ')
          WRITE(STRING,*)'Histo number = ',I,' Name = ',CUT_TIT(I,1)
          CALL RITE(STRING(1:LENSIG(STRING)))
          WRITE(STRING,*)'Title            : ',CUT_TIT(I,2)
          CALL RITE(STRING(1:LENSIG(STRING)))
        END DO
      ELSE
        NAME = 'HNAMES'
        LN = 6
        NOLDS = 0
        DO I = 1, NHIST
          LENG = LENSIG(CUT_TIT(I,1))
          IF( LENG .GT. NOLDS )THEN
            CALL CHSIZE(SADDR,NOLDS,LENG,1,*99)
            NOLDS = LENG
          END IF
          DO K = 1, LENG
            L1D(SADDR+K) = ICHAR(CUT_TIT(I,1)(K:K))
          END DO
          CALL PUT_TEXT_VARIABLE( NAME(1:LN), I, 0, IDX1
     &     ,SADDR, LENG, 'RESTORE'//BS//'CHAOS',*92)
        END DO

C    title name

        NAME = 'TITLES'
        LN = 6
        DO I = 1, NHIST
          LENG = LENSIG(CUT_TIT(I,2))
          IF( LENG .GT. NOLDS )THEN
            CALL CHSIZE(SADDR,NOLDS,LENG,1,*99)
            NOLDS = LENG
          END IF
          DO K = 1, LENG
            L1D(SADDR+K) = ICHAR(CUT_TIT(I,2)(K:K))
          END DO
          CALL PUT_TEXT_VARIABLE( NAME(1:LN), I, 0, IDX1
     &     ,SADDR, LENG, 'RESTORE'//BS//'CHAOS',*92)
        END DO
        IF( NOLDS .GT. 0 )CALL CHSIZE(SADDR,NOLDS,0,1,*99)
      END IF

C    actual histograms

      IF( .NOT.LIST )THEN
        IF( NCHAN .GT. NOLD4 )THEN
          CALL CHSIZE(ITMP4,NOLD4,NCHAN,4,*99)
          NOLD4 = NCHAN
        END IF
        READ(IUN,ERR=9998,END=9999)(I4D(ITMP4+I),I=1,NCHAN)
        IF( NCHAN .GT. NOLD8 )THEN
          CALL CHSIZE(ITMP8,NOLD8,NHIST,8,*99)
          NOLD8 = NCHAN
        END IF
        DO I = 1, NCHAN
          R8D(ITMP8+I) = I4D(ITMP4+I)
        END DO
        NAME = 'HIST'
        LN = 5
        CALL PUT_VARIABLE(NAME(1:LN),0,IDX1,IDX2,IDX3,1,0.0D0,ITMP8
     &   ,NCHAN,0,0,'RESTORE'//BS//'CHAOS '//FNAME(1:LENF),'RESTORE'
     &   ,*92)
      END IF
      IF( NOLD4 .GT. 0 )CALL CHSIZE(ITMP4,NOLD4,0,4,*99)
      IF( NOLD8 .GT. 0 )CALL CHSIZE(ITMP8,NOLD8,0,8,*99)
      RETURN

C...ERROR detected on read.  Return with error message... 

 9998 CALL ERROR_MESSAGE('RESTORE'//BS//'CHAOS'
     &   ,'error reading file '//FNAME(1:LENF))
   92 IF( NOLD4 .GT. 0 )CALL CHSIZE(ITMP4,NOLD4,0,4,*99)
      IF( NOLD8 .GT. 0 )CALL CHSIZE(ITMP8,NOLD8,0,8,*99)
      RETURN 1
   99 CALL ERROR_MESSAGE('RESTORE'//BS//'CHAOS'
     & ,'modifying dynamic array space')
      RETURN 1

C...EOF detected on read.  Return with error message... 

 9999 CALL ERROR_MESSAGE('RESTORE'//BS//'CHAOS'
     & ,'end of file while reading '//FNAME(1:LENF))
      IF( NOLD4 .GT. 0 )CALL CHSIZE(ITMP4,NOLD4,0,4,*99)
      IF( NOLD8 .GT. 0 )CALL CHSIZE(ITMP8,NOLD8,0,8,*99)
      RETURN 1
      END   
