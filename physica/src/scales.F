      SUBROUTINE SCALES( * )

C   SCALES xmin xmax nlxinc ymin ymax nlyinc
C   SCALES xmin xmax ymin ymax

      IMPLICIT NONE

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT, MXCHAR
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20,   MXCHAR=1024)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN
      
      real*8 xmin_scales, xmax_scales, ymin_scales, ymax_scales
      logical*1 comm_scales
      common /scales_comm/ xmin_scales, xmax_scales,
     &                     ymin_scales, ymax_scales, comm_scales

C  local variables

      CHARACTER*15 SOUT, BLANKS
      CHARACTER*1  BS
      REAL*8       XMIN, XMAX, YMIN, YMAX, XNLINC, YNLINC
      REAL*4       GETNAM
      INTEGER*4    I, II, L, ICNT, LTMP1, LTMP2, LTMP3, LTMP4, LTMP5
      INTEGER*4    LTMP6, LENSIG
      LOGICAL*4    VALUE_GIVEN
      LOGICAL*1    COMMENSURATE, VIRTUAL
CCC
      BS = CHAR(92)   ! backslash

      BLANKS = ' '
      IF( STACK )THEN
        STRINGS(1) = 'SCALES'
        LENST(1) = 6
      END IF
      COMMENSURATE = .FALSE.
      VIRTUAL = .FALSE.
      DO I = 1, NQUAL(1)
        IF( QUALIFIERS(1,I)(1:1) .EQ. '-' )THEN
          II = 2
        ELSE
          II = 1
        END IF
        L = LENQL(1,I)
        IF( INDEX('COMMENSURATE',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            COMMENSURATE = .TRUE.
          ELSE
            COMMENSURATE = .FALSE.
          END IF
          IF( STACK )THEN
            STRINGS(1) =
     &       STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
            LENST(1) = LENST(1)+1+L
          END IF
        ELSE IF(INDEX('NOCOMMENSURATE',QUALIFIERS(1,I)(II:L)).EQ.1)THEN
          IF( II .EQ. 1 )THEN
            COMMENSURATE = .FALSE.
          ELSE
            COMMENSURATE = .TRUE.
          END IF
          IF( STACK )THEN
            STRINGS(1) =
     &       STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
            LENST(1) = LENST(1)+1+L
          END IF
        ELSE
          CALL WARNING_MESSAGE('SCALES'
     &     ,'unknown qualifier: '//QUALIFIERS(1,I)(1:L))
        END IF
      END DO

      CALL SETNAM('XAUTO',0.0)
      CALL SETNAM('YAUTO',0.0)
      XMIN   = GETNAM('XMIN')
      XMAX   = GETNAM('XMAX')
      XNLINC = GETNAM('NLXINC')
      YMIN   = GETNAM('YMIN')
      YMAX   = GETNAM('YMAX')
      YNLINC = GETNAM('NLYINC')
      IF( NFLD .EQ. 1 )THEN
        IF( (IAUTO.NE.0) .AND. ((NUNIT.EQ.1).OR.ECHO) )
     &   CALL RITE('Graph autoscaling is off')
        IAUTO = 0
        GO TO 80
      END IF

C  Autoscale variable
C  IAUTO = 0  means OFF
C        = -1 means COMMENSURATE
C        = 1  means ON,  = 4  means ON /VIRTUAL
C        = 2  means X,   = 5  means X /VIRTUAL
C        = 3  means Y,   = 6  means Y /VIRTUAL

      IAUTO = 0
      IF( NFLD .EQ. 7 )GO TO 60
      IF( NFLD .EQ. 5 )GO TO 40
      CALL ERROR_MESSAGE('SCALES'
     & ,'expecting: xmin xmax nlxinc ymin ymax nlyinc  or'//
     &  '  xmin xmax ymin ymax')
      GO TO 92

   40 ICNT = 2
      CALL GET_SCALAR( STRINGS(ICNT),LENST(ICNT),REALS(ICNT),ITYPE(ICNT)
     & ,'SCALES','x-axis minimum',VALUE_GIVEN,.TRUE.,XMIN,*92,*91)
      IF( STACK )THEN
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
      END IF
      XMIN = REALS(ICNT)

      ICNT = 3
      CALL GET_SCALAR( STRINGS(ICNT),LENST(ICNT),REALS(ICNT),ITYPE(ICNT)
     & ,'SCALES','x-axis maximum',VALUE_GIVEN,.TRUE.,XMAX,*92,*91)
      IF( STACK )THEN
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
      END IF
      XMAX = REALS(ICNT)

      XNLINC = 0.0D0

      ICNT = 4
      CALL GET_SCALAR( STRINGS(ICNT),LENST(ICNT),REALS(ICNT),ITYPE(ICNT)
     & ,'SCALES','y-axis minimum',VALUE_GIVEN,.TRUE.,YMIN,*92,*91)
      IF( STACK )THEN
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
      END IF
      YMIN = REALS(ICNT)

      ICNT = 5
      CALL GET_SCALAR( STRINGS(ICNT),LENST(ICNT),REALS(ICNT),ITYPE(ICNT)
     & ,'SCALES','y-axis maximum',VALUE_GIVEN,.TRUE.,YMAX,*92,*91)
      IF( STACK )THEN
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
      END IF
      YMAX = REALS(ICNT)

      YNLINC = 0.0D0
      GO TO 70

  60  ICNT = 2
      CALL GET_SCALAR( STRINGS(ICNT),LENST(ICNT),REALS(ICNT),ITYPE(ICNT)
     & ,'SCALES','x-axis minimum',VALUE_GIVEN,.TRUE.,XMIN,*92,*91)
      IF( STACK )THEN
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
      END IF
      XMIN = REALS(ICNT)

      ICNT = 3
      CALL GET_SCALAR( STRINGS(ICNT),LENST(ICNT),REALS(ICNT),ITYPE(ICNT)
     & ,'SCALES','x-axis maximum',VALUE_GIVEN,.TRUE.,XMAX,*92,*91)
      IF( STACK )THEN
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
      END IF
      XMAX = REALS(ICNT)

      ICNT = 4
      CALL GET_SCALAR( STRINGS(ICNT),LENST(ICNT),REALS(ICNT),ITYPE(ICNT)
     & ,'SCALES','x-axis long tic marks',VALUE_GIVEN,.TRUE.,0.0D0
     & ,*92,*91)
      IF( STACK )THEN
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
      END IF
      XNLINC = REALS(ICNT)

      ICNT = 5
      CALL GET_SCALAR( STRINGS(ICNT),LENST(ICNT),REALS(ICNT),ITYPE(ICNT)
     & ,'SCALES','y-axis minimum',VALUE_GIVEN,.TRUE.,YMIN,*92,*91)
      IF( STACK )THEN
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
      END IF
      YMIN = REALS(ICNT)

      ICNT = 6
      CALL GET_SCALAR( STRINGS(ICNT),LENST(ICNT),REALS(ICNT),ITYPE(ICNT)
     & ,'SCALES','y-axis maximum',VALUE_GIVEN,.TRUE.,YMAX,*92,*91)
      IF( STACK )THEN
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
      END IF
      YMAX = REALS(ICNT)

      ICNT = 7
      CALL GET_SCALAR( STRINGS(ICNT),LENST(ICNT),REALS(ICNT),ITYPE(ICNT)
     & ,'SCALES','y-axis long tic marks',VALUE_GIVEN,.TRUE.,0.0D0
     & ,*92,*91)
      IF( STACK )THEN
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
      END IF
      YNLINC = REALS(ICNT)

  70  CALL SETNAM('XMIN',SNGL(XMIN))
      CALL SETNAM('XMAX',SNGL(XMAX))
      CALL SETNAM('NLXINC',SNGL(XNLINC))
      CALL SETNAM('YMIN',SNGL(YMIN))
      CALL SETNAM('YMAX',SNGL(YMAX))
      CALL SETNAM('NLYINC',SNGL(YNLINC))

      IF( STACK )WRITE(IOUT_UNIT,10)STRINGS(1)(1:LENST(1))
  10  FORMAT(' ',A)
  80  IF( (NUNIT .EQ. 1) .OR. ECHO )THEN
        STRINGS(1) = ' '
        CALL REAL8_TO_CHAR(XMIN,.FALSE.,SOUT,LTMP1)
        STRINGS(1) = ' '//SOUT
        LTMP1 = MAX(LTMP1,4)
        LENST(1) = LTMP1 + 1
        CALL REAL8_TO_CHAR(XMAX,.FALSE.,SOUT,LTMP2)
        STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//SOUT
        LTMP2 = MAX(LTMP2,4)
        LENST(1) = LENST(1) + LTMP2 + 1
        CALL REAL8_TO_CHAR(XNLINC,.FALSE.,SOUT,LTMP3)
        STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//SOUT
        LTMP3 = MAX(LTMP2,6)
        LENST(1) = LENST(1) + LTMP3 + 1
        CALL REAL8_TO_CHAR(YMIN,.FALSE.,SOUT,LTMP4)
        STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//SOUT
        LTMP4 = MAX(LTMP4,4)
        LENST(1) = LENST(1) + LTMP4 + 1
        CALL REAL8_TO_CHAR(YMAX,.FALSE.,SOUT,LTMP5)
        STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//SOUT
        LTMP5 = MAX(LTMP5,4)
        LENST(1) = LENST(1) + LTMP5 + 1
        CALL REAL8_TO_CHAR(YNLINC,.FALSE.,SOUT,LTMP6)
        STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//SOUT
        LENST(1) = LENST(1) + LTMP6 + 1
        STRINGS(2) = ' '
        WRITE(STRINGS(2),82)BLANKS(1:LTMP1-3),BLANKS(1:LTMP2-3),
     &   BLANKS(1:LTMP3-5),BLANKS(1:LTMP4-3),BLANKS(1:LTMP5-3)
  82    FORMAT(' Xmin',A,'Xmax',A,'#Xincs',A,'Ymin',A,'Ymax',A,'#Yincs')
        CALL RITE(STRINGS(2)(1:LENSIG(STRINGS(2))))
        CALL RITE(STRINGS(1)(1:LENST(1)))
      END IF
      IF( (XMIN.EQ.0.D0).AND.(XMAX.EQ.0.D0).AND.(XNLINC.EQ.0.D0) )THEN
        IAUTO = 2
        CALL SETNAM('XAUTO',2.0)
        COMMENSURATE = .FALSE.
        CALL RITE('x-axis autoscaling is on')
      END IF
      IF( (YMIN.EQ.0.D0).AND.(YMAX.EQ.0.D0).AND.(YNLINC.EQ.0.D0) )THEN
        IAUTO = 3
        CALL SETNAM('YAUTO',2.0)
        COMMENSURATE = .FALSE.
        CALL RITE('y-axis autoscaling is on')
      END IF
      
      xmin_scales = xmin
      xmax_scales = xmax
      ymin_scales = ymin
      ymax_scales = ymax
      comm_scales = commensurate
      
      IF( COMMENSURATE )call scales_commensurate

      CALL SETNAM('NXDEC',-1.0)
      CALL SETNAM('NYDEC',-1.0)
      CALL SETNAM('NXDIG',10.0)
      CALL SETNAM('NYDIG',10.0)
   91 CALL DELETE_TEMP_SPACE
      RETURN
   92 CALL DELETE_TEMP_SPACE
      RETURN 1
      END

      subroutine scales_commensurate
      implicit none
      real*8 xmin_scales, xmax_scales, ymin_scales, ymax_scales
      logical*1 comm_scales
      common /scales_comm/ xmin_scales, xmax_scales,
     &                     ymin_scales, ymax_scales, comm_scales
      real*4 getnam
      real*8 xlwind, xuwind, ylwind, yuwind, delxwind, delywind
      real*8 xlaxis, xuaxis, ylaxis, yuaxis, xmin, ymin, xd, yd
      real*8 xmid, ymid, delx, dely, xlp, xup, ylp, yup
ccc
      if( .not.comm_scales )return
      XLWIND   = GETNAM('XLWIND')
      XUWIND   = GETNAM('XUWIND')
      YLWIND   = GETNAM('YLWIND')
      YUWIND   = GETNAM('YUWIND')
      DELXWIND = ABS(XUWIND-XLWIND)
      DELYWIND = ABS(YUWIND-YLWIND)

      XLAXIS = 0.15D0 * DELXWIND+XLWIND
      XUAXIS = 0.90D0 * DELXWIND+XLWIND
      YLAXIS = 0.15D0 * DELYWIND+YLWIND
      YUAXIS = 0.90D0 * DELYWIND+YLWIND
      XMID   = 0.50D0 * (XLWIND+XUWIND)
      YMID   = 0.50D0 * (YLWIND+YUWIND)

      XD = XMAX_SCALES-XMIN_SCALES
      YD = YMAX_SCALES-YMIN_SCALES

      IF( XD .GE. YD )THEN
        DELX   = XUAXIS-XLAXIS
        DELY   = DELX * YD / XD
        YLAXIS = YMID-0.5D0*DELY
        YUAXIS = YMID+0.5D0*DELY
      ELSE
        DELY   = YUAXIS-YLAXIS
        DELX   = DELY * XD / YD
        XLAXIS = XMID-0.5D0*DELX
        XUAXIS = XMID+0.5D0*DELX
      END IF

      IF( (YLAXIS-YLWIND)/DELYWIND .LT. 0.15 )THEN
        DELY = 0.15*DELYWIND + YLWIND - YLAXIS
        YLAXIS = 0.15*DELYWIND + YLWIND
        YUAXIS = YUAXIS - DELY
        XLAXIS = XLAXIS + DELY
        XUAXIS = XUAXIS - DELY
      ELSE IF( (XLAXIS-XLWIND)/DELXWIND .LT. 0.15 )THEN
        DELX = 0.15*DELXWIND + XLWIND - XLAXIS
        XLAXIS = 0.15*DELXWIND + XLWIND
        XUAXIS = XUAXIS - DELX
        YLAXIS = YLAXIS + DELX
        YUAXIS = YUAXIS - DELX
      END IF

      YLP = (YLAXIS-YLWIND) / DELYWIND * 100.D0
      YUP = (YUAXIS-YLWIND) / DELYWIND * 100.D0
      XLP = (XLAXIS-XLWIND) / DELXWIND * 100.D0
      XUP = (XUAXIS-XLWIND) / DELXWIND * 100.D0

      CALL SETNAM('%YLAXIS',SNGL(YLP))
      CALL SETNAM('%YUAXIS',SNGL(YUP))
      CALL SETNAM('%XLAXIS',SNGL(XLP))
      CALL SETNAM('%XUAXIS',SNGL(XUP))
      return 
      end
