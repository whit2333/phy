      SUBROUTINE GET_VECTOR( STRING,LENST,ITYPE,COMMAND,WHAT,IADR,NPT
     &                      ,*,* )

      IMPLICIT NONE

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

      CHARACTER*(*) COMMAND, WHAT
      CHARACTER*1024 STRING, MSSG
      INTEGER*4     ITYPE, LENST, IADR, NPT

C   local variables

      CHARACTER*6 VARTYP(0:3)
      REAL*8      R8
      INTEGER*4   NDM, NCOL, NPLN, LWHAT
      DATA VARTYP / 'scalar', 'vector', 'matrix', 'tensor' /
CCC
      LWHAT = LEN(WHAT)
      IF( ITYPE .EQ. 0 )THEN
        IF( BATCH_MODE .OR. NO_PROMPT )THEN
          MSSG = WHAT//' not entered'
          CALL ERROR_MESSAGE(COMMAND,MSSG(1:LWHAT+12))
        END IF
        GO TO 20
      ELSE IF( ITYPE .EQ. 1 )THEN
        MSSG = WHAT//' is constant'
        CALL ERROR_MESSAGE(COMMAND,MSSG(1:LWHAT+12))
        GO TO 20
      ELSE IF( ITYPE .EQ. 3 )THEN
        MSSG = 'equal sign encountered; expecting '//WHAT
        CALL ERROR_MESSAGE(COMMAND,MSSG(1:34+LWHAT))
        GO TO 20
      END IF
   10 CALL GET_VARIABLE( STRING(1:LENST),NDM,R8,IADR,NPT,NCOL,NPLN,*20 )
      IF( NDM .EQ. 1 )RETURN
      MSSG = STRING(1:LENST)//' is a '//VARTYP(NDM)
      CALL WARNING_MESSAGE(COMMAND,MSSG(1:LENST+6+6))
   20 IF( BATCH_MODE .OR. NO_PROMPT )GO TO 90
      LENST = 0
      DO WHILE ( LENST .EQ. 0 )
        MSSG = ' '//WHAT//' >> '
cc        CALL TT_GET_INPUT(MSSG(1:1+LWHAT+4),STRING,LENST,*92,*90)
        CALL readline_wrapper(MSSG(1:1+LWHAT+4)//char(0),STRING,LENST)
        if( lenst .eq. 0 )goto 90
      END DO
      GO TO 10
   90 RETURN 1
   92 RETURN 2
      END
