      SUBROUTINE LIST_MATRIX( * )

C   LIST\MATRIX  matrix  
C   LIST\MATRIX\FORMAT  matrix `(format)'

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT, MXCHAR
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20,   MXCHAR=1024)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

      LOGICAL*4 CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED

C  local variables

      CHARACTER*132 STRING
      CHARACTER*1   BS
      LOGICAL*4     FRMTIN
CCC
      BS = CHAR(92)   ! backslash

      IF( STACK )THEN
        STRINGS(1) = 'LIST'
        LENST(1) = 4
      END IF
      FRMTIN = .FALSE.
      DO I = 1, NQUAL(1)
        IF( QUALIFIERS(1,I)(1:1) .EQ. '-' )THEN
          II = 2
        ELSE
          II = 1
        END IF
        L = LENQL(1,I)
        IF( INDEX('MATRIX',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( STACK )THEN
            STRINGS(1) =
     &       STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
            LENST(1) = LENST(1)+1+L
          END IF
        ELSE IF( INDEX('FORMAT',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            FRMTIN = .TRUE.
          ELSE
            FRMTIN = .FALSE.
          END IF
          IF( STACK )THEN
            STRINGS(1) =
     &       STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
            LENST(1) = LENST(1)+1+L
          END IF
        ELSE IF( INDEX('NOFORMAT',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            FRMTIN = .FALSE.
          ELSE
            FRMTIN = .TRUE.
          END IF
          IF( STACK )THEN
            STRINGS(1) =
     &       STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
            LENST(1) = LENST(1)+1+L
          END IF
        ELSE
          CALL WARNING_MESSAGE('LIST'//BS//'MATRIX'
     &     ,'unknown qualifier: '//QUALIFIERS(1,I)(1:L))
        END IF
      END DO

      CALL GET_MATRIX( STRINGS(2), LENST(2), ITYPE(2)
     & ,'LIST'//BS//'MATRIX'
     & ,'matrix to list', IARR, NROW, NCOL, *92, *92 )
      IF( STACK )THEN
        STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(2)(1:LENST(2))
        LENST(1) = LENST(1)+1+LENST(2)
      END IF

      IF( FRMTIN )THEN
        IF( ITYPE(3) .NE. 2 )THEN
          CALL ERROR_MESSAGE('LIST'//BS//'MATRIX','expecting format')
          GO TO 92
        END IF
        strings(4) = ' '
        CALL GET_TEXT_VARIABLE( STRINGS(3),-LENST(3)
     &   ,STRINGS(4),LENST(4), *92 )
        IF( STACK )THEN
          STRINGS(1) =
     &     STRINGS(1)(1:LENST(1))//' '//STRINGS(3)(1:LENST(3))
          LENST(1) = LENST(1)+1+LENST(3)
        END IF
        IF( STACK )WRITE(IOUT_UNIT,10)STRINGS(1)(1:LENST(1))
   10   FORMAT(' ',A)
        CALL RITE(' ')
        IF( STRINGS(4)(1:LENST(4)) .EQ. '(*)' )THEN
          DO I = 1, NROW
            IF( CTRLC_CALLED )GO TO 90
            WRITE(STRING,*,ERR=999)(R8D(IARR+I+(J-1)*NROW),J=1,NCOL)
            CALL RITE(STRING(1:LENSIG(STRING)))
          END DO
        ELSE
          DO I = 1, NROW
            IF( CTRLC_CALLED )GO TO 90
            WRITE(STRING,STRINGS(4)(1:LENST(4)),ERR=999)
     &       (R8D(IARR+I+(J-1)*NROW),J=1,NCOL)
            CALL RITE(STRING(1:LENSIG(STRING)))
          END DO
        END IF
      ELSE
        IF( STACK )WRITE(IOUT_UNIT,10)STRINGS(1)(1:LENST(1))
        CALL RITE(' ')
        DO I = 1, NROW
          IF( CTRLC_CALLED )GO TO 90
          L = (IWIDTH-1)/13
          K = 1
          DO WHILE( NCOL .GT. (K-1)*L )
            ND2 = MIN(NCOL,K*L)-(K-1)*L
            WRITE(STRINGS(1),40,ERR=999)
     &       (R8D(IARR+I+(J-1)*NROW),J=(K-1)*L+1,MIN(NCOL,K*L))
   40       FORMAT(1P10D13.4)
            CALL RITE(STRINGS(1)(1:ND2*13))
            K = K+1
          END DO
        END DO
      END IF
   90 CALL DELETE_TEMP_SPACE
      RETURN
   92 CALL DELETE_TEMP_SPACE
      RETURN 1
  999 CALL ERROR_MESSAGE('LIST'//BS//'MATRIX','during writing')
      CALL DELETE_TEMP_SPACE
      RETURN 1
      END
