      SUBROUTINE PUT_HBOOK_VARIABLE( NAME, NDIMX, VALX, TYPE
     &                              ,INITX, NUMX, NCOLX, LINE, * )

C   TYPE = 0  means the input is INTEGER*4
C   TYPE = 1  means the input is REAL*4

      IMPLICIT NONE

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      INTEGER*4 VARALC, VARADR, VARLEN, VARNUM, VARMAX
     &         ,TSCAL, NSCAL, ASCAL, ESCAL

C   VARALC = number of bytes allocated for numeric variable
C            characteristics (does not include the data)
C            VARALC should be a multiple of 512 
C   VARADR = starting address of the numeric variable table
C   VARLEN = # of bytes needed for an entry in the table
C   VARNUM = # of numeric variables currently defined
C   VARMAX = # of variables (including scalars) for which space is allocated 
C
C   TSCAL  = # of scalars for which space is allocated 
C   NSCAL  = # of current scalars
C   ASCAL  = starting address for scalar data
C   ESCAL  = starting address of the scalars existence list
C
C   Allocate the space as bytes. Let BASE = (VARADR+(I-1)*VARLEN)/4, for
C   1 <= I <= VARNUM.  The last byte used for I'th variable at 4*BASE+VARLEN
C   and the start of the next variable is at 4*BASE+VARLEN+1
C
C   I4$(BASE+1)   = length of the I'th variable name
C                   always allocate 32 (4*8) bytes for every name
C                   name is in L1$((BASE+2)*4+j) for j = 1, length
C   I4$(BASE+10)  = -1 if scalar is a dummy index
C                    0 if normal scalar
C                   +1 if scalar is allowed to vary in fit
C   I4$(BASE+11)  = number of dimensions: 0=scalar, 1=vector, 2=matrix, 3=tensor
C   I4$(BASE+12)  = starting address of data
C   I4$(BASE+13)  = number of elements if a vector
C                   or number of rows if a matrix (1st dimension)
C   I4$(BASE+14)  = number of columns (2nd dimension)
C   I4$(BASE+15)  = number of planes (3rd dimension)
C   I4$(BASE+16)  = initial index of 1st dimension
C   I4$(BASE+17)  = initial index of 2nd dimension
C   I4$(BASE+18)  = initial index of 3rd dimension
C   I4$(BASE+19)  = final index of 1st dimension
C   I4$(BASE+20)  = final index of 2nd dimension
C   I4$(BASE+21)  = final index of 3rd dimension
C   I4$(BASE+22)  = number of lines of history for variable I
C   I4$(BASE+23)  = total allocation for history for variable I
C   I4$(BASE+24)  = address for history line lengths
C   I4$(BASE+25)  = address for history line characters
C    for history line 1:
C       length = I4$(I4$(BASE+24)+1) 
C       line = L1$(I4$(BASE+25)+j) for j = 1 --> I4$(I4$(BASE+24)+1) 
C    for history line k > 1:
C       length = I4$(I4$(BASE+24)+k) 
C         prev_len = sum(I4$(I4$(BASE+24)+j),j,1:k-1)
C       line = L1$(I4$(BASE+25)+prev_len+j) for j = 1 --> I4$(I4$(BASE+24)+k)

      COMMON /NUMERIC_VARIABLES/ VARALC, VARADR, VARLEN, VARNUM, VARMAX
     &                          ,TSCAL, NSCAL, ASCAL, ESCAL
      INTEGER*4 TVARALC, TVARADR, TVARLEN, TVARNUM
C
C   TVARALC = number of bytes allocated for text variable characteristics
C             (does not include the data)
C   TVARADR = starting address of the text variable table
C   TVARLEN = number of bytes needed for an entry in the table (60 bytes)
C   TVARNUM = number of text variables currently defined
C
C   Allocate the space as bytes. Let BASE = (TVARADR+(I-1)*TVARLEN)/4, for
C   1 <= I <= TVARNUM.  The last byte used for I'th variable at 4*BASE+VARLEN
C   and the start of the next variable is at 4*BASE+VARLEN+1
C                     
C   I4$(BASE+1)  = length of the I'th variable name
C                  always allocate 32 (4*8) bytes for every name
C   L1$((BASE+2)*4+j) for j = 1, I4$(BASE+1)  is the name
C   I4$(BASE+10) = number of dimensions: 0=scalar, 1=vector
C   I4$(BASE+11) = starting address of data
C   I4$(BASE+12) = number of strings if a vector
C   I4$(BASE+13) = total length of data
C   I4$(BASE+14) = starting address for offsets for vector
C                   I4$(I4$(BASE+14)+1) = 0 for every variable
C   I4$(BASE+15) = starting address for lengths for vector
C
C    scalar variable:
C       length = I4$(BASE+13) 
C       line = L1$(I4$(BASE+11)+j) for j = 1 --> I4$(BASE+13) 
C    vector variable:
C     for string k:  1 <= k <= I4$(BASE+12)
C       length = I4$(I4$(BASE+13)+k)
C       line = L1$(I4$(BASE+11)+j) for
C        j = I4$(I4$(BASE+14)+k)+1 --> I4$(I4$(BASE+14)+k)+I4$(I4$(BASE+15)+k)

      COMMON /TEXT_VARIABLES/ TVARALC, TVARADR, TVARLEN, TVARNUM

      CHARACTER*(*) NAME, LINE
      REAL*8        VALX
      INTEGER*4     INITX, NDIMX, TYPE, NUMX, NCOLX

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      BYTE      L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

      INTEGER*4 BASE, INEW, LNL, LNM, I, J
CCC
      LNL = LEN( LINE )
      LNM = LEN( NAME )

      DO 101 I = 1, VARNUM
        BASE = (VARADR+(I-1)*VARLEN)/4
        IF( LNM .NE. I4D(BASE+1) )GO TO 101   ! lengths not equal
        DO J = 1, LNM
          IF( NAME(J:J) .NE. CHAR(L1D((BASE+2)*4+J)) )GO TO 101
        END DO
        CALL DELETE_VARIABLE( I )
        GO TO 105
  101 CONTINUE
      DO 102 I = 1, TVARNUM
        BASE = (TVARADR+(I-1)*TVARLEN)/4
        IF( LNM .NE. I4D(BASE+1) )GO TO 102   ! lengths not equal
        DO J = 1, LNM
          IF( NAME(J:J) .NE. CHAR(L1D((BASE+2)*4+J)) )GO TO 102
        END DO
        CALL DELETE_TEXT_VARIABLE( I )
        GO TO 105
  102 CONTINUE
  105 DO I = 1, VARMAX
        BASE = (VARADR+(I-1)*VARLEN)/4              
        IF( I4D(BASE+1) .EQ. 0 )THEN    ! found an empty section
          INEW = I
          GO TO 200
        END IF
      END DO
      IF( (VARMAX+1)*VARLEN .GT. VARALC )THEN
        CALL CHSIZE( VARADR, VARALC, VARALC+512, 1, *9000 )
        DO I = 1, 512
          L1D(VARADR+VARALC+I) = 0
        END DO
        VARALC = VARALC+512
      END IF
      VARMAX = VARMAX+1
      INEW = VARNUM+1
  200 VARNUM = VARNUM+1
      BASE = (VARADR+(INEW-1)*VARLEN)/4
      I4D(BASE+1) = LNM
      DO J = 1, LNM
        L1D((BASE+2)*4+J) = ICHAR(NAME(J:J))
      END DO
      I4D(BASE+11) = NDIMX
      IF( NDIMX .EQ. 0 )THEN
        DO I = 1, TSCAL                ! check the existence table for
          IF( L1D(ESCAL+I).EQ.0 )THEN  !  an empty slot, 1 means occupied
            I4D(BASE+12) = I           ! index to scalar table
            L1D(ESCAL+I4D(BASE+12)) = 1
            R8D(ASCAL+I4D(BASE+12)) = VALX
            NSCAL = NSCAL+1
            GO TO 500
          END IF
        END DO
C                                   all the slots are full, get more space
        CALL CHSIZE( ASCAL, TSCAL, TSCAL+64, 8, *9001 )
        CALL CHSIZE( ESCAL, TSCAL, TSCAL+64, 1, *9002 )
        DO I = TSCAL+1, TSCAL+64
          L1D(ESCAL+I) = 0
          R8D(ASCAL+I) = 0.0D0
        END DO
        NSCAL = NSCAL+1
        I4D(BASE+12) = NSCAL
        TSCAL = TSCAL+64
        L1D(ESCAL+I4D(BASE+12)) = 1
        R8D(ASCAL+I4D(BASE+12)) = VALX
      ELSE IF( NDIMX .EQ. 1 )THEN
        i4d(base+10) = 0     ! indicates not ordered
        I4D(BASE+13) = NUMX
        I4D(BASE+16) = 1
        I4D(BASE+19) = NUMX
        CALL CHSIZE(I4D(BASE+12),0,NUMX,8,*9003)
        IF( TYPE .EQ. 0 )THEN
          DO I = 1, NUMX
            R8D(I4D(BASE+12)+I) = DBLE(FLOAT(I4D(INITX+I)))
          END DO
        ELSE IF( TYPE .EQ. 1 )THEN
          DO I = 1, NUMX
            R8D(I4D(BASE+12)+I) = DBLE(R4D(INITX+I))
          END DO
        END IF
      ELSE IF( NDIMX .EQ. 2 )THEN
        I4D(BASE+13) = NUMX
        I4D(BASE+14) = NCOLX
        I4D(BASE+16) = 1
        I4D(BASE+17) = 1
        I4D(BASE+19) = NUMX
        I4D(BASE+20) = NCOLX
        CALL CHSIZE(I4D(BASE+12),0,NUMX*NCOLX,8,*9004)
        IF( TYPE .EQ. 0 )THEN
          DO I = 1, NUMX*NCOLX
            R8D(I4D(BASE+12)+I) = DBLE(FLOAT(I4D(INITX+I)))
          END DO
        ELSE IF( TYPE .EQ. 1 )THEN
          DO I = 1, NUMX*NCOLX
            R8D(I4D(BASE+12)+I) = DBLE(R4D(INITX+I))
          END DO
        END IF
      END IF
  500 CALL CHSIZE(I4D(BASE+24),0,1,4,*9100)
      CALL CHSIZE(I4D(BASE+25),0,LNL,1,*9101)
      I4D(BASE+22) = 1                              ! number of history lines
      I4D(BASE+23) = LNL                            ! total length of history 
      DO J = 1, LNL
        L1D(I4D(BASE+25)+J) = ICHAR(LINE(J:J))
      END DO
      I4D(I4D(BASE+24)+1) = LNL                     ! length of history line
      RETURN
 9000 CALL ERROR_MESSAGE('PUT_HBOOK_VARIABLE routine'
     & ,'creating dynamic array space for the numeric variable table')
      RETURN 1
 9001 CALL ERROR_MESSAGE('PUT_HBOOK_VARIABLE routine'
     & ,'creating dynamic array space for the scalar data table')
      RETURN 1
 9002 CALL ERROR_MESSAGE('PUT_HBOOK_VARIABLE routine'
     & ,'creating dynamic array space for the auxiliary scalar'//
     &  ' data table')
      CALL CHSIZE( ASCAL, TSCAL+64, TSCAL, 8, *9400 )
      RETURN 1
 9003 CALL ERROR_MESSAGE('PUT_HBOOK_VARIABLE routine'
     & ,'creating dynamic array space for the data for a vector')
      RETURN 1
 9004 CALL ERROR_MESSAGE('PUT_HBOOK_VARIABLE routine'
     & ,'creating dynamic array space for the data for a matrix')
      RETURN 1
 9100 CALL ERROR_MESSAGE('PUT_HBOOK_VARIABLE routine'
     & ,'creating dynamic array space for history line lengths')
      RETURN 
 9101 CALL ERROR_MESSAGE('PUT_HBOOK_VARIABLE routine'
     & ,'creating dynamic array space for history line characters')
      CALL CHSIZE(I4D(BASE+24),1,0,4,*9500)
      RETURN 
 9400 CALL ERROR_MESSAGE('PUT_HBOOK_VARIABLE routine'
     & ,'deallocating dynamic array space for the scalar data table')
      RETURN 1
 9500 CALL ERROR_MESSAGE('PUT_HBOOK_VARIABLE routine'
     & ,'deallocating dynamic array space for the history line'//
     &  ' lengths array')
      RETURN 1
      END
