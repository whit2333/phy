      SUBROUTINE PAUSE2(MESS,KEYRET)

C      This routine will pause until a key is typed on the terminal.
C    Any key will cause a RETURN. The key corresponding to ICHAR(KEYRET)
C    is the key that was typed. Finally, if MESS is .NE. ' ', then it is
C    the prompt.  If MESS is .EQ. ' ', then prompt defaults to 'Pausing...'
C 
C    INPUT:         MESS  CHARACTER*(*)
C    OUTPUT:        KEYRET INTEGER*4
C
C                                      Author:  Joe Chuma
C                                               June 29, 1984
C                                               TRIUMF
C
C  Modified by J.Chuma, 20Mar97 for g77
C    can't have intrinsic functions inside a PARAMETER statement
C    also, can't have variable formats
C  Modified by J.Chuma, 15Jul97 
C    removed all the column, row information, since it doesn't work
C    on the unix systems
C
      CHARACTER*(*) MESS
      INTEGER       KEYRET

      CHARACTER*1   ALTKEY, READ_KEY, KEY_IN
      CHARACTER*1024 PROMPT

      INTEGER IMONITOR, IOUTM
      COMMON /PLOTMONITOR/ IMONITOR,IOUTM
CCC
      PROMPT = ' Pausing...'
      NP     = 11
      LMESS  = LEN(MESS)
      IF( LMESS .NE. 0 )THEN
        IF( MESS(1:LMESS) .NE. ' ' )THEN
          PROMPT = ' '//MESS(1:LMESS)
          NP = 1+LMESS
        END IF
      END IF
      IF( IMONITOR .GT. 0 )CALL TRANSPARENT_MODE( 0 )
   40 KEY_IN = READ_KEY('KEYPAD PROMPT',PROMPT(1:NP),ALTKEY,IDUM)
      KEYRET = ICHAR( KEY_IN )
#ifdef VMS
#else
      KEY_IN = READ_KEY('RESET',' ',ALTKEY,0)
      write(*,*)
#endif
      RETURN
      END
