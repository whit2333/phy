      SUBROUTINE GRID( * )

C  GRID  x y z m
C  GRID\SIZE  size  x y z m
C  GRID\XYOUT  x y z m xout yout 
C  GRID\SIZE\XYOUT  size x y z m xout yout 
C  GRID\BOUNDS  x y z m minx maxx miny maxy
C  GRID\SIZE\BOUNDS  size x y z m minx maxx miny maxy
C  GRID\XYOUT\BOUNDS  x y z m xout yout minx maxx miny maxy
C  GRID\SIZE\XYOUT\BOUNDS  size x y z m xout yout minx maxx miny maxy
C  GRID\SIZE matrix { size } x y z { xout yout } { minx maxx miny maxy }
C
C  GRID\PATTERN x y z m
C  GRID\PATTERN\XYOUT x y z m xout yout
C
C  GRID\INDICES   x y z m
C  GRID\INDICES\XYOUT  x y z m  xout yout

C    This PHYSICA routine takes a scattered set of data points
C    (X(i), Y(i)) with associated amplitudes Z(i) and interpolates
C    ZI(j) onto a regular grid, (XI(j), YI(j))

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT, MXCHAR
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20,   MXCHAR=1024)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      CHARACTER*(LENVSM) NAMEG
      CHARACTER*256      NAMEV, NAMET
      COMMON /GETVARIABLEERROR/ NAMEG, LNG, NAMEV, LNV, NAMET, LNT

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

      LOGICAL*4 CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED

C   local variables

      CHARACTER*15  SOUT, SOUT2
      CHARACTER*1   BS
      REAL*8        XMIN, XMAX, XINC, YMIN, YMAX, YINC, EPS, XTMP, SIZE
      INTEGER*4     IDX1(4,3), IDX2(4,3), IDX3(4,3)
      LOGICAL*4     VALUE_GIVEN
      LOGICAL*1     POLAR, INTERP, INDICES, SIZE_GIVEN, INVERT, CHECK
     &             ,XYOUT, BOUNDS
CCC
      BS = CHAR(92) ! backslash

      STRINGS(1) = 'GRID'
      LENST(1) = 4

      POLAR      = .FALSE.
      INTERP     = .TRUE.
      INDICES    = .FALSE.
      CHECK      = .FALSE.       ! default = do not check for duplicate points
      SIZE_GIVEN = .FALSE.
      XYOUT      = .FALSE.
      BOUNDS     = .FALSE.
      DO I = 1, NQUAL(1)
        IF( QUALIFIERS(1,I)(1:1) .EQ. '-' )THEN
          II = 2
        ELSE
          II = 1
        END IF
        L = LENQL(1,I)
        IF( INDEX('POLAR',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            POLAR = .TRUE.
          ELSE
            POLAR = .FALSE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('NOPOLAR',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            POLAR = .FALSE.
          ELSE
            POLAR = .TRUE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('INTERPOLATE',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            INTERP = .TRUE.
          ELSE
            INTERP = .FALSE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('NOINTERPOLATE',QUALIFIERS(1,I)(II:L)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            INTERP = .FALSE.
          ELSE
            INTERP = .TRUE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('PATTERN',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            INTERP = .FALSE.
          ELSE
            INTERP = .TRUE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('INDICES',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            INDICES = .TRUE.
          ELSE
            INDICES = .FALSE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('NOINDICES',QUALIFIERS(1,I)(II:L)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            INDICES = .FALSE.
          ELSE
            INDICES = .TRUE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('SIZE',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            SIZE_GIVEN = .TRUE.
          ELSE
            SIZE_GIVEN = .FALSE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('NOSIZE',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            SIZE_GIVEN = .FALSE.
          ELSE
            SIZE_GIVEN = .TRUE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('XYOUT',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            XYOUT = .TRUE.
          ELSE
            XYOUT = .FALSE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('NOXYOUT',QUALIFIERS(1,I)(II:L)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            XYOUT = .FALSE.
          ELSE
            XYOUT = .TRUE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('BOUNDS',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            BOUNDS = .TRUE.
          ELSE
            BOUNDS = .FALSE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('NOBOUNDS',QUALIFIERS(1,I)(II:L)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            BOUNDS = .FALSE.
          ELSE
            BOUNDS = .TRUE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('CHECKDUP',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            CHECK = .TRUE.
          ELSE
            CHECK = .FALSE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('NOCHECKDUP',QUALIFIERS(1,I)(II:L)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            CHECK = .FALSE.
          ELSE
            CHECK = .TRUE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE
          CALL WARNING_MESSAGE('GRID'
     &     ,'unknown qualifier: '//QUALIFIERS(1,I)(1:L))
        END IF
      END DO

      ICNT = 2
      IF( SIZE_GIVEN )THEN
        IF( .NOT.INTERP )THEN
          IF( INDICES )THEN
            CALL ERROR_MESSAGE('GRID','SIZE incompatible with INDICES')
            GO TO 92
          END IF
          CALL ERROR_MESSAGE('GRID','SIZE incompatible with PATTERN')
          GO TO 92
        END IF
        CALL GET_SCALAR( STRINGS(2), LENST(2), REALS(2), ITYPE(2)
     &   ,'GRID', 'matrix output size', VALUE_GIVEN, .FALSE., 0.0D0,*92)
        SIZE = REALS(2)
        IF( SIZE .LT. 0.0D0 )THEN
          CALL ERROR_MESSAGE('GRID','output matrix size < 0')
          GO TO 92
        END IF
        IF( SIZE .EQ. 0.0D0 )THEN
          CALL ERROR_MESSAGE('GRID','output matrix size = 0')
          GO TO 92
        END IF
        STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(2)(1:LENST(2))
        LENST(1)   = LENST(1)+1+LENST(2)
        ICNT = ICNT+1
      END IF

      CALL GET_VECTOR( STRINGS(ICNT), LENST(ICNT), ITYPE(ICNT), 'GRID'
     & ,'x vector', INITX, NUMX, *92, *92 )
      STRINGS(1) =
     & STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
      LENST(1)   = LENST(1)+1+LENST(ICNT)
      IX = ICNT
      ICNT = ICNT+1

      CALL GET_VECTOR( STRINGS(ICNT), LENST(ICNT), ITYPE(ICNT), 'GRID'
     & ,'y vector', INITY, NUMY, *92, *92 )
      IF( NUMY .NE. NUMX )CALL WARNING_MESSAGE('GRID',
     & 'x and y vectors have different lengths, using minimum')
      NUM = MIN( NUMX, NUMY )
      STRINGS(1) =
     & STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
      LENST(1)   = LENST(1)+1+LENST(ICNT)
      IY = ICNT
      ICNT = ICNT+1

      CALL GET_VECTOR( STRINGS(ICNT), LENST(ICNT), ITYPE(ICNT), 'GRID'
     & ,'z vector', INITZ, NUMZ, *92, *92 )
      IF( NUMZ .NE. NUM )CALL WARNING_MESSAGE('GRID'
     & ,'x and z vectors have different lengths, using minimum')
      NUM = MIN( NUM, NUMZ )
      IF( INTERP .AND. .NOT.INDICES .AND. (NUM.LT.4) )THEN
        CALL ERROR_MESSAGE('GRID'
     &   ,'grid interpolation requires at least 4 points')
        GO TO 92
      END IF
      STRINGS(1) =
     & STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
      LENST(1)   = LENST(1)+1+LENST(ICNT)
      ICNT = ICNT+1

      IF( ITYPE(ICNT) .NE. 2 )THEN
        CALL ERROR_MESSAGE('GRID','expecting output matrix')
        GO TO 92
      END IF
      CALL GET_OUTPUT_VARIABLE( STRINGS(ICNT)(1:LENST(ICNT)), INDM
     & ,NDIMM, IDX1(1,1), IDX2(1,1), IDX3(1,1), IER )
      IF( IER .NE. 0 )THEN
        CALL GET_VARIABLE_ERROR('GRID',IER)
        GO TO 92
      END IF
      STRINGS(1) =
     & STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
      LENST(1)   = LENST(1)+1+LENST(ICNT)
      STRINGS(ICNT) = NAMEG
      LENST(ICNT)   = LNG
      IMNAME = ICNT
      ICNT = ICNT+1

      IF( CHECK )THEN
        CALL CHSIZE(IDUP,0,NUM,4)
        NDUP = 0
        DO I = 1, NUM-1
          DO J = I+1, NUM
            IF( CTRLC_CALLED )THEN
              CALL CHSIZE(IDUP,NUM,0,4)
              GO TO 90
            END IF
            IF( R8D(INITX+I) .EQ. R8D(INITX+J) )THEN
              IF( R8D(INITY+I) .EQ. R8D(INITY+J) )THEN
                CALL INTEGER_TO_CHAR(I,.FALSE.,SOUT,LS1)
                CALL INTEGER_TO_CHAR(J,.FALSE.,SOUT2,LS2)
                CALL WARNING_MESSAGE('GRID'
     &           ,'duplicate points found at '//
     &            'indices '//SOUT(1:LS1)//' and '//SOUT2(1:LS2))
                I4D(IDUP+NDUP+1) = J
                NDUP = NDUP+1
              END IF
            END IF
          END DO
        END DO
        IF( NDUP .GT. 0 )THEN
          CALL INTEGER_TO_CHAR(NDUP,.FALSE.,SOUT,LS1)
          CALL RITE('               '//SOUT(1:LS1)//
     &     ' duplicate points found')
          DO I = 1, NDUP
            IF( CTRLC_CALLED )THEN
              CALL CHSIZE(IDUP,NUM,0,4)
              GO TO 90
            END IF
            J = NDUP-I+1
            DO K = I4D(IDUP+J)+1, NUM
              R8D(INITX+K-1) = R8D(INITX+K)
              R8D(INITY+K-1) = R8D(INITY+K)
              R8D(INITZ+K-1) = R8D(INITZ+K)
            END DO
          END DO
        END IF
        CALL CHSIZE(IDUP,NUM,0,4)
        NUM = NUM - NDUP
      END IF
      IF( POLAR )THEN
        DO I = 1, NUM
          XTMP = R8D(INITX+I) * COSD(R8D(INITY+I))
          R8D(INITY+I) = R8D(INITX+I) * SIND(R8D(INITY+I))
          R8D(INITX+I) = XTMP
        END DO
      END IF

      IF( INDICES )GO TO 40
      IF( INTERP )GO TO 50

C   GRID\PATTERN x y z m
C   GRID\PATTERN\XYOUT x y z m xout yout
C
C   Make matrix without interpolation using a full data set 

      NROW = 1
      DO WHILE ( R8D(INITX+1) .EQ. R8D(INITX+NROW+1) )
        NROW = NROW + 1
      END DO
      IF( NROW .EQ. 1 )THEN
        INVERT = .TRUE.
        NCOL   = 1
        DO WHILE ( R8D(INITY+1) .EQ. R8D(INITY+NCOL+1) )
          NCOL = NCOL + 1
        END DO
        NROW = NUM / NCOL
        IF( NUM .NE. NROW*NCOL )THEN
          CALL ERROR_MESSAGE('GRID'
     &     ,'non-interpolated matrix is not possible with this data')
          GO TO 92
        END IF
        CALL GET_TEMP_SPACE(INITYI,NROW,8)
        DO I = 1, NROW
          R8D(INITYI+I) = R8D(INITY+1+(I-1)*NCOL)
        END DO
        CALL GET_TEMP_SPACE(INITXI,NCOL,8)
        IF( R8D(INITX+2) .LT. R8D(INITX+1) )THEN
          DO I = 1, NCOL
            R8D(INITXI+I) = R8D(INITX+NCOL-I+1)
          END DO
          DO I = 1, NCOL/2
            XTMP = R8D(INITZ+I)
            R8D(INITZ+I) = R8D(INITZ+NCOL-I+1)
            R8D(INITZ+NCOL-I+1) = XTMP
          END DO
        ELSE
          DO I = 1, NCOL
            R8D(INITXI+I) = R8D(INITX+I)
          END DO
        END IF
        DO J = 2, NROW
          IF( R8D(INITX+2+(J-1)*NCOL) .LT. R8D(INITX+1+(J-1)*NCOL) )THEN
            DO I = 1, NCOL/2
              XTMP = R8D(INITZ+I+(J-1)*NCOL)
              R8D(INITZ+I+(J-1)*NCOL) = R8D(INITZ+NCOL-I+1+(J-1)*NCOL)
              R8D(INITZ+NCOL-I+1+(J-1)*NCOL) = XTMP
            END DO
          END IF
        END DO
      ELSE
        INVERT = .FALSE.
        NCOL   = NUM / NROW
        IF( NUM .NE. NROW*NCOL )THEN
          CALL ERROR_MESSAGE('GRID'
     &     ,'non-interpolated matrix is not possible with this data')
          GO TO 92
        END IF
        CALL GET_TEMP_SPACE(INITXI,NCOL,8)
        DO I = 1, NCOL
          R8D(INITXI+I) = R8D(INITX+1+(I-1)*NROW)
        END DO
        CALL GET_TEMP_SPACE(INITYI,NROW,8)
        IF( R8D(INITY+2) .LT. R8D(INITY+1) )THEN
          DO I = 1, NROW
            R8D(INITYI+I) = R8D(INITY+NROW-I+1)
          END DO
          DO I = 1, NROW/2
            XTMP = R8D(INITZ+I)
            R8D(INITZ+I) = R8D(INITZ+NROW-I+1)
            R8D(INITZ+NROW-I+1) = XTMP
          END DO
        ELSE
          DO I = 1, NROW
            R8D(INITYI+I) = R8D(INITY+I)
          END DO
        END IF
        DO J = 2, NCOL
          IF( R8D(INITY+2+(J-1)*NROW) .LT. R8D(INITY+1+(J-1)*NROW) )THEN
            DO I = 1, NROW/2
              XTMP = R8D(INITZ+I+(J-1)*NROW)
              R8D(INITZ+I+(J-1)*NROW) = R8D(INITZ+NROW-I+1+(J-1)*NROW)
              R8D(INITZ+NROW-I+1+(J-1)*NROW) = XTMP
            END DO
          END IF
        END DO
      END IF
      IF( XYOUT )THEN
        IF( ITYPE(ICNT) .NE. 2 )THEN
          CALL ERROR_MESSAGE('GRID','expecting xout vector')
          GO TO 92
        END IF
        CALL GET_OUTPUT_VARIABLE( STRINGS(ICNT)(1:LENST(ICNT)), INDM
     &   ,NDIMX, IDX1(1,2), IDX2(1,2), IDX3(1,2), IER )
        IF( IER .NE. 0 )THEN
          CALL GET_VARIABLE_ERROR('GRID',IER)
          GO TO 92
        END IF
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1)   = LENST(1)+1+LENST(ICNT)
        STRINGS(ICNT) = NAMEG
        LENST(ICNT)   = LNG
        IXNAME = ICNT
        ICNT = ICNT+1

        IF( ITYPE(ICNT) .NE. 2 )THEN
          CALL ERROR_MESSAGE('GRID','expecting yout vector')
          GO TO 92
        END IF
        CALL GET_OUTPUT_VARIABLE( STRINGS(ICNT)(1:LENST(ICNT)), INDM
     &   ,NDIMY, IDX1(1,3), IDX2(1,3), IDX3(1,3), IER )
        IF( IER .NE. 0 )THEN
          CALL GET_VARIABLE_ERROR('GRID',IER)
          GO TO 92
        END IF
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1)   = LENST(1)+1+LENST(ICNT)
        STRINGS(ICNT) = NAMEG
        LENST(ICNT)   = LNG
        IYNAME = ICNT
        ICNT = ICNT+1
      END IF
      IF( NFLD .GT. ICNT )
     & CALL WARNING_MESSAGE('GRID','extra parameters ignored')

      CALL GET_TEMP_SPACE(IARR,NROW*NCOL,8)
      II = 0
      IF( INVERT )THEN
        DO I = 1, NROW
          DO J = 1, NCOL
            II = II + 1
            R8D(IARR+I+(J-1)*NROW) = R8D(INITZ+II)
          END DO
        END DO
      ELSE
        DO J = 1, NCOL
          DO I = 1, NROW
            II = II + 1
            R8D(IARR+I+(J-1)*NROW) = R8D(INITZ+II)
          END DO
        END DO
      END IF
      IF( XYOUT )THEN
        CALL PUT_VARIABLE( STRINGS(IXNAME)(1:LENST(IXNAME)), NDIMX
     &   ,IDX1(1,2), IDX2(1,2), IDX3(1,2)
     &   ,1, 0.0D0, INITXI, NCOL, 0,0, STRINGS(1)(1:LENST(1)),'GRID'
     &   ,*92)
        CALL PUT_VARIABLE( STRINGS(IYNAME)(1:LENST(IYNAME)), NDIMY
     &   ,IDX1(1,3), IDX2(1,3), IDX3(1,3)
     &   ,1, 0.0D0, INITYI, NROW, 0,0, STRINGS(1)(1:LENST(1)),'GRID'
     &   ,*92)
      END IF
      CALL PUT_VARIABLE( STRINGS(IMNAME)(1:LENST(IMNAME)), NDIMM
     & ,IDX1(1,1), IDX2(1,1), IDX3(1,1)
     & ,2, 0.0D0, IARR, NROW, NCOL, 0, STRINGS(1)(1:LENST(1)),'GRID'
     & ,*92)
      GO TO 80

C   Make matrix without interpolation with INDICES data

   40 NCOL = INT( R8D(INITX+1) )
      DO I = 1, NUM
        IF( NCOL .LT. INT(R8D(INITX+I)) )NCOL = INT(R8D(INITX+I))
      END DO
      NROW = INT( R8D(INITY+1) )
      DO I = 1, NUM
        IF( NROW .LT. INT(R8D(INITY+I)) )NROW = INT(R8D(INITY+I))
      END DO

      IF( XYOUT )THEN
        IF( ITYPE(ICNT) .NE. 2 )THEN
          CALL ERROR_MESSAGE('GRID','expecting xout vector')
          GO TO 92
        END IF
        CALL GET_OUTPUT_VARIABLE( STRINGS(ICNT)(1:LENST(ICNT)), INDM
     &   ,NDIMX, IDX1(1,2), IDX2(1,2), IDX3(1,2), IER )
        IF( IER .NE. 0 )THEN
          CALL GET_VARIABLE_ERROR('GRID',IER)
          GO TO 92
        END IF
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1)   = LENST(1)+1+LENST(ICNT)
        STRINGS(ICNT) = NAMEG
        LENST(ICNT)   = LNG
        IXNAME = ICNT
        ICNT = ICNT+1

        IF( ITYPE(ICNT) .NE. 2 )THEN
          CALL ERROR_MESSAGE('GRID','expecting yout vector')
          GO TO 92
        END IF
        CALL GET_OUTPUT_VARIABLE( STRINGS(ICNT)(1:LENST(ICNT)), INDM
     &   ,NDIMY, IDX1(1,3), IDX2(1,3), IDX3(1,3), IER )
        IF( IER .NE. 0 )THEN
          CALL GET_VARIABLE_ERROR('GRID',IER)
          GO TO 92
        END IF
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1)   = LENST(1)+1+LENST(ICNT)
        STRINGS(ICNT) = NAMEG
        LENST(ICNT)   = LNG
        IYNAME = ICNT
        ICNT = ICNT+1
        CALL GET_TEMP_SPACE(INITYI,NROW,8)
        DO I = 1, NROW
          R8D(INITYI+I) = I
        END DO
        CALL GET_TEMP_SPACE(INITXI,NCOL,8)
        DO I = 1, NCOL
          R8D(INITXI+I) = I
        END DO
      END IF
      IF( NFLD .GT. ICNT )
     & CALL WARNING_MESSAGE('GRID','extra parameters ignored')
      CALL GET_TEMP_SPACE(IARR,NROW*NCOL,8)
      CALL ZERO_ARRAY(R8D(IARR+1),8*NROW*NCOL)
      DO I = 1, NUM
        JROW = INT( R8D(INITY+I) )
        JCOL = INT( R8D(INITX+I) )
        R8D(IARR+JROW+(JCOL-1)*NROW) = R8D(INITZ+I)
      END DO
      IF( XYOUT )THEN
        CALL PUT_VARIABLE( STRINGS(IXNAME)(1:LENST(IXNAME)), NDIMX
     &   ,IDX1(1,2), IDX2(1,2), IDX3(1,2)
     &   ,1, 0.0D0, INITXI, NCOL, 0,0, STRINGS(1)(1:LENST(1)),'GRID'
     &   ,*92)
        CALL PUT_VARIABLE( STRINGS(IYNAME)(1:LENST(IYNAME)), NDIMY
     &   ,IDX1(1,3), IDX2(1,3), IDX3(1,3)
     &   ,1, 0.0D0, INITYI, NROW, 0,0, STRINGS(1)(1:LENST(1)),'GRID'
     &   ,*92)
      END IF
      CALL PUT_VARIABLE( STRINGS(IMNAME)(1:LENST(IMNAME)), NDIMM
     & ,IDX1(1,1), IDX2(1,1), IDX3(1,1)
     & ,2, 0.0D0, IARR, NROW, NCOL, 0, STRINGS(1)(1:LENST(1)),'GRID'
     & ,*92)
      GO TO 80

C   Make matrix using interpolation

   50 IF( SIZE_GIVEN )THEN
        NCOL = INT( SIZE )
      ELSE
        NCOL = INT( 5.0D0 * SQRT(1.0D0*NUM) )
      END IF
      NROW = NCOL
      IF( CTRLC_CALLED )GO TO 90
      CALL GET_TEMP_SPACE(IARR,NROW*NCOL,8)

      IF( XYOUT )THEN
        IF( ITYPE(ICNT) .NE. 2 )THEN
          CALL ERROR_MESSAGE('GRID','expecting xout vector')
          GO TO 92
        END IF
        CALL GET_OUTPUT_VARIABLE( STRINGS(ICNT)(1:LENST(ICNT)), INDM
     &   ,NDIMX, IDX1(1,2), IDX2(1,2), IDX3(1,2), IER )
        IF( IER .NE. 0 )THEN
          CALL GET_VARIABLE_ERROR('GRID',IER)
          GO TO 92
        END IF
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1)   = LENST(1)+1+LENST(ICNT)
        STRINGS(ICNT) = NAMEG
        LENST(ICNT)   = LNG
        IXNAME = ICNT
        ICNT = ICNT+1

        IF( ITYPE(ICNT) .NE. 2 )THEN
          CALL ERROR_MESSAGE('GRID','expecting yout vector')
          GO TO 92
        END IF
        CALL GET_OUTPUT_VARIABLE( STRINGS(ICNT)(1:LENST(ICNT)), INDM
     &   ,NDIMY, IDX1(1,3), IDX2(1,3), IDX3(1,3), IER )
        IF( IER .NE. 0 )THEN
          CALL GET_VARIABLE_ERROR('GRID',IER)
          GO TO 92
        END IF
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1)   = LENST(1)+1+LENST(ICNT)
        STRINGS(ICNT) = NAMEG
        LENST(ICNT)   = LNG
        IYNAME = ICNT
        ICNT = ICNT+1
      END IF

C    If the xmin, xmax, ymin, and ymax were not entered as parameters
C    with the PHYSICA command then they will be taken from the minimum
C    and the maximum of the data

      IF( BOUNDS )THEN
        CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT)
     &   ,ITYPE(ICNT), 'GRID', 'xmin', VALUE_GIVEN, .FALSE., 0.0D0, *92)
        XMIN = REALS(ICNT)
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
        ICNT = ICNT+1

        CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT)
     &   ,ITYPE(ICNT), 'GRID', 'xmax', VALUE_GIVEN, .FALSE., 0.0D0, *92)
        XMAX = REALS(ICNT)
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
        ICNT = ICNT+1

        CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT)
     &   ,ITYPE(ICNT), 'GRID', 'ymin', VALUE_GIVEN, .FALSE., 0.0D0, *92)
        YMIN = REALS(ICNT)
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
        ICNT = ICNT+1

        CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT)
     &   ,ITYPE(ICNT), 'GRID', 'ymax', VALUE_GIVEN, .FALSE., 0.0D0, *92)
        YMAX = REALS(ICNT)
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
        ICNT = ICNT+1
      ELSE
        XMIN = R8D(INITX+1)
        XMAX = XMIN
        YMIN = R8D(INITY+1)
        YMAX = YMIN
        DO I = 1, NUM
          IF( XMIN .GT. R8D(INITX+I) )XMIN = R8D(INITX+I)
          IF( XMAX .LT. R8D(INITX+I) )XMAX = R8D(INITX+I)
          IF( YMIN .GT. R8D(INITY+I) )YMIN = R8D(INITY+I)
          IF( YMAX .LT. R8D(INITY+I) )YMAX = R8D(INITY+I)
        END DO
      END IF
      XINC = (XMAX-XMIN)/(NCOL-1)
      YINC = (YMAX-YMIN)/(NCOL-1)
      CALL GET_TEMP_SPACE(INITXI,NCOL,8)
      CALL GET_TEMP_SPACE(INITYI,NCOL,8)
      DO I = 1, NCOL-1
        R8D(INITXI+I) = XMIN + (I-1)*XINC
        R8D(INITYI+I) = YMIN + (I-1)*YINC
      END DO
      R8D(INITXI+NCOL) = XMAX
      R8D(INITYI+NCOL) = YMAX
      NUMADJ  = 6 * NUM - 9
      NUMZXZY = 2 * NUM
      CALL GET_TEMP_SPACE(IND,NUM,4)
      CALL GET_TEMP_SPACE(IADJ,NUMADJ,4)
      CALL GET_TEMP_SPACE(IZXZY,NUMZXZY,8)
      CALL ZERO_ARRAY(R8D(IZXZY+1),8*NUMZXZY)

      IF( TRAP )CALL LOK_E_TRAP( *94, ISTAT )
      IF( CTRLC_CALLED )GO TO 90
      CALL REORDR( NUM,3,R8D(INITX+1),R8D(INITY+1),R8D(INITZ+1)
     & ,I4D(IND+1) )

      IF( CTRLC_CALLED )GO TO 90
      CALL TRMESH( NUM,R8D(INITX+1),R8D(INITY+1),I4D(IADJ+1)
     & ,I4D(IND+1),IER )
      IF( CTRLC_CALLED )GO TO 90

C  IER = 0 if no errors were encountered.
C  IER = 1 if num .lt. 3.
C  IER = 2 if num .ge. 3 and all nodes are collinear.

      IF( IER .EQ. 1 )THEN
        CALL ERROR_MESSAGE('GRID'
     &   ,'error return from TRMESH: number of points < 3')
        CALL RITE('NO MATRIX HAS BEEN MADE')
        GO TO 92
      ELSE IF( IER .EQ. 2 )THEN
        CALL ERROR_MESSAGE('GRID'
     &   ,'error return from TRMESH: all data points are collinear')
        CALL RITE('NO MATRIX HAS BEEN MADE')
        GO TO 92
      END IF
      EPS = 1.0D-3
      NIT = 128
      CALL GRADG( NUM, R8D(INITX+1), R8D(INITY+1), R8D(INITZ+1)
     & ,I4D(IADJ+1), I4D(IND+1), EPS, NIT, R8D(IZXZY+1), IER )
      IF( CTRLC_CALLED )GO TO 90

C  IER = 0 if the convergence criterion was achieved.
C  IER = 1 if convergence was not achieved within NIT iterations.
C  IER = 2 if NUM or EPS is out of range or NIT < 0 on input.

      IF( IER .EQ. 1 )THEN
        CALL ERROR_MESSAGE('GRID','error return from GRADG: '//
     &  'convergence not achieved within 128 iterations')
        CALL RITE('NO MATRIX HAS BEEN MADE')
        GO TO 92
      ELSE IF( IER .EQ. 2 )THEN
        CALL ERROR_MESSAGE('GRID','error return from GRADG: '//
     &  'number of iterations or tolerance out of range')
        CALL RITE('NO MATRIX HAS BEEN MADE')
        GO TO 92
      END IF
      IFLAG = 1
      CALL UNIF( NUM, R8D(INITX+1), R8D(INITY+1), R8D(INITZ+1)
     &            ,I4D(IADJ+1), I4D(IND+1), NROW, NROW, NROW
     &            ,R8D(INITXI+1), R8D(INITYI+1), IFLAG, R8D(IZXZY+1)
     &            ,R8D(IARR+1), IER )
      IF( CTRLC_CALLED )GO TO 90
   79 IF( IER .EQ. -2 )THEN
        CALL ERROR_MESSAGE('GRID','all data points are collinear')
        CALL RITE('NO MATRIX HAS BEEN MADE')
        GO TO 92
      ELSE IF( IER .GE. 0 )THEN
        IF( XYOUT )THEN
          CALL PUT_VARIABLE( STRINGS(IXNAME)(1:LENST(IXNAME)), NDIMX
     &     ,IDX1(1,2), IDX2(1,2), IDX3(1,2)
     &     ,1, 0.0D0, INITXI, NCOL, 0,0, STRINGS(1)(1:LENST(1)),'GRID'
     &     ,*92)
          CALL PUT_VARIABLE( STRINGS(IYNAME)(1:LENST(IYNAME)), NDIMY
     &     ,IDX1(1,3), IDX2(1,3), IDX3(1,3)
     &     ,1, 0.0D0, INITYI, NROW, 0,0, STRINGS(1)(1:LENST(1)),'GRID'
     &     ,*92)
        END IF
        CALL PUT_VARIABLE( STRINGS(IMNAME)(1:LENST(IMNAME)), NDIMM
     &   ,IDX1(1,1), IDX2(1,1), IDX3(1,1)
     &   ,2, 0.0D0, IARR, NROW, NCOL, 0, STRINGS(1)(1:LENST(1)),'GRID'
     &   ,*92)
        IF( (NUNIT .EQ. 1) .OR. ECHO )THEN
          CALL RITE('GRID completed successfully')
          CALL INTEGER_TO_CHAR(NCOL,.FALSE.,SOUT,LENS)
          CALL RITE('matrix '//STRINGS(IMNAME)(1:LENST(IMNAME))//
     &    ' has '//SOUT(1:LENS)//' rows and '//SOUT(1:LENS)//' columns')
        END IF
      ELSE
        CALL INTEGER_TO_CHAR(IER,.FALSE.,SOUT,LENS)
        CALL ERROR_MESSAGE('GRID','error number '//SOUT)
        CALL RITE('NO MATRIX HAS BEEN MADE')
        GO TO 92
      END IF                    
      IF( TRAP )CALL LOK_E_UNTRAP
   80 IF( STACK )WRITE(IOUT_UNIT,20)STRINGS(1)(1:LENST(1))
   20 FORMAT(' ',A)
   90 CALL DELETE_TEMP_SPACE
      RETURN
   92 CALL DELETE_TEMP_SPACE
      RETURN 1
   94 IF( TRAP )CALL LOK_E_UNTRAP
      CALL ERROR_MESSAGE('GRID','error in grid interpolation')
      CALL PUT_SYSMSG( ISTAT )
      CALL DELETE_TEMP_SPACE
      RETURN 1
      END
