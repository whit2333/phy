      SUBROUTINE PD_CROSS_HAIR(STR1,STR2,STR3,XIN,YIN,LINE,NLNE,FIRST
     &                        ,XB,YB,XBC,YBC,GRAPH_UNITS,XHAIR,*)

C  GRAPH_UNITS =  0 --> graph units
C              =  1 --> percentages of the window
C              =  2 --> world units ( cm | in )
C              = -1 --> percentages of the world

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

      CHARACTER*(*) STR1, STR2, STR3
      CHARACTER*15  SOUT
      CHARACTER*1024 LINE
      REAL*4        XB, YB, XBC, YBC
      INTEGER*4     GRAPH_UNITS, NLNE
      LOGICAL*4     XIN, YIN, FIRST, XHAIR

      INTEGER*4 IINS
      COMMON /PLOT_INPUT_UNIT/ IINS

      INTEGER*4 IOUTS
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS

      REAL*4    XMINHP, XMAXHP, YMINHP, YMAXHP
      INTEGER*4 IORIENTH
      COMMON /HARDCOPYRANGE/ XMINHP, XMAXHP, YMINHP, YMAXHP, IORIENTH

C X Window Odometer

      REAL*4 XCROFF, XCRSCALE, YCROFF, YCRSCALE, YCOF, XCOF
      COMMON /CURSOR_READOUT/ XCROFF, XCRSCALE, YCROFF, YCRSCALE
     &                       ,YCOF, XCOF

C   local variables

      BYTE BYTECODE
      CHARACTER CODE 

C  statement functions:
C   convert from user units to graph units

      SCX(X) = (X - XLAXIS) / (XUAXIS - XLAXIS) * (XMAX - XMIN) + XMIN
      SCY(Y) = (Y - YLAXIS) / (YUAXIS - YLAXIS) * (YMAX - YMIN) + YMIN

C   convert from graph units to user units

      SCX_INV(X) = (X - XMIN)*(XUAXIS - XLAXIS)/(XMAX - XMIN) + XLAXIS
      SCY_INV(Y) = (Y - YMIN)*(YUAXIS - YLAXIS)/(YMAX - YMIN) + YLAXIS

C   convert from user units to percentages of the current window

      SCXP(X)  = 100. * (X - XLWIND) / (XUWIND - XLWIND)
      SCYP(Y)  = 100. * (Y - YLWIND) / (YUWIND - YLWIND)

C   convert from percentages of the current window to user units

      SCXP_INV(X)  = X / 100. * (XUWIND - XLWIND) + XLWIND
      SCYP_INV(Y)  = Y / 100. * (YUWIND - YLWIND) + YLWIND

C   convert from user units to percentages of the world

      SCXPW(X)  = 100. * (X - XMINHP) / (XMAXHP - XMINHP)
      SCYPW(Y)  = 100. * (Y - YMINHP) / (YMAXHP - YMINHP)

C   convert from percentages of the world to user units

      SCXPW_INV(X)  = X / 100. * (XMAXHP - XMINHP) + XMINHP
      SCYPW_INV(Y)  = Y / 100. * (YMAXHP - YMINHP) + YMINHP
CCC
      IF( FIRST )THEN
        FIRST = .FALSE.
        XLAXIS = GETNAM('XLAXIS')
        XUAXIS = GETNAM('XUAXIS')
        YLAXIS = GETNAM('YLAXIS')
        YUAXIS = GETNAM('YUAXIS')
        XMIN   = GETNAM('XMIN')
        XMAX   = GETNAM('XMAX')
        YMIN   = GETNAM('YMIN')
        YMAX   = GETNAM('YMAX')
        XUWIND = GETNAM('XUWIND')
        XLWIND = GETNAM('XLWIND')
        YUWIND = GETNAM('YUWIND')
        YLWIND = GETNAM('YLWIND')

C   X window odometer settings

        IF( CRSRDOUT .EQ. 0 )THEN       ! graph units
          XCROFF = SCX(XMINHP)
          YCROFF = SCY(YMINHP)
          XCRSCALE = SCX(XMAXHP)-XCROFF
          YCRSCALE = (SCY(YMAXHP)-YCROFF)/0.75
        ELSE IF( CRSRDOUT .EQ. 1 )THEN  ! %window
          XCROFF = SCXP(XMINHP)
          YCROFF = SCYP(YMINHP)
          XCRSCALE = SCXP(XMAXHP)-XCROFF
          YCRSCALE = (SCYP(YMAXHP)-YCROFF)/0.75
        ELSE                            ! world units
          XCROFF = XMINHP
          YCROFF = YMINHP
          XCRSCALE = XMAXHP-XCROFF
          YCRSCALE = (YMAXHP-YCROFF)/0.75
        END IF
      END IF
      IF( XIN )THEN
        IF( GRAPH_UNITS .EQ. 0 )THEN
          CALL GPLOT_CONVERT(XB,0.,XB,YDUM,+1)
        ELSE IF( GRAPH_UNITS .EQ. 1 )THEN
          XB = SCXP_INV(XB)
        ELSE IF( GRAPH_UNITS .EQ. -1 )THEN
          XB = SCXPW_INV(XB)
        END IF
      END IF
      IF( YIN )THEN
        IF( GRAPH_UNITS .EQ. 0 )THEN
          CALL GPLOT_CONVERT(0.,YB,XDUM,YB,+1)
        ELSE IF( GRAPH_UNITS .EQ. 1 )THEN
          YB = SCYP_INV(YB)
        ELSE IF( GRAPH_UNITS .EQ. -1 )THEN
          YB = SCYPW_INV(YB)
        END IF
      END IF
      IF( XIN .AND. YIN )RETURN
      XHAIR = .TRUE.
      CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      WRITE(IOUTS,10)'Q = quit                 / = clear alphanumerics'
      WRITE(IOUTS,10)'K = keyboard entry       X = display coordinates'
   10 FORMAT(' ',A)
      IF( .NOT.XIN )THEN
        IF( .NOT.YIN )THEN
          WRITE(IOUTS,10)STR1
        ELSE 
          WRITE(IOUTS,10)STR2
        END IF
      ELSE
        WRITE(IOUTS,10)STR3
      END IF
   20 CALL CROSSHAIR_R(XBI,YBI,BYTECODE,XBC,YBC)
      CODE = CHAR(BYTECODE)
      IF( GRAPH_UNITS .EQ. 0 )THEN
        S1 = SCX(XBI)
        S2 = SCY(YBI)
      ELSE IF( GRAPH_UNITS .EQ. 1 )THEN
        S1 = SCXP(XBI)
        S2 = SCYP(YBI)
      ELSE IF( GRAPH_UNITS .EQ. -1 )THEN
        S1 = SCXPW(XBI)
        S2 = SCYPW(YBI)
      ELSE
        S1 = XBI
        S2 = YBI
      END IF
      IF( (CODE.EQ.'Q') .OR. (CODE.EQ.'q') )THEN
   21   CALL TRANSPARENT_MODE(0)
        CALL TRANSPARENT_MODE2(0)
        CALL XVST_ALPHA
        RETURN 1
      ELSE IF( CODE .EQ. '/' )THEN
        CALL TRANSPARENT_MODE(0)
        CALL TRANSPARENT_MODE2(0)
        CALL CLTRANS
        GO TO 20
      ELSE IF( (CODE.EQ.'X') .OR. (CODE.EQ.'x') )THEN
        CALL TRANSPARENT_MODE(0)
        CALL TRANSPARENT_MODE2(0)
        CALL REAL4_TO_CHAR(S1,.FALSE.,SOUT,LENS)
        WRITE(IOUTS,10)'X = '//SOUT
        CALL REAL4_TO_CHAR(S2,.FALSE.,SOUT,LENS)
        WRITE(IOUTS,10)'Y = '//SOUT
        GO TO 20
      ELSE IF( (CODE.EQ.'K') .OR. (CODE.EQ.'k') )THEN
        CALL TRANSPARENT_MODE(0)
        CALL TRANSPARENT_MODE2(0)
        CALL XVST_ALPHA
        IF( GRAPH_UNITS .EQ. 0 )THEN
   22     WRITE(IOUTS,10)'Enter: x  y  (in graph units)'
          READ(IINS,*,END=21,ERR=22)S1,S2
        ELSE IF( GRAPH_UNITS .EQ. 1 )THEN
   23     WRITE(IOUTS,10)'Enter: x  y  (in % of the current window)'
          READ(IINS,*,END=21,ERR=23)S1,S2
        ELSE IF( GRAPH_UNITS .EQ. -1 )THEN
  231     WRITE(IOUTS,10)'Enter: x  y  (in % of the world)'
          READ(IINS,*,END=21,ERR=231)S1,S2
        ELSE 
   24     WRITE(IOUTS,10)'Enter: x  y  (units= '//UNITS//')'
          READ(IINS,*,END=21,ERR=24)S1,S2
        END IF
      END IF
      IF( STACK )THEN
        CALL REAL4_TO_CHAR(S1,.FALSE.,SOUT,LENS)
        LINE = LINE(1:NLNE)//' '//SOUT(1:LENS)
        NLNE = NLNE+1+LENS
        CALL REAL4_TO_CHAR(S2,.FALSE.,SOUT,LENS)
        LINE = LINE(1:NLNE)//' '//SOUT(1:LENS)
        NLNE = NLNE+1+LENS
      END IF
      IF( GRAPH_UNITS .EQ. 0 )THEN
        XBI = SCX_INV(S1)
        YBI = SCY_INV(S2)
      ELSE IF( GRAPH_UNITS .EQ. 1 )THEN
        XBI = SCXP_INV(S1)
        YBI = SCYP_INV(S2)
      ELSE IF( GRAPH_UNITS .EQ. -1 )THEN
        XBI = SCXPW_INV(S1)
        YBI = SCYPW_INV(S2)
      ELSE
        XBI = S1
        YBI = S2
      END IF
      IF( .NOT.XIN )THEN
        XB = XBI
        IF( .NOT.YIN )YB = YBI
      ELSE
        YB = YBI
      END IF
      CALL PLOT_X(XB,YB)
      CALL FLUSH_PLOT
      CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      CALL XVST_ALPHA
      RETURN
      END
