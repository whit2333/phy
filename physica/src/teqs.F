      SUBROUTINE TEQS( * )

C  TEQS txt vecout

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT, MXCHAR
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20,   MXCHAR=1024)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      INTEGER*4 TVARALC, TVARADR, TVARLEN, TVARNUM
C
C   TVARALC = number of bytes allocated for text variable characteristics
C             (does not include the data)
C   TVARADR = starting address of the text variable table
C   TVARLEN = number of bytes needed for an entry in the table (60 bytes)
C   TVARNUM = number of text variables currently defined
C
C   Allocate the space as bytes. Let BASE = (TVARADR+(I-1)*TVARLEN)/4, for
C   1 <= I <= TVARNUM.  The last byte used for I'th variable at 4*BASE+VARLEN
C   and the start of the next variable is at 4*BASE+VARLEN+1
C                     
C   I4D(BASE+1)  = length of the I'th variable name
C                  always allocate 32 (4*8) bytes for every name
C   L1D((BASE+2)*4+j) for j = 1, I4D(BASE+1)  is the name
C   I4D(BASE+10) = number of dimensions: 0=scalar, 1=vector
C   I4D(BASE+11) = starting address of data
C   I4D(BASE+12) = number of strings if a vector
C   I4D(BASE+13) = total length of data
C   I4D(BASE+14) = starting address for offsets for vector
C                   I4D(I4D(BASE+14)+1) = 0 for every variable
C   I4D(BASE+15) = starting address for lengths for vector
C
C    scalar variable:
C       length = I4D(BASE+13) 
C       line = L1D(I4D(BASE+11)+j) for j = 1 --> I4D(BASE+13) 
C    vector variable:
C     for string k:  1 <= k <= I4D(BASE+12)
C       length = I4D(I4D(BASE+13)+k)
C       line = L1D(I4D(BASE+11)+j) for
C        j = I4D(I4D(BASE+14)+k)+1 --> I4D(I4D(BASE+14)+k)+I4D(I4D(BASE+15)+k)

      COMMON /TEXT_VARIABLES/ TVARALC, TVARADR, TVARLEN, TVARNUM

      CHARACTER*(LENVSM) NAMEG
      CHARACTER*256      NAMEV, NAMET
      COMMON /GETVARIABLEERROR/ NAMEG, LNG, NAMEV, LNV, NAMET, LNT

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      BYTE      L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

C   local variables

      CHARACTER*1024 TEXT_STRING
      CHARACTER*15  SOUT
      CHARACTER*1   BS
      INTEGER*4     BASE, IDX1(4), IDX2(4), IDX3(4), LLEN, ICNT
      INTEGER*4     I, J, I1, I2, LOUT
      LOGICAL*1     MESSAGES
CCC
      BS = CHAR(92)  ! backslash 

      STRINGS(1) = 'TEQS'
      LENST(1)   = 4
      MESSAGES   = .TRUE.
      DO I = 1, NQUAL(1)
        IF( QUALIFIERS(1,I)(1:1) .EQ. '-' )THEN
          II = 2
        ELSE
          II = 1
        END IF
        L = LENQL(1,I)
        IF( INDEX('MESSAGES',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            MESSAGES = .TRUE.
          ELSE
            MESSAGES = .FALSE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('NOMESSAGES',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            MESSAGES = .FALSE.
          ELSE
            MESSAGES = .TRUE.
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE
          CALL WARNING_MESSAGE('TEQS'
     &     ,'unknown qualifier: '//QUALIFIERS(1,I)(1:L))
        END IF
      END DO
      IF( ITYPE(2) .NE. 2 )THEN
        CALL ERROR_MESSAGE('TEQS','expecting character variable name')
        GO TO 92
      END IF
      CALL UPRCASE( STRINGS(2)(1:LENST(2)), STRINGS(2)(1:LENST(2)) )
      LOUT = 0
      DO 4 I = 1, TVARNUM
        BASE = (TVARADR+(I-1)*TVARLEN)/4
        IF( LENST(2) .NE. I4D(BASE+1) )GO TO 4   ! lengths not equal
        DO J = 1, LENST(2)
          IF( STRINGS(2)(J:J) .NE. CHAR(L1D((BASE+2)*4+J)) )GO TO 4
        END DO
        IF( I4D(BASE+10) .NE. 1 )THEN
          CALL ERROR_MESSAGE('TEQS'
     &     ,'expecting array character variable')
          GO TO 92
        END IF
        LOUT = I4D(BASE+12)
        GOTO 5
    4 CONTINUE
      IF( LOUT .EQ. 0 )THEN
        CALL ERROR_MESSAGE('TEQS','expecting character variable')
        GO TO 92
      END IF
    5 STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(2)(1:LENST(2))
      LENST(1) = LENST(1)+1+LENST(2)
      
      IF( ITYPE(3) .NE. 2 )THEN
        CALL ERROR_MESSAGE('TEQS','expecting character string')
        GO TO 92
      END IF
      text_string = ' '
      CALL GET_TEXT_VARIABLE(STRINGS(3),LENST(3),TEXT_STRING,LLEN,*92)
      IF( LLEN .EQ. 0 )THEN
        CALL ERROR_MESSAGE('TEQS','character string is blank')
        GO TO 92
      END IF
      STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(3)(1:LENST(3))
      LENST(1) = LENST(1)+1+LENST(3)
      
C     for string k:  1 <= k <= I4D(BASE+12)
C       length = I4D(I4D(BASE+13)+k)
C       line = L1D(I4D(BASE+11)+j) for
C        j = I4D(I4D(BASE+14)+k)+1 --> I4D(I4D(BASE+14)+k)+I4D(I4D(BASE+15)+k)
      
      CALL GET_TEMP_SPACE(INITV,LOUT,8,*999)
      ICNT = 0
      DO 6 I = 1, LOUT
        I1 = I4D(I4D(BASE+14)+I)+1
        I2 = I4D(I4D(BASE+14)+I)+I4D(I4D(BASE+15)+I)
        IF( I4D(I4D(BASE+15)+I) .EQ. LLEN )THEN
          DO J = I1, I2
            IF( CHAR( L1D(I4D(BASE+11)+J) ) .NE.
     &          TEXT_STRING(J-I1+1:J-I1+1) )GOTO 6
          ENDDO
          ICNT = ICNT+1
          R8D(INITV+ICNT) = I
        ENDIF
    6 CONTINUE
      IF( ICNT .EQ. 0 )THEN
        CALL WARNING_MESSAGE('TEQS','no matches found')
        GOTO 90
      ENDIF
      IF( NFLD .EQ. 3 )THEN
        CALL ERROR_MESSAGE('TEQS','expecting output vector')
        GO TO 92
      ENDIF
      IF( ITYPE(4) .NE. 2 )THEN
        CALL ERROR_MESSAGE('TEQS','expecting output vector')
        GO TO 92
      ENDIF
      CALL GET_OUTPUT_VARIABLE( STRINGS(4)(1:LENST(4)), INDX, NDIMX
     &     ,IDX1, IDX2, IDX3, IER )
      IF( IER .NE. 0 )THEN
        CALL GET_VARIABLE_ERROR('TEQS',IER)
        GO TO 92
      END IF
      STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(4)(1:LENST(4))
      LENST(1) = LENST(1)+1+LENST(4)

      STRINGS(4) = NAMEG
      LENST(4) = LNG
      CALL PUT_VARIABLE( STRINGS(4)(1:LENST(4)), NDIMX
     &     ,IDX1,IDX2,IDX3,1,0.0,INITV,ICNT,0,0
     &     ,STRINGS(1)(1:LENST(1)),'TEQS',*92)
      
      IF( STACK )WRITE(IOUT_UNIT,10)STRINGS(1)(1:LENST(1))
   10 FORMAT(' ',A)
   90 CALL DELETE_TEMP_SPACE
      RETURN
   92 CALL DELETE_TEMP_SPACE
      RETURN 1
  999 CALL ERROR_MESSAGE('TEQS','allocating dynamic array space')
      RETURN 1
      END
