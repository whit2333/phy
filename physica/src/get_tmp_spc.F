      SUBROUTINE GET_TEMP_SPACE( INIT, ISIZE, IB, * )

      INTEGER*4 INIT, ISIZE, IB

      INTEGER*4  ISAVE, IAD(500), NALLOC(500), IBITE(500)
      COMMON /PHYSICA_TEMP_SPACE/ IAD, NALLOC, IBITE, ISAVE
      DATA ISAVE /0/
CCC
      IF( ISAVE .EQ. 500 )THEN
        CALL ERROR_MESSAGE('GET_TEMP_SPACE routine'
     &,'maximum number of concurrent temporary space allocations = 500')
        RETURN 1
      END IF
      CALL CHSIZE( INIT, 0, ISIZE, IB, *99 )
      ISAVE = ISAVE+1
      IAD(ISAVE) = INIT
      NALLOC(ISAVE) = ISIZE
      IBITE(ISAVE) = IB
      RETURN
   99 CALL ERROR_MESSAGE('GET_TEMP_SPACE routine'
     & ,'allocating dynamic array space')
      RETURN 1
      END
CCC
      SUBROUTINE DELETE_TEMP_SPACE

      INTEGER*4  ISAVE, IAD(500), NALLOC(500), IBITE(500)
      COMMON /PHYSICA_TEMP_SPACE/ IAD, NALLOC, IBITE, ISAVE

      CHARACTER*44 STRING
CCC
      DO I = 1, ISAVE
        IF( NALLOC(I) .GT. 0 )THEN
          CALL CHSIZE( IAD(I),NALLOC(I),0,IBITE(I),*99 )
        ELSE
          WRITE(STRING,10)I,NALLOC(I)
   10     FORMAT('temp. array ',I3,' has nalloc = ',I10)
          CALL WARNING_MESSAGE('DELETE_TEMP_SPACE routine',STRING(1:39))
        END IF
      END DO
      ISAVE = 0
      RETURN
   99 CALL ERROR_MESSAGE('DELETE_TEMP_SPACE routine'
     & ,'de-allocating dynamic array space')
      RETURN
      END
CCC
      SUBROUTINE DELETE_ONE_TEMP_ARRAY( IADDR )

      INTEGER*4 IADDR

      INTEGER*4  ISAVE, IAD(500), NALLOC(500), IBITE(500)
      COMMON /PHYSICA_TEMP_SPACE/ IAD, NALLOC, IBITE, ISAVE
CCC
      DO I = 1, ISAVE
        IF( IADDR .EQ. IAD(I) )THEN
          IF( NALLOC(I) .GT. 0 )
     &     CALL CHSIZE(IAD(I),NALLOC(I),0,IBITE(I),*99)
          DO J = I, ISAVE-1
            IAD(J) = IAD(J+1)
            NALLOC(J) = NALLOC(J+1)
            IBITE(J) = IBITE(J+1)
          END DO
          ISAVE = ISAVE-1
          RETURN
        END IF
      END DO
      CALL ERROR_MESSAGE('DELETE_ONE_TEMP_ARRAY routine'
     & ,'could not find dynamic array address')
      RETURN
   99 CALL ERROR_MESSAGE('DELETE_ONE_TEMP_ARRAY routine'
     & ,'deleting dynamic array space')
      RETURN
      END
CCC
      SUBROUTINE GET_MORE_SPACE( INIT, OLDSIZE, NEWSIZE, IBYTS, * )

      INTEGER*4 INIT, OLDSIZE, NEWSIZE, IBYTS

      INTEGER*4  ISAVE, IAD(500), NALLOC(500), IBITE(500)
      COMMON /PHYSICA_TEMP_SPACE/ IAD, NALLOC, IBITE, ISAVE

      CHARACTER*40 STRING
CCC
      IF( OLDSIZE .EQ. 0 )THEN
        IF( ISAVE .EQ. 500 )THEN
          CALL ERROR_MESSAGE('GET_MORE_SPACE routine'
     &,'maximum number of concurrent temporary space allocations = 500')
          RETURN 1
        END IF
        CALL CHSIZE( INIT, 0, NEWSIZE, IBYTS, *99 )
        ISAVE = ISAVE+1
        IAD(ISAVE) = INIT
        NALLOC(ISAVE) = NEWSIZE
        IBITE(ISAVE) = IBYTS
        RETURN
      ELSE
        DO I = 1, ISAVE
          IF( IAD(I) .EQ. INIT )THEN
            IF( IBYTS .NE. IBITE(I) )THEN
              WRITE(STRING,10)IBITE(I),IBYTS
   10         FORMAT('inconsistent types: old=',I3,', new=',I3)
              CALL ERROR_MESSAGE('GET_MORE_SPACE routine',STRING(1:36))
            END IF
            CALL CHSIZE( INIT, OLDSIZE, NEWSIZE, IBYTS, *99 )
            IAD(I) = INIT
            NALLOC(I) = NEWSIZE
            RETURN
          END IF
        END DO
      END IF
      CALL ERROR_MESSAGE('GET_MORE_SPACE routine'
     & ,'could not find dynamic array address')
      RETURN 1
   99 CALL ERROR_MESSAGE('GET_MORE_SPACE routine'
     & ,'allocating dynamic array space')
      RETURN 1
      END
