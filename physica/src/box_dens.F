      SUBROUTINE BOX_DENS(
     &     IARR,NROW,NCOL,IX,NX,IY,NY,Z,NZ
     &    ,XMN,XMX,YMN,YMX
     &    ,FMINI,FMAXI,FMINI2,FMAXI2,BSCALE,DERIV,PARTIAL,POLAR,PROFILE)
C
C   This routine produces a density plot of a density distribution
C   stored in the matrix
C
C           FMINI ---> FMAXI specify the range of densities
C           desired on a scale from 0 --> 1
C           When logical DERIV=.TRUE. a density plot of the derivative
C           of the matrix is produced ( (x'**2+y'**2)**.5)
C
      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

      CHARACTER*132 XLABEL, YLABEL, GETLAB
      REAL*8        XMN, XMX, YMN, YMX, FMINI, FMAXI, FMINI2, FMAXI2
     &             ,BSCALE, Z(1), X, Y, FMIN, FMAX, XF, YF
      INTEGER*4     PROFILE
      LOGICAL*4     DERIV, PARTIAL, POLAR

      REAL*8    AMIN, AMAX, AVAL, AX, AY, ARG, DUM, XDUM, YDUM, XX, YY
     &         ,XSIDE, YSIDE, AMAXX, DA, XRMIN, XRMAX, XCMIN, XCMAX
      LOGICAL*1 FILL, DITH
      LOGICAL   ERASE

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

      LOGICAL CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED
CCC
      XF(X) = XLAXIS + (X - XMIN)*(XUAXIS - XLAXIS)/(XMAX - XMIN)
      YF(Y) = YLAXIS + (Y - YMIN)*(YUAXIS - YLAXIS)/(YMAX - YMIN)

C   Get the axis locations assuming GPLOT has been called to draw the
C   axes before calling this routine

      IF( CTRLC_CALLED )GO TO 90
      YLAXIS = GETNAM('YLAXIS')
      XLAXIS = GETNAM('XLAXIS')
      YUAXIS = GETNAM('YUAXIS')
      XUAXIS = GETNAM('XUAXIS')

C   Determine the min and max values on the axes that have been plotted

      XMIN = GETNAM('XMIN')
      XMAX = GETNAM('XMAX')
      YMIN = GETNAM('YMIN')
      YMAX = GETNAM('YMAX')
      XLOG = ABS(GETNAM('XLOG'))
      YLOG = ABS(GETNAM('YLOG'))
      IF( XLOG .GT. 1.0 )THEN
        XMIN = XLOG ** XMIN
        XMAX = XLOG ** XMAX
      END IF
      IF( YLOG .GT. 1.0 )THEN
        YMIN = YLOG ** YMIN
        YMAX = YLOG ** YMAX
      END IF

C   Use the following for zooming in

      IF( POLAR )THEN
        MLO  = 1
        MHI  = NX
        NLO  = 1
        NHI  = NY
      ELSE
        DX = XMX-XMN
        DY = YMX-YMN
        IF( DX .EQ. 0.0D0 )THEN
          MLO = 1
          MHI = NX
        ELSE
          MLO = MAX(0,   INT((XMIN-XMN)/DX*(NX-1))) + 1
          MHI = MIN(NX-1,INT((XMAX-XMN)/DX*(NX-1))) + 1
        END IF
        IF( DY .EQ. 0.0D0 )THEN
          NLO = 1
          NHI = NY
        ELSE
          NLO = MAX(0,   INT((YMIN-YMN)/DY*(NY-1))) + 1
          NHI = MIN(NY-1,INT((YMAX-YMN)/DY*(NY-1))) + 1
        END IF
      END IF

C   Determine min and max values in array A. The minimum value will
C   correspond to zero box size, the maximum to the largest box size

      AMIN = 1.D30
      AMAX = -1.D30
      IF( PARTIAL )THEN
        J1 = NLO
        J2 = NHI
        I1 = MLO
        I2 = MHI
      ELSE
        J1 = 1
        J2 = NY
        I1 = 1
        I2 = NX
      END IF
      IF( CTRLC_CALLED )GO TO 90
      IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 2) )THEN
        CALL GET_TEMP_SPACE(IXR,NY,8)
        CALL ZERO_ARRAY(R8D(IXR+1),8*NY)
      END IF
      IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 1) )THEN
        CALL GET_TEMP_SPACE(IXC,NX,8)
        CALL ZERO_ARRAY(R8D(IXC+1),8*NX)
      END IF
      IF( .NOT.DERIV )THEN   ! use linear density scale
        IF( NZ .EQ. 0 )THEN   ! a matrix and not x,y,z data 
          INDEX = IARR
          DO I = I1, I2
            DO J = J1, J2
              IF( CTRLC_CALLED )GO TO 90
              AVAL = R8D(IARR+J+(I-1)*NROW)
              IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 2) )THEN
                R8D(IXR+J) = R8D(IXR+J) + AVAL
              END IF
              IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 1) )THEN
                R8D(IXC+I) = R8D(IXC+I) + AVAL
              END IF
              IF( (AVAL .LT. AMIN) .AND. (AVAL .GT. -1.D30) )AMIN = AVAL
              IF(AVAL .GT. AMAX)AMAX = AVAL
            END DO
          END DO
        ELSE               ! x,y,z data and not a matrix
          DO I = I1, I2
            IF( (Z(I) .LT. AMIN) .AND. (Z(I) .GT. -1.D30) )AMIN = Z(I)
            IF( Z(I) .GT. AMAX )AMAX = Z(I)
          END DO
        END IF
      ELSE      ! use derivative contrast scale
C                 This only works if using a matrix and not x y z data
        CALL GET_TEMP_SPACE(INITAXY,NROW*NCOL,8)
        INDEX = INITAXY
        DO I = I1, I2-1
          DO J = J1, J2-1
            IF( CTRLC_CALLED )GO TO 90
            AX = R8D(IARR+J+1+(I-1)*NROW) - R8D(IARR+J+(I-1)*NROW)
            AY = R8D(IARR+J  +  I  *NROW) - R8D(IARR+J+(I-1)*NROW)
            ARG = SQRT(AX**2+AY**2)
            R8D(INITAXY+I+(J-1)*NCOL) = ARG
            IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 2) )
     &       R8D(IXR+J) = R8D(IXR+J) + ARG
            IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 1) )
     &       R8D(IXC+I) = R8D(IXC+I) + ARG
            IF( ARG .LT. AMIN )AMIN = ARG
            IF( ARG .GT. AMAX )AMAX = ARG
            IF( I .EQ. I2-1 )R8D(INITAXY+J+I*NROW) = ARG
          END DO
          R8D(INITAXY+J2+(I-1)*NROW) = ARG
        END DO
        R8D(INITAXY+J2+(I2-1)*NROW) = R8D(INITAXY+(J2-1)+(I2-2)*NROW)
      END IF

      IF( FMINI .LT. 0.0D0 )FMINI = 0.0D0
      FMIN = FMINI
      FMAX = FMAXI
      AMAX = AMAX + 0.05D0*(AMAX - AMIN)

      CALL GET_TEMP_SPACE(IXT,NX,8)
      DO I = 1, NX
        R8D(IXT+I) = R8D(IX+I)
      END DO
      CALL SSORT8(R8D(IXT+1),DUM,NX,1)
      DX = R8D(IXT+NX)-R8D(IXT+1)

      CALL GET_TEMP_SPACE(IYT,NY,8)
      DO J = 1, NY
        R8D(IYT+J) = R8D(IY+J)
      END DO
      CALL SSORT8(R8D(IYT+1),DUM,NY,1)
      DY = R8D(IYT+NY)-R8D(IYT+1)

      XDUM = R8D(IXT+1)-1.0D0
      NXS = 0
      DO I = 1, NX
        IF( (R8D(IXT+I)-XDUM) .GT. 0.001*DX )THEN
          NXS = NXS + 1
          XDUM = R8D(IXT+I)
        END IF
      END DO

      YDUM = R8D(IYT+1)-1.0D0
      NYS = 0
      DO J = 1, NY
        IF( (R8D(IYT+J)-YDUM) .GT. 0.001*DY )THEN
          NYS = NYS + 1
          YDUM = R8D(IYT+J)
        END IF
      END DO

      XSIDE = (XF(XMX) - XF(XMN)) / MAX(1,NXS) * BSCALE
      YSIDE = (YF(YMX) - YF(YMN)) / MAX(1,NYS) * BSCALE

      AMAXX = FMAXI2 * (AMAX - AMIN) + AMIN
      AMIN  = FMINI2 * (AMAX - AMIN) + AMIN
      AMAX  = AMAXX
      IF( AMAX .EQ. AMIN )THEN
        DA = 1.0D0
      ELSE
        DA = AMAX - AMIN
      END IF

      IF( CTRLC_CALLED )GO TO 90
      IF( (PROFILE .GT. 0) .AND. (NZ .EQ. 0) )THEN
        XLABEL = GETLAB('XLABEL')
        YLABEL = GETLAB('YLABEL')
        CALL SETLAB('XLABEL',' ')
        CALL SETLAB('YLABEL',' ')
        XLWIND = GETNAM('XLWIND')
        XUWIND = GETNAM('XUWIND')
        YLWIND = GETNAM('YLWIND')
        YUWIND = GETNAM('YUWIND')
        HISTYP = GETNAM('HISTYP')
        YAXIS  = GETNAM('YAXIS')
        XAXIS  = GETNAM('XAXIS')
        XLINC  = GETNAM('NLXINC')
        YLINC  = GETNAM('NLYINC')
        BOX    = GETNAM('BOX')
        XNUMSZ = GETNAM('XNUMSZ')
        YNUMSZ = GETNAM('YNUMSZ')

        IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 2) )THEN
          CALL SETNAM('HISTYP',3.)
          CALL SETNAM('YAXIS',0.)
          CALL SETNAM('XMIN',0.)
          CALL SETNAM('XMAX',1.)
          CALL SETNAM('NLXINC',1.)
          CALL SETNAM('BOX',0.)
          CALL SETNAM('XNUMSZ',0.)
          CALL SETNAM('XLAXIS',XUAXIS)
          CALL SETNAM('XUAXIS',XUAXIS+0.1*(XUWIND-XLWIND))
          XRMIN = R8D(IXR+J1)
          XRMAX = XRMIN
          DO J = J1, J2
            IF( XRMAX .LT. R8D(IXR+J) )XRMAX = R8D(IXR+J)
            IF( XRMIN .GT. R8D(IXR+J) )XRMIN = R8D(IXR+J)
          END DO
          IF( XRMIN .EQ. XRMAX )THEN
            DO J = J1, J2
              R8D(IXR+J) = 1.0D0
            END DO
          ELSE
            DO J = J1, J2
              R8D(IXR+J) = (R8D(IXR+J) - XRMIN) / (XRMAX - XRMIN)
            END DO
          END IF
          XMASK = GETNAM('MASK')
          CALL SETNAM('MASK',-1.)
          CALL PHYSICA_GPLOT(IXR+J1-1,0,0,IY+J1-1,0,0,J2-J1+1,1)
          CALL SETNAM('MASK',XMASK)
          IF( CTRLC_CALLED )GO TO 20
        END IF

        IF( (PROFILE .EQ. 3) .OR. (PROFILE .EQ. 1) )THEN
          CALL SETNAM('HISTYP',1.)
          CALL SETNAM('BOX',0.)
          CALL SETNAM('YAXIS',1.)
          CALL SETNAM('XAXIS',0.)
          CALL SETNAM('NLYINC',1.)
          CALL SETNAM('YMIN',0.)
          CALL SETNAM('YMAX',1.)
          CALL SETNAM('XMIN',XMIN)
          CALL SETNAM('XMAX',XMAX)
          CALL SETNAM('YNUMSZ',0.)
          CALL SETNAM('XLAXIS',XLAXIS)
          CALL SETNAM('XUAXIS',XUAXIS)
          CALL SETNAM('YLAXIS',YUAXIS)
          CALL SETNAM('YUAXIS',YUAXIS+0.1*(YUWIND-YLWIND))
          XCMIN = R8D(IXC+I1)
          XCMAX = XCMIN
          DO I = I1, I2
            IF( XCMIN .GT. R8D(IXC+I) )XCMIN = R8D(IXC+I)
            IF( XCMAX .LT. R8D(IXC+I) )XCMAX = R8D(IXC+I)
          END DO
          IF( XCMAX .EQ. XCMIN )THEN
            DO I = I1, I2
              R8D(IXC+I) = 1.0D0
            END DO
          ELSE
            DO I = I1, I2
              R8D(IXC+I) = (R8D(IXC+I) - XCMIN) / (XCMAX - XCMIN)
            END DO
          END IF
          XMASK = GETNAM('MASK')
          CALL SETNAM('MASK',-1.)
          CALL PHYSICA_GPLOT(IX+I1-1,0,0,IXC+I1-1,0,0,I2-I1+1,1)
          CALL SETNAM('MASK',XMASK)
        END IF
20      CALL SETNAM('XMIN',XMIN)
        CALL SETNAM('XMAX',XMAX)
        CALL SETNAM('YMIN',YMIN)
        CALL SETNAM('YMAX',YMAX)
        CALL SETNAM('HISTYP',HISTYP)
        CALL SETNAM('YAXIS',YAXIS)
        CALL SETNAM('XAXIS',XAXIS)
        CALL SETNAM('NLXINC',XLINC)
        CALL SETNAM('NLYINC',YLINC)
        CALL SETNAM('BOX',BOX)
        CALL SETNAM('%XNUMSZ',100.*XNUMSZ/(YUWIND-YLWIND))
        CALL SETNAM('%YNUMSZ',100.*YNUMSZ/(YUWIND-YLWIND))
        CALL SETNAM('%YLAXIS',100.*(YLAXIS-YLWIND)/(YUWIND-YLWIND))
        CALL SETNAM('%YUAXIS',100.*(YUAXIS-YLWIND)/(YUWIND-YLWIND))
        CALL SETNAM('%XLAXIS',100.*(XLAXIS-XLWIND)/(XUWIND-XLWIND))
        CALL SETNAM('%XUAXIS',100.*(XUAXIS-XLWIND)/(XUWIND-XLWIND))
        CALL SETLAB('XLABEL',XLABEL)
        CALL SETLAB('YLABEL',YLABEL)
      END IF
      IF( CTRLC_CALLED )GO TO 90

      FILL = .FALSE.
      DITH = .FALSE.
      ERASE = .FALSE.
      IF( IPFILL .LT. 0 )ERASE = .TRUE.
      IPFILLA = ABS(IPFILL)
      IF( (IPFILLA .GE. 1) .AND. (IPFILLA .LE. 10) )FILL = .TRUE.
      IF( IPFILLA .GE. 11 )THEN
        DITH = .TRUE.
        IDX = IPFILLA/10
        IDY = IPFILLA - IDX*10
      END IF

CC      write(6,1000)mlo,mhi,nlo,nhi
CC1000  format(' mlo,mhi,nlo,nhi= ',4i5)
CC      write(6,1001)amin,da,fmin,fmax
CC1001  format(' amin,da,fmin,fmax= ',4(d10.3,2x))
CC      write(6,1002)xside,yside
CC1002  format(' xside,yside= ',2(d10.3,2x))

      IF( NZ .EQ. 0 )THEN  ! Using the matrix and not x y z data
        DO I = MLO, MHI
          IF( CTRLC_CALLED )GO TO 90
          DO J = NLO, NHI
            IF( CTRLC_CALLED )GO TO 90
            IF( POLAR )THEN
              XX = R8D(IX+I) * COSD(R8D(IY+J))
              YY = R8D(IX+I) * SIND(R8D(IY+J))
            ELSE
              XX = R8D(IX+I)
              YY = R8D(IY+J)
            END IF

CC      write(6,1003)xx,yy,xf(xx),yf(yy)
CC1003  format(' xx,yy,xf(xx),yf(yy)= ',4(d10.3,2x))

            CALL DRAW_DENS_BOX(XF(XX),YF(YY),R8D(INDEX+J+(I-1)*NROW)
     &                        ,AMIN,DA,FMIN,FMAX
     &                        ,XSIDE,YSIDE,XLAXIS,XUAXIS,YLAXIS,YUAXIS
     &                        ,FILL,DITH,IPFILLA,IDX,IDY,ERASE)
          END DO
        END DO
      ELSE               ! Using x y z data and not the matrix
        DO J = 1, NY
          IF( CTRLC_CALLED )GO TO 90
          IF( POLAR )THEN
            XX = R8D(IX+J) * COSD(R8D(IY+J))
            YY = R8D(IX+J) * SIND(R8D(IY+J))
          ELSE
            XX = R8D(IX+J)
            YY = R8D(IY+J)
          END IF

CC      write(6,1003)xx,yy,xf(xx),yf(yy)
CC1003  format(' xx,yy,xf(xx),yf(yy)= ',4(d10.3,2x))

          IF( (XX-XMIN)*(XMAX-XX) .GE. 0.0 )THEN
            IF( (YY-YMIN)*(YMAX-YY) .GE. 0.0 )THEN
              CALL DRAW_DENS_BOX(XF(XX),YF(YY),Z(J)
     &                      ,AMIN,DA,FMIN,FMAX
     &                      ,XSIDE,YSIDE,XLAXIS,XUAXIS,YLAXIS,YUAXIS
     &                      ,FILL,DITH,IPFILLA,IDX,IDY,ERASE)
            END IF
          END IF
        END DO
      END IF
90    CALL FLUSH_PLOT
      RETURN
      END

      SUBROUTINE DRAW_DENS_BOX(XA,YA,AVALUE
     &                        ,AMIN,DA,FMIN,FMAX
     &                        ,XSIDE,YSIDE,XLAXIS,XUAXIS,YLAXIS,YUAXIS
     &                        ,FILL,DITH,IPFILLA,IDX,IDY,ERASE)
C
cc      PARAMETER (FRACMIN=0.01D0)
C
      REAL*8    XA, YA, FRAC, AVALUE, AMIN, DA, FMIN, FMAX
     &         ,XSIDE, YSIDE, DX, DY, XL, XU, YL, YU
      REAL*4    X(5), Y(5), XLAXIS, XUAXIS, YLAXIS, YUAXIS
      INTEGER*4 IPFILLA, IDX, IDY
      LOGICAL   ERASE
      LOGICAL*1 FILL, DITH
C
      FRAC = (AVALUE - AMIN) / DA
cc      FRAC = MAX(0.0D0,MIN(1.0D0,FRAC))
C
cc      IF( (FRAC.LE.FRACMIN).OR.(FRAC.LT.FMIN).OR.(FRAC.GT.FMAX) )RETURN
      IF( (FRAC.LT.FMIN) .OR. (FRAC.GT.FMAX) )RETURN
C
      DX = 0.5D0 * XSIDE * FRAC
      DY = 0.5D0 * YSIDE * FRAC
      XL = MIN( XUAXIS, MAX( SNGL(XA-DX), XLAXIS ))
      XU = MAX( XLAXIS, MIN( SNGL(XA+DX), XUAXIS ))
      YL = MIN( YUAXIS, MAX( SNGL(YA-DY), YLAXIS ))
      YU = MAX( YLAXIS, MIN( SNGL(YA+DY), YUAXIS ))
C
      X(1) = XL
      X(2) = XU
      X(3) = XU
      X(4) = XL
      X(5) = XL
      Y(1) = YL
      Y(2) = YL
      Y(3) = YU
      Y(4) = YU
      Y(5) = YL
C
      CALL PLOT_R(X(1),Y(1),3)
      DO I = 2, 5
        CALL PLOT_R(X(I),Y(I),2)
      END DO
C
      IF( FILL )CALL HATCH_DRAW(X,Y,5,IPFILLA)
      IF( DITH )CALL DITHER(X,Y,5,IDX,IDY,ERASE)
C
      RETURN
      END
