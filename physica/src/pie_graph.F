      SUBROUTINE PIE_GRAPH( * )

C   PIEGRAPH value explode fnum cnum ocnum file radius { cx cy { ang }} 

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT, MXCHAR
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20,   MXCHAR=1024)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      INTEGER*4 IMONITOR2, IOUTM2
      COMMON /PLOTMONITOR2/ IMONITOR2, IOUTM2

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

      LOGICAL*4 CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED

      LOGICAL*4 WELL
      COMMON /TO_BIT_OR_NOT/ WELL

C   local variables

      CHARACTER*1 BS
      REAL*8      PIESUM
      REAL*4      PI, RADIUS, ANGLE, FRAC, XBC, YBC, XUWIND, XLWIND
     &           ,YUWIND, YLWIND, XYWIND, XMIN, XMAX, XLAXIS, XUAXIS
      INTEGER*4   I, II, L, GRAPH_UNITS, NUM, INIT1, NUM1, INIT2, ICNT
     &           ,IMON2_SAVE
      LOGICAL*4   XHAIR, XIN, YIN, FIRST, WELL_SAVE, VALUE_GIVEN
      DATA XBC /-100./, YBC /-100./
CCC
      BS = CHAR(92)  ! backslash

      FIRST = .TRUE.

      XUWIND = GETNAM('XUWIND')
      XLWIND = GETNAM('XLWIND')
      YUWIND = GETNAM('YUWIND')
      YLWIND = GETNAM('YLWIND')
      XYWIND = MIN( (YUWIND - YLWIND), (XUWIND - XLWIND) )
      PI = ACOS(-1.0)

      STRINGS(1) = 'PIEGRAPH'
      LENST(1) = 8

      GRAPH_UNITS = 1
      DO I = 1, NQUAL(1)
        IF( QUALIFIERS(1,I)(1:1) .EQ. '-' )THEN
          II = 2
        ELSE
          II = 1
        END IF
        L = LENQL(1,I)
        IF( INDEX('GRAPH',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            GRAPH_UNITS = 0
          ELSE
            GRAPH_UNITS = 1
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('PERCENT',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            GRAPH_UNITS = 1
          ELSE
            GRAPH_UNITS = 2
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE IF( INDEX('WORLD',QUALIFIERS(1,I)(II:L)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            GRAPH_UNITS = 2
          ELSE
            GRAPH_UNITS = 1
          END IF
          STRINGS(1) = STRINGS(1)(1:LENST(1))//BS//QUALIFIERS(1,I)(1:L)
          LENST(1) = LENST(1)+1+L
        ELSE
          CALL WARNING_MESSAGE('PIEGRAPH'
     &     ,'unknown qualifier: '//QUALIFIERS(1,I)(1:L))
        END IF
      END DO

C   PIEGRAPH  value explode fnum cnum ocnum  radius { cx cy } 

      CALL GET_VECTOR( STRINGS(2), LENST(2), ITYPE(2), 'PIEGRAPH'
     & ,'pie wedge values vector', INIT1, NUM, *92, *90 )
      STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(2)(1:LENST(2))
      LENST(1) = LENST(1)+1+LENST(2)

      CALL GET_VECTOR( STRINGS(3), LENST(3), ITYPE(3), 'PIEGRAPH'
     & ,'explode percentage vector', INIT2, NUM1, *92, *90 )
      IF( NUM1 .NE. NUM )CALL WARNING_MESSAGE('PIEGRAPH'
     & ,'pie values vector length not equal to explode vector length,'//
     &  ' using minimum')
      NUM = MIN( NUM, NUM1 )
      STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(3)(1:LENST(3))
      LENST(1) = LENST(1)+1+LENST(3)

      CALL GET_VECTOR( STRINGS(4), LENST(4), ITYPE(4), 'PIEGRAPH'
     & ,'fill pattern vector', INIT3, NUM1, *92, *90 )
      IF( NUM1 .NE. NUM )CALL WARNING_MESSAGE('PIEGRAPH'
     &  ,'pie values vector length not equal to fill pattern'//
     & ' vector length, using minimum')
      NUM = MIN( NUM, NUM1 )
      STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(4)(1:LENST(4))
      LENST(1) = LENST(1)+1+LENST(4)

      CALL GET_VECTOR( STRINGS(5), LENST(5), ITYPE(5), 'PIEGRAPH'
     & ,'fill colour vector', INIT4, NUM1, *92, *90 )
      IF( NUM1 .NE. NUM )CALL WARNING_MESSAGE('PIEGRAPH'
     & ,'pie values vector length not equal to colour vector length,'//
     &  ' using minimum')
      NUM = MIN( NUM, NUM1 )
      STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(5)(1:LENST(5))
      LENST(1) = LENST(1)+1+LENST(5)

      CALL GET_VECTOR( STRINGS(6), LENST(6), ITYPE(6), 'PIEGRAPH'
     & ,'outline colour vector', INIT5, NUM1, *92, *90 )
      IF( NUM1 .NE. NUM )CALL WARNING_MESSAGE('PIEGRAPH'
     &  ,'pie values vector length not equal to outline colour'//
     & ' vector length, using minimum')
      NUM = MIN( NUM, NUM1 )
      STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(6)(1:LENST(6))
      LENST(1) = LENST(1)+1+LENST(6)
      ICNT = 7

      WELL_SAVE = WELL
      IMON2_SAVE = IMONITOR2
      CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT)
     & ,ITYPE(ICNT), 'PIEGRAPH', 'pie radius', VALUE_GIVEN, .FALSE.
     & ,0.0D0, *92,*90 )
      IF( REALS(ICNT) .LT. 0.0D0 )THEN
        CALL ERROR_MESSAGE('PIEGRAPH','pie radius < 0')
        GO TO 92
      ELSE IF( REALS(ICNT) .EQ. 0.0D0 )THEN
        CALL ERROR_MESSAGE('PIEGRAPH','pie radius = 0')
        GO TO 92
      END IF
      RADIUS = REALS(ICNT)
      STRINGS(1) =
     & STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
      LENST(1) = LENST(1)+1+LENST(ICNT)
      ICNT = ICNT+1

      IF( GRAPH_UNITS .EQ. 0 )THEN

C   Convert from graph units to user units

        XMIN   = GETNAM('XMIN')
        XMAX   = GETNAM('XMAX')
        XLAXIS = GETNAM('XLAXIS')
        XUAXIS = GETNAM('XUAXIS')
        RADIUS = (RADIUS - XMIN)*(XUAXIS - XLAXIS)/(XMAX - XMIN)
      ELSE IF( GRAPH_UNITS .EQ. 1 )THEN

C   Convert from percentages to user units

        RADIUS = RADIUS / 100. * (XUWIND - XLWIND)
      END IF
      CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT)
     & ,ITYPE(ICNT), 'PIEGRAPH', 'x-coordinate of pie centre', XIN
     & ,.TRUE., 0.0D0, *92, *90 )
      IF( XIN )THEN
        XCENTRE = SNGL(REALS(ICNT))
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
        ICNT = ICNT+1
      END IF
      CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT)
     & ,ITYPE(ICNT), 'PIEGRAPH', 'y-coordinate of pie centre', YIN
     & ,.TRUE., 0.0D0, *92, *90 )
      IF( YIN )THEN
        YCENTRE = SNGL(REALS(ICNT))
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
        ICNT = ICNT+1
      END IF
      WELL = .FALSE.
      IMONITOR2 = 0
      CALL DWG_OUTPUT('OFF')
      CALL SETNAM('COLOUR',1.)
      CALL PD_CROSS_HAIR(
     &  'Choose the location of the pie centre'
     & ,'Choose the x-coordinate of the pie centre'
     & ,'Choose the y-coordinate of the pie centre'
     & ,XIN, YIN, STRINGS(1),LENST(1), FIRST, XCENTRE, YCENTRE, XBC, YBC
     & ,GRAPH_UNITS, XHAIR, *88 )
      IF( XHAIR )THEN
        CALL PLOT_DATA_LEVEL( 1 )
        CALL PLOT_X(XCENTRE,YCENTRE)
        CALL PLOT_DATA_LEVEL( 0 )
      END IF
      WELL = WELL_SAVE
      IMONITOR2 = IMON2_SAVE
      CALL DWG_OUTPUT('ON')
      CALL SETNAM('COLOUR',COLOUR)

      NVERT  = MAX(500,INT(500*RADIUS/XYWIND))
      DTHETA = 2.0*PI / NVERT
      ANGLE  = 0.0
      CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT)
     & ,ITYPE(ICNT), 'PIEGRAPH', 'starting angle', XIN, .TRUE., 0.0D0
     & ,*92, *92 )
      IF( XIN )THEN
        ANGLE = SNGL(REALS(ICNT))
        STRINGS(1) =
     &   STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
        LENST(1) = LENST(1)+1+LENST(ICNT)
      END IF
      IF( STACK )WRITE(IOUT_UNIT,10)STRINGS(1)(1:LENST(1))
   10 FORMAT(' ',A)

      PIESUM = 0.0D0
      DO I = 1, NUM
        PIESUM = PIESUM+R8D(INIT1+I)
      END DO
      NVOLD = 0
      THETAS = ANGLE * PI / 180.0
      DO I = 1, NUM
        FRAC   = R8D(INIT1+I) / PIESUM
        THETAF = THETAS + 2.0*PI * FRAC
        NV     = MAX(10,INT(NVERT*FRAC))
        DTHETA = ABS(THETAF-THETAS)/NV
        NV     = NV + 2
        IF( NV .GT. NVOLD )THEN
          CALL CHSIZE(INITX,NVOLD,NV,4,*99)
          CALL CHSIZE(INITY,NVOLD,NV,4,*99)
          NVOLD = NV
        END IF
        CALL PLOT_PIE_WEDGE(XCENTRE,YCENTRE,SNGL(R8D(INIT2+I)),RADIUS
     &   ,NV,R4D(INITX+1),R4D(INITY+1),THETAS,THETAF,DTHETA
     &   ,INT(R8D(INIT3+I)),SNGL(R8D(INIT4+I)),SNGL(R8D(INIT5+I)))
        IF( CTRLC_CALLED )GO TO 89
        THETAS = THETAF
      END DO
      CALL FLUSH_PLOT
      GO TO 89
   88 WELL = WELL_SAVE
      IMONITOR2 = IMON2_SAVE
      CALL DWG_OUTPUT('ON')
      CALL SETNAM('COLOUR',COLOUR)
      CALL DELETE_TEMP_SPACE
      RETURN
   89 CALL CHSIZE(INITX,NVOLD,0,4,*999)
      CALL CHSIZE(INITY,NVOLD,0,4,*999)
      CALL SETNAM('COLOUR',COLOUR)
      CALL DELETE_TEMP_SPACE
      RETURN
   90 CALL SETNAM('COLOUR',COLOUR)
      CALL DELETE_TEMP_SPACE
      RETURN 
   92 CALL SETNAM('COLOUR',COLOUR)
      CALL DELETE_TEMP_SPACE
      RETURN 1
   99 CALL ERROR_MESSAGE('PIEGRAPH','allocating dynamic array space')
      CALL SETNAM('COLOUR',COLOUR)
      CALL DELETE_TEMP_SPACE
      RETURN 1
  999 CALL ERROR_MESSAGE('PIEGRAPH','deleting dynamic array space')
      CALL SETNAM('COLOUR',COLOUR)
      CALL DELETE_TEMP_SPACE
      RETURN 1
      END
CCC
      SUBROUTINE PLOT_PIE_WEDGE(XCENTRE,YCENTRE,EXPLODE,RADIUS,NVERTI
     & ,X,Y,THETAS,THETAF,DTHETA,IHATCH,COLOUR,OCOL)

      REAL*4    X(NVERTI), Y(NVERTI)
      LOGICAL*1 ERASE
CCC
      ERASE = .FALSE.

      NVERT = NVERTI
      THETAMID = THETAS + 0.5 * (THETAF - THETAS)
      X(1) = XCENTRE + 0.01 * EXPLODE * RADIUS * COS(THETAMID)
      Y(1) = YCENTRE + 0.01 * EXPLODE * RADIUS * SIN(THETAMID)
      X(2) = X(1) + RADIUS * COS(THETAS)
      Y(2) = Y(1) + RADIUS * SIN(THETAS)
      DO I = 1, NVERT - 3
        X(I+2) = X(1) + RADIUS * COS(THETAS + DTHETA * I)
        Y(I+2) = Y(1) + RADIUS * SIN(THETAS + DTHETA * I)
      END DO
      X(NVERT) = X(1) + RADIUS * COS(THETAF)
      Y(NVERT) = Y(1) + RADIUS * SIN(THETAF)

      IHA = ABS(IHATCH)
      IF( IHA .NE. 0 )THEN
        CALL SETNAM('COLOUR',COLOUR)
        IF( IHA .GT. 10 )THEN
          IDITHX = IHA/10
          IDITHY = IHA - IDITHX*10
          IF( IDITHX .EQ. 0 )IDITHX = 1
          IF( IDITHY .EQ. 0 )IDITHY = 1
          CALL DITHER(X,Y,NVERT,IDITHX,IDITHY,ERASE)
        ELSE
          CALL HATCH_DRAW(X,Y,NVERT,IHA)
        END IF
      END IF

      IF( IHATCH .GE. 0 )THEN
        CALL SETNAM('COLOUR',OCOL)
        CALL PLOT_R(X(1),Y(1),3)
        CALL PLOT_R(X(2),Y(2),2)
        DO I = 1, NVERT - 3
          CALL PLOT_R(X(I+2),Y(I+2),2)
        END DO
        CALL PLOT_R(X(NVERT),Y(NVERT),2)
        CALL PLOT_R(X(1),Y(1),2)
      END IF
      RETURN
      END
