      SUBROUTINE FIT_ELLIPSE( OUTPUT, * )
C
      LOGICAL*1 OUTPUT
C
      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT, MXCHAR
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20,   MXCHAR=1024)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      CHARACTER*(LENVSM) NAMEG
      CHARACTER*256      NAMEV, NAMET
      COMMON /GETVARIABLEERROR/ NAMEG, LNG, NAMEV, LNV, NAMET, LNT

      LOGICAL*4 CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

C   local variables

      character*(lenvsm) parvec
      CHARACTER*15 SOUT, SOUT1
      CHARACTER*11 COMMAND
      CHARACTER*1  BS
      INTEGER*4    IDX1(4), IDX2(4), IDX3(4), parveclen
      integer*4    nvary, ik(5), jk(5), modepar(5), nvaried(5)
      real*8       varmat(5,5), b(5), sigmap(5), array(5,5), alpha(5,5)
      real*8       beta(5), deriv(5,10000), old_par(5)
      real*8       chisqr, chisqr_old, flambda, flambda_max, factor
      real*8       flambda_ini, toler
      logical      more

      data more/.true./
CCC
      BS = CHAR(92) ! backslash

      COMMAND = 'FIT'//BS//'ELLIPSE'
      
      STRINGS(1) = COMMAND
      LENST(1)   = 11
      
      ICNT = 2
      CALL GET_VECTOR( STRINGS(ICNT), LENST(ICNT), ITYPE(ICNT)
     & ,COMMAND, 'independent input variable', INITX, NUMX, *92, *92 )
      STRINGS(1) =
     & STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
      LENST(1) = LENST(1)+1+LENST(ICNT)
      ICNT = ICNT+1

      CALL GET_VECTOR( STRINGS(ICNT), LENST(ICNT), ITYPE(ICNT)
     & ,COMMAND, 'dependent input variable', INITY, NUMY, *92, *92 )
      STRINGS(1) =
     & STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
      LENST(1) = LENST(1)+1+LENST(ICNT)
      ICNT = ICNT+1
      
      IF( NUMX .NE. NUMY )CALL WARNING_MESSAGE(COMMAND,
     & 'input vectors have different lengths, using minimum')
      npts = MIN( NUMX, NUMY )
      
      IF( npts .LE. 2 )THEN
        CALL ERROR_MESSAGE(COMMAND,'input vector lengths must be > 2')
        GO TO 92
      END IF

      CALL GET_VECTOR( STRINGS(ICNT), LENST(ICNT), ITYPE(ICNT)
     & ,COMMAND, 'parameter vector', INITP, NUMP, *92, *92 )
      STRINGS(1) =
     &     STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
      LENST(1) = LENST(1)+1+LENST(ICNT)
      parvec = strings(icnt)
      parveclen = lenst(icnt)
      ICNT = ICNT+1
      
      IF( NUMP .NE. 5 )THEN
        CALL ERROR_MESSAGE(COMMAND,'parameter vector length must be 5')
        GO TO 92
      END IF

      CALL GET_VECTOR( STRINGS(ICNT), LENST(ICNT), ITYPE(ICNT)
     & ,COMMAND, 'parameter increment vector', INITDP, NUMDP, *92, *92 )
      STRINGS(1) =
     &     STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
      LENST(1) = LENST(1)+1+LENST(ICNT)
      ICNT = ICNT+1
      
      IF( NUMDP .NE. 5 )THEN
        CALL ERROR_MESSAGE(COMMAND,
     &       'parameter increment vector length must be 5')
        GO TO 92
      END IF
      
      CALL GET_VECTOR( STRINGS(ICNT), LENST(ICNT), ITYPE(ICNT)
     & ,COMMAND, 'weight variable', INITW, NUMW, *92, *92 )
      STRINGS(1) =
     &     STRINGS(1)(1:LENST(1))//' '//STRINGS(ICNT)(1:LENST(ICNT))
      LENST(1) = LENST(1)+1+LENST(ICNT)
      ICNT = ICNT+1
      
      IF( npts .NE. NUMW )CALL WARNING_MESSAGE(COMMAND,
     &     'input vectors have different lengths, using minimum')
      npts = MIN( npts, NUMW )
      
      IF( npts .LE. 2 )THEN
        CALL ERROR_MESSAGE(COMMAND,'input vector lengths must be > 2')
        GO TO 92
      END IF
      
      CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT),
     &     ITYPE(ICNT), COMMAND, 'LAMBDA', VALUE_GIVEN, .FALSE.,
     &     0.0D0, *92, *92 )
      flambda = REALS(ICNT)
      icnt = icnt+1
      CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT),
     &     ITYPE(ICNT), COMMAND, 'LAMBDAMAX', VALUE_GIVEN, .FALSE.,
     &     0.0D0, *92, *92 )
      flambda_max = REALS(ICNT)
      icnt = icnt+1
      CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT),
     &     ITYPE(ICNT), COMMAND, 'FACTOR', VALUE_GIVEN, .FALSE.,
     &     0.0D0, *92, *92 )
      factor = REALS(ICNT)
      icnt = icnt+1
      CALL GET_SCALAR( STRINGS(ICNT), LENST(ICNT), REALS(ICNT),
     &     ITYPE(ICNT), COMMAND, 'TOLERANCE', VALUE_GIVEN, .FALSE.,
     &     0.0D0, *92, *92 )
      toler = REALS(ICNT)
      icnt = icnt+1
      
      CALL GET_TEMP_SPACE(INITYF,npts,8)
      CALL GET_TEMP_SPACE(IDERIV,5*npts,8)
      CALL GET_TEMP_SPACE(IWORK1,npts,8)
      CALL GET_TEMP_SPACE(IWORKM,npts,8)
      CALL GET_TEMP_SPACE(IWORKH,npts,8)
      
c     Call fitting subprogram
      
      nvary = 0
      do i = 1, 5
        if( r8d(initdp+i).ne.0 )then
          modepar(i) = 1
          nvary = nvary+1
        else
          modepar(i) = 0
        endif
      enddo

      nfree = npts-nvary-1
      iter = 0
      maxit = 100

      i=0
      DO WHILE ( more )
        CALL CURFIT( npts, r8d(initx+1), r8d(inity+1), r8d(initw+1),
     &               r8d(inityf+1), varmat, r8d(initp+1), b, sigmap,
     &               nvary, array, alpha, beta, r8d(initdp+1), ik, jk,
     &               r8d(ideriv+1), r8d(iworkl+1), r8d(iworkm+1),
     &               r8d(iworkh+1), modepar, nvaried, nfree,
     &               iter, chisqr, chisqr_old, flambda, flambda_max,
     &               factor, old_par, toler, *92 )

c       Check for finish

        CALL EXIT_SUB( toler, maxit, iter, factor, chisqr, chisqr_old,
     &                 flambda, flambda_ini, flambda_max, nvary,
     &                 r8d(initp+1),
     &                 old_par, varmat, sigmap, nparam, modepar,
     &                 nvaried, r8d(initdp+1), nfree, more )

        i = i+1
        write(*,*)'i= ',i
      ENDDO

C     output results

      IF( OUTPUT )THEN
        CALL REAL8_TO_CHAR( r8d(initp+1), .FALSE., SOUT, L )
        CALL RITE('A = '//SOUT(1:L))
        CALL REAL8_TO_CHAR( r8d(initp+2), .FALSE., SOUT, L )
        CALL RITE('B = '//SOUT(1:L))
        CALL REAL8_TO_CHAR( r8d(initp+3), .FALSE., SOUT, L )
        CALL RITE('C = '//SOUT(1:L))
        CALL REAL8_TO_CHAR( r8d(initp+4), .FALSE., SOUT1, L1 )
        CALL REAL8_TO_CHAR( r8d(initp+5), .FALSE., SOUT, L )
        CALL RITE('Centre = ('//SOUT1(1:L1)//','//SOUT(1:L)//')')
      END IF
      CALL GET_OUTPUT_VARIABLE( parvec(1:parveclen), INDX
     &     ,NDIMX, IDX1, IDX2, IDX3, IER )
      IF( IER .NE. 0 )THEN
        CALL GET_VARIABLE_ERROR(COMMAND,IER)
        GO TO 92
      END IF
      CALL PUT_VARIABLE( parvec(1:parveclen), NDIMX, IDX1, IDX2, IDX3,
     &     1,0.0d0,initp,5,0,0, STRINGS(1)(1:LENST(1)), COMMAND, *92 )
   80 IF( STACK )WRITE(IOUT_UNIT,10)STRINGS(1)(1:LENST(1))
   10 FORMAT(' ',A)
   90 CALL DELETE_TEMP_SPACE
      RETURN
   92 CALL DELETE_TEMP_SPACE
      RETURN 1
      END

      SUBROUTINE CURFIT( npts, xe, ye, weight, yf, varmat, par, b,
     &                   sigmap, nvar, array, alpha, beta, deltap,
     &                   ik, jk, deriv, workl, workm, workh,
     &                   modepar, nvaried, nfree,
     &                   iter, chisqr, chisqr_old, flambda, flambda_max,
     &                   factor, old_par, toler, * )
c
c     Purpose:  Make a least-squares fit to a non-linear function
c               with linearization of the fitting function
c
c     Usage:
c     CALL CURFIT(vars)
c
c     Discription of parameters:
c     x                 - array of data points for independent variable
c     y                 - array of data points for dependent variable
c     weight            - array of weights for each data point
c     npts              - number of pairs of data points
c     npar              - number of parameters = 5
c     par               - array of parameters
c     deltap            - array of increments for parameters 
c     sigmap            - array of standard deviations for parameters 
c     workl,workm,workh - working arrays to calculate derivatives
c     flambda           - proportion of gradient search included
c     yf                - array of calculated values of YE
c     chisqr            - reduced CHI square for fit
c     iter              - number of CURFIT iterations
c
c     SUBROUTINES and FUNCTION subprograms required:
c     FUNCTN(npts,x,yfit,npar,a) 
c     - evaluates the fitting function
c     FCHISQ(npts,y,yf,weight,chisqr,nyfpts) 
c      - evaluates reduced CHI square for fit to data
c     FDERIV(npts,x,workl,workm,workh,npar,a,deltap,deriv) 
c      - evaluates the derivatives of the fitting function 
c        with respect to each parameter
c     MATINV(array,npar,det) 
c      - inverts a symmetric two-dimesional matix of degree
c        NP and calculates its determinant
c
c     Comments:
c     DIMENSION statement valid for NTERMS up to NDM
c     Set FL=0.001 at beginning of search
c
c---------------------------------------------------------------------
      IMPLICIT   NONE

      INTEGER    npts,npar,nvar,ik,jk,modepar,nvaried,nfree,nyfpts,
     &           i,iv,j,k,iter,jv,iter_old,old_iter,nfn,
     &           ifdrv,ilima,idelp

      REAL*8     xe,ye,weight,yf,par,b,sigmap,array,alpha,beta,deltap,
     &           deriv,workl,workm,workh,varmat,chisqr,chisqr_old,
     &           flambda,flambda_max,factor,det,ratio,old_par,
     &           old_chi,old_flambda,toler,flami

      LOGICAL    first_call,iterate

      CHARACTER  cdum*1,
     &           cdum1*10,cdum2*10,
     &           lfn*13,lfo*13,lcn*13,lco*13,lcnn*13,lcon*13,
     &           lold*79,lnew*79,spc*79

      DIMENSION  xe(*),ye(*),weight(*),yf(*),
     &           par(*),b(*),sigmap(*),ik(*),jk(*),
     &           array(nvar,nvar),alpha(nvar,nvar),beta(*)
      DIMENSION  deltap(*),deriv(nvar,npts),
     &           workl(*),workm(*),workh(*),old_par(*),
     &           modepar(*),nvaried(*)
      DIMENSION  varmat(nvar,nvar)

      COMMON     /lim/ flami, ifdrv, ilima, idelp
      
      DATA       first_call/.true./ 
      DATA       spc
     & /'                                                           '/
      DATA       npar /5/
      data       ifdrv /3/
c
c     During first CALL only:
c
c     EVALUATE FREEDOM DEGREE NUMBER,
c     INITIALIZE WORK PARAMETERS AND ERRORS ARRAYS,
c     ELIMINATE FIXED PARAMETERS FROM VARIATIONS:

      IF( first_call )THEN
        nfree = npts-nvar-1
        IF( nfree .LE. 0 )THEN
          call error_message( 'ELLIPSE',
     &         'Sorry, there is no degree of freedom... ' )
          RETURN 1
        ENDIF
        iterate = .true.
        IF( nvar.EQ.0 )iterate = .false.
        iv = 0
        DO i = 1, npar
          b(i) = par(i)
          sigmap(i) = 0.d0
          IF( modepar(i) .EQ. 1 )THEN
            iv = iv+1
            nvaried(iv) = i
          ENDIF
        ENDDO
c
c      EVALUATE CHI SQUARE AT STARTING POINT:
c
       CALL FUNCTN( npts, xe, ye, yf, npar, par )
       CALL FCHISQ( npts, xe, ye, weight, yf, chisqr, nyfpts )
       nfree = nyfpts-nvar-1
       iter_old = 0
       iter = 0
       chisqr_old = chisqr
       first_call = .false.
       GOTO 120
      ENDIF
      
 110  CONTINUE
      
      IF( flami.EQ.0 .OR. flambda.GT.flami )flambda = flambda/factor
c
c     INITIALIZE ALPHA AND BETA MATRICES (LOWER TRIANGLE):
c
      DO j = 1, nvar
        beta(j) = 0.d0
        DO k = 1, j
          alpha(j,k) = 0.d0
        ENDDO
      ENDDO
c
c      EVALUATE ALPHA AND BETA MATRICES:
c
      IF( ifdrv.EQ.2 )THEN
        CALL FDERIV2( npts, xe, ye, workl, workm, workh, npar, par,
     &       nvar, nvaried, deltap, deriv )
      ELSEIF( ifdrv.EQ.3 )THEN
        CALL FDERIV3( npts, xe, ye, workl, workm, workh, npar, par,
     &       nvar, nvaried, deltap, deriv, *92 )
      ENDIF
      DO i = 1, npts
        DO j = 1, nvar
          beta(j) = beta(j)+weight(i)*(1.d0-yf(i))*deriv(j,i)
          DO k = 1, j
            alpha(j,k) = alpha(j,k)+weight(i)*deriv(j,i)*deriv(k,i)
          ENDDO
        ENDDO
      ENDDO
c
c     MAKE SYMMETRIC MATRIX (FILL UPPER TRIANGLE):
c
      DO j = 1, nvar
        DO k = 1,j
          alpha(k,j) = alpha(j,k)
        ENDDO
      ENDDO
c
c     INVERT MODIFIED CURVATURE MATRIX TO FIND NEW PARAMETERS:
c
 10   CONTINUE
      DO j = 1, nvar
        DO k = 1, nvar
          array(j,k) = alpha(j,k)/SQRT(alpha(j,j)*alpha(k,k))
        ENDDO
        array(j,j) = 1.d0+flambda
      ENDDO
      CALL MATINV( nvar, array, ik, jk, det )
c
c     CALCULATE NEW PARAMETER VALUES:
c
      IF( flambda.EQ.0 )GOTO 130
      DO j = 1, nvar
        jv = nvaried(j)
        b(jv) = par(jv)
        DO k = 1, nvar
          b(jv) = b(jv)+beta(k)*array(j,k)/SQRT(alpha(j,j)*alpha(k,k))
        ENDDO
      ENDDO
c
c     CALCULATE NEW VALUE OF CHISQR:
c
      CALL FUNCTN( npts, xe, ye, yf, npar, b )
      CALL FCHISQ( npts, xe, ye, weight, yf, chisqr, nyfpts )
c
      nfree = nyfpts-nvar-1
      nfn = INT(LOG(FLOAT(nfree))/LOG(10.))+1
      ratio = (chisqr_old-chisqr)/chisqr_old
c
c     IF xi^2 IS BIGGER THAN PREVIOUS ONE 
c     AND (RELATIVE CHANGE IS BIGGER THAN TOLERANCE.OR.strict_minimum), 
c     INCREASE FLAMBDA AND TRY AGAIN:
c
      IF( ratio.LT.0 )THEN
c
c     INCREASE flambda and FIND NEW b()
c
        IF( ABS(ratio).GT.toler )THEN
          IF( flambda.LT.flambda_max )THEN
            flambda = flambda*factor
            GOTO 10
          ELSE
            flambda    = old_flambda
            chisqr_old = old_chi
            iter_old   = old_iter
            DO j = 1, nvar
              jv = nvaried(j)
              par(jv) = old_par(jv)
            ENDDO
            iter = old_iter
            GOTO 110
          ENDIF
        ENDIF
      ENDIF
c
c     OTHERWISE, EVALUATE PARAMETERS AND UNCERTAINTIES:
c
      IF( ratio.GE.0 )THEN
        iter_old = iter
        iter     = iter+1
        DO i = 1, nvar
          DO j = 1, nvar
            varmat(i,j) = array(i,j)/SQRT(alpha(i,i)*alpha(j,j))
          ENDDO
        ENDDO
        old_chi     = chisqr_old
        old_flambda = flambda*factor
        old_iter    = iter_old
        DO j = 1, nvar
          jv         = nvaried(j)
          old_par(jv)  = par(jv)
          par(jv)      = b(jv)
        ENDDO
        DO i = 1,npar
          par(i)= b(i)
        ENDDO
        DO i = 1,npts
          workm(i)= yf(i)
        ENDDO
        DO j = 1, nvar
          jv = nvaried(j)
          sigmap(jv) = SQRT(varmat(j,j))
          IF( idelp.NE.0 )deltap(jv) =
     &         idelp*sigmap(jv)*SQRT(1.d0+flambda) !My:renew
        ENDDO
        DO i = 1, nvar
          iv = nvaried(i)
          DO j = 1, nvar
            jv = nvaried(j)
            varmat(i,j) = varmat(i,j)/sigmap(iv)/sigmap(jv)
          ENDDO
        ENDDO
      ENDIF
 130  IF( flambda.EQ.0 )THEN
        CALL FUNCTN( npts, xe, ye, yf, npar, par )  
        DO i = 1, nvar
          DO j = 1, nvar
            varmat(i,j) = array(i,j)/SQRT(alpha(i,i)*alpha(j,j))
          ENDDO
        ENDDO
        DO j = 1, nvar
          jv = nvaried(j)
          sigmap(jv) = SQRT(varmat(j,j))
        ENDDO
        DO i = 1, nvar
          iv = nvaried(i)
          DO j = 1, nvar
            jv = nvaried(j)
            varmat(i,j) = varmat(i,j)/sigmap(iv)/sigmap(jv)
          ENDDO
        ENDDO
      ENDIF
 120  RETURN
 92   return 1
      END
      
c    This SUBROUTINE modifies data points with a space transformation, 
c    which converts arbitrary ellipse into the centered circle of unit 
c    radius and fills an array yf*) with distances from the modified 
c    data points to center of the circle.

      SUBROUTINE FUNCTN( npts, xe, ye, yf, npar, par )
      IMPLICIT   NONE
      REAL*8     xe,ye,yf,par,A,B,C,X0,Y0,ui,vi,xo,yo
      INTEGER    npts,npar,i,nbn
      PARAMETER  (nbn=10000)
      DIMENSION  xe(*),ye(*),yf(*),par(*),xo(nbn),yo(nbn)
      COMMON     /OOO/xo,yo
ccc
      A = par(1)
      B = par(2)
      C = par(3)
      X0= par(4)
      Y0= par(5)

      DO i = 1, npts
        ui = SQRT(1-C*C)*(xe(i)-X0)/A
        vi = C*(xe(i)-X0)/A+(ye(i)-Y0)/B
        yf(i) = SQRT(ui**2+vi**2)
        xo(i) = ui
        yo(i) = vi
      ENDDO
      RETURN
      END

      SUBROUTINE FDERIV2( npts, xe, ye, workl, workm, workh, npar, par,
     &     nvar, nvaried, deltap, deriv )
      
      IMPLICIT   NONE
      INTEGER    npts,npar,nvar,nvaried,i,j,jv
      REAL*8     xe,ye,workl,workm,workh,par,deltap,deriv,pjv,delpa
      DIMENSION  xe(*),ye(*),workl(*),workm(*),workh(*),par(*),
     &           deltap(*),nvaried(*),deriv(nvar,npts)
c-----------------------------------------------------------------------
c     SUBROUTINE FDERIV(non analitical)
c
c     Purpose:
c     Evaluate derivatives of function for least-squares search
c     for arbitrary function given by FUNCTN
c
c     Usage:
c     CALL FDERIV(npts,x,workl,workm,workh,npar,a,deltap,deriv)
c
c     Description of parameters:
c     x                 - array of data points for independent variable
c     npts              - number of data points
c     workl,workm,workh - work arrays
c     par               - array of parameters
c     deltap            - array of parameter inctrements
c     npar              - number of parameters
c     deriv             - 2D array of derivatives of function over parameter
c
c     SUBROUTINE and FUNCTION subprograms required:
c     FUNCTN(npts,x,yf,npar,a) - evaluates the fitting function
c-----------------------------------------------------------------------
      DO i = 1,npts
        workl(i) = 0.d0
        workh(i) = 0.d0
        DO j = 1,nvar
          deriv(j,i) = 0.d0
        ENDDO
      ENDDO
      DO j = 1, nvar
        jv = nvaried(j)
        pjv = par(jv)
        delpa = deltap(jv)
        par(jv) = pjv-delpa
        CALL FUNCTN( npts, xe, ye, workl, npar, par )
        par(jv) = pjv+delpa
        CALL FUNCTN( npts, xe, ye, workh, npar, par )
        DO i = 1, npts
          deriv(j,i) = 0.5d0*(workh(i)-workl(i))/delpa
        ENDDO
        par(jv) = pjv
      ENDDO
      RETURN
      END
      
      SUBROUTINE FDERIV3( npts, xe, ye, workl, workm, workh, npar, par,
     &     nvar, nvaried, deltap, deriv, * )
      
      IMPLICIT  NONE
      INTEGER   npts,npar,nvar,nvaried,i,j,jv
      REAL*8    xe,ye,workl,workm,workh,par,deltap,deriv,pjv,delpa,
     &          xl,xm,xh,yl,ym,yh,d0,da,db
      DIMENSION xe(*),ye(*),workl(*),workm(*),workh(*),par(*),
     &          deltap(*),nvaried(*),deriv(nvar,npts)
c-----------------------------------------------------------------------
c     SUBROUTINE FDERIV(non analitical)
c
c     Purpose:
c     Evaluate derivatives of function for least-squares search
c     for arbitrary function given by FUNCTN
c
c     Usage:
c     CALL FDERIV(npts,x,workl,workm,workh,npar,par,deltap,deriv)
c
c     Description of parameters:
c     x                 - array of data points for independent variable
c     npts              - number of data points
c     workl,workm,workh - work arrays
c     par               - array of parameters
c     deltap            - array of parameter inctrements
c     npar              - number of parameters
c     deriv             - 2D array of derivatives of function over parameter
c
c     SUBROUTINE and FUNCTION subprograms required:
c     FUNCTN(npts,x,yf,npar,par) - evaluates the fitting function
c     The derivative of the function is evaluated in the middle point.
c     Near this point the function is approximated by the second order 
c     polinomial y= a*x^2+b*x+c (througn three point it is possible to
c     draw one  and only one parabola) and derivative is evaluated as
c     y'= 2*a*x(0)+b
c-----------------------------------------------------------------------
      DO j = 1, nvar
        jv = nvaried(j)
        pjv = par(jv)
        xm = pjv
        delpa = deltap(jv)
        xl = pjv-delpa
        par(jv) = xl
        CALL FUNCTN( npts, xe, ye, workl, npar, par )
        xh = pjv+delpa
        par(jv) = xh
        CALL FUNCTN( npts, xe, ye, workh, npar, par )
        DO i = 1, npts
          yl = workl(i)
          ym = workm(i)
          yh = workh(i)
          d0=(xm**2*xh-xm*xh**2)-(xl**2*xh-xl*xh**2)+(xl**2*xm-xl*xm**2)
          da=(ym   *xh-xm*yh   )-(yl   *xh-xl*yh   )+(yl   *xm-xl*ym   )
          db=(xm**2*yh-ym*xh**2)-(xl**2*yh-yl*xh**2)+(xl**2*ym-yl*xm**2)
          IF( d0.EQ.0 )THEN
            CALL ERROR_MESSAGE('ELLIPSE',
     &           'FDERIV cannot calculate derivative')
            return 1
          ENDIF
          deriv(j,i) = (2.d0*xm*da+db)/d0
        ENDDO
        par(jv) = pjv
      ENDDO
      RETURN
      END
      
      SUBROUTINE MATINV( norder, array, ik, jk, det )
      IMPLICIT   NONE
      INTEGER    norder,i,j,k,ik,jk,imk,jmk,l
      REAL*8     det,array,amax,save
      DIMENSION  ik(*),jk(*),array(norder,norder)
c-----------------------------------------------------------------------
c      SUBROUTINE MATINV
c
c      Purpose:
c      Invert a symmetric matrix and calculate its determinant
c
c      Usage:
c      CALL MATINV(norder,array,ik,jk,det)
c
c      Description of parameters:
c      NORDER      - degree of matrix (order of determinant)
c      ARRAY      - input matrix wich is replaced by its inverse
c      DET      - determinant of input matrix
c
c      SUBRROUTINE and FUNCTION subprograms required:
c      none
c
c      Comments:
c      Dimension statement is valid for NORDER up to NORDER
c--------------------------------------------------------------------
      det = 1.d0
      DO k = 1, norder
c
c     find largest element array(i,j) in rest of matrix
c
        amax= 0.d0
 20     CONTINUE
        DO i = k, norder
          DO j = k, norder
            IF( abs(amax).LT.abs(array(i,j)) )THEN
              amax = array(i,j)
              ik(k) = i
              jk(k) = j
            ENDIF
          ENDDO
        ENDDO
c
c     interchange rows and columns to put amax in array(k,k)
c
        IF( amax.EQ.0.d0 )THEN
          det = 0.d0
          GOTO 888
        ENDIF
        i = ik(k)
        imk = i-k
        IF( imk.LT.0 )GOTO 20
        IF( imk.GT.0 )THEN
          DO j = 1, norder
            save = array(k,j)
            array(k,j) = array(i,j)
            array(i,j) = -save
          ENDDO
        ENDIF
        j = jk(k)
        jmk = j-k
        IF( jmk.LT.0 )GOTO 20
        IF( jmk.GT.0 )THEN
          DO i = 1, norder
            save = array(i,k)
            array(i,k) = array(i,j)
            array(i,j) = -save
          ENDDO
        ENDIF
c
c     accumulate elements of inverse matrix
c
        DO i = 1, norder
          IF( i.NE.k )array(i,k) = -array(i,k)/amax
        ENDDO
        DO i = 1, norder
          IF( i.NE.k )THEN
            DO j = 1, norder
              IF( j.ne.k )array(i,j) = array(i,j)+array(i,k)*array(k,j)
            ENDDO
          ENDIF
        ENDDO
        DO j = 1, norder
          IF( j.NE.k )array(k,j) = array(k,j)/amax
        ENDDO
        array(k,k) = 1.d0/amax
        det = det*amax
      ENDDO
c
c     restore ordering of matrix
c
      DO l = 1, norder
        k = norder-l+1
        j = ik(k)
        IF( j.GE.k )THEN
          DO i = 1, norder
            save = array(i,k)
            array(i,k) = -array(i,j)
            array(i,j) = save
          ENDDO
        ENDIF
        i = jk(k)
        IF( i.GE.k )THEN
          DO j = 1, norder
            save = array(k,j)
            array(k,j) = -array(i,j)
            array(i,j) = save
          ENDDO
        ENDIF
      ENDDO
 888  CONTINUE
      RETURN
      END

      SUBROUTINE  FCHISQ( npts, xe, ye, weight, yf, chisq, nyfpts )
      IMPLICIT    NONE
      INTEGER     npts,i,nyfpts
      REAL*8      xe,ye,weight,yf,chisq,chisq_i
      CHARACTER*6 err_distr
      DIMENSION   xe(*),ye(*),weight(*),yf(*)
      
      nyfpts = 0
      chisq = 0.d0
      DO i = 1, npts
        nyfpts = nyfpts+1
        chisq_i = weight(i)*(1-yf(i))**2
        chisq = chisq+chisq_i
      ENDDO
      RETURN
      END

      SUBROUTINE  EXIT_SUB( toler, maxit, iter, factor,
     &     chisqr, chisqr_old, flambda, flambda_ini, flambda_max,
     &     nvary, par, old_par, varmat, sigmap, nparam,
     &     modepar, nvaried, deltap, nfree, more )
      IMPLICIT    NONE
      REAL*8      toler,chisqr,chisqr_old,factor,flambda,flambda_out,
     &            flambda_ini,flambda_max,par,old_par,varmat,sigmap,
     &            deltap,ratio,chisqr_out,dpar,conf,CNFL,avout,asout,
     &            parout,sigout,drvt,flami
      INTEGER     maxit,iter,nvary,nparam,modepar,
     &            nvaried,i,iv,j,jv,nfree,itlen,lambda,
     &            ifdrv,ilima,idelp
      LOGICAL     more
      CHARACTER*1 ap
      CHARACTER*4 itlab
      CHARACTER*8  cormat
      CHARACTER*14 labrat
      CHARACTER*12 ratlab
      CHARACTER*79 label
      DIMENSION    par(*),old_par(*),sigmap(*),deltap(*),modepar(*),
     &             nvaried(*),cormat(nvary,nvary),
     &             varmat(nvary,nvary)
      COMMON       /lim/flami,ifdrv,ilima,idelp
      DATA         ap/"'"/, lambda/1/
      
111   FORMAT(1x,i3,a,1pe14.7)
112   FORMAT(1x,i3,a14,2x,2(1pe14.7,2x))
113   FORMAT(1x,i3,a14,2x,1pe14.7,2x,a)
114   FORMAT(1x,4x,15x,100(a8,2x))
115   FORMAT(1x,i3,a,110(a8,2x))

121   FORMAT(1x,3x,a,1pe14.7)
122   FORMAT(1x,3x,a14,2x,2(1pe14.7,2x))
123   FORMAT(1x,3x,a14,2x,1pe14.7,2x,a)
124   FORMAT(1x,4x,15x,100(a8,2x))
125   FORMAT(1x,3x,a,110(a8,2x))

222   FORMAT(1x,a6,i3,4x,a6,1pe14.7,2(5x,a7,1pe7.0))
333   FORMAT(1x,a15,4x,     1pe14.7,2(5x,a7,1pe7.0))
444   FORMAT(1x,2(a13),3x,a13)
445   FORMAT(1x,7x,a)
552   FORMAT(1x,i3,    5('\t'),'! maximal iteration number')
553   FORMAT(1x,1pe6.0,5('\t'),'! tolerance in FIT')
554   FORMAT(1x,1pe6.0,5('\t'),'! flambda_ini')
555   FORMAT(1x,1pe6.0,5('\t'),'! flambda_max')
556   FORMAT(1x,1pe6.0,5('\t'),'! factor, increase/reduse of flambda')
557   FORMAT(1x,"'",a6,"'",4('\t'),'! poiss/gauss - error distribution')
558   FORMAT(1x,a1,a6,a1,'\t',1pe13.6,'\t',1pe7.1,'\t','! par(',i2,')')

      conf = CNFL(chisqr,nfree)
      dpar = 0.d0
      DO i = 1, nvary
        iv = nvaried(i)
        dpar = dpar+(par(iv)-old_par(iv))**2
        DO j = 1, nvary
          jv = nvaried(j)
          WRITE(cormat(i,j),'(1pe8.1)') varmat(i,j)
        ENDDO
      ENDDO
      
      IF( lambda.NE.0 )THEN
        dpar = SQRT(dpar)
        ratlab = 'D_chn/chon'
        ratio = ABS((chisqr_old-chisqr)/chisqr_old)
        IF( ratio.GE.0 )THEN 
          WRITE(labrat,'(1pe13.7)') ratio
          labrat = ' '//labrat
        ELSE
          WRITE(labrat,'(1pe14.7)') ratio
        ENDIF
      ENDIF
      
      IF( ratio.LE.toler .OR. iter.GE.maxit )THEN
        IF( maxit.LT.1 )THEN 
          chisqr_out = chisqr
          itlen = INT(LOG(maxit+.9)/LOG(10.))+1
          WRITE(itlab,'(i4)') iter
          label = 'Maximum iteration number ['
     &         //itlab(5-itlen:4)//'] has been reached'
          flambda_out = flambda
          flambda = 0.d0
          lambda  = 0
        ENDIF
        IF( lambda.NE.0 )THEN
          itlen = INT(LOG(iter+.9)/LOG(10.))+1
          WRITE(itlab,'(i4)') iter
          IF( ratio.LE.toler .AND. iter.NE.0 )THEN
            label = 'Fit has converged after ['
     &           //itlab(5-itlen:4)//'] iterations'
            flambda_out = flambda
            flambda = 0.d0
            lambda = 0
            GOTO 11
          ELSEIF( iter.GE.maxit )THEN
            label = 'Maximum iteration number ['
     &           //itlab(5-itlen:4)//'] has been reached'
            flambda_out = flambda
            flambda = 0.d0
            lambda = 0
            GOTO 11
          ELSE
            GOTO 10
          ENDIF
        ENDIF
        
        WRITE(6,'($/)')
        WRITE(6,'(1x,a)') label
        WRITE(6,'($/)')
        WRITE(6,121)      'f:  flambda     ',flambda_out
        WRITE(6,121)      'f:  Chi2        ',chisqr_out
        WRITE(6,121)      'f:  N_freedom   ',nfree*1.
        WRITE(6,121)      'f:  Chi2/N_fr   ',chisqr_out/nfree
        WRITE(6,121)      'f:  Conf.Level  ',conf
        WRITE(6,'(1x,a)') '   '//'f:  '//ratlab//labrat
        WRITE(6,'($/)')
        WRITE(6,445) 'Par.name     Par.value       Par.error'
        WRITE(6,'($/)')
        DO i= 1,nparam
          parout= par(i)
          sigout= sigmap(i)
        ENDDO
        CLOSE(14)
        WRITE(6,'($/)')
        WRITE(6,'(1x,13x,a)') 'CORRELATION MATRIX:'
        WRITE(6,'($/)')
        WRITE(6,'($/)')
        WRITE(6,'($/)')
        more = .false.
      ENDIF
 10   chisqr_old = chisqr
 11   RETURN
      END

      REAL*8 FUNCTION CNFL( x, n )
      IMPLICIT    NONE
      INTEGER     n,m,i
      REAL*8      x,sqr2pi,upl,emyo2,sum,term,fi,srty,value,const,
     &            anu,an9,xp,z,ERF,FREQ
      LOGICAL     first_call
      DATA        first_call/.true./, upl/170./
c----------------------------------------------------------------------
c      CNFL COMPUTES THE PROBABILITY THAT CHI-SQUARED, WITH N DEGREES
c      OF FREEDOM, WILL EXCEED X
c      CNFL(X,N)= 1/[2**(N/2)*GAMMA(N/2)]*INTEGRAL from X to infinity of
c                  t**(N/2-1)*EXP(-t/2) dt ; where 0<=X<infinity
c      in ABRAMOWITZ & STEGUN: HANDBOOK OF MATHEMATICAL FUNCTIONS,
c      CNFL(X,N) is denoted as Q(X|N)
c      FREQ(X)= 0.5*ERFC(-X/SQRT(2.))

c      RESTRICTIONS:  X >= 0  ;  N >= 1 is an integer

c      ACCURACY: TO AT LEAST 1.00E-5 IN TESTED REGION: UNKNOWN=>AG
c      0.001 <= X <=74, various N from 1 to 30

c      AUTHOR: Joe Chuma
c      MODIFIED by A.Gorelov in September 1999
c
c----------------------------------------------------------------------
      FREQ(x) = 0.5*(1.+ERF(x/SQRT(2.)))
      IF( first_call )THEN
        sqr2pi = SQRT(2.d0/ACOS(-1.d0))
        first_call = .false.
      ENDIF
      CNFL = 0.
      IF( n.LE.0 .OR. x.LT.0. )RETURN
      emyo2 = EXP(-0.5*x)
      sum =  1.
      term = 1.
      m = n/2
      IF( n.LE.100 )THEN         ! Nonasymptotic calculations:
        IF( 2*m.EQ.n )THEN       ! Entry if "n" is even
          IF( m.NE.1 )THEN
            DO i = 2,m
              fi = i-1
              term = 0.5*term*x/fi
              sum = sum+term
            ENDDO
          ENDIF
          CNFL = emyo2*sum
        ELSE                    !Entry if "n" is odd
          srty = SQRT(x)
          value = 2.*(1.-FREQ(srty))
          IF( n.EQ.1 ) THEN
            const = 0.
          ELSE
            const = sqr2pi*srty*emyo2
            IF( n.NE.3 )THEN
              DO i = 1, m-1
                fi = i
                term = term*x/(2.*fi+1.)
                sum = sum+term
              ENDDO
            ENDIF
          ENDIF
          CNFL = const*sum+value
        ENDIF
      ELSE                      ! Use asimptotic formula
        anu = 1./FLOAT(n)
        an9 = anu/4.5
        xp = 1./3.
        z = ((x*anu)**xp-(1.-an9))/SQRT(an9)
        CNFL = 1.-FREQ(z)
      ENDIF
      RETURN
      END
      
c     end of file
