defaults
alias curve graph\noaxes
quote=char(39)
device png
disable BORDER 
DISABLE REPLAY
WINDOW 0
CLEAR\alphanumeric
CLEAR
DISPLAY `This is a demo of some of the featues of PHYSICA'
DISPLAY ` '
DISPLAY `To start, let'//quote//`s just plot some simple graphs'
!
label\xaxis `cosh(x)+sin(1/(.1+x<^>2<_>)) vs. x'
x=[0:3:.01]
graph x cosh(x)+sin(1/(.1+x^2))
TERMINAL
clear
label\xaxis `cos(x)*x  vs.  sin(x)*x'
x=[0:19*pi:.5]*2.55555
graph sin(x)*x cos(x)*x
!
terminal `Now some examples of the graph legend'
clear
label\xaxis
!
X=[1:2*pi:.1]
autoscale on
legend on
legend autoheight on
legend\percent frame 20 60 50 80
legend title %height 3
legend title `Test Legend'
legend transparency off
set lintyp 1
T1=`x vs. sin(x)'
T2=`x vs. cos(x)'
T3=`sin(x) vs. cos(x)'
GRAPH t1 X SIN(X)
set lintyp 3
CURVE t2 X COS(X)
set lintyp 4
CURVE t3 SIN(X) COS(X)
CLEAR\NOREPLOT
REPLOT
!
TERMINAL
clear
!
T1=`<Froman.3>sin(<fitalic.3>x<froman.3>)<fitalic.3>e-
<froman.3,^><fitalic.3>x<froman.3>/5'
T2=`<fitalic.3>x<froman.3>/2-5'
T3=`<froman.3>(<fitalic.3>x<froman.3>/3.5)<^>2<_>+3-
<fitalic.3>x<froman.3>/3.5'
T4=`<froman.3>cos(<fitalic.3>x<froman.3>)<fitalic.3>e-
<froman.3,^>-<fitalic.3>x/9'
X=[0:4*pi:.2]
Y=SIN(X)*EXP(X/5)
Y2=X/2-5
Y3=(X/3.5)^2+3*X/3.5
Y4=COS(X)*EXP(-X/9)
autoscale on
LEGEND ON
LEGEND FRAME ON
LEGEND FRAME 25 55 57 75 
LEGEND TITLE %HEIGHT 3 
LEGEND TITLE `<FROMAN.SERIF>Test legend'
LEGEND TRANSPARENCY OFF
SET %TXTHIT 2.2
LEGEND AUTOHEIGHT OFF
set
 colour 1
 LINTYP 1
 PCHAR 1

LEGEND NSYMBOLS 1
GRAPH T1 X Y
set
 colour 2
 LINTYP 10
 PCHAR 2

LEGEND NSYMBOLS 2
CURVE T2 X Y2
set
 colour 3
 LINTYP 5
 PCHAR 3

LEGEND NSYMBOLS 3
CURVE T3 X Y3
set
 colour 4
 LINTYP 7
 PCHAR 13

LEGEND NSYMBOLS 4
CURVE T4 X Y4
DISPLAY `Now we replot'
DISPLAY ` '
CLEAR\NOREPLOT
REPLOT
TERMINAL
DEFAULTS
DISPLAY `For another example, let'//quote//`s simply plot some data'
DISPLAY ` '
CLEAR
X=[0:720:5]
Y=5+SIND(X+20)*EXP(X/800)+RAN(X)
SET
 PCHAR -1
 FONT TSAN
 NSXINC 5
 NSYINC 5

set COLOUR 1
GRAPH\AXESONLY X Y
set COLOUR 2
curve X Y
DISPLAY ` '
DISPLAY `Now fit a function with variable parameters to this data'
DISPLAY ` '
DISPLAY `Let'//quote//`s fit the function  F(x) = a+SIND(x+b)*EXP(x/c)'
DISPLAY `with the variable parameters are  a, b, and c'
DISPLAY ` '
SCALAR\VARY A,B,C
C=100
FIT\E1\E2 Y=A+SIND(X+B)*EXP(X/C)
DISPLAY `The fit is done'
DISPLAY ` '
DISPLAY `listing of root mean square standard errors of estimate-
 for each parameter'
DISPLAY `-----------------------------------------------
----------------------------'
DISPLAY `for a: '//rchar(FIT$E2[1])
DISPLAY `for b: '//rchar(FIT$E2[2])
DISPLAY `for c: '//rchar(FIT$E2[3])
DISPLAY ` '
FIT\UPDATE YF
SET PCHAR 0
DISPLAY ` '
DISPLAY `Overlay a plot of the fitted function'
DISPLAY `and then label the plot'
DISPLAY ` '
set COLOUR 4
CURVE X YF
SET
  %XLOC   18
  %YLOC   85
  %TXTHIT 2
   CURSOR -1

TEXT `Fitted function y=a*sin(x+b)*e<^>(x/c)'
SET
  %XLOC 70
  %YLOC 85

set COLOUR 1
TEXT `a = '//rchar(a)
SET %YLOC 81
TEXT `b = '//rchar(b)
SET %YLOC 77
TEXT `c = '//rchar(c)
SET
 %XLOC 55
 %YLOC 94
 %TXTHIT 3
 CURSOR -2

TEXT `<FROMAN.FASHON,B11>PHYSICA <b,FTRIUMF.2>fitting demonstration'
DISPLAY ` '
TERMINAL
CLEAR
DEFAULTS
WINDOW 0
generate theta 0,,180 100
x = cosd(theta)
y = x*x
x2={0;45;90;135;180}
x3={5;10;15;20;25;30;35;40}
!
y2=[1:len(x2)]
xupper = cosd(x2)
y3=[1:len(x3)]
set
 toptic 0             ! turns off tic marks on top axis
 pchar 1
 %yuaxis 85

autoscale
label\xaxis `<c4>Cos(<theta>)'
label\yaxis `<c7>Cos<^>2<_>(<theta>)'
set colour 1
graph\axes x y
set colour 2
graph\noaxes x y
get
 %xnumsz xnumsz
 %ylaxis ylaxis
 %yuaxis yuaxis
 %xiticl xiticl
  ylaxis ylaxisw
  yuaxis yuaxisw
  xiticl xiticlw
  ymin   ymin
  ymax   ymax

ticlen = xiticlw/(yuaxisw-ylaxisw)*(ymax-ymin)
y1 = ymax+ticlen
y11 = ymax+0.5*ticlen
yloc = yuaxis+1.2*xiticl
set
 %txthit xnumsz
  cursor -2
 %yloc yloc
  txtang 0
  clip 0

set pchar 0
world\percent xupper y2 xp yp
set colour 1
do j = [1:len(x2)]
  set %xloc xp[j]
  text rchar(x2[j])
  graph\noaxes {xupper[j];xupper[j]} {ymax;y1}
  xtmp = cosd(x2[j]+x3)
  world\percent xtmp y3 xpp ypp
  do k = [1:len(x3)]
    graph\noaxes {xtmp[k];xtmp[k]} {ymax;y11}
  enddo
enddo
get
 %xuaxis xux
 %xlaxis xlx

set
 %xloc (xux+xlx)/2
 %yloc 95
 cursor -2

text `<c3><theta> (degrees)'
TERMINAL
CLEAR
DEFAULTS
WINDOW 0
DISPLAY ` '
DISPLAY `Now for some examples of density plots'
DISPLAY ` '
R=MOD([0:143],4)+1
SORT\UP R
T=MOD([0:143],36)*10
Z=EXP(-R/2)*COSD(180*(R-1))
X=R*COSD(T)
Y=R*SIND(T)
GRID\XYOUT X Y Z M XOUT YOUT 
L1 = [-1;-0.5;0;0.5]
D1 = [0;0;2;1;2;2;3;2;0;0]
DEFAULTS
!DEVICE OFF
DISABLE BORDER           
SET LEGFRMT `F7.2'
WINDOW 5
DENSITY\POINTS\LEGEND XOUT YOUT M
WINDOW 6
DENSITY\POINTS\LEGEND\DITHER D1 XOUT YOUT M
WINDOW 7
DENSITY\POINTS\LEGEND\LEVELS L1 XOUT YOUT M
WINDOW 8
DENSITY\POINTS\LEGEND\DITHER\LEVELS D1 L1 XOUT YOUT M
terminal 
clear
X=[1;0;1;0;.2;.5;.8]
Y=[1;0;0;1;.2;.5;.8]
Z=[0;0;0;0;10;0;-5]
DISPLAY `Consider the following set of scattered data points'
DISPLAY ` '
enable echo
LIST X Y Z
disable echo
DISPLAY ` '
DISPLAY `Interpolate a rectangular grid from this scattered data'
DISPLAY ` '
GRID\XYOUT X Y Z M XOUT YOUT
AUTOSCALE ON
SET
 %XLOC 50
 %YLOC 2.5
 CURSOR -2

DISPLAY `First a density plot with boxes'
DISPLAY `whose areas are proportional to the grid values'
DISPLAY ` '
WINDOW 5
set fill 22
DENSITY\BOXES XOUT YOUT M
set fill 00
TEXT `Box type density plot'
DISPLAY `Now a plot with density indicated by dithering (grey scales)'
DISPLAY ` '
WINDOW 6
DENSITY\POINTS XOUT YOUT M
TEXT `Dithering type density plot'
DISPLAY `Now a plot with density indicated by diffusion -
 (digital halftoning)'
DISPLAY ` '
WINDOW 7
DENSITY\DIFFUSION XOUT YOUT M
TEXT `Diffusion type density plot'
DISPLAY `And finally, a plot with density indicated by the density -
 of random points'
DISPLAY ` '
WINDOW 8
DENSITY\RANDOM\colour XOUT YOUT M
TEXT `Random point type density plot'
TERMINAL
CLEAR
WINDOW 0
DISPLAY ` '
DISPLAY `A contour plot example using the same data'
DISPLAY ` '
clrs=mod([1:16],7)+1
CONTOUR\LEGEND\NOBORDER\colour clrs XOUT YOUT M 16
terminal
clear
defaults
autoscale commensurate
RADIUS = 50
X = [-RADIUS:RADIUS]
Y = X
SCALAR\DUMMY I J
SPHERE=LOOP(LOOP(MAX(0,(RADIUS^2-X[I]^2-Y[J]^2)),I,1:LEN(X)),J,1:LEN(Y))
SPHERE=SQRT(SPHERE)
window 5
density\diffusion\noaxes sphere
window 6
density\points\noaxes sphere
window 7
density\random\noaxes\colour sphere
defaults
window 8
autoscale 
contour\legend\colour mod([1:9],7)+1 sphere 9
TERMINAL
defaults
clear
window 0
surface\normal\colour sphere
terminal
DEFAULTS
CLEAR
DISPLAY ` '
DISPLAY `Now for a 3-d surface plot of some generated data'
DISPLAY ` '
WINDOW 0
L=72
R=MOD([0:L-1],4)+1
SORT\UP R
T=MOD([0:L-1],L/4)*10
Z=EXP(-R^4)*SIN(PI/R)
X=R*COSD(T)
Y=R*SIND(T)
GRID X Y Z M 
SURFACE\COLOUR\NORMAL M
TERMINAL
DISPLAY ` '
DISPLAY `A 3-d surface plot with a '//char(96)//`lego'//quote//-
` or histogram surface'
DISPLAY ` '
CLEAR
SURFACE\HISTOGRAM -M 
TERMINAL
DISPLAY ` '
CLEAR
DISPLAY `Examples of histograms with filled bars'
DISPLAY ` '
DEFAULTS
DISABLE BORDER
X=[1:10]
!
SET PCHAR 33 .8
!
DO J = [1:2]
 Y=10*(RAN(X)-0.5)
 SET HISTYP 2
 WINDOW 5
 SCALE 1 9 0 -5 5 0
 GRAPH X Y
 ZEROLINE\H
 WINDOW 6
 SCALE 1 9 0 -3 3 0
 GRAPH X Y
 ZEROLINE\H
!
 SET HISTYP 4
 WINDOW 7
 SCALE -5 5 0 1 9 0 
 GRAPH Y X 
 ZEROLINE\V
 WINDOW 8
 SCALE -3 3 0 1 9 0
 GRAPH Y X
 ZEROLINE\V
!
 TERMINAL
 CLEAR
ENDDO
!
Y=10*(RAN(X)-0.5)
SET HISTYP 2
WINDOW 5
SCALE 1 9 0 -5 5 0
GRAPH X Y
ZEROLINE\H
WINDOW 6
SCALE 1 9 0 -3 3 0
GRAPH X Y
ZEROLINE\H
!
SET HISTYP 4
WINDOW 7
SCALE -5 5 0 1 9 0 
GRAPH Y X 
ZEROLINE\V
WINDOW 8
SCALE -3 3 0 1 9 0
GRAPH Y X
ZEROLINE\V
!
SET HISTYP 2
WINDOW 5
SCALE 1 9 0 -5 5 0
SET PCHAR -33 .8
GRAPH X Y
SET PCHAR 0 .8
GRAPH X Y
ZEROLINE\h
WINDOW 6
SCALE 1 9 0 -3 3 0
SET PCHAR -33 .8
GRAPH X Y
SET PCHAR 0 .8
GRAPH X Y
ZEROLINE\h
!
SET HISTYP 4
WINDOW 7
SCALE -5 5 0 1 9 0 
SET PCHAR -33 .8
GRAPH Y X
ZEROLINE\V
SET PCHAR 0 .8
GRAPH Y X
WINDOW 8
SCALE -3 3 0 1 9 0
SET PCHAR -33 .8
GRAPH Y X
SET PCHAR 0 .8
GRAPH Y X
ZEROLINE\V
!
TERMINAL
!
SET HISTYP 2
WINDOW 5
SCALE 1 9 0 -5 5 0
p=ifix(ran([1:9])*7+1)*11
SET PCHAR p .8
GRAPH X Y
WINDOW 6
SCALE 1 9 0 -3 3 0
p=ifix(ran([1:9])*7+1)*11
SET PCHAR p .8
GRAPH X Y
!
SET HISTYP 4
WINDOW 7
SCALE -5 5 0 1 9 0 
p=ifix(ran([1:9])*7+1)*11
SET PCHAR p .8
GRAPH Y X
WINDOW 8
SCALE -3 3 0 1 9 0
p=ifix(ran([1:9])*7+1)*11
SET PCHAR p .8
GRAPH Y X
!
TERMINAL
defaults
window 0
clear
scale 0 11 0 10              ! set the axis scales
set nlxinc -11               ! don't draw first and last numbers on the x-axis
generat\random y 1 5 10      ! generate 10 random numbers from 1 to 5
set pchar 22 .8              ! use dot pattern 22, histogram bar width 80%
graph\hist [1:10] y+4        ! draw histogram with axes
set pchar -22 .8             ! prepare to erase dot pattern for next histogram
graph\noaxes\hist [1:10] y+2 ! erase next histogram
set pchar 33 .8              ! use dot pattern 33
graph\noaxes\hist [1:10] y+2 ! draw next histogram
set pchar -33 .8             ! prepare to erase dot pattern for next histogram
graph\noaxes\hist [1:10] y   ! erase next histogram
set pchar 44 .8              ! use dot pattern 44
graph\noaxes\hist [1:10] y   ! draw next histogram
graph\axes
terminal
clear
generate\random y  1 5 5       ! generate 10 random numbers from 1 to 5
generate\random y2 1 5 5       ! generate 10 random numbers from 1 to 5
generate\random y3 1 5 5       ! generate 10 random numbers from 1 to 5
set nlxinc -11                  ! don't draw first/last numbers on the x-axis
set pchar 22 .5                ! use dot pattern 22, histogram bar width 80%
graph\hist [1.4:10.4:2] y3     ! draw histogram with axes
set pchar -11 .5
graph\noaxes\hist [1.2:10.2:2] y2 ! erase next histogram
set pchar 33 .5                 ! use dot pattern 33
graph\noaxes\hist [1.2:10.2:2] y2 ! draw next histogram
set pchar -11 .5
graph\noaxes\hist [1:10:2] y      ! erase next histogram
set pchar 44 .5                 ! use dot pattern 44
graph\noaxes\hist [1:10:2] y      ! draw next histogram
set nlxinc -11                  ! don't draw first/last numbers on the x-axis
graph\axes                      ! draw axes to fix up any parts that were erased
terminal
CLEAR
DO K = [4;8]
  WINDOW K/4+4
  DO J = ([1:K^2]/(K^2))^2*100
    FIGURE BOX J/2 7*SQRT(J/2) J 10*SQRT(J)
  ENDDO
ENDDO
!
terminal `FFT examples'
defaults
clear
window 0
!
N = 8
X = [0:2*N-1]*PI/N
Y = SIN(X)+5*RAN(X)
SET PCHAR -1
GRAPH X Y
m=FFT(y,`cos&sin')
XI = [0:2*PI:.05]
SCALAR\DUMMY K
SET PCHAR 0
DO L = [3:N+1:2]
  YI = m[1,1]/2+SUM(m[K,1]*COS((K-1)*XI)+m[K,2]*SIN((K-1)*XI),K,2:L)
  SET LINTYP L
  CURVE XI YI
ENDDO
replot
!
terminal `now for a pie chart'
defaults
clear
window 0
!
v=[10;20;30;40;50]
e=[0;10;0;20;0]
f=[33;44;8;88;7]
c=[1;2;3;4;5]
o=[1;1;1;1;1]
pie v e f c o 30 50 50
!
terminal `Some convolution examples'
DEFAULTS
clear
window 0
!
X=[1:256]
SET
 %XNUMSZ 4
 %YNUMSZ 4
 %XLAXIS 20

GET
 %YUAXIS YUAXIS
 %XTICL XTICL

SET
 %XLOC 50
 %YLOC YUAXIS+XTICL+1
 CURSOR -2

YJ=`A[J]*EXP(-(X-B[J])^2/C[J])'
A=[100;10;200;1]
B=[50;75;100;40]
C=[25;25;25;50]
Y[1:256] = 0
DO J = [1:3]
 Y = Y+YJ
ENDDO
!
display `Ideal function'
WINDOW 5
GRAPH X Y
TEXT `Ideal function'
!
display `Convoluting function'
WINDOW 7
J=4
GRAPH X YJ
TEXT `Convoluting function'
!
display `Result of convolution'
WINDOW 6
R=CONVOL(Y,YJ,1)
GRAPH X R
TEXT `Result of convolution'
!
display `Deconvoluted'
WINDOW 8
GRAPH X CONVOL(R,YJ,-1)
TEXT `Deconvoluted'
!
TERMINAL 
CLEAR
!
display `Ideal function'
WINDOW 5
GRAPH X Y
TEXT `Ideal function'
!
display `Convoluting function'
WINDOW 7
J=4
B[J]=18
GRAPH X YJ
TEXT `Convoluting function'
!
display `Result of convolution'
WINDOW 6
R=CONVOL(Y,YJ,1)
GRAPH X R
TEXT `Result of convolution'
!
display `Deconvoluted'
WINDOW 8
GRAPH X CONVOL(R,YJ,-1)
TEXT `Deconvoluted'
!
TERMINAL 
CLEAR
R=CONVOL(Y,YJ,1)
!
display `10% noise on convoluting function'
WINDOW 5
GRAPH X CONVOL(R,YJ+.2*(RAN(X)-.5),-1)  
TEXT `10% noise on convoluting function'
!
display `1% noise on convoluting function'
WINDOW 7
GRAPH X CONVOL(R,YJ+.02*(RAN(X)-.5),-1) 
TEXT `1% noise on convoluting function'
!
display `0.1% noise on convoluting function'
WINDOW 6
GRAPH X CONVOL(R,YJ+.002*(RAN(X)-.5),-1) 
TEXT `0.1% noise on convoluting function'
!
display `0.01% noise on convoluting function'
WINDOW 8
GRAPH X CONVOL(R,YJ+.0002*(RAN(X)-.5),-1) 
TEXT `0.01% noise on convoluting function'
!
TERMINAL 
CLEAR
R=CONVOL(Y,YJ,1)
!
display `10% noise on data'
WINDOW 5
GRAPH X CONVOL(R+.2*(RAN(R)-.5),YJ,-1)  
TEXT `10% noise on data'
!
display `1% noise on data'
WINDOW 7
GRAPH X CONVOL(R+.02*(RAN(R)-.5),YJ,-1) 
TEXT `1% noise on data'
!
display `0.1% noise on data'
WINDOW 6
GRAPH X CONVOL(R+.002*(RAN(R)-.5),YJ,-1) 
TEXT `0.1% noise on data'
!
display `0.01% noise on data'
WINDOW 8
GRAPH X CONVOL(R+.0002*(RAN(R)-.5),YJ,-1) 
TEXT `0.01% noise on data'
!
TERMINAL 
CLEAR
!
F=[.06666;.25;.375;.25;.06666]
R=CONVOL(Y,YJ,1)
!
display `10% noise - 5 pt. filter'
WINDOW 5
FILTER\nonrecursive R+.2*(RAN(R)-.5) RN F
GRAPH X CONVOL(RN,YJ,-1)
TEXT `10% noise - 5 pt. filter'
!
display `1% noise - 5 pt. filter'
WINDOW 7
FILTER\nonrecursive  R+.02*(RAN(R)-.5) RN F
GRAPH X CONVOL(RN,YJ,-1)
TEXT `1% noise - 5 pt. filter'
!
display `0.1% noise - 5 pt. filter'
WINDOW 6
FILTER\nonrecursive  R+.002*(RAN(R)-.5) RN F
GRAPH X CONVOL(RN,YJ,-1)
TEXT `0.1% noise - 5 pt. filter'
!
display `0.01% noise - 5 pt. filter'
WINDOW 8
FILTER\nonrecursive  R+.0002*(RAN(R)-.5) RN F
GRAPH X CONVOL(RN,YJ,-1)
TEXT `0.01% noise - 5 pt. filter'
!
TERMINAL 
CLEAR
!
F = [-21;14;39;54;59;54;39;14;-21]/231
R=CONVOL(Y,YJ,1)
!
display `10% noise - 9 pt. filter'
WINDOW 5
FILTER\nonrecursive  R+.2*(RAN(R)-.5) RN F
GRAPH X CONVOL(RN,YJ,-1)
TEXT `10% noise - 9 pt. filter'
!
display `1% noise - 9 pt. filter'
WINDOW 7
FILTER\nonrecursive  R+.02*(RAN(R)-.5) RN F
GRAPH X CONVOL(RN,YJ,-1)
TEXT `1% noise - 9 pt. filter'
!
display `0.1% noise - 9 pt. filter'
WINDOW 6
FILTER\nonrecursive  R+.002*(RAN(R)-.5) RN F
GRAPH X CONVOL(RN,YJ,-1)
TEXT `0.1% noise - 9 pt. filter'
!
display `0.01% noise - 9 pt. filter'
WINDOW 8
FILTER\nonrecursive  R+.0002*(RAN(R)-.5) RN F
GRAPH X CONVOL(RN,YJ,-1)
TEXT `0.01% noise - 9 pt. filter'
!
display `demo done...'
