cmake_minimum_required (VERSION 3.0)
project (PHYSICA C Fortran)

set(CMAKE_MODULE_PATH
   ${CMAKE_MODULE_PATH}
   ${PROJECT_SOURCE_DIR}/cmake
   ${PROJECT_SOURCE_DIR}/cmake/Modules 
   )

find_package(Threads REQUIRED)

find_package(X11 REQUIRED)
include_directories(${X11_INCLUDE_DIRS})

find_package(GD REQUIRED)
include_directories(${GD_INCLUDE_DIR})
link_libraries(${GD_LIBRARIES})

find_package(Readline REQUIRED)
include_directories(${Readline_INCLUDE_DIR})

find_library(KERNLIB
    NAMES libkernlib  kernlib
    #HINTS "${CMAKE_PREFIX_PATH}/lib"
)
find_library(PACKLIB
    NAMES libpacklib  packlib
    #HINTS "${CMAKE_PREFIX_PATH}/lib"
)


set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -g -malign-double -fno-second-underscore -fno-automatic -frecord-marker=4 -Dgfortran -Dunix ")

add_subdirectory (gplot) 
add_subdirectory (mud) 
add_subdirectory (physica) 

configure_file (
   "${PROJECT_SOURCE_DIR}/physica/physica.sh.in"
   "${PROJECT_BINARY_DIR}/physica.sh"
   )

add_executable(Physica-bin physica/src/phys_main.F )
target_link_libraries(Physica-bin physicalib Threads::Threads ${PACKLIB} ${KERNLIB} ${X11_LIBRARIES} mudlib gplot ${GD_LIBRARY} ${Readline_LIBRARY})


#install(Physica 

install( TARGETS Physica-bin
         RUNTIME DESTINATION bin
         LIBRARY DESTINATION lib
         ARCHIVE DESTINATION lib/static )

install( FILES physica/physica.hlp physica/physica.hli DESTINATION bin )

install( FILES gplot/vaxfont.dat
  DESTINATION share/physica )

install( PROGRAMS "${PROJECT_BINARY_DIR}/physica.sh"
  DESTINATION bin 
  RENAME physica
  )


#$add_executable(physica physicalib gplot mud kernlib packlib )

#physica :       $(PHYSICA_LIB)
#                $(FORTRAN) -o $@ -Wl,-u,MAIN__ \
#                $(PHYSICA_LIB) $(MUD_LIB) $(GPLOT_LIB) $(KERNLIB) $(PACKLIB) $(OTHER_LIBS)

#ifeq (${HAVE_CERNLIB},YES)
#  XSRCS = $(PHYSICA_DIR)/src/physica_minuit.F \
#	  $(PHYSICA_DIR)/src/map_hbook_dum.F \
#	  $(PHYSICA_DIR)/src/rstr_hbook.F
#else
#  XSRCS = $(PHYSICA_DIR)/src/cern_dum.F
#endif
#
#CSRCS = $(PHYSICA_DIR)/src/physica_malloc.c \
#        $(PHYSICA_DIR)/src/physica_free.c \
#        $(PHYSICA_DIR)/src/x_resize_window.c \
#        $(PHYSICA_DIR)/src/digitize_png.c \
#        $(PHYSICA_DIR)/src/readline_wrapper.c \
#        $(PHYSICA_DIR)/src/linux/rstr_mudc.c
