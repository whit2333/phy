Physica
=======

Installation
------------

```
git clone git@eicweb.phy.anl.gov:whit/physica.git
cd physica
mkdir build && cd build
cmake ../.
make
make install
```

