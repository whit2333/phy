cmake_minimum_required (VERSION 3.6)
project (PHYSICA)

set( FSRCS 
  src/amsinit.F 
  src/arc.F 
  src/arc_dwg.F 
  src/arcv.F 
  src/asort.F 
  src/aux_port_out.F 
  src/bell.F 
  src/bitmap_free.F 
  src/bitmap_get.F 
  src/btd.F 
  src/calculator.F 
  src/cft.F 
  src/ch_real.F 
  src/check_buff.F 
  src/check_buff2.F 
  src/chsize.F 
  src/cils2.F 
  src/cltrans.F 
  src/conv_r_to_ch.F 
  src/conv_to_ch.F 
  src/conv_to_uc.F 
  src/convlc_to_uc.F 
  src/copy_array.F 
  src/datetime.F 
  src/del_array.F 
  src/deriv3.F 
  src/dexp10.F 
  src/dither.F 
  src/dline.F 
  src/dspl11.F 
  src/dsplxy.F 
  src/dspxys.F 
  src/dwg_batch_cl.F 
  src/dwg_close.F 
  src/dwg_format.F 
  src/dwg_next.F 
  src/dwg_output.F 
  src/dwg_stroke.F 
  src/ellcon.F 
  src/ellfit.F 
  src/ellips.F 
  src/end_plot.F 
  src/erase_recta.F 
  src/find_unit.F 
  src/flush_plot.F 
  src/fnice.F 
  src/fourt.F 
  src/free_space.F 
  src/get_array.F 
  src/get_plot_dev.F 
  src/get_space.F 
  src/gks_plot.F 
  src/gksdum.F 
  src/hardcopy_rng.F 
  src/hatch.F 
  src/hex_to_ascii.F 
  src/houston_plot.F 
  src/hpsub.F 
  src/impress_plot.F 
  src/iplot.F 
  src/it_is.F 
  src/jplot.F 
  src/left_justify.F 
  src/lensig.F 
  src/lib_out.F 
  src/line_thick.F 
  src/lower_case.F 
  src/lqline.F 
  src/lwrcase.F 
  src/minmax.F 
  src/mod2.F 
  src/monitor2_rng.F 
  src/monitor_rng.F 
  src/movec.F 
  src/mxhelp.F 
  src/nargs.F 
  src/pack_buffer.F 
  src/pack_buffer2.F 
  src/pen_down.F 
  src/pen_up.F 
  src/pixel_extent.F 
  src/plot_color.F 
  src/plot_device.F 
  src/plot_device2.F 
  src/plot_end.F 
  src/plot_level.F 
  src/plot_level2.F 
  src/plot_mode.F 
  src/plot_mode2.F 
  src/plot_mon.F 
  src/plot_mon2.F 
  src/plot_point.F 
  src/plot_r.F 
  src/plot_x.F 
  src/plots.F 
  src/plottplt.F 
  src/ps_plot.F 
  src/r_plot.F 
  src/rdgl.F 
  src/read_plot.F 
  src/readline.F 
  src/readln.F 
  src/regis_mode.F 
  src/regis_mode2.F 
  src/ricon.F 
  src/scale1.F 
  src/scale2.F 
  src/segment_cr.F 
  src/segment_crh.F 
  src/seiko_plot.F 
  src/seiko_plot2.F 
  src/set_plot_dev.F 
  src/setc.F 
  src/simq.F 
  src/sinter.F 
  src/status.F 
  src/symbol.F 
  src/transparent.F 
  src/upper_case.F 
  src/uprcase.F 
  src/uprcasel.F 
  src/vbuff.F 
  src/vdt_cursor.F 
  src/vhelp.F 
  src/video_off.F 
  src/window_clip.F 
  src/write_hppai.F 
  src/write_la100.F 
  src/write_gks.F 
  src/write_houst.F 
  src/write_hpp.F 
  src/write_impr.F 
  src/write_ln03.F 
  src/write_rdgl.F 
  src/write_ps.F 
  src/write_pt.F 
  src/write_px.F 
  src/zero_array.F 
  src/chreal.F 
  src/clear_plot.F 
  src/cpu_limit.F 
  src/cpu_time.F 
  src/crosshair_r.F 
  src/ctrlc_trap.F 
  src/dsplft.F 
  src/dwg.F 
  src/dwg_batch.F 
  src/dwg_list.F 
  src/dwg_open.F 
  src/findc.F 
  src/findst.F 
  src/formsg.F 
  src/generic_term.F 
  src/get_hardtype.F 
  src/get_pltype.F 
  src/get_prcnam.F 
  src/get_termtype.F 
  src/get_username.F 
  src/graphics_hc.F 
  src/igc.F 
  src/lib_free_vm.F 
  src/lib_get_vm.F 
  src/list_fnames.F 
  src/lswap.F 
  src/node_name.F 
  src/pause2.F 
  src/plot_hardc.F 
  src/print_buff.F 
  src/print_buff2.F 
  src/psym_dwg.F 
  src/psymbold.F 
  src/put_formsg.F 
  src/put_sysmsg.F 
  src/read_key.F 
  src/realch.F 
  src/sleep2.F 
  src/sysmsg.F 
  src/tt_get_input.F 
  src/tt_input.F 
  src/write_hp300.F 
  src/write_hp300c.F 
  src/write_hpjet.F 
  src/write_hpjetc.F 
  src/put_dwg.F 
  src/ascebc.F 
  src/pascebc.F 
  src/pfont.F 
  src/psym.F 
  src/term_width.F 
  src/g77/trigd.F 
  src/unix/ctrlc_hand.F 
  src/unix/evaluate.F 
  src/unix/ibatch.F 
  src/unix/lok_e_trap.F 
  src/unix/read_key_res.F 
  src/gplot/auto_scale.F 
  src/gplot/axline.F 
  src/gplot/axlog.F 
  src/gplot/do_command.F 
  src/gplot/gauto.F 
  src/gplot/gaxis.F 
  src/gplot/get_item.F 
  src/gplot/getdyn.F 
  src/gplot/getlab.F 
  src/gplot/getnam.F 
  src/gplot/getpcnt.F 
  src/gplot/gplot.F 
  src/gplot/gplot_conv.F 
  src/gplot/gplot_disp_n.F 
  src/gplot/gplot_menu.F 
  src/gplot/gplot_number.F 
  src/gplot/gplot_r.F 
  src/gplot/gplot_r_init.F 
  src/gplot/gplot_rest.F 
  src/gplot/gplot_rest_f.F 
  src/gplot/gplot_save.F 
  src/gplot/gplot_save_f.F 
  src/gplot/gplot_setup.F 
  src/gplot/gplot_short.F 
  src/gplot/gplot_symbol.F 
  src/gplot/gplot_txt.F 
  src/gplot/gploti.F 
  src/gplot/gplt.F 
  src/gplot/is_it_dynam.F 
  src/gplot/labxy.F 
  src/gplot/name_in_hex.F 
  src/gplot/setlab.F 
  src/gplot/setnam.F 
  src/gplot/symbol2.F 
  src/gplot/gline_sub.F 
  src/gplot/gplot_cont.F 
  src/gplot/gplot_line.F
  )

Set( CSRCS 
  src/keypress.c 
  src/png_setup.c 
  src/readpng.c 
  src/resize_act.c 
  src/rpng-x.c 
  src/sig_off_on.c 
  src/x_alpha.c 
  src/x_close_z.c 
  src/x_create_z.c 
  src/x_crosshair.c 
  src/x_define_col.c 
  src/x_draw_fsc.c 
  src/x_draw_zb.c 
  src/x_error.c 
  src/x_event.c 
  src/x_plot.c 
  src/x_plot_col.c 
  src/x_plot_rep.c 
  src/x_refresh.c 
  src/x_replay.c 
  src/x_setup.c 
  src/x_store.c 
  src/x_sync.c 
  src/x_wname.c 
  src/x_zoom.c 
  src/unix/fgetcpulimit.c 
  src/unix/fitof.c 
  src/unix/read_key_trp.c 
  src/unix/tcgeta.c 
  src/unix/tcseta.c 
  src/unix/tcsetaf.c 
  src/unix/tistor_put.c 
  src/g77/getc.c 
  src/g77/getpid.c 
  src/g77/malloc.c 
  src/g77/perror.c 
  src/g77/sleep.c
  )


# FFLAGS  = -g -malign-double -fno-second-underscore -fno-automatic -frecord-marker=4
# MACROS  = -Dgfortran -Dunix
set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -g -malign-double -fno-second-underscore -fno-automatic -frecord-marker=4 -Dgfortran -Dunix ")

#ifeq (${HAVE_CERNLIB},YES)
#  KERNLIB = $(CERN_DIR)/libkernlib.a
#  PACKLIB = $(CERN_DIR)/libpacklib.a
#else
#  KERNLIB =
#  PACKLIB =
#endif
#
#GPLOT_LIB   = $(GPLOT_DIR)/libgplot.a
#MUD_LIB     = $(MUD_DIR)/lib/libmud.a
#PHYSICA_LIB = $(PHYSICA_DIR)/physica.a
#
#INCLUDES    = -I/usr/X11R6/include -I/usr/local/include -I/$(HOME)/include
#OTHER_LIBS  = -L/usr/lib -lX11  -L$(HOME)/lib64 -lgd $(HOME)/lib64/libgd.a -lpng -lz -lreadline -lncurses -lnsl -lpthread

enable_language( C Fortran )

add_library(gplot ${FSRCS} ${CSRCS})


#$add_executable(physica physicalib gplot mud kernlib packlib )

#physica :       $(PHYSICA_LIB)
#                $(FORTRAN) -o $@ -Wl,-u,MAIN__ \
#                $(PHYSICA_LIB) $(MUD_LIB) $(GPLOT_LIB) $(KERNLIB) $(PACKLIB) $(OTHER_LIBS)

#ifeq (${HAVE_CERNLIB},YES)
#  XSRCS = $(PHYSICA_DIR)/src/physica_minuit.F \
#	  $(PHYSICA_DIR)/src/map_hbook_dum.F \
#	  $(PHYSICA_DIR)/src/rstr_hbook.F
#else
#  XSRCS = $(PHYSICA_DIR)/src/cern_dum.F
#endif
#
#CSRCS = $(PHYSICA_DIR)/src/physica_malloc.c \
#        $(PHYSICA_DIR)/src/physica_free.c \
#        $(PHYSICA_DIR)/src/x_resize_window.c \
#        $(PHYSICA_DIR)/src/digitize_png.c \
#        $(PHYSICA_DIR)/src/readline_wrapper.c \
#        $(PHYSICA_DIR)/src/linux/rstr_mudc.c



