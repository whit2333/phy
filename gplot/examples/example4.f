      implicit none
      real x(200), y(200), sf, pi
      integer i
C  Set up the device configuration:
C   X window monitor, no second monitor, no bitmap
C   units are inches, portrait mode
      call set_plot_devices(18,6,0,7,0,'IN','PORTRAIT',1)
      call clear_plot
      sf = 10./640.
      pi = acos(-1.)
      do i = 1, 10
        call hatch_scale(i,sf)
      enddo
      do i = 1, 100
        x(i) = (i-1)*200./99.
        y(i) = 3.*sin(x(i)*pi/180.)
      enddo
      do i = 101, 199
        x(i) = x(200-i)
        y(i) = sin(x(i)*3./2.*pi/180.)
      enddo
      call setlab('font','triumf.2')
      call setnam('lintyp',103.)
      call setnam('%ylaxis',55.)
      call nargsi(4)
      call gplot(x,y,199,1)
      do i = 1, 10
        x(i) = (i-1)*200./9.
        y(i) = 3.*sin(x(i)*pi/180.)
      enddo
      x(11) = x(10)
      y(11) = 0.0
      x(12) = 300.0
      y(12) = 0.0
      x(13) = 0.0
      y(13) = 0.0
C  Use the bottom half of the page
      call setnam('%yuaxis',45.)
      call setnam('%ylaxis',15.)
      call setnam('histyp',1.)
      call setnam('lintyp',108.)
C  Set the graph scales
      call setnam('xmin',0.0)
      call setnam('xmax',250.)
      call setnam('nlxinc',5.0)
      call setnam('ymin',-2.0)
      call setnam('ymax',4.0)
      call setnam('nlyinc',6.0)
      call nargsi(4)
      call gplot(x,y,13,1)

      write(*,*)'Hit the enter key to end the program'
      read(*,*)

      stop
      end
