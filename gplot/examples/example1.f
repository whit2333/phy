      implicit none
      real size
      integer i
cc
      call set_plot_devices(18,6,14,7,0,'CM','LANDSCAPE',1)
      call clear_plot
      size = 10.
      do i = 1, 5
        call plot_r(1.,5.,3)
        call plot_r(1.,5.+size,2)
        call plot_r(1.+size,5.+size,2)
        call plot_r(1.+size,5.,2)
        call plot_r(1.,5.,2)
        size = size*0.7
      enddo
      call psym(1.,1.,2.,'some string',0.,11)
      call flush_plot
      call nargsi(1)
      call graphics_hardcopy(12)
      stop
      end
