      implicit none
      real x(50), y(50)
      real sf
      integer i, itmp
      logical*1 pchar(50), ltmp
      equivalence (ltmp,itmp)
c
      call set_plot_devices(18,6,14,7,0,'CM','PORTRAIT',1)
      call clear_plot
c
      sf = 25./640.
      do i = 1, 10
        call hatch_scale(i,sf)
      enddo
      do i = 1, 50
        x(i) = i*10.
        y(i) = abs( sin(i/10.) )
        itmp = mod(i,10)+1
        pchar(i) = ltmp
      enddo
      call setlab('FONT','TSAN')
      call setnam('HISTYP',2.0)
      call setnam('MASK',-1.0)
      call nargsi(5)
      call gplot(x,y,50,1,pchar)
      call graphics_hardcopy(0)
      stop
      end
