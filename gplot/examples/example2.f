      implicit none
cc
      call set_plot_devices(18,6,14,7,0,'CM','PORTRAIT',1)
      call clear_plot
      call setnam('%XLOC',50.)
      call setnam('%YLOC',95.)
      call setnam('CURSOR',-2.)
      call setnam('%TXTHIT',3.)
      call setlab('FONT','ROMAN.SWISSL')
      call setlab('TEXT',
     * 'An example containing many fonts')
      call setnam('%YLOC',90.)
      call setlab('TEXT',
     * '<FGOTHIC.ENGLISH>Gothic example <FSCRIPT.2>Script example')
      call setnam('%YLOC',85.)
      call setlab('TEXT','<FKANJI4>Kanji example')
      call setnam('%YLOC',80.)
      call setlab('TEXT','<FCYRILLIC.2>Cyrillic example')
      call setnam('%YLOC',70.)
      call setlab('TEXT','<FMATH>0123456789 SJKMOPQR')
      call flush_plot
      call nargsi(1)
      call graphics_hardcopy(0)
      stop
      end
