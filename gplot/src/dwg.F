      SUBROUTINE DWG
C======================================================================C
C                                                                      C
C  DWG                                             F.W. Jones, TRIUMF  C
C                                                                      C
C  Graphics editor routine.                                            C
C  Interactively opens an EDGR drawing file and enables it for output  C
C  from PLOT_R, PSYM, PLOT_POINT, DLINE, and ARC.                      C
C                                                                      C
C======================================================================C

C======================================================================C
C  COMMON/CDWG/
C    DWGON:   Drawing file is enabled for writing.
C    DWGTXT:  Text is currently being plotted; input of font vectors
C             to DWG file is disabled.
C    LDWG:    LUN for DWG file. (lines & points)
C    LDWT:    LUN for DWT file. (text)
C    IRECG:   Current record number for graphical file.
C    IRECT:   Current record number for text file.
C    STROKE:  Stroke precision text enabled.
C======================================================================C
      COMMON/CDWG/DWGON,DWGTXT,LDWG,LDWT,IRECG,IRECT,STROKE
      LOGICAL DWGON,DWGTXT,STROKE

C======================================================================C
C  COMMON/CDWGNAM/
C    DWGOPEN:   Flag indicating that a drawing file is open.
C    DWGNAM:    Drawing name.
C======================================================================C
      COMMON/CDWGNAM/DWGOPEN,DWGNAM
      LOGICAL DWGOPEN
      CHARACTER*80 DWGNAM

      COMMON/DWG_PREVIOUS/XP,YP,IPENP

C Current plot window:
      COMMON/HARDCOPYRANGE/XMINHP2,XMAXHP2,YMINHP2,YMAXHP2,IORIENTH2
      COMMON/PLOT_INPUT_UNIT/IINS

      REAL DLINTAB(10,3)
      INTEGER*2 NPAGE2,ILINT2
      LOGICAL ISTAT
      LOGICAL*1 FILL(28)
      CALL DWG_CLOSE      !close previous drawing file
#ifdef VMS
      WRITE(*,*)'Enter new drawing name or CTRL-Z to exit.'//
     &  '  (Use * for directory list)'
#endif
#ifdef unix
      WRITE(*,*)'Enter new drawing name or CTRL-D to exit.'//
     &  '  (Use * for directory list)'
#endif
15    WRITE(*,1000)
1000  FORMAT(' Drawing name > ',$)
      READ(IINS,2000,END=99,ERR=15)DWGNAM
2000  FORMAT(A)
      IF(LENSIG(DWGNAM).EQ.0)GO TO 15
#ifdef VMS
      CALL UPRCASE(DWGNAM,DWGNAM)
#endif
      IF(INDEX(DWGNAM,'*').NE.0)THEN
        CALL DWG_LIST(DWGNAM)
        GO TO 15
      ENDIF

      CALL DWG_OPEN(DWGNAM,'NEW',LDWG,LDWT,ISTAT)
      IF(.NOT.ISTAT)GO TO 15

C Write the limits of the drawing coordinate system into the
C first two records of the file:
      WRITE(LDWG,REC=1)XMINHP2,YMINHP2,0
      NPAGE2=0
      ILINT2=1
      WRITE(LDWG,REC=2)XMAXHP2,YMAXHP2,NPAGE2,ILINT2
C Get the line table and record it in the DWT file:
      DO LTYP=1,10
        CALL DLINESET(-LTYP,P1,P2,P3)
        DLINTAB(LTYP,1)=P1
        DLINTAB(LTYP,2)=P2
        DLINTAB(LTYP,3)=P3
      ENDDO
#ifdef VMS
      WRITE(LDWT,REC=1)DLINTAB
#endif
#ifdef unix
      WRITE(LDWT,REC=1)DLINTAB,FILL
#endif

C Set starting record numbers and flags
      IRECG=3
      IRECT=2
      DWGOPEN=.TRUE.
      DWGON=.TRUE.
      IPENP=0
      XP=0.
      YP=0.

99    RETURN
      END
