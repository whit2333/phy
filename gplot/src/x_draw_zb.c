/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#include <X11/Xlib.h>

/******************************************************************
* THIS ROUTINE DRAWS THE RUBBER-BANDED ZOOM BOX, USING THE FGC,
* WHICH HAS THE GXFUNCTION SET TO BE: 1) XOR IF THE MONITOR IS COLOR
* AND 2) INVERT FOR MONOCHROME OR GRAYSCALE MONITORS.
*******************************************************************/

#define Min(a,b)  ((a) < (b) ? (a) : (b))
#define Max(a,b)  ((a) > (b) ? (a) : (b))

void xvst_draw_zb_(dpy, pix, win, gc, x1, y1, x2, y2)
   Display *dpy;
   Pixmap pix;
   Window win;
   GC gc;
   int x1, y1, x2, y2;
{
   int x, y, w, h;

   x = Min(x1, x2);
   y = Min(y1, y2);
   w = Max(x1, x2) - x;
   h = Max(y1, y2) - y;

   XDrawRectangle(dpy, win, gc, x, y, w, h);

   XFlush(dpy);
}
