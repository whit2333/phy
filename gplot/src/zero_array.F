      SUBROUTINE ZERO_ARRAY(A,NBYTES)
C
C  Zero the first NBYTES bytes of the array A
C  As of May/91 timings done on the VAX-8650 indicate
C  that there is little to be gained by using a macro
C  routine. C.J.K.
C
      BYTE A(1)
      DO I=1,NBYTES
        A(I)=0
      ENDDO
      RETURN
      END
