/* Modified 15-APR-93 by FWJ: argument list removed.  This is   */
/* an unnecessary overhead since all parameters are available   */
/* as global variables.                                         */
/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#include <X11/Xlib.h>

/*    GLOBAL VARIABLES   */
Display *dpy;
Window g_window, z_window;
int gwidth, gheight, zwidth, zheight;
int cx, cy, zcx, zcy;
GC fgc;
Bool zoom;

/*************************************************************************
*
*  THIS ROUTINE DRAWS THE FULL_SCREEN CURSOR IN COMPLEMENTARY MODE.
*  IN ORDER FOR THIS COMPLEMENTARY MODE TO WORK PROPERLY, THE FOREGROUND
*  COLOR OF THE GRAPHICS CONTEXT MUST BE SET TO BE THE SAME FOREGROUND 
*  COLOR AS BEFORE.
*
*************************************************************************/

void xvst_draw_fsc()
{
   XDrawLine(dpy, g_window, fgc, 0, cy, gwidth, cy );
   XDrawLine(dpy, g_window, fgc, cx, 0, cx, gheight);
   if (zoom) {
      XDrawLine(dpy, z_window, fgc, 0, zcy, zwidth, zcy );
      XDrawLine(dpy, z_window, fgc, zcx, 0, zcx, zheight);
   }
}
