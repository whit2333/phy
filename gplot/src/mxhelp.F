      SUBROUTINE MXHELP_START
C======================================================================C
C These are dummy entries for user-supplied routines to interface
C VHELP to a Motif help dialog or other external dialog.
C See VHELP.F for details.
C======================================================================C
      ENTRY MXHELP_PROMPT
      ENTRY MXHELP_TEXT
      RETURN
      END
