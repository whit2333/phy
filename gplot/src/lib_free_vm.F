      FUNCTION LIB_FREE_VM(NBYTES,IADDR)
      INTEGER LIB_FREE_VM

C Note: on OSF/1 this function assumes that the -T and -D loader flags
C have been used to ensure that addresses are in the low 2gb of 
C virtual memory (i.e. 32-bit-addressable).

#ifdef unix
#ifdef __osf__
      INTEGER*8 IADDR8

      IADDR8=IADDR
      CALL FREE(IADDR8)
#else
      CALL FREE(IADDR)
#endif
      LIB_FREE_VM=1
#endif
#ifdef VMS
      EXTERNAL SS$_NORMAL,LIB$_BADBLOADR

      ISTAT=LIB$FREE_VM(NBYTES,IADDR)
      LIB_FREE_VM=ISTAT
      IF(ISTAT.EQ.%LOC(SS$_NORMAL))RETURN
      IF(ISTAT.EQ.%LOC(LIB$_BADBLOADR))THEN
          WRITE(6,10)
10        FORMAT('0*** Error in FREE_SPACE: bad address specification')
      ELSE
          WRITE(6,20)
20        FORMAT('0*** Error in FREE_SPACE: who knows?')
      END IF
#endif
      RETURN
      END
