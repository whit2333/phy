.Title	EXE_CUT
.IDENT	/X01.05/
;
;reqd. routines - NONE
;
;EXE_CUT - FORTRAN - callable subroutine to execute array CODE as subroutine
;
;		CALL EXE_CUT(Arg1,Arg2,Arg3, ...ArgN,CODE)
;
;	where Arg1, Arg2, ...ArgN are passed as arguments to the
;		user routine held in array CODE.
;
;	Note:	Control is passed to the next page boundary in CODE
;
; Modifications:
;
;	Who		When		What
;	---		----		----
;	T. Miles	18-Mar-1982	Original
;
;	T. Miles	29-Apr-1983	Changed name of PSECT
;					Used Stack for Storage
;
.PSECT	$CODE,PIC,CON,REL,LCL,SHR,EXE,RD,NOWRT,LONG
;
.ENTRY	EXE_CUT,^M<IV,R2,R11>
	MOVZBL	(AP),R0		; Get Size of Argument List
	MULL	#4,R0		;  ...in bytes
	SUBL	R0,SP		; Make room for Argument List
	MOVAL	(SP),R0		; R0 --> our argument List
	MOVAL	(AP),R1		; R1 --> his argument list
	MOVL	(R1)+,R2	; R2 = argument counter
	BICL	#^C^XFF,R2	;  ...mask off funny bits
	BEQL	99$		;  ...error
	DECB	R2		;  ...don't count array
	MOVZBL	R2,(R0)+	;  ...insert argument count
	BEQL	20$		;  ...no arguments
;
10$:	MOVL	(R1)+,(R0)+	; Copy Arguments
	SOBGTR	R2,10$		;  ...until done
;
20$:	MNEGL	(R1),R0		; R1 --> Array address
	BICL	#^C^X1FF,R0	; R0  =  Offset to next page boundary
	ADDL	(R1),R0		; R0 --> Next page boundary
	CALLG	(SP),(R0)	; Execute his code...
	RET			;  ...a miracle, we survived it
;
99$:	MOVZWL	#SS$_INSFARG,-(SP)
	CALLS	#1,G^LIB$SIGNAL	;  ...crash him
;
.END
