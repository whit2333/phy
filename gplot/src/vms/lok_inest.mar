.Title	LOK_I$NEST
.Ident	/X01.07/
;
;      Reqd. [KOST.LIBRARY] routines - LOOKUP.MAR
;
; LOK_I$NEST - Internal procedure to find procedure name by stack level
;
;	Istat = LOK_I$NEST(Level,Line,Procedure_Name,Language_Name)
;
; Author:
;
;	T. Miles, TRIUMF
;	 4004 Wesbrook Mall
;	  Vancouver, B.C.
;	   CANADA, V6T 2A3
;
;	(604) 228-4711
;
; External procedures:
;
;	LIB$SIG_TO_RET	- Procedure converts exception to return code
;
;	LOK_I$LOOK_UP	- Procedure finds context from user P.C.
;
; Restrictions:
;
; (1)	Not all language types are supported
;
; (2)	Image must be linked /TRACEBACK (which is the default)
;
; Modifications:
;
;	Who		When		What
;	---		----		----
;	T. Miles	07-Aug-83	Original
;
;	T. Miles	12-Aug-83	Cleaned up code
;
;	T. Miles	14-Aug-83	Used symbolic names for
;					exception offsets
;
;	T. Miles	29-Aug-83	Renamed procedure to conform
;					to new TRIUMF naming convention
;
; Define fields in condition handler
;
$CHFDEF
;
; Define fields in call frame
;
$SFDEF
;
.PSECT	$CODE,PIC,CON,REL,LCL,SHR,EXE,RD,NOWRT,LONG
;
.ENTRY	LOK_I$NEST,^M<R2,R3,R4,R5>
	MOVAL	G^LIB$SIG_TO_RET,SF$A_HANDLER(FP)
	MOVAL	(FP),R0			; R0 --> stack frame
	MOVL	@4(AP),R1		; R1  =  depth
	BGEQ	30$			; + ==> show calls
;
	SUBL3	R1,#-2,R1		; Else do traceback
	BLSS	20$			;  ...wants 'guilty' frame
;
10$:	BITL	#^XC0000000,-		; Found system call?
		SF$L_SAVE_PC(R0)	;  ...on stack
	BNEQ	60$			; Yes, done
	MOVL	SF$L_SAVE_FP(R0),R0	;  ...else unwind more
	BNEQ	10$			;  ...until done
	BRB	70$			;  ...oops
;
20$:	MOVL	SF$L_SAVE_AP(FP),R1	; Else load old A.P.
	MOVAL	@CHF$L_SIGARGLST(R1),R1	;  R1 --> Signal Arguments
	SUBL	#20.,SP			; Get some storage
	MOVZBL	CHF$L_SIG_ARGS(R1),R2	; Get number of arguments
	MOVAL	-4(R1)[R2],(SP)		; Get 'guilty' P.C.
	BRB	40$			;  ...and continue
;
30$:	BITL	#^XC0000000,-		; Unwound to the depth
		SF$L_SAVE_PC(R0)	;  ...of the CLI?
	BNEQ	70$			; Yes, done
;
	SOBGEQ	R1,60$			;  ...else unwind more
;
	SUBL	#20.,SP			; Get work storage
	MOVAL	SF$L_SAVE_PC(R0),(SP)	; Install P.C.
;
40$:	MOVC5	#0,(SP),#0,#16.,4(SP)	;  ...zero remainder
;
	CMPB	(AP),#2			; Sufficient arguments?
	BLSSU	50$			;  ...no, die
	MOVAL	@8.(AP),8.(SP)		; Else install line number
;
	CMPB	(AP),#3			; Sufficient arguments?
	BLSSU	50$			;  ...no, die
	MOVAL	@12.(AP),12.(SP)	; Else install routine name
;
	CMPB	(AP),#4			; Sufficient arguments?
	BLSSU	50$			;  ...no, die
	MOVAL	@16.(AP),16.(SP)	; Else install language name
;
50$:	CALLS	#5,G^LOK_I$LOOK_UP	; Look_up context
	RET
;
60$:	MOVL	SF$L_SAVE_FP(R0),R0	; Unwind this frame
	BNEQ	30$			;  ...if it exists
;
70$:	MOVZWL	#SS$_INSFRAME,R0	; Else insufficient frames
	RET				;  ...to unwind
;
.End
