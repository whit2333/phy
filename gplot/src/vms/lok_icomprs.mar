.Title	LOK_I$COMPRS
.Ident	/X01.01/
;LOK_I$COMPRS - Procedure to copy String, removing blanks and tabls
;
;	Istat = LOK_I$COMPRS(Out_String,In_String[,Length])
;
; Author:
;
;	T. Miles, TRIUMF
;	 4004 Wesbrook Mall
;	  Vancouver, B.C.
;	   CANADA, V6T 2A3
;
;	(604) 228-4711
;
; External procedures:
;
;	LIB$SIG_TO_RET		- Procedure converts exception to return code
;
; Restrictions:
;
; Modifications:
;
;	Who		When		What
;	---		----		----
;	T. Miles	01-Sep-1983	Original
;
$SFDEF
;
.PSECT	$CODE,PIC,CON,REL,LCL,SHR,EXE,RD,NOWRT,LONG
;
.ENTRY	LOK_I$COMPRS,^M<R2,R3,R4>
	MOVAL	G^LIB$SIG_TO_RET,-	; Install catch-all
		SF$A_HANDLER(FP)	;  ...for handler
	MOVQ	@4(AP),R0		; [R0,R1] output string
	MOVZWL	R0,R0			;  ...zero extend
	DECL	R0			;  ...and decrement
	MOVQ	@8(AP),R2		; [R2,R3] input  string
	MOVZWL	R2,R2			;  ...zero extend
	CLRL	R4			; Zero extend it...
;
10$:	DECL	R2			; Decrement source string
	BLSS	30$			;  ...done
	BICB3	#^X80,(R3)+,(R1)	; Copy byte from source
	CMPB	#^X20,(R1)		; Space character?
	BGEQU	10$			;  ...yes, ignore
	CMPB	#^X60,(R1)		; Lower case?
	BGEQU	20$			;  ...no, O.K.
	BICB	#^X20,(R1)		; Else convert to upper
20$:	INCL	R1			;  ...accept byte
	AOBLEQ	R0,R4,10$		;  ...and continue
	BRB	50$			; Else truncated
;
30$:	PUSHL	R4			; Save old value
;
40$:	CLRB	(R1)+			; Pad output string
	AOBLEQ	R0,R4,40$		;  ...and continue
;
	MOVL	(SP)+,R4		;  ...restore R4
;
50$:	CMPB	(AP),#3			; Length argument given
	BLSSU	60$			;  ...no
	TSTL	12.(AP)			; Length defaulted?
	BEQL	60$			;  ...yes
	MOVZWL	R4,@12.(AP)		; Else return length
;
60$:	MOVZWL	#SS$_NORMAL,R0		; Return success
	RET				;  ...and away
;
.End
