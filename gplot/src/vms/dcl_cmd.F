      SUBROUTINE DCL_CMD(CMD,*)
C
C     reqd. routines - NONE
C=================================================================
C=================================================================
C==                                                             ==
C==   DCL_CMD: Executes a DCL command by spawning a subprocess. ==
C==            "CMD" is a character variable which contains     ==
C==            the command string.                              ==
C==                                                             ==
C==   Written by Arthur Haynes, TRIUMF U.B.C., DEC. 18, 1981.   ==
C==                                                             ==
C==   Input Parameters: CMD (CHARACTER*(*)).                    ==
C==                                                             ==
C==   Examples of use:                                          ==
C==   CALL DCL_CMD('$SHOW SYS')                                 ==
C==   CALL DCL_CMD('$@[ART]COMMAND.COM')                        ==
C==                                                             ==
C==   Modified by Alan Carruthers, March 3, 1983 to use         ==
C==      LIB$SPAWN instead of LIB$EXECUTE_DCL.                  ==
C=================================================================
C=================================================================
      CHARACTER*(*) CMD
      ISTAT=LIB$SPAWN(CMD)
      IF(ISTAT.NE.1)RETURN1
      RETURN
      END
