.Title	LOK_I$LOOK_UP
.Ident	/X01.19/
.Library/lok_trace.mlb/
.Library/sys$library:lib.mlb/
.Enable	DBG
.Nlist	ME
;
;     Reqd. [KOST.LIBRARY] routines - VMP.MAR
;
; LOK_I$LOOK_UP - Internal procedure which translates P.C. to procedure name
;
; Istat = LOK_I$LOOK_UP(Ipc,Ipc_rel,Line,Procedure,Language,Module)
;
; Author:
;
;	T. Miles, TRIUMF
;	 4004 Wesbrook Mall
;	  Vancouver, B.C.
;	   CANADA, V6T 2A3
;
;	(604) 228-4711
;
; External procedures:
;
;	LIB$FREE_EF	- Procecure frees     event flag
;
;	LIB$GET_EF	- Procedure allocates event flag
;
;	LIB$SIG_TO_RET	- Procedure converts exception to return code
;
;	LOK_FREE_VM_P	- Procedure frees	virtual memory
;
;	LOK_GET_VM_P	- Procedure allocates	virtual memory
;
;	LOK_LOCK_VM_P	- Procedure protects	virtual memory
;
; Restrictions:
;
; (1)	Not all languages are supported
;
; (2)	Image must be linked /TRACEBACK (which is the default)
;
; Modifications:
;
;	Who		When		What
;	---		----		----
;	T. Miles	07-Aug-83	Original
;
;	T. Miles	08-Aug-83	Allocate event flag
;
;	T. Miles	09-Aug-83	Make code modular
;
;	T. Miles	10-Aug-83	Use direct access
;
;	T. Miles	11-Aug-83	Back to sequential access
;					Lock symbols in memory
;
;	T. Miles	12-Aug-83	Write protect symbol table
;					Fix bug in I_PRC routine
;
;	T. Miles	12-Aug-83	Use KOST$GET_VM and KOST$PUT_VM
;					to page_align symbol table
;
;	T. Miles	29-Aug-83	Rename procedure to agree with
;					new TRIUMF naming conventions
;
;	Y. Miles	09-Aug-86	Get offset of debug symbol table
;					correctly, by using image header
;					offset IHD$W_SYMDBGOFF ...
;
;	Y. Miles	12-Aug-86	Use DSTDEF$ macro and modify
;					for VMS 4.2 module types
;
.MACRO	.CSTR	X
	.Enable	LSB
	.word	20$-10$
10$:	.ASCII	X
20$:
	.Disable LSB
.ENDM
;
; Define vax standard type codes
;
$DSCDEF
;
; Define traceback symbol table entries
;
$DSTDEF
;
; Define image header offsets
;
$IHDDEF
;
; Define fields in Call Frame
;
$SFDEF
;
; Define the following symbol to lock symbol table in memory
;
LOCK	=0				; Lock symbol table in memory
;
BLOK	=^X200
SIZE	=^X20
;
.PSECT	$CODE,PIC,CON,REL,LCL,SHR,EXE,RD,NOWRT,LONG
;
.ENTRY	LOK_I$LOOK_UP,^M<R2,R3,R4,R5,R6>
	JSB	I_PRC			; Initialize procedure
	JSB	S_IMG			; Scan image file
	MOVZWL	#SS$_NOTRAN,R0		; No name found for image
	BRW	CRASH			;  ...and exit
;
; I_PRC - Routine to initialize procedure, allocate scratchpad memory
;
I_PRC:	MOVAL	G^LIB$SIG_TO_RET,-	; Install condition
		SF$A_HANDLER(FP)	;  ...handler
	$SETAST_S ENBFLG=#0		; Test_and_clear ast enable
	MOVZBL	#1,ASTS			;  ...assume enabled
	CMPL	#SS$_WASCLR,R0		; Were they enabled?
	BNEQ	10$			;  ...yes, O.K.
	CLRL	ASTS			; No, show them disabled
10$:	CLRL	STKD			; Reset stack depth
	MOVL	BUFF,R6			; Valid table in memory?
	BNEQ	20$			;  ...yes, skip init
;
	JSB	L_IMG			; Lookup image file
	JSB	O_IMG			; Open   image file
	JSB	R_SYM			; Read   image symbol table
	JSB	C_IMG			; Close  image file
	MOVL	BUFF,R6			; R6 --> Symbol Table
;
20$:	RSB				; Else do init stuff
;
; L_IMG - Routine to lookup name of task image and install it in FAB
;
L_IMG:	MOVL	#BLOK,MEM		; Request a block
	PUSHAL	BUFF			;  ...of memory
	PUSHAL	MEM			;  ...from system
	CALLS	#2,G^LOK_GET_VM_P	;  ...library routine
	BLBC	R0,10$			;  ...couldn't
	MOVL	BUFF,R6			; R6 --> Memory
;
	PUSHAL	EFN			; Go and allocate
	CALLS	#1,G^LIB$GET_EF		;  ...event flag
	BLBC	R0,10$			;  ...couldn't
	MOVL	BUFF,ITMLST+4		; Install address
	$GETJPI_S EFN=EFN,ITMLST=ITMLST	; Get name of task image
	BLBC	R0,10$			;
	$WAITFR_S	EFN=EFN		;  ...wait for answer
	BLBC	R0,10$			;
	PUSHAL	EFN			; Now deallocate
	CALLS	#1,G^LIB$FREE_EF	;  ...event flag
	BLBC	R0,10$			;
	MOVB	IFNS,FAB+FAB$B_FNS	; Install file name size
	MOVL	BUFF,FAB+FAB$L_FNA	;  ...and file name address
	RSB				;  ...done
;
10$:	BRW	CRASH			; Error somewhere
;
; O_IMG - Routine to open and connect to task image file
;
O_IMG:	$OPEN	FAB=FAB			; Open task image
	BLBC	R0,20$			;
	MOVL	BUFF,RAB+RAB$L_UBF	; Point RAB at buffer
	$CONNECT RAB=RAB		;  ...connect
	BLBC	R0,20$			;
	$GET	RAB=RAB			;  ...image header
	BLBC	R0,20$			;  ...couldn't
;
	MOVZWL	IHD$W_SYMDBGOFF(R6),R0	; R0  =  debug table offset
	ADDL	R6,R0			; R0 --> Debug table address
	MOVL	(R0)+,REOB		; Point to symbol table
	BEQL	30$			;  ...not found
	MOVL	(R0)+,REOS		; End of symbol table
	BNEQ	10$			;  ...exists
	ADDL3	#1,XAB+XAB$L_EBK,REOS	; Else end of file
;
10$:	PUSHAL	BUFF			; Free scratch
	PUSHAL	MEM			;  ...memory
	CALLS	#2,G^LOK_FREE_VM_P	;  ...with call
	BLBC	R0,20$			;  ...failed
	RSB				; Else succeeded
;
20$:	BRW	CRASH			; Error somewhere
;
30$:	BRW	GIMPED			; No symbol table
;
; R_SYM - Routine to read task image symbol table
;
R_SYM:	SUBL3	REOB,REOS,MEM		; Calculate blocks to allocate
	MULL	#BLOK,MEM		;  ...convert to bytes
	ADDL	#BLOK,MEM		;  ...fudge
	PUSHAL	BUFF			;  ...then go
	PUSHAL	MEM			;  ...get them
	CALLS	#2,G^LOK_GET_VM_P	;  ...from system
	BLBC	R0,40$			;  ...couldn't
	MOVL	BUFF,RAB+RAB$L_UBF	; Else point RAB at buffer
;
10$:	$GET	RAB=RAB			; Read next block
	BLBC	R0,20$			;  ...error
	CMPL	RAB+RAB$L_RFA0,REOB	; Start of symbol table?
	BLSSU	10$			;  ...not yet
	ADDL	#BLOK,RAB+RAB$L_UBF	; Point to next block
	CMPL	RAB+RAB$L_RFA0,REOS	; End   of symbol table?
	BLSSU	10$			;  ...not yet
	BRB	30$			; Else finished read
;
20$:	CMPL	#RMS$_EOF,R0		; End of image found?
	BNEQ	40$			;  ...no, gronk
;
30$:	RSB				; Read completed successfully
;
40$:	BRW	CRASH			; Error somewhere
;
; C_IMG - Routine to close image file and protect symbol table
;
C_IMG:	$CLOSE	FAB=FAB			; Close (implied disconnect)
	BLBC	R0,10$			;  ...error somewhere
;
	PUSHAL	BUFF			; Write protect the
	PUSHAL	MEM			;  ...symbol table
	CALLS	#2,G^LOK_LOCK_VM_P	;  ...with call
	BLBC	R0,10$			;  ...oops
;
	RSB				;  ...done
;
10$:	BRW	CRASH			;  ...error somewhere
;
; CRASH - Routine to return failure status to user
;
CRASH:	PUSHR	#^M<R0,R1>		; Status is in R0
	$CLOSE	FAB=FAB			;  ...release file
	MOVL	@4(AP),STAPC		; Offset = P.C.
	MOVW	UNKNOW,MODULE		; Module is unknown
	MOVC3	UNKNOW,UNKNOW+2,MODULE+2;
	MOVW	UNKNOW,ROUTIN		; Procedure is unknown
	MOVC3	UNKNOW,UNKNOW+2,ROUTIN+2;
	CLRL	LINENO			; Line number is unknown
	MOVW	UNKNOW,LANG		; Language is unknown
	MOVC3	UNKNOW,UNKNOW+2,LANG+2	;
	POPR	#^M<R0,R1>		; Restore status
	BRW	EXIT			;  ...and exit
;
; EXIT - Routine to return results to user
;
EXIT:	PUSHR	#^M<R0,R1>		; Status is in R0
;
.IIF DF LOCK,BLBS R0,10$		; Skip if success
;
	PUSHAL	BUFF			;  ...else go
	PUSHAL	MEM			;  ...deallocate
	CALLS	#2,G^LOK_FREE_VM_P	;  ...memory
	CLRL	BUFF			;  ...show none
	CLRL	MEM			;  ...no memory
;
10$:	CMPB	(AP),#2			; Relative P.C.
	BLSSU	60$			;
	TSTL	8(AP)			;
	BEQL	20$			;
	SUBL3	STAPC,@4(AP),@8(AP)	;  ...error
;
20$:	CMPB	(AP),#3			; Line number
	BLSSU	60$			;
	TSTL	12.(AP)			;
	BEQL	30$			;
	MOVL	LINENO,@12.(AP)		;
;
30$:	CMPB	(AP),#4			; Procedure name
	BLSSU	60$			;
	TSTL	16.(AP)			;
	BEQL	40$			;
	MOVQ	@16.(AP),R0		;
	MOVC5	ROUTIN,ROUTIN+2,#^X20,R0,(R1)
;
40$:	CMPB	(AP),#5			; Language name
	BLSSU	60$			;
	TSTL	20.(AP)			;
	BEQL	50$			;
	MOVQ	@20.(AP),R0		;
	MOVC5	LANG,LANG+2,#^X20,R0,(R1)
;
50$:	CMPB	(AP),#6			; Module name
	BLSSU	60$			;
	TSTL	24.(AP)			;
	BEQL	60$			;
	MOVQ	@24.(AP),R0		;
	MOVC5	MODULE,MODULE+2,#^X20,R0,(R1)
;
60$:	$SETAST_S ENBFLG=ASTS		; Restore ast status
	POPR	#^M<R0,R1>		;  ...return code
	RET				;  ...to caller
;
; S_IMG - Routine to scan entire symbol table, to find procedure
;
SNEXT:	MOVZBL	(R6),R0			; Offset to next
	INCL	R0			;  ...fixup
	ADDL	R0,R6			; R6 --> Next
;
S_IMG:	TSTB	(R6)			; Next record present?
	BEQL	NOTRAN			;  ...no
;
	CMPB	1(R6),#DST$K_PSECT	; Psect definition?
	BEQL	10$			;  ...yes
	CMPB	1(R6),#DST$K_LINE_NUM	; Line number definition?
	BEQL	20$			;  ...yes
	CMPB	1(R6),#DST$K_MODBEG	; Start module definition?
	BEQL	30$			;  ...yes
	CMPB	1(R6),#DST$K_MODEND	; End   module definition?
	BEQL	40$			;  ...yes
	CMPB	1(R6),#DST$K_RTNBEG	; Start routin definition?
	BEQL	50$			;  ...yes
	CMPB	1(R6),#DST$K_RTNEND	; End   routin definition?
	BEQL	60$			;  ...yes
;
	CMPB	1(R6),-			; Is record too low for
		#DSC$K_DTYPE_LOWEST	;  ...vax standard type code
	BLSSU	GIMPED			;  ... yes, hosed
	CMPB	1(R6),-			; Is record within range of
		#DSC$K_DTYPE_HIGHEST	;  ...vax standard type code
	BLEQU	SNEXT			;  ... yes, ignore
;
	CMPB	1(R6),#DST$K_LOWEST	; Is record too low?
	BLSSU	GIMPED			;  ...yes, invalid
	CMPB	1(R6),#DST$K_HIGHEST	; Is record too high?
	BGTRU	GIMPED			;  ...yes, invalid
	BRB	SNEXT			; Else legal, ignore
;
10$:	BRW	PSECT			; Psect record
20$:	BRW	LINE			; Line  number record
30$:	BRW	STARTM			; Start module record
40$:	BRW	ENDM			; End   module record
50$:	BRW	STARTR			; Start record record
60$:	BRW	ENDR			; End   record record
;
GIMPED:	MOVZWL	#SS$_BADIMGHDR,R0	; Corrupted task image
	BRW	CRASH			;  ...causes crash
;
NOTRAN:	RSB				;  ...no routine found
;
; Procedure end record encountered
;
ENDR:	DECL	STKD			; Decrement depth
	CMPL	#1,STKD			;  ...should be module
	BNEQ	20$			;  ...if not, bomb
	ADDL3	3(R6),STAPC,ENDPC	; Calculate end P.C.
;
	CMPL	STAPC,@4.(AP)		; Routine start above P.C.?
	BGTRU	10$			;  ...yes, not here
	CMPL	@4.(AP),ENDPC		; P.C. above routine end?
	BGEQU	10$			;  ...yes, not found
;
	CLRL	LINENO			; Else found routine with
	BRW	GOT_IT			;  ...bad line numbers
;
10$:	MOVW	UNKNOW,ROUTIN		; Invalidate the
	MOVC3	UNKNOW,UNKNOW+2,ROUTIN+2;  ...routine
	BRW	SNEXT			;  ...back for more
;
20$:	BRW	GIMPED			; Incorrect stack depth
;
; Procedure start record encountered
;
STARTR:	CMPL	#1,STKD			; Depth should be
	BNEQ	20$			;  ...module
	INCL	STKD			; Set to routine
;
10$:	MOVZBW	7(R6),ROUTIN		; Store the
	MOVC3	ROUTIN,8(R6),ROUTIN+2	;  ...routine name
	MOVL	3(R6),STAPC		;  ...start P.C.
	MOVL	3(R6),CURPC		;  ...current P.C.
	MOVZBL	#1,LININC		;  ...reset line increment
	CLRL	LINENO			;  ...invalidate Line #
	BRW	SNEXT			;  ...back for more
;
20$:	CMPL	#2,STKD			; Stack depth routine?
	BNEQ	30$			;  ...no, fatal
	CMPB	#DST$K_MACRO,LANG	; Language Macro?
	BEQL	10$			;  ...yes, macro is rude
;
30$:	BRW	GIMPED			; Incorrect stack depth
;
; Module end record encountered
;
ENDM:	DECL	STKD			; Decrement depth
	BNEQ	10$			;  ...should be empty
	MOVW	UNKNOW,MODULE		; Invalidate the
	MOVC3	UNKNOW,UNKNOW+2,MODULE+2;  ...module
	MOVW	UNKNOW,LANG		; Invalidate the
	MOVC3	UNKNOW,UNKNOW+2,LANG+2	;  ...language
	BRW	SNEXT			;  ...back for more
;
10$:	CMPB	#DST$K_MACRO,LANG	; Is the thing Macro?
	BEQL	ENDM			;  ...yes, ignore
;
20$:	BRW	GIMPED			; Incorrect stack depth
;
; Module start record encountered
;
STARTM:	TSTL	STKD			; Depth should be
	BNEQ	20$			;  ...empty
	INCL	STKD			; Set to module
	MOVZBW	7(R6),MODULE		; Store the
	MOVC3	MODULE,8(R6),MODULE+2	;  ...module name
	CLRL	LINENO			;  ...line #
;
	MOVZBL	3(R6),R0		; R0 = Language code
	CMPL	R0,#DST$K_MAX_LANGUAGE	;  ...within range?
	BLSSU	10$			;  ...yes
	MOVZBL	#DST$K_MAX_LANGUAGE,R0	;  ...else force limit
;
10$:	MOVL	G^LNGTBL[R0],R1		; R1 --> Language header
	MOVW	(R1),LANG		;  ...install length
	MOVC3	(R1),2(R1),LANG+2	;  ...and string
	BRW	SNEXT			;  ...then continue
;
20$:	BRW	GIMPED			; Incorrect stack depth
;
; Line numbering record encountered
;
LINE:	CMPL	#2,STKD			; Within routine?
	BNEQ	GONGED			;  ...no, bad image
LINE10:	CMPL	STAPC,@4.(AP)		; Out of range?
	BGTRU	10$			;  ...yes, skip this record
	MOVAL	2(R6),R2		; R2 --> Current op. code
	MOVZBL	(R6),R5			; R5  =  Length of Record
	ADDL	R6,R5			;   -->  End    of Record
	BRB	LNEXT			;  ...and continue
;
10$:	BRW	SNEXT			; Skip, out of range record
;
LNEXT:	CMPL	R2,R5			; Processed entire record?
	BLSSU	10$			;  ...no
	BRW	SNEXT			; Else fetch next record
;
10$:	CASEB	(R2)+,#0,#16.		; Dispatch on op. code
20$:	.word	D_PC_B-20$		;  ...delta_pc, byte
	.word	D_PC_W-20$		;  ...delta_pc, word
	.word	D_LN_B-20$		;  ...delta_line_no, byte
	.word	D_LN_W-20$		;  ...delta_line_no, word
	.word	LN_I_B-20$		;  ...line_no_inc, byte
	.word	LN_I_W-20$		;  ...line_no_inc, word
	.word	LN_I_R-20$		;  ...line_no_inc, reset
	.word	LN_ERR-20$		;  ...Begin_Statement_Mode (BASIC)
	.word	LN_ERR-20$		;  ...End_Statement_Mode   (BASIC)
	.word	LN_S_W-20$		;  ...line_no_set, word
	.word	S_PC_B-20$		;  ...set_pc, byte
	.word	S_PC_W-20$		;  ...set_pc, word
	.word	S_PC_L-20$		;  ...set_pc, longword
	.word	LN_ERR-20$		;  ...Set_Statement_No     (BASIC)
	.word	K_PC_B-20$		;  ...term_pc, byte
	.word	K_PC_W-20$		;  ...term_pc, word
	.word	S_PC_A-20$		;  ...set_pc, absolute
;	BRB	D_PC_B			;  ...delta_pc, byte
;
D_PC_B:	CVTBL	-1(R2),R0		; Get neg. displacement
	BGTR	GONGED			;  ...else invalid
	SUBL	R0,CURPC		; Subtract (neg.) displacement
;	BRB	CHECK			;  ...then check
;
CHECK:	CMPL	CURPC,@4.(AP)		; Is it within this line?
	BGEQU	10$			;  ...yes, found it
	ADDL	LININC,LINENO		; Else bump line number
	BRB	LNEXT			;  ..and continue
;
10$:	BRW	GOT_IT			; P.C. lies within this line
;
GONGED:	CMPB	#DST$K_PASCAL,LANG	; Language PASCAL
	BEQL	LINE10			;  ...yes, ignore
	BRW	GIMPED			; Unknown line # descriptor
;
D_PC_W:	MOVZWL	(R2)+,R0		; Get word displacement
	ADDL	R0,CURPC		; Add displacement
	BRB	CHECK			;  ...then check
;
D_LN_B:	MOVZBL	(R2)+,R0		; Get byte displacement
	ADDL	R0,LINENO		;  ...add it in
	BRB	LNEXT			;  ...and continue
;
D_LN_W:	MOVZWL	(R2)+,R0		; Get word displacement
	ADDL	R0,LINENO		;  ...add it in
	BRW	LNEXT			;  ...and continue
;
LN_I_B:	MOVZBL	(R2)+,LININC		; Set line_no_inc
	BRW	LNEXT			;  ...and continue
;
LN_I_W:	MOVZWL	(R2)+,LININC		; Set line_no_inc
	BRW	LNEXT			;  ...and continuue
;
LN_I_R:	MOVZBL	#1,LININC		; Reset line_no_inc
	BRW	LNEXT			;  ...and continue
;
LN_ERR:	CLRL	LINENO			; Can't handle BASIC stuff
	BRW	SNEXT			;  ...give up
;
LN_S_W:	MOVZWL	(R2)+,LINENO		; Set_line_no, word
	BRW	LNEXT			;  ...and contine
;
S_PC_B:	MOVZBL	(R2)+,R0		; Set_pc, byte
	ADDL3	STAPC,R0,CURPC		;  ...from start
	BRW	LNEXT			;  ...and continue
;
S_PC_W:	MOVZWL	(R2)+,R0		; Set_pc_word
	ADDL3	STAPC,R0,CURPC		;  ...from start
	BRW	LNEXT			;  ...and continue
;
S_PC_L:	ADDL3	STAPC,(R2)+,CURPC	; Set_pc_longword, from start
	BRW	LNEXT			;  ...and continue
;
K_PC_B:	MOVZBL	(R2)+,R0		; Do term, byte
	ADDL	R0,CURPC		;  ...bump p.c.
	BRW	CHECK			;  ...then check
;
K_PC_W	=D_PC_W				; Process as delta_pc, word
;
S_PC_A:	MOVL	(R2)+,CURPC		; Set_absolute_pc
	BRW	LNEXT			;  ...and continue
;
GOT_IT:	MOVZWL	#SS$_NORMAL,R0		;  ...show success
	BRW	EXIT			;  ...and away
;
; Psect definition record encountered
;
PSECT:	MOVZBW	7(R6),ROUTIN		; Store the
	MOVC3	ROUTIN,8(R6),ROUTIN+2	;  ...psect name
	MOVL	3(R6),STAPC		;  ...start P.C.
	ADDL3	STAPC,(R1),ENDPC	;  ...end   P.C.
	CMPL	STAPC,@4.(AP)		; Psect start above P.C.?
	BGTRU	10$			;  ...yes, not here
	CMPL	@4.(AP),ENDPC		; P.C. above psect end?
	BGEQU	10$			;  ...yes, not found
;
	MOVW	MACRO,LANG		; Else show language
	MOVC3	MACRO,MACRO+2,LANG+2	;  ...as macro
	MOVW	MODULE,ROUTIN		; Set routine name to
	MOVC3	MODULE,MODULE+2,ROUTIN+2;  ...module  name
	BRW	GOT_IT			;  ...and continue
;
10$:	BRW	SNEXT			; Not in this psect
;
.PSECT	$LOCAL,PIC,CON,REL,LCL,NOSHR,NOEXE,RD,WRT,LONG
;
FAB:	$FAB	FOP=SQO,MRS=BLOK,RFM=FIX,XAB=XAB
RAB:	$RAB	FAB=FAB,USZ=BLOK
XAB:	$XABFHC
;
ITMLST:	.word	BLOK,JPI$_IMAGNAME
	.long	0
	.address IFNS
	.long	0
;
LININC:	.long	1			; Line increment
LINENO:	.long	0			; Line number
;
CURPC:	.long	0			; Current P.C.
ENDPC:	.long	0			; End     P.C.
STAPC:	.long	0			; Start   P.C.
;
ASTS:	.long	0			; Saved ast status
BUFF:	.long	0			; Address of symbol table
EFN:	.long	0			; Local event flag number
IFNS:	.long	0			; Size of image file name
LANG:	.blkb	SIZE+2			; Language name
MEM:	.long	0			; Bytes to allocate
MODULE:	.blkb	SIZE+2			; Name of module
REOB:	.long	0			; End of image binary
REOS:	.long	0			; End of debug symbol table
ROUTIN:	.blkb	SIZE+2			; Name of procedure
STKD:	.long	0			; Stack depth
;
.PSECT	$PDATA,PIC,CON,REL,LCL,SHR,NOEXE,RD,NOWRT,LONG
;
; All language 'known' are listed in following table by order of 'code'
;
LNGTBL:	.=LNGTBL
	.blkl	DST$K_MACRO
	.address	MACRO		; 0 ==> macro
;
	.=LNGTBL
	.blkl	DST$K_FORTRAN
	.address	FORTRA		; 1 ==> fortran
;
	.=LNGTBL
	.blkl	DST$K_BLISS
	.address	BLISS		; 2 ==> bliss
;
	.=LNGTBL
	.blkl	DST$K_COBOL
	.address	COBOL		; 3 ==> cobol
;
	.=LNGTBL
	.blkl	DST$K_BASIC
	.address	BASIC		; 4 ==> basic
;
	.=LNGTBL
	.blkl	DST$K_PL1
	.address	PL1		; 5 ==> PL/1
;
	.=LNGTBL
	.blkl	DST$K_PASCAL
	.address	PASCAL		; 6 ==> Pascal
;
	.=LNGTBL
	.blkl	DST$K_C
	.address	C		; 7 ==> C
;
	.=LNGTBL
	.blkl	DST$K_RPG
	.address	RPG		; 8 ==> RPG
;
	.=LNGTBL
	.blkl	DST$K_ADA
	.address	ADA		; 9 ==> ADA
;
	.=LNGTBL
	.blkl	DST$K_UNKNOWN
	.address	UNKNOW		; 10 ==> Unknown
;
	.=LNGTBL
	.blkl	DST$K_MAX_LANGUAGE+1.
;
ADA:	.CSTR	/Ada/
BASIC:	.CSTR	/Basic/
BLISS:	.CSTR	/Bliss/
C:	.CSTR	/C/
COBOL:	.CSTR	/Cobol/
FORTRA:	.CSTR	/Fortran/
MACRO:	.CSTR	/Macro/
PASCAL:	.CSTR	/Pascal/
PL1:	.CSTR	%PL/1%
RPG:	.CSTR	/Rpg/
UNKNOW:	.CSTR	/??????/
;
.End
