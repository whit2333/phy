	SUBROUTINE L$OAD(FILNAM,CODE,*)
C======================================================================C
C
C.		Subroutine to load core image files dynamically at
C.	run time.  The core image file will be loaded into the
C.	holding array CODE at the next page boundary.
C.
C.		A core image file is created by linking object modules
C.	with the DCL command:
C.
C.			$ LINK obj1,obj2,obj3,...objn/SYSTEM
C.
C.		The image file thus created is called a SYSTEM image,
C.	and does not contain the usual task header and traceback 
C.	information.  Note that the modules in the image are ordered
C.	ALPHABETICALLY.  Because function EXE_CUT transfers control to
C.	the START of the system image, the first module executed will
C.	be the first in ALPHABETIC order, which may NOT be the same
C.	as the order you specified to the linker...
C.
C.		If the FILNAM specified includes the switch '/DELETE',
C.	then the image file will be deleted after it has been loaded.
C.
C.		Compile this subroutine with the /D_LINES switch to
C.	get the load statistics printed.
C.
C.		T. Miles, TRIUMF, 19-Mar-1982
C.
C======================================================================C

C--->	Specification Statements
	IMPLICIT    INTEGER*4 (A-Z)
	PARAMETER   L   =128	   ! Record  Size in Longwords
	PARAMETER   LUN = 98       ! Logical Unit for File I/O
	BYTE        CODE      ( * )
        BYTE        UNIT_BYTE (4*L)
	CHARACTER   FILNAM *  ( * )
	INTEGER*4   QUAD_BYTE ( L )
	COMMON      /BYTES_USED/    BYTES_USED
	EQUIVALENCE (QUAD_BYTE,UNIT_BYTE)

C--->	Procedure begins...

	BYTES_USED    = - %LOC(CODE) .AND. '01FF'X ! Align Page...
	SIZE          = + INDEX(FILNAM,'/D')-1
	IF (SIZE .GT. 0)  GOTO 50
D	SIZE          = + LEN(FILNAM)

C--	Permanent File
	OPEN (UNIT=LUN,NAME=FILNAM,FORM='UNFORMATTED',RECL=L,
     1	      TYPE='OLD',READONLY,RECORDTYPE='FIXED',ERR=999)
	GOTO 100

C--	Temporary File
50	OPEN (UNIT=LUN,NAME=FILNAM(1:SIZE),FORM='UNFORMATTED',RECL=L,
     1	      TYPE='OLD',DISP='DELETE',RECORDTYPE='FIXED',ERR=999)

C--	Read loop
100	READ (LUN,END=200,ERR=998) (QUAD_BYTE(LWORD),LWORD=1,L)
	DO 150 BYTE=1,4*L
150	CODE(BYTE+BYTES_USED)=UNIT_BYTE(BYTE)
	BYTES_USED=BYTES_USED+4*L
	GO TO 100

C--	Normal Exit
200	CLOSE(UNIT=LUN)
D	TYPE 300, FILNAM(1:SIZE), BYTES_USED, %LOC(CODE)
D300	FORMAT('  --> File "',A,'" loaded ',Z8.8,' bytes into ',Z8.8)
	RETURN

C--	Error during Read
998	CLOSE(UNIT=LUN)

C.	Error during Open
999	RETURN 1
	END
