.Title	LOK_VM_P
.Ident	/X01.05/
; LOK_VM_P - Procedures to allocate/protect virtual memory by pages...
;
;	Istat = LOK_FREE_VM_P(Ibytes,Loc)		- Releases  'Ibytes'
;
;	Istat = LOK_GET_VM_P(Ibytes,Loc)		- Allocates 'Ibytes'
;
;	Istat = LOK_LOCK_VM_P(Ibytes,Loc)		- Protects  'Ibytes'
;
;	Istat = LOK_UNLOCK_VM_P(Ibytes,Loc)		- Unprotects'Ibytes'
;
; Author:
;
;	T. Miles, TRIUMF
;	 4004 Wesbrook Mall
;	  Vancouver, B.C.
;	   CANADA, V6T 2A3
;
;	(604) 228-4711
;
; External procedures:
;
;	LIB$SIG_TO_RET	- Procedure converts exception to return code
;
; Restrictions:
;
; (1)	Address 'Loc' must be page aligned
;
; Modifications:
;
;	Who		When		What
;	---		----		----
;	T. Miles	12-Aug-1983	Original
;
;	T. Miles	12-Aug-1983	Added LIB$LOCK_VM and LIB$UNLOCK_VM
;					to control write access to region
;
;	T. Miles	14-Aug-1983	Force page alignment
;
;	T. Miles	29-Aug-1983	Rename procedures to agree with
;					new TRIUMF naming convention
;
; Define access modes
;
$PSLDEF
;
; Define fields in Call Frame
;
$SFDEF
;
BLOK	=^X0200				; Bytes per page
;
.PSECT	$CODE,PIC,CON,REL,LCL,SHR,EXE,RD,NOWRT,LONG
;
; LOK_GET_VM_P - Procedure allocates page-aligned virtual memory from P0 space
;
.ENTRY	LOK_GET_VM_P,^M<>
	MOVAL	G^LIB$SIG_TO_RET,SF$A_HANDLER(FP)
	MOVAQ	-(SP),R0		; R0 --> Storage
	ADDL3	#BLOK-1,@4(AP),R1	; R1  =  Bytes to add
	DIVL	#BLOK,R1		;   ...pages to add
	$EXPREG_S PAGCNT=R1,RETADR=(R0),ACMODE=#PSL$C_USER,REGION=#0
	MOVL	(SP),@8.(AP)		; Return start address
	RET
;
; LOK_FREE_VM_P - Procedure frees page-aligned virtual memory
;
.ENTRY	LOK_FREE_VM_P,^M<>
	JSB	SETUP			; Setup for call
	$DELTVA_S INADR=(R0),ACMODE=#PSL$C_USER
	RET
;
; LOK_LOCK_VM_P - Procedure write-protects page-aligned virtual memory
;
.ENTRY	LOK_LOCK_VM_P,^M<>
	JSB	SETUP			; Setup for call
	$SETPRT_S INADR=(R0),ACMODE=#PSL$C_USER,PROT=#PRT$C_UR
	RET
;
; LOK_UNLOCK_VM_P - Procedure write-enables page-aligned virtual memory
;
.ENTRY	LOK_UNLOCK_VM_P,^M<>
	JSB	SETUP			; Setup for call
	$SETPRT_S INADR=(R0),ACMODE=#PSL$C_USER,PROT=#PRT$C_UW
	RET
;
; SETUP - Routine checks page alignment and builds address quadword in R0
;
SETUP:	MOVAL	G^LIB$SIG_TO_RET,-	; Install condition
		SF$A_HANDLER(FP)	;  ...handler
	MOVL	(SP)+,R1		; R1 = Return Address
	BITL	#BLOK-1,@8.(AP)		; Check page alignment
	BNEQ	10$			;  ...bad alignment
	MOVAQ	-(SP),R0		; R0 --> Storage
	MOVL	@8.(AP),(R0)		; Install start address
	ADDL3	@4(AP),(R0),4(R0)	;  ...end address
	DECL	4(R0)			;  ...(minus one)
	JMP	(R1)			;  ...and return
;
10$:	MOVL	#SS$_BUFNOTALIGN,R0	; Buffer not page aligned
	RET				;  ...back to caller
;
.End
