 /* Modified July 1991 by F.Jones: zoom window code revised    */
 /* and expanded.  Support added for graphics input, odometer, */
 /* long crosshairs, and control by applications.              */
 
 /* Modified 20-Nov-91 by FWJ: now calls xvst_store for vector replay */
 
 /* Modified 15-APR-93 by FWJ: general review of all routines    */
 /* to rationalize the naming and scoping of variables and       */
 /* eliminate redundant code.                                    */
 
 /* Modified 31-MAY-94 by FWJ: check for nlines corrected in     */
 /* xvst_flush_lines.                                            */
 
 /* Modified 16-Sep-97 by Joe Chuma: reset alarm to 100000 seconds */
 /* in xvst_flush_lines_ and xvst_flush_points_ so graphics would  */
 /* not hang up for DigitalUnix 4.0, SGI, HP-UX, Solaris, LINUX    */
 /* The user must reset the alarm to 1 second in a calling program */
 /* when graphics is finished (can CALL SIG_ON in FORTRAN)         */
 /* but only if X graphics (imonitor=18)                           */
 
#include <stdio.h>
#include <X11/Xlib.h>
#include <signal.h>
 
#ifdef VMS
#define xvst_zoom_window_ xvst_zoom_window
#define xvst_rep_ xvst_rep
#define xvst_plot_clear_ xvst_plot_clear
#define xvst_plot_flush_ xvst_plot_flush
#define xvst_plot_mode_ xvst_plot_mode
#define xvst_plot_ xvst_plot
#endif
 
 /*      GLOBAL VARIABLES    */
 
 Display *dpy;
 Window g_window, z_window;
 GC ggc, zgc; 
 XPoint glines[500], gpoints[500];
 XPoint zlines[500], zpoints[500];
 unsigned long gcfore, gcback;
 int gheight, gwidth;
 int nlines, npoints;
 Bool replay_zoom, zoom, zoom_box;
 Bool fsc_exists;
 float ratio;
 float scalx,scaly;
 int offset_x, offset_y;

 /* COMMON BLOCK FOR ZOOM WINDOW */
 struct {Bool zopen,zact; float zw,zh,zx1,zx2,zy1,zy2;} xvst_zoom_window_;
 
 struct {int istore;} xvst_rep_;
 
 /*************************************************************************
  *
  *   CLEAR THE GRAPHICS WINDOW AND INITIALIZE THE LENGTH OF DYNAMIC
  *   MEMORY, LENGTH OF MEMORY USED, AND ISTORE.
  *   ISTORE=0 ==>  DYNAMIC MEMORY EXHAUSTED, DONT STORE GRAPHICS 
  *
  *************************************************************************/
 
 void xvst_plot_clear_() 
  {
    if (dpy != NULL) {
      XClearWindow(dpy, g_window);
      if (zoom) XClearWindow(dpy, z_window);
      XFlush(dpy);
      zoom_box = False;
      fsc_exists = False;
    }
    else
      xvst_setup_(0);
    
    /* INITIALIZE BUFFERS */
    nlines = 0;
    npoints = 0;
    
    XFlush(dpy);
    
    /* RESET REPLAY MEMORY */
    xvst_store_reset_();
  }
 

/*****************************
*  FLUSHES THE LINE BUFFER  
******************************/
void xvst_flush_lines_()
{
   int ix,iy;

   if (nlines < 2) return;

#ifdef VMS
#else
#ifndef sgi
#ifndef g77
#ifndef __hpux
#ifndef __SVR4
#ifndef _AIX
   alarm(100000);
#endif
#endif
#endif
#endif
#endif
#endif
   
   if (replay_zoom)
      XDrawLines(dpy, z_window, zgc, zlines, nlines, CoordModeOrigin);
   else {
      XDrawLines(dpy, g_window, ggc, glines, nlines, CoordModeOrigin);
      if (zoom) XDrawLines(dpy,z_window,zgc,zlines,nlines,CoordModeOrigin);
   }
/* XDrawLines(dpy, pixmap, pgc, glines, nlines, CoordModeOrigin); */

/* Following is a work-around for problem that for linewidth=0 nothing
 * is drawn if polyline occupies only one pixel (FWJ 26-APR-91) */
   if(!replay_zoom && glines[0].x==glines[nlines-1].x &&
      glines[0].y==glines[nlines-1].y) {
      ix = glines[0].x;
      iy = glines[0].y;
      XDrawPoint(dpy,g_window,ggc,ix,iy);
   }
   if(zoom && zlines[0].x==zlines[nlines-1].x &&
      zlines[0].y==zlines[nlines-1].y) {
      ix = zlines[0].x;
      iy = zlines[0].y;
      XDrawPoint(dpy,z_window,zgc,ix,iy);
   }

   glines[0].x = glines[nlines-1].x;
   glines[0].y = glines[nlines-1].y;
   if (zoom) {
      zlines[0].x = zlines[nlines-1].x;
      zlines[0].y = zlines[nlines-1].y;
   }
   nlines = 1;

   XFlush(dpy);
}


/******************************
*   FLUSHES THE POINT BUFFER  
*******************************/
void xvst_flush_points_()  
{
   if (npoints < 1) return;

#ifdef VMS
#else
#ifndef sgi
#ifndef g77
#ifndef __hpux
#ifndef __SVR4
#ifndef _AIX
   alarm(100000);
#endif
#endif
#endif
#endif
#endif
#endif

   if (replay_zoom)
      XDrawPoints(dpy, z_window, zgc, zpoints, npoints, CoordModeOrigin);
   else {
     XDrawPoints(dpy, g_window, ggc, gpoints, npoints, CoordModeOrigin);
     if(zoom)XDrawPoints(dpy,z_window,zgc,zpoints,npoints,CoordModeOrigin);
   }

/* XDrawPoints(dpy, pixmap, pgc, gpoints, npoints, CoordModeOrigin); */
 
   glines[0].x = gpoints[npoints-1].x;
   glines[0].y = gpoints[npoints-1].y;
   if (zoom) {
      zlines[0].x = zpoints[npoints-1].x;
      zlines[0].y = zpoints[npoints-1].y;
   }
   nlines = 1;
   npoints = 0;

   XFlush(dpy);
}


/*****************************************
*   FLUSHES THE POINT AND LINE BUFFERS  
******************************************/
void xvst_plot_flush_()
{
   xvst_flush_lines_();
   xvst_flush_points_();
}


/*******************************
*  SETS THE PLOTTING MODE  
********************************/
void xvst_plot_mode_(imode)
   int *imode;
{
   int icode = 2;

   if (xvst_rep_.istore == 1) xvst_store_(&icode, imode);

   if (*imode == 0) {
      XSetForeground(dpy, ggc, gcfore);
      if (zoom) XSetForeground(dpy, zgc, gcfore);
/*    XSetForeground(dpy, pgc, gcfore); */
   }
   else if (*imode == 1) {
      XSetForeground(dpy, ggc, gcback);
      if (zoom) XSetForeground(dpy, zgc, gcback);
/*    XSetForeground(dpy, pgc, gcback); */
   }
   XFlush(dpy);
}

/**********************************************************************
* xvst_plot_
*   PLOTS THE GRAPHICS AND STORES IT IN DYNAMIC MEMORY.
*   (EXECUTES MOVE (IPEN=3); DRAW (IPEN=2); PLOT POINT (IPEN=20)
***********************************************************************/

void xvst_plot_(x, y, ipen)
   int *ipen;
   float *x, *y;
{
   int k;
   int icode = 1;

   if (xvst_rep_.istore == 1) xvst_store_(&icode,x,y,ipen);

   if (nlines >= 299) xvst_flush_lines_();
   if (npoints >= 299) xvst_flush_points_();

   if (*ipen == 2) {
      if (npoints >= 1) xvst_flush_points_();
      nlines++;
      glines[nlines-1].x = (*x)*gwidth + 0.5;
      glines[nlines-1].y = (1-(*y)/0.75)*gheight + 0.5;
      if (zoom) {
         zlines[nlines-1].x = ((*x)*gwidth - offset_x)/scalx + 0.5;
         zlines[nlines-1].y = ((1.-(*y)/0.75)*gheight - offset_y)/scaly + 0.5;
      }
   }
   else if (*ipen == 3) {
      if (npoints >= 1) xvst_flush_points_();
      if (nlines >= 2) xvst_flush_lines_();
      nlines = 1;
      glines[nlines-1].x = (*x)*gwidth + 0.5;
      glines[nlines-1].y = (1-(*y)/0.75)*gheight + 0.5;
      if (zoom) {
         zlines[nlines-1].x = ((*x)*gwidth - offset_x)/scalx + 0.5;
         zlines[nlines-1].y = ((1.-(*y)/0.75)*gheight - offset_y)/scaly +0.5;
      }
   }
   else if (*ipen == 20) {
      if (nlines >= 2) xvst_flush_lines_();
      npoints = npoints + 1;
      gpoints[npoints-1].x = (*x)*gwidth + 0.5;
      gpoints[npoints-1].y = (1-(*y)/0.75)*gheight + 0.5;
      if (zoom) {
         zpoints[npoints-1].x = ((*x)*gwidth - offset_x)/scalx + 0.5;
         zpoints[npoints-1].y = ((1-(*y)/0.75)*gheight - offset_y)/scaly + 0.5;
      }
   }
}
