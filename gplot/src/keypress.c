/* Modified July 1991 by F.Jones: zoom window code revised     */
/* and expanded.  Support added for graphics input, odometer,  */
/* long crosshairs, and control by applications.               */
/* Modified 5-Dec-91 by FWJ: added sanity checks to prevent    */
/* divide-by-zero errors on spurious input events.             */
/* Modified 9-Nov-92 by FWJ: cleaned up do_ConfigureNotify,    */
/* flag "resized" eliminated.                                  */
/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/cursorfont.h>
#include <X11/Xutil.h>

#ifdef VMS
#define xvst_zoom_window_ xvst_zoom_window
#define cursor_readout_ cursor_readout
#define xvst_var_ xvst_var
#define xvst_rep_ xvst_rep
#endif

#define Max(a,b)  ((a) > (b) ? (a) : (b))
#define Min(a,b)  ((a) < (b) ? (a) : (b))
#define Abs(a)    ((a) <  0  ? -(a) : (a))

/*   GLOBAL VARIABLES   */

Display *dpy;
Screen *screen;
Window g_window, c_window, z_window;
GC fgc, ggc, cgc, zgc;
Bool creadout, fsc, button1,
     zoom, zoom_box, replay_zoom, fsc_exists;
int gwidth, gheight, cx, cy, zcx, zcy;
int event_mask;
int zwidth, zheight;
int corner1_x, corner1_y, corner2_x, corner2_y;
int drag1_x, drag2_x, drag1_y, drag2_y;
float ratio;
float scalx, scaly;
int offset_x, offset_y;

/* COMMON BLOCK FOR ZOOM WINDOW */
struct {Bool zopen,zact; float zw,zh,zx1,zx2,zy1,zy2;} xvst_zoom_window_;
/* COMMON BLOCK FOR ODOMETER */
struct { float xcroff; float xcrscale; float ycroff; float ycrscale;
           float ycof; float xcof; } cursor_readout_;
/* COMMON BLOCK - VARIOUS */
struct { int curheight; int curwidth; } xvst_var_;

struct { int istore; } xvst_rep_;

/*************************************************************************
*  do_KeyPress
*
*    THIS ROUTINE HANDLES ALL THE SINGLE KEYSTROKE INPUTS IN THE 
*    GRAPHICS WINDOW.  THE KEYSYM IS RETURNED BY XLOOKUPSTRING, WHICH
*    IS THEN TRANSLATED TO THE CORRESPONDING ASCII VALUE.  THIS IS THE
*    VALUE WHICH IS RETURNED.  SPECIAL CODES ARE GIVEN TO THE ARROW KEYS
*    SINCE THEY ARE NOT ASCII CHARACTERS.  THE LOCATION OF THE POINTER 
*    AT THE TIME WHICH THE KEY WAS PRESSED IS ALSO RETURNED.  THE KEY 
*    CAN ONLY BE ENTERED IF THE POINTER IS IN THE GRAPHICS WINDOW.
*    OTHERWISE IT BEEPS THE USER TO NOTIFY THEM OF AN INVALID ENTRY.
*
***************************************************************************/

do_KeyPress (eventp, code, done, x, y)
   XEvent *eventp;
   int *code, *done;
   float *x, *y;
{
   XKeyEvent *e = (XKeyEvent *) eventp;
   KeySym ks;
   char *ksname;
   int nbytes;
   int keycode;
   char str[256+1];
   char ch;
   float fx, fy, fwidth, fheight;
   int win_x, win_y;

   nbytes = XLookupString (e, str, 256, &ks, NULL);
   if (ks == NoSymbol)
      ksname = "NoSymbol";
   else if (!(ksname = XKeysymToString(ks)))
      ksname = "(no name)";

/* printf ("    root 0x%lx, subw 0x%lx, time %lu, (%d,%d), root:(%d,%d),\n",
*      e->root, e->subwindow, e->time, e->x, e->y, e->x_root, e->y_root);
*  printf ("    state 0x%x, keycode %u (keysym 0x%x, %s), same_screen %s,\n",
*      e->state, e->keycode, ks, ksname, e->same_screen ? Yes : No);
*/
   if (nbytes < 0) nbytes = 0;
   if (nbytes > 256) nbytes = 256;
   str[nbytes] = '\0';

/****************************************
*     GET THE CODE OF THE KEY PRESSED  
*****************************************/
   ch = toupper(str[0]);
   *code = toascii(ch);

/******************************************
*     GET THE LOCATION OF THE CURSOR  
*******************************************/
   win_x = e->x;
   win_y = e->y;

/***************************************
*     ONE OF THE ARROW KEYS WAS HIT  
****************************************/
   if (ks == XK_Up) *code = 94;
   if (ks == XK_Down) *code = 118;
   if (ks == XK_Left) *code = 60;
   if (ks == XK_Right) *code = 62;

/***********************************************************************
*  CHECK THAT KEY IS VALID AND CALCULATE THE POSITION OF THE POINTER
************************************************************************/
   if (ks == XK_Return || ks == XK_Escape) {
      XBell(dpy, 0);
      printf("Invalid key\n");
      *done = False;
   }

/* Ignore shift keys */
   else if(ks==XK_Shift_L||ks==XK_Shift_R)
      *done = False;

/* INPUT IN MAIN WINDOW */
   else if (e->window == g_window && win_x >= 0 && win_x <= gwidth && 
            win_y >= 0 && win_y <= gheight) {
      *done = True;
      fx = cx = win_x;
      fy = cy = win_y;
      fwidth = gwidth;
      fheight = gheight;
      *x = fx/fwidth;
      *y = 0.75*(fheight-fy)/fheight;
/* SET INPUT WINDOW INDICATOR */
      xvst_zoom_window_.zact = False;
   }

/* INPUT IN ZOOM WINDOW */
   else if (e->window == z_window && win_x >= 0 && win_x <= zwidth && 
            win_y >= 0 && win_y <= zheight) {
      *done = True;
      fx = win_x;
      fy = win_y;
      fwidth = gwidth;
      fheight = gheight;
      *x = (fx*scalx + (float)offset_x)/fwidth;
      *y = 0.75*(fheight - fy*scaly - (float)offset_y)/fheight;
/* SET INPUT WINDOW INDICATOR */
      xvst_zoom_window_.zact = True;
   }

/* INPUT IS OUTSIDE ACTIVE WINDOW */
   else {
      XBell(dpy, 0);
      printf("Pointer must be in active graphics or zoom window\n");
   }

   XFlush(dpy);
   return;
}


/***********************************************************************
*  do_ButtonPress
*
*    THE FIRST MOUSE BUTTON IS RECOGNIZED AS INPUT FOR THE LOCATION
*    OF TEXT, DURING THE CROSSHAIR MODE.  THE SECOND BUTTON IS 
*    RECOGNIZED AS A TOGGLE FOR THE ODOMETER WINDOW.  THE FULL SCREEN
*    CURSOR NEED NOT BE ACTIVE TO HAVE THE ODOMETER ACTIVE.  THE THIRD
*    BUTTON TOGGLES THE FULL SCREEN CROSSHAIRS.  THE POINTERMOTION
*    MASK IS ALSO TOGGLED ON AND OFF: WHEN EITHER THE ODOMETER OR THE
*    FULL SCREEN CROSSHAIRS ARE ON OR BOTH OFF.
*
************************************************************************/

do_ButtonPress (eventp, code, done, x, y)
   XEvent *eventp;
   int *code, *done;
   float *x, *y;
{
   XButtonEvent *e = (XButtonEvent *) eventp;
   float fx, fy, fwidth, fheight;
   unsigned int mask;
   Cursor cursor;

/* printf ("    root 0x%lx, subw 0x%lx, time %lu, (%d,%d), root:(%d,%d),\n",
*  e->root, e->subwindow, e->time, e->x, e->y, e->x_root, e->y_root);
*  printf ("    state 0x%x, button %d, same_screen %s\n",
*  e->state, e->button, e->same_screen ? Yes : No);
*/

/************************************************************************* 
*  BUTTON 1:
*    BUTTON 1 WILL ALSO BE USED FOR RUBBER-BANDING THE ZOOM BOX, IN 
*    ADDITION TO JUST ENTERING POINTS.
**************************************************************************/
   if (e->button == 1) {

/* IF IN ZOOM WINDOW, IGNORE PRESS OF BUTTON 1 */
      if (e->window == z_window) return;

/* DONE WITH FSC: ERASE IT */  
      if (fsc) {
         xvst_draw_fsc();
         fsc_exists = False;
      }
      else { 
         cx = e->x;
         cy = e->y;
      }   

/**********************************************************************
*   BUTTON1 SIGNALS TO THE MOTIONNOTIFY ROUTINE WHETHER TO DRAW THE 
*   ZOOM BOX OR NOT.  IT INITIALIZES THE TWO CORNERS TO BE THE SAME POINT.
***********************************************************************/
      button1 = True;
      drag1_x = e->x;
      drag1_y = e->y;
      drag2_x = e->x;
      drag2_y = e->y;

/***********************************************************************
*   GRAB THE POINTER AS SOON AS THE FIRST BUTTON IS PRESSED, SINCE THE
*   WINDOW MANAGER GRABS THE POINTER (IF BOTH THE BUTTON PRESS MASK AND
*   BUTTON RELEASE MASK ARE ON), AND WON'T SEND ANY MOTION EVENTS UNTIL
*   THE BUTTON IS RELEASED.  GRABBING THE POINTER ENSURES THAT MOTION
*   EVENTS ARE STILL SENT WHEN THE BUTTON IS PRESSED.  THE POINTER 
*   MUST BE UNGRABBED WHEN THE BUTTON IS RELEASED.
************************************************************************/ 
      mask = ButtonPressMask | ButtonReleaseMask | PointerMotionMask;
      cursor = XCreateFontCursor(dpy, XC_sizing);
      XGrabPointer(dpy, g_window, True, mask, GrabModeAsync,
                    GrabModeAsync, g_window, cursor, CurrentTime);

      xvst_draw_zb_(dpy, screen, g_window, fgc, drag1_x, drag1_y,
                    drag2_x, drag2_y);
   }

/*******************************
*    BUTTON 2: toggle odometer
*******************************/

   else if (e->button == 2) {      /*  TOGGLE ODOMETER  */
      if (creadout) {
         creadout = False;
         XUnmapWindow(dpy, c_window);
         if (!fsc) {
            event_mask = event_mask & (~PointerMotionMask);
            XSelectInput(dpy, g_window, event_mask);
            XSelectInput(dpy, z_window, event_mask);
         }
      }  
      else {
         creadout = True;
         XMapRaised(dpy, c_window);
         event_mask = event_mask | PointerMotionMask;
         XSelectInput(dpy, g_window, event_mask);  
         XSelectInput(dpy, z_window, event_mask);  
      }
   } 

/******************************
*   BUTTON 3:  toggle FSC
*******************************/
    
   else if (e->button == 3) {
      if (fsc) {
         fsc = False;
/* ERASE THE FULL SCREEN CROSSHAIRS */
         xvst_draw_fsc();
         fsc_exists = False;
/* Disable motion event reporting unless cursor readout on */
         if (!creadout) event_mask = event_mask & (~PointerMotionMask);
      }  
      else {
         fsc = True;
         cx = e->x;
         cy = e->y;
         if (zoom) {
            zcx = (cx - offset_x)/scalx + 0.5;
            zcy = (cy - offset_y)/scaly + 0.5;
         }
         xvst_draw_fsc();
         fsc_exists = True;
         event_mask = event_mask | PointerMotionMask;
      }
      XSelectInput(dpy, g_window, event_mask);
      XSelectInput(dpy, z_window, event_mask);
      XFlush(dpy);
   }
   return;
}

/*****************************************************************
*  do_ButtonRelease
*
*   THIS ROUTINE DETERMINES IF A ZOOM BOX SHOULD BE DRAWN AND MAPPED
*   OR WHETHER THIS WAS JUST A POINT ENTRY, IN WHICH CASE THE LOCATION 
*   WILL BE THE LOCATION OF THE BUTTON RELEASE.  IF A ZOOM BOX IS TO 
*   BE DRAWN, THE TWO CORNERS OF THE ZOOM BOX ARE TO BE CALCULATED
*   ACCORDING TO THE ASPECT RATIOS OF THE ZOOM WINDOW ITSELF.
*
******************************************************************/

do_ButtonRelease (eventp, code, done, x, y)
   XEvent *eventp;
   int *code, *done;
   float *x, *y;
{
   XButtonEvent *e = (XButtonEvent *) eventp;
   XEvent TheEvent;
   int w, h, win_x, win_y, win_w, win_h;
   float fx, fy, fwidth, fheight;
   float dx,dy;

   if (e->button == 1) {
/* HANDLE BUTTON RELEASE IN THE ZOOM WINDOW */
      if (e->window == z_window) {
         *done = True;
/* SET BUTTON CODE TO SPACE */
         *code = 32;
/* RETURN POINTER COORDINATES */
         fx = e->x;
         fy = e->y;
         fwidth = gwidth;
         fheight = gheight;
         *x = (fx*scalx + (float)offset_x)/fwidth;
         *y = 0.75*(fheight - fy*scaly - (float)offset_y)/fheight;
/* SET INPUT WINDOW INDICATOR */
         xvst_zoom_window_.zact = True;
         return;
      }

/*******************************************************************
*   THE VERY FIRST THING THIS ROUTINE MUST DO IS UNGRAB THE POINTER.
*   OTHERWISE SHOULD THE APPLICATION STOP WHILE THE POINTER IS STILL
*   GRABBED IN A WINDOW, ONE WOULD NOT BE ABLE TO DO ANYTHING SINCE 
*   THE POINTER IS TRAPPED INSIDE THAT WINDOW.  SHOULD YOU EVER GET
*   STUCK IN THIS SITUATION AND YOU ARE USING THE MOTIF WINDOW MANAGER,
*   PRESSING SHIFT AND ESC KEYS WOULD ALLOW YOU TO CLOSE THE WINDOW
*   AND THUS UNGRAB THE POINTER.
*********************************************************************/
      XUngrabPointer(dpy, CurrentTime);
/* Don't do anything else until the ungrab takes effect FWJ 25SEP92 */
      XSync(dpy, True);
      button1 = False;

/* DISABLE MOTION REPORTING IF NOT NEEDED */
      if (!creadout && !fsc) {
         event_mask = event_mask & (~PointerMotionMask);
         XSelectInput(dpy, g_window, event_mask);
         XSelectInput(dpy, z_window, event_mask);
      }

/* IF NOT DRAGGING A ZOOM BOX, ENTER A POINT... */

/* TRY LOOKING AT DRAG BOX COORDINATES (they are initially equal) */
      if (Abs(drag2_x - drag1_x)<10 || Abs(drag2_y - drag1_y)<10) {
         *done = True;
/* SET BUTTON CODE TO SPACE */
         *code = 32;
/* RETURN POINTER COORDINATES */
         *x = (float) e->x/(float) gwidth;
         *y = 0.75*(float) (gheight - e->y)/(float) gheight;
/* SET INPUT WINDOW INDICATOR */
         xvst_zoom_window_.zact = False;
         return;
      }

/* DRAGGED A ZOOM BOX... */

      *done = False;
      XFlush(dpy);

/* Erase old zoom box */
      if (zoom_box) xvst_draw_zb_(dpy,screen,g_window,fgc,corner1_x,corner1_y,
                                  corner2_x, corner2_y);
/* Update the parameters */
/*    printf("%d %d %d %d\n",drag1_x,drag1_y,drag2_x,drag2_y); */
      corner1_x = drag1_x;
      corner2_x = drag2_x;
      corner1_y = drag1_y;
      corner2_y = drag2_y;
      offset_x = Min(corner1_x, corner2_x);
      offset_y = Min(corner1_y, corner2_y);
      dx = Abs(corner2_x-corner1_x);
      dy = Abs(corner2_y-corner1_y);
      scalx = dx/(float)zwidth;
      scaly = dy/(float)zheight;

      if (!zoom) xvst_create_zoom_();
      zoom = True;
      zoom_box = True;

/* SET COMMON BLOCK VARIABLES */
      xvst_zoom_window_.zopen = True;
      xvst_zoom_window_.zw = zwidth;
      xvst_zoom_window_.zh = zheight;
/* Lower left zoom corner */
      xvst_zoom_window_.zx1 = Min(corner1_x,corner2_x);
      xvst_zoom_window_.zy1 = Max(corner1_y,corner2_y);
/* Upper right zoom corner */
      xvst_zoom_window_.zx2 = Max(corner1_x,corner2_x);
      xvst_zoom_window_.zy2 = Min(corner1_y,corner2_y);

/************************************************************************
*   REPLAY_ZOOM SIGNALS TO THE FLUSH ROUTINES WHETHER TO FLUSH INTO
*   THE MAIN GRAPHICS WINDOW OR THE ZOOM WINDOW.
*************************************************************************/
      replay_zoom = True;
      xvst_replay_(0);
      replay_zoom = False;
   }
}

/***********************************************************************
*  do_MotionNotify
*
*    THIS ROUTINE IS CALLED WHEN POINTERMOTION EVENTS ARE RECEIVED.
*    IT EITHER CALCULATES AND DISPLAYS THE LOCATION OF THE POINTER
*    IN THE ODOMETER WINDOW OR DRAWS THE FULL SCREEN CROSSHAIRS IN 
*    COMPLEMENTARY MODE.  THE SCALES USED ARE PROVIDED BY THE USER 
*    IN A COMMON BLOCK.  TO ACCESS THE COMMON BLOCK IN THE FORTRAN 
*    CODE, THE VARIABLES MUST BE STORED IN A STRUCTURE, WITH THE
*    STRUCTURE NAME THE SAME AS THE COMMON BLOCK NAME APPENDED WITH
*    AN UNDERSCORE !
*
************************************************************************/

do_MotionNotify(eventp)
   XEvent *eventp;
{
   XMotionEvent *e = (XMotionEvent *) eventp;
   float xx, yy, xuser, yuser;
   int newcx,newcy,newzcx,newzcy;
   static char banner[50];

   if (creadout || fsc) {
      if (e->window == g_window) {
         xx = (float)e->x/(float)gwidth;
         yy = 0.75*(float)(gheight - e->y)/(float)gheight;
         newcx = e->x;
         newcy = e->y;
         newzcx = (cx-offset_x)/scalx + 0.5;
         newzcy = (cy-offset_y)/scaly + 0.5;
      }
      else if (e->window == z_window) {
         xx = ((float)e->x*scalx + (float)offset_x)/(float)gwidth;
         yy = 0.75*((float)gheight - (float)e->y*scaly - (float)offset_y)/
	      (float)gheight;
         newcx = offset_x + scalx*e->x + 0.5;
         newcy = offset_y + scaly*e->y + 0.5;
         newzcx = e->x;
         newzcy = e->y;
      }
      xuser = xx * cursor_readout_.xcrscale + cursor_readout_.xcroff + 
              yy * cursor_readout_.ycof;
      yuser = yy * cursor_readout_.ycrscale + cursor_readout_.ycroff +
              xx * cursor_readout_.xcof;

/* DISPLAY CURSOR COORDINATES */
      sprintf(banner,"X = %-13.6g  Y = %-13.6g\0", xuser, yuser);
      XDrawImageString(dpy, c_window, cgc, 25, 25, banner,
                        strlen(banner));
      
/* ERASE LONG CROSSHAIRS IF PRESENT */
      if (fsc_exists) xvst_draw_fsc();

/* UPDATE CURSOR COORDINATES */
      cx = newcx;
      cy = newcy;
      zcx = newzcx;
      zcy = newzcy;

/* DRAW LONG CROSSHAIRS */
      if (fsc) {
         xvst_draw_fsc();
         fsc_exists = True;
      }
   }
   XFlush(dpy);
}


/*************************************************************************
*  do_zoom_MotionNotify
*   THIS ROUTINE REACTS TO MOTION  EVENTS WHEN BUTTON IS PRESSED DOWN.
*   IT CALCULATES THE TWO CORNERS OF THE ZOOM BOX ACCORDING TO THE
*   ASPECT RATIOS OF THE ZOOM WINDOW.  IT THEN DRAWS THE ZOOM BOX AND
*   ALSO CALCULATES THE MAGNIFICATION FACTOR WHICH IS CALLED RATIO.
**************************************************************************/

do_zoom_MotionNotify(eventp)
   XEvent *eventp;

{
   XMotionEvent *e = (XMotionEvent *) eventp;
   int w, h;
   float box_ratio;

   w = Abs(drag1_x - e->x);
   h = Abs(drag1_y - e->y);
   if (w < 10 || h < 10) return;

/* ERASE PREVIOUS DRAG BOX */
   xvst_draw_zb_(dpy, screen, g_window, fgc, drag1_x, drag1_y,
                 drag2_x, drag2_y);

   box_ratio = (float)zwidth/(float)zheight;

   if ((float)w/(float)h > box_ratio)
      h = w / box_ratio;
   else
      w = h * box_ratio;

   if (drag1_x < e->x)
      drag2_x = drag1_x + w;
   else
      drag2_x = drag1_x - w;

   if (drag1_y < e->y)
      drag2_y = drag1_y + h;
   else
      drag2_y = drag1_y - h;

/* DRAW NEW DRAG BOX */
   xvst_draw_zb_(dpy, screen, g_window, fgc, drag1_x, drag1_y,
                 drag2_x, drag2_y);
}


/**********************************************************************
*  do_ConfigureNotify
*
*    THIS ROUTINE IS CALLED TO REPLAY THE GRAPHICS WHEN A RESIZE
*    (CONFIGURENOTIFY) EVENT IS RECEIVED.  IT CAN BE CALLED IN
*    THE MAIN EVENT HANDLING LOOP, OR BY THE EXPOSE_ACTION ROUTINE
*    WHICH PERIODICALY (EVERY SECOND) CHECKS IF THERE IS A RESIZE.
*
***********************************************************************/

do_ConfigureNotify(eventp)
   XEvent *eventp;
{
   XConfigureEvent *e = (XConfigureEvent *) eventp;
   int oldw, oldh;
   float scalx_save,scaly_save;
   float dx,dy;

   oldw = gwidth;
   oldh = gheight;
   gwidth = e->width;
   gheight = e->height;
   xvst_var_.curwidth = gwidth;
   xvst_var_.curheight = gheight;

   if (oldw == gwidth && oldh == gheight) return;

   if (zoom) {
      corner1_x = corner1_x*(float)gwidth/(float)oldw;
      corner2_x = corner2_x*(float)gwidth/(float)oldw;
      corner1_y = corner1_y*(float)gheight/(float)oldh;
      corner2_y = corner2_y*(float)gheight/(float)oldh;
      offset_x = Min(corner1_x, corner2_x);
      offset_y = Min(corner1_y, corner2_y);
      scalx = scalx * (float)gwidth/(float)oldw;
      scaly = scaly * (float)gheight/(float)oldh;
/* SET COMMON BLOCK VARIABLES */
/* Lower left zoom corner */
      xvst_zoom_window_.zx1 = Min(corner1_x,corner2_x);
      xvst_zoom_window_.zy1 = Max(corner1_y,corner2_y);
/* Upper right zoom corner */
      xvst_zoom_window_.zx2 = Max(corner1_x,corner2_x);
      xvst_zoom_window_.zy2 = Min(corner1_y,corner2_y);
   }

   if (creadout) XRaiseWindow(dpy, c_window);

/* Erase fsc in zoom window to keep in sync with main window */
   if (fsc_exists) {
      XDrawLine(dpy, z_window, fgc, 0, zcy, zwidth, zcy );
      XDrawLine(dpy, z_window, fgc, zcx, 0, zcx, zheight);
      fsc_exists = False;
   }

/* Replay */
   if (xvst_rep_.istore) xvst_replay_(0);

/* Draw new zoom box if there was one before */
    if (zoom_box) xvst_draw_zb_(dpy,screen,g_window,fgc,corner1_x,
                                corner1_y, corner2_x, corner2_y);
    return;
}


/*******************************************************************
*  do_zoom_Configurenotify
*
*    THIS ROUTINE REACTS TO CONFIGURE NOTIFY EVENTS(RESIZE) IN 
*    THE ZOOM WINDOW.  IT REPLAYS THE SCALED GRAPHICS INTO THE ZOOM
*    WINDOW AND REFLECTS INTO THE MAIN GRAPHICS WINDOW THE NEW PORTION
*    OF IT WHICH IS DISPLAYED IN THE ZOOM WINDOW.
*
********************************************************************/

do_zoom_ConfigureNotify(eventp)  /*  REPLAY GRAPHICS  */
   XEvent *eventp;
{
   XConfigureEvent *e = (XConfigureEvent *) eventp;

   if (zwidth == e->width && zheight == e->height) return;
/* REJECT SPURIOUS CONFIGURENOTIFY EVENTS */
   if (e->width == 0 || e->height == 0) return;

/* ERASE OLD ZOOM BOX */
   if (zoom_box)
      xvst_draw_zb_(dpy, screen, g_window, fgc, corner1_x, corner1_y,
                     corner2_x, corner2_y);

   if (corner1_x < corner2_x)
      corner2_x = corner2_x + (e->width - zwidth)*scalx + 0.5;
   else
      corner1_x = corner1_x + (e->width - zwidth)*scalx + 0.5;
   if (corner1_y < corner2_y)
      corner2_y = corner2_y + (e->height - zheight)*scaly + 0.5;
   else
      corner1_y = corner1_y + (e->height - zheight)*scaly + 0.5;

   offset_x = Min(corner1_x, corner2_x);
   offset_y = Min(corner1_y, corner2_y);

/* DRAW NEW ZOOM BOX */
   if (zoom_box) xvst_draw_zb_(dpy, screen, g_window, fgc,
                               corner1_x, corner1_y, corner2_x, corner2_y);
   zwidth = e->width;
   zheight = e->height;

/* SET COMMON BLOCK VARIABLES */
   xvst_zoom_window_.zw = zwidth;
   xvst_zoom_window_.zh = zheight;
/* Lower left zoom corner */
   xvst_zoom_window_.zx1 = Min(corner1_x,corner2_x);
   xvst_zoom_window_.zy1 = Max(corner1_y,corner2_y);
/* Upper right zoom corner */
   xvst_zoom_window_.zx2 = Max(corner1_x,corner2_x);
   xvst_zoom_window_.zy2 = Min(corner1_y,corner2_y);

/* ERASE FSC IN MAIN WINDOW to keep in sync with zoom window */
   if (fsc_exists) {
      zoom = False;      /* Hide zoom window from draw_fsc */
      xvst_draw_fsc();
      fsc_exists = False;
      zoom = True;
   }

/*******************************************************************
*  REPLAY INTO THE ZOOM WINDOW AND NOT THE MAIN GRAPHICS WINDOW.
********************************************************************/
   replay_zoom = True;
   xvst_replay_(0);
   replay_zoom = False;
}
