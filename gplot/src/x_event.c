/* xvst_event                                     FWJ, 15-APR-93 */

/* This routine can be called by a Motif or other Toolkit or X   */
/* application that has its own event loop.  It processes events */
/* in the graphics window and zoom window, similarly to the      */
/* xvst_crosshair event loop.  For button 1 release and keypress */
/* events, itype is returned as 1 or 2, respectively, and the    */
/* user coordinates of the cursor are returned in xx and yy.     */
/* For keypress events, the ascii code is returned in key.  For  */
/* other events, itype is returned as 0.  Handling is provided   */
/* for long crosshairs, cursor position readout window, the      */
/* rubber-band zoom box and zoom window, and refreshing of the   */
/* window contents after a resize.                               */

#include <X11/Xlib.h>

#ifdef VMS
#define monitor_to_plot_ monitor_to_plot
#endif

Display *dpy;
Bool button1;
Window g_window, z_window;

/* COMMON/MONITOR_TO_PLOT/RMATRIXPM(2,2),SHIFTPM(2) */
struct {float rmatrixpm[2][2]; float shiftpm[2];} monitor_to_plot_;

void xvst_event(event, itype, key, xx, yy)
   XEvent *event;
   int *itype;
   char *key;
   float *xx, *yy;
{
   Bool done;
   int icode;
   float x, y;
   XEvent ahead;

   *itype = 0;
   done = False;

   switch (event->type) {

   case ButtonPress:
      do_ButtonPress(event, &icode, &done, &x, &y);
      return;
   case ButtonRelease:
      do_ButtonRelease(event, &icode, &done, &x, &y);
      if (done) {
         *itype = 1;
         *xx = monitor_to_plot_.rmatrixpm[0][0]*x +
               monitor_to_plot_.rmatrixpm[1][0]*y +
               monitor_to_plot_.shiftpm[0];
         *yy = monitor_to_plot_.rmatrixpm[0][1]*x +
               monitor_to_plot_.rmatrixpm[1][1]*y +
               monitor_to_plot_.shiftpm[1];
      }
      return;
   case KeyPress:
      do_KeyPress(event, &icode, &done, &x, &y);
      if (done) {
         *itype = 2;
         *key = icode;
         *xx = monitor_to_plot_.rmatrixpm[0][0]*x +
               monitor_to_plot_.rmatrixpm[1][0]*y +
               monitor_to_plot_.shiftpm[0];
         *yy = monitor_to_plot_.rmatrixpm[0][1]*x +
               monitor_to_plot_.rmatrixpm[1][1]*y +
               monitor_to_plot_.shiftpm[1];
      }
      return;
   case MotionNotify:
      while (XEventsQueued(dpy, QueuedAfterReading) > 0) {
         XPeekEvent(dpy, &ahead);
         if (ahead.type != MotionNotify) break;
         XNextEvent(dpy, event);
      }
      if (button1)
         do_zoom_MotionNotify(event);
      else
         do_MotionNotify(event);
      return;
   case ConfigureNotify:
      if (event->xconfigure.window == g_window)
         do_ConfigureNotify(event);
      else if (event->xconfigure.window == z_window)
         do_zoom_ConfigureNotify(event);
      return;

   } /* END SWITCH */

}
