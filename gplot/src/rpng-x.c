/*---------------------------------------------------------------------------
  
  Copyright (c) 1998-2000 Greg Roelofs.  All rights reserved.
  
  This software is provided "as is," without warranty of any kind,
  express or implied.  In no event shall the author or contributors
  be held liable for any damages arising in any way from the use of
  this software.
  
  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute
  it freely, subject to the following restrictions:
  
  1. Redistributions of source code must retain the above copyright
     notice, disclaimer, and this list of conditions.
  2. Redistributions in binary form must reproduce the above copyright
     notice, disclaimer, and this list of conditions in the documenta-
     tion and/or other materials provided with the distribution.
  3. All advertising materials mentioning features or use of this
     software must display the following acknowledgment:
     
     This product includes software developed by Greg Roelofs
     and contributors for the book, "PNG: The Definitive Guide,"
     published by O'Reilly and Associates.
     
 ---------------------------------------------------------------------------*/

#define LONGNAME "Physica PNG viewer"
#define PROGNAME "Physica"
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/keysym.h>
 
#ifndef TRUE
#  define TRUE 1
#  define FALSE 0
#endif
 
 /* prototypes for public functions in readpng.c */
 
 int readpng_init( FILE *infile, unsigned long *pWidth, unsigned long *pHeight );
 
 int readpng_get_bgcolor( unsigned char *bg_red, unsigned char *bg_green,
                          unsigned char *bg_blue);
 
 unsigned char *readpng_get_image( double display_exponent, int *pChannels,
                                   unsigned long *pRowbytes );
 
 void readpng_cleanup( int free_image_data );
 
 /*
   could just include png.h, but this macro is the only thing we need
   (name and typedefs changed to local versions); note that side effects
   only happen with alpha (which could easily be avoided with
   "unsigned short acopy = (alpha);")
 */

#define alpha_composite(composite, fg, alpha, bg) \
 { \
    unsigned short temp = ((unsigned short)(fg)*(unsigned short)(alpha) + \
    (unsigned short)(bg)*(unsigned short)(255 - (unsigned short)(alpha)) + \
    (unsigned short)128); \
    (composite) = (unsigned char)((temp + (temp >> 8)) >> 8); \
 }

 /* local prototypes */
 static int  rpng_x_create_window(void);
 int  rpng_x_display_image_();
 void rpng_x_cleanup_();
 static int  rpng_x_msb(unsigned long u32val);
 
 static char titlebar[1024], *window_name = titlebar;
 static char *appname = LONGNAME;
 static char *icon_name = PROGNAME;
 static FILE *infile;
 
 static char *bgstr;
 static unsigned char bg_red=0, bg_green=0, bg_blue=0;
 
 static double display_exponent;
 
 static unsigned long image_width, image_height, image_rowbytes;
 static int image_channels;
 static unsigned char *image_data;
 
 /* X-specific variables */
 Display *dpy;
 Window png_window;

 int png_width, png_height;
 
 static char *displayname;
 static XImage *ximage;
 static int depth;
 static Visual *visual;
 static XVisualInfo *visual_list;
 static int RShift, GShift, BShift;
 static unsigned long RMask, GMask, BMask;
 static GC gc;
 static Colormap colormap;
 
 static int have_nondefault_visual = FALSE;
 static int have_colormap = FALSE;
 static int have_window = FALSE;

 int rpng_main_( char * );
 
 int rpng_main_( char *filename )
  {
#ifdef sgi
    char tmpline[80];
#endif
    char *p;
    int rc, alen, flen;
    double LUT_exponent;               /* just the lookup table */
    double CRT_exponent = 2.2;         /* just the monitor */
    double default_display_exponent;   /* whole display system */
    XEvent e;
    KeySym k;
    displayname = (char *)NULL;
    /*
      First set the default value for our display-system exponent, i.e.,
      the product of the CRT exponent and the exponent corresponding to
      the frame-buffer's lookup table (LUT), if any.  This is not an
      exhaustive list of LUT values (e.g., OpenStep has a lot of weird
      ones), but it should cover 99% of the current possibilities.
    */
#if defined(sgi)
    LUT_exponent = 1.0 / 1.7;
    /*
      there doesn't seem to be any documented function to get the
      "gamma" value, so we do it the hard way
    */
    infile = fopen( "/etc/config/system.glGammaVal", "r" );
    if( infile )
    {
      double sgi_gamma;  
      fgets(tmpline, 80, infile);
      fclose(infile);
      sgi_gamma = atof(tmpline);
      if( sgi_gamma > 0.0 )LUT_exponent = 1.0 / sgi_gamma;
    }
#else
    LUT_exponent = 1.0;   /* assume no LUT:  most PCs */
#endif
    /*
      the defaults above give 1.0, 1.3, 1.5 and 2.2, respectively:
    */
    default_display_exponent = LUT_exponent * CRT_exponent;
    display_exponent = default_display_exponent;
    if( !(infile = fopen(filename, "rb")) )
    {
      fprintf( stderr, ": can't open PNG file [%s]\n", filename );
      return 1;
    }
    else
    {
      if( (rc = readpng_init( infile, &image_width, &image_height )) != 0 )
      {
        switch (rc)
        {
         case 1:
           fprintf( stderr, ": [%s] is not a PNG file: incorrect signature\n", filename );
           break;
         case 2:
           fprintf( stderr, ": [%s] has bad IHDR (libpng longjmp)\n", filename );
           break;
         case 4:
           fprintf( stderr, ": insufficient memory\n" );
           break;
         default:
           fprintf( stderr, ": unknown readpng_init() error\n" );
           break;
        }
        fclose( infile );
        return 1;
      }
    }
    /*
      set the titlebar string
    */
    alen = strlen(appname);
    flen = strlen(filename);
    if( alen+flen+3 > 1023 )
      sprintf( titlebar, "%s  ...%s", appname, filename+(alen+flen+6-1023) );
    else
      sprintf( titlebar, "%s:  %s", appname, filename );
    /*
      check for a background color in the PNG file --
      if not, the initialized values of 0 (black) will be used
    */
    if( readpng_get_bgcolor( &bg_red, &bg_green, &bg_blue ) > 1 )
    {
      readpng_cleanup( TRUE );
      fprintf( stderr, ":  libpng error while checking for background color\n" );
      return 2;
    }
    /*
      do the basic X initialization stuff, make the window and fill it
      with the background color
    */
    if( rpng_x_create_window() )return 2;
    /*
      decode the image, all at once
    */
    image_data = readpng_get_image( display_exponent, &image_channels, &image_rowbytes );
    /*
      done with PNG file, so clean up to minimize memory usage
      (but do NOT nuke image_data!)
    */
    readpng_cleanup( FALSE );
    fclose( infile );
    if( !image_data )
    {
      fprintf( stderr, ":  unable to decode PNG image\n" );
      return 3;
    }
    /*
      display image (composite with background if requested)
    */
    if( rpng_x_display_image_() )
    {
      free( image_data );
      return 4;
    }
    /*
      wait for the user to tell us when to quit
    */
    printf( "click right mouse button in window to refresh\n" );
    fflush( stdout );
    
    do
    {
      XNextEvent( dpy, &e );
    }
    while( !(e.type == ButtonPress && e.xbutton.button == Button3) );
    rpng_x_display_image_();
    XSync( dpy, True );
    
    /*
      OK, we're done:  clean up all image and X resources and go away
    */
    /* rpng_x_cleanup_(); */
    return 0;
  }
 
 static int rpng_x_create_window( void )
  {
    unsigned char *xdata;
    int need_colormap = FALSE;
    int screen, pad;
    unsigned long bg_pixel = 0L;
    unsigned long attrmask;
    Window root;
    XEvent e;
    XGCValues gcvalues;
    XSetWindowAttributes attr;
    XSizeHints *size_hints;
    XTextProperty windowName, *pWindowName = &windowName;
    XTextProperty iconName, *pIconName = &iconName;
    XVisualInfo visual_info;
    XWMHints *wm_hints;
    
    screen = DefaultScreen(dpy);
    depth = DisplayPlanes(dpy, screen);
    root = RootWindow(dpy, screen);
    
    if( depth != 16 && depth != 24 && depth != 32 )
    {
      int visuals_matched = 0;
      
      /* 24-bit first */
      visual_info.screen = screen;
      visual_info.depth = 24;
      visual_list = XGetVisualInfo( dpy, VisualScreenMask | VisualDepthMask,
                                    &visual_info, &visuals_matched );
      if( visuals_matched == 0 )
      {
        fprintf( stderr, "default screen depth %d not supported, and no"
                 " 24-bit visuals found\n", depth );
        return 2;
      }
      visual = visual_list[0].visual;
      depth = visual_list[0].depth;
      have_nondefault_visual = TRUE;
      need_colormap = TRUE;
    }
    else
    {
      XMatchVisualInfo( dpy, screen, depth, TrueColor, &visual_info );
      visual = visual_info.visual;
    }
    RMask = visual->red_mask;
    GMask = visual->green_mask;
    BMask = visual->blue_mask;
    if( depth == 8 || need_colormap )
    {
      colormap = XCreateColormap( dpy, root, visual, AllocNone );
      if( !colormap )
      {
        fprintf(stderr, "XCreateColormap() failed\n");
        return 2;
      }
      have_colormap = TRUE;
    }
    if( depth == 15 || depth == 16 )
    {
      RShift = 15 - rpng_x_msb(RMask);    /* these are right-shifts */
      GShift = 15 - rpng_x_msb(GMask);
      BShift = 15 - rpng_x_msb(BMask);
    }
    else if( depth > 16 )
    {
      RShift = rpng_x_msb(RMask) - 7;     /* these are left-shifts */
      GShift = rpng_x_msb(GMask) - 7;
      BShift = rpng_x_msb(BMask) - 7;
    }
    if( depth >= 15 && (RShift < 0 || GShift < 0 || BShift < 0) )
    {
      fprintf( stderr, "rpng internal logic error:  negative X shift(s)!\n" );
      return 2;
    }
    /*
      create the window.
    */
    attr.backing_store = Always;
    attr.event_mask = ExposureMask | KeyPressMask | ButtonPressMask;
    attrmask = CWBackingStore | CWEventMask;
    if( have_nondefault_visual )
    {
      attr.colormap = colormap;
      attr.background_pixel = 0;
      attr.border_pixel = 1;
      attrmask |= CWColormap | CWBackPixel | CWBorderPixel;
    }

    png_window = XCreateWindow( dpy, root, 0, 0, (unsigned int)image_width,
                                (unsigned int)image_height, 0,
                                depth, InputOutput, visual, attrmask, &attr );

    png_width = (int)image_width;
    png_height = (int)image_height;
    
    if( png_window == None )
    {
      fprintf( stderr, "XCreateWindow() failed\n" );
      return 2;
    }
    else
    {
      have_window = TRUE;
    }
    if( depth == 8 )XSetWindowColormap( dpy, png_window, colormap );
    if( !XStringListToTextProperty( &window_name, 1, pWindowName ) )pWindowName = NULL;
    if( !XStringListToTextProperty( &icon_name, 1, pIconName ) )pIconName = NULL;
    /*
      OK if either hints allocation fails; XSetWMProperties() allows NULL
    */
    if( (size_hints = XAllocSizeHints()) != NULL )
    {
      /* window will not be resizable */
      size_hints->flags = PMinSize | PMaxSize;

      size_hints->min_width = size_hints->max_width = (int)image_width;
      size_hints->min_height = size_hints->max_height = (int)image_height;
    }
    if( (wm_hints = XAllocWMHints()) != NULL )
    {
      wm_hints->initial_state = NormalState;
      wm_hints->input = True;
      wm_hints->flags = StateHint | InputHint  /* | IconPixmapHint */ ;
    }
    XSetWMProperties( dpy, png_window, pWindowName, pIconName, NULL, 0,
                      size_hints, wm_hints, NULL );
    XMapWindow( dpy, png_window );
    gc = XCreateGC( dpy, png_window, 0, &gcvalues );
    /*
      Fill window with the specified background color.
    */
    if( depth == 24 || depth == 32 )
    {
      bg_pixel = ((unsigned long)bg_red   << RShift) |
                 ((unsigned long)bg_green << GShift) |
                 ((unsigned long)bg_blue  << BShift);
    }
    else if( depth == 16 )
    {
      bg_pixel = ((((unsigned long)bg_red   << 8) >> RShift) & RMask) |
                 ((((unsigned long)bg_green << 8) >> GShift) & GMask) |
                 ((((unsigned long)bg_blue  << 8) >> BShift) & BMask);
    }
    XSetForeground( dpy, gc, bg_pixel );

    XFillRectangle( dpy, png_window, gc, 0, 0, (unsigned int)image_width,
                    (unsigned int)image_height );
    /*
      Wait for first Expose event to do any drawing, then flush.
    */
    do
    {
      XNextEvent( dpy, &e );
    }
    while( e.type != Expose || e.xexpose.count );
    XFlush( dpy );
    /*
      Allocate memory for the X- and display-specific version of the image.
    */
    if( depth == 24 || depth == 32 )
    {
      xdata = (unsigned char *)malloc(4*image_width*image_height);
      pad = 32;
    }
    else if( depth == 16 )
    {
      xdata = (unsigned char *)malloc(2*image_width*image_height);
      pad = 16;
    }
    if( !xdata )
    {
      fprintf( stderr, "unable to allocate image memory\n" );
      return 4;
    }
    ximage = XCreateImage( dpy, visual, depth, ZPixmap, 0,
                           (char *)xdata, (unsigned int)image_width,
                           (unsigned int)image_height, pad, 0 );
    if( !ximage )
    {
      fprintf( stderr, "XCreateImage() failed\n" );
      free( xdata );
      return 3;
    }
    /*
      to avoid testing the byte order every pixel (or doubling the size of
      the drawing routine with a giant if-test), we arbitrarily set the byte
      order to MSBFirst and let Xlib worry about inverting things on little-
      endian machines (like Linux/x86, old VAXen, etc.)--this is not the most
      efficient approach (the giant if-test would be better), but in the
      interest of clarity, we take the easy way out...
    */
    ximage->byte_order = MSBFirst;
    return 0;
  }
 
 int rpng_x_display_image_()
  {
    unsigned char *src;
    char *dest;
    unsigned char r, g, b, a;
    unsigned long i, row, lastrow = 0;
    unsigned long pixel;
    int ximage_rowbytes = ximage->bytes_per_line;
    unsigned long redl, greenl, bluel;
    unsigned short reds, greens, blues;
    if( depth == 24 || depth == 32 )
    {
      lastrow = 0;
      for( row = 0; row < image_height; ++row )
      {
        src = image_data + row*image_rowbytes;
        dest = ximage->data + row*ximage_rowbytes;
        if( image_channels == 3 )
        {
          for( i = image_width;  i > 0;  --i )
          {
            redl   = *src++;
            greenl = *src++;
            bluel  = *src++;
            pixel = (redl   << RShift) | (greenl << GShift) | (bluel  << BShift);
            *dest++ = (char)((pixel >> 24) & 0xff);
            *dest++ = (char)((pixel >> 16) & 0xff);
            *dest++ = (char)((pixel >>  8) & 0xff);
            *dest++ = (char)( pixel        & 0xff);
          }
        }
        else /* if (image_channels == 4) */
        {
          for( i = image_width; i > 0; --i )
          {
            r = *src++;
            g = *src++;
            b = *src++;
            a = *src++;
            if( a == 255 )
            {
              redl   = r;
              greenl = g;
              bluel  = b;
            }
            else if( a == 0 )
            {
              redl   = bg_red;
              greenl = bg_green;
              bluel  = bg_blue;
            }
            else
            {
              /*
                this macro (from png.h) composites the foreground
                and background values and puts the result into the
                first argument
              */
              alpha_composite(redl,   r, a, bg_red);
              alpha_composite(greenl, g, a, bg_green);
              alpha_composite(bluel,  b, a, bg_blue);
            }
            pixel = (redl << RShift) | (greenl << GShift) | (bluel << BShift);
            /*
              recall that we set ximage->byte_order = MSBFirst above
            */
            *dest++ = (char)((pixel >> 24) & 0xff);
            *dest++ = (char)((pixel >> 16) & 0xff);
            *dest++ = (char)((pixel >>  8) & 0xff);
            *dest++ = (char)( pixel        & 0xff);
          }
        }
        /*
          display after every 16 lines
        */
        /*
        if( ((row+1) & 0xf) == 0 )
        {
          XPutImage( dpy, png_window, gc, ximage, 0, (int)lastrow, 0,
                     (int)lastrow, (unsigned int)image_width, 16 );
          XFlush( dpy );
          lastrow = row + 1;
        }
        */
      }
    }
    else if( depth == 16 )
    {
      lastrow = 0;
      for( row = 0; row < image_height; ++row )
      {
        src = image_data + row*image_rowbytes;
        dest = ximage->data + row*ximage_rowbytes;
        if( image_channels == 3 )
        {
          for( i = image_width; i > 0; --i )
          {
            reds  = ((unsigned short)(*src) << 8);
            ++src;
            greens = ((unsigned short)(*src) << 8);
            ++src;
            blues = ((unsigned short)(*src) << 8);
            ++src;
            pixel = ((reds   >> RShift) & RMask) |
                    ((greens >> GShift) & GMask) |
                    ((blues  >> BShift) & BMask);
            *dest++ = (char)((pixel >>  8) & 0xff);
            *dest++ = (char)( pixel        & 0xff);
          }
        }
        else /* if (image_channels == 4) */
        {
          for( i = image_width;  i > 0;  --i )
          {
            r = *src++;
            g = *src++;
            b = *src++;
            a = *src++;
            if( a == 255 )
            {
              reds   = ((unsigned short)r << 8);
              greens = ((unsigned short)g << 8);
              blues  = ((unsigned short)b << 8);
            }
            else if( a == 0 )
            {
              reds   = ((unsigned short)bg_red   << 8);
              greens = ((unsigned short)bg_green << 8);
              blues  = ((unsigned short)bg_blue  << 8);
            }
            else
            {
              alpha_composite(r, r, a, bg_red);
              alpha_composite(g, g, a, bg_green);
              alpha_composite(b, b, a, bg_blue);
              reds   = ((unsigned short)r << 8);
              greens = ((unsigned short)g << 8);
              blues  = ((unsigned short)b << 8);
            }
            pixel = ((reds   >> RShift) & RMask) |
                    ((greens >> GShift) & GMask) |
                    ((blues  >> BShift) & BMask);
            *dest++ = (char)((pixel >>  8) & 0xff);
            *dest++ = (char)( pixel        & 0xff);
          }
        }
        /*
          display after every 16 lines
        */
        /*
        if( ((row+1) & 0xf) == 0 )
        {
          XPutImage( dpy, png_window, gc, ximage, 0, (int)lastrow, 0,
                     (int)lastrow, image_width, 16 );
          XFlush( dpy );
          lastrow = row + 1;
        }
        */
      }
    }
    XPutImage( dpy, png_window, gc, ximage, 0, 0, 0, 0,
               (unsigned int)image_width, (unsigned int)image_height );
    XFlush( dpy );
    /*
    if( lastrow < image_height )
    {
      XPutImage( dpy, png_window, gc, ximage, 0, (int)lastrow, 0,
                 (int)lastrow, image_width, image_height-lastrow );
      XFlush( dpy );
    }
    */
    return 0;
  } 
 
 void rpng_x_cleanup_()
  {
    if( image_data )
    {
      free( image_data );
      image_data = NULL;
    }
    if( ximage )
    {
      if( ximage->data )
      {
        free( ximage->data );         /* we allocated it, so we free it */
        ximage->data = (char *)NULL;  /*  instead of XDestroyImage() */
      }
      XDestroyImage( ximage );
      ximage = NULL;
      XFreeGC( dpy, gc );
    }
    if( have_window )
    {
      XDestroyWindow( dpy, png_window );
      XUnmapWindow( dpy, png_window );
      have_window = FALSE;
      XFlush( dpy );
    }
    if( have_colormap )
    {
      XFreeColormap( dpy, colormap );
      have_colormap = FALSE;
    }
    if( have_nondefault_visual )
    {
      XFree( visual_list );
      have_nondefault_visual = FALSE;
    }
  }
 
 static int rpng_x_msb( unsigned long u32val )
  {
    int i;
    for( i = 31; i >= 0; --i )
    {
      if( u32val & 0x80000000L )break;
      u32val <<= 1;
    }
    return i;
  }

 /* end of file */
