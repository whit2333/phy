      SUBROUTINE DWG_STROKE(ACTION)
C======================================================================C
C  Turn stroke precision text on or off.
C======================================================================C
      COMMON/CDWG/DWGON,DWGTXT,LDWG,LDWT,IRECG,IRECT,STROKE
      LOGICAL DWGON,DWGTXT,STROKE
C
      CHARACTER*(*) ACTION
C
      CHARACTER*3 ACT
      CALL UPRCASE(ACTION,ACT)
      IF(ACT.EQ.'ON')STROKE=.TRUE.
      IF(ACT.EQ.'OFF')STROKE=.FALSE.
      RETURN
      END

