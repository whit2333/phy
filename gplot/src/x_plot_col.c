/* Modified 20-Nov-91 by FWJ: now calls xvst_store for vector replay */

/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

/* Modified 30-MAY-94 by FWJ: colour 0 could not be set!        */

#include <stdio.h>
#include <X11/Xlib.h>

#ifdef VMS
#define xvst_rep_ xvst_rep
#define xvst_plot_colour_ xvst_plot_colour
#endif

/*       GLOBAL  VARIABLES     */

Display *dpy;
GC ggc,zgc;
Bool zoom;
unsigned long gcfore, gcback;
unsigned long icmap[12];

struct { int istore; } xvst_rep_;

#define Max(a,b)  ((a) > (b) ? (a) : (b))
#define Min(a,b)  ((a) < (b) ? (a) : (b))

/************************************************************************
*
*     THIS ROUTINE ASSIGNS THE COLOR INDEX (IC) TO THE CORRESPONDING
*     FOREGROUND COLOR DEFINED BY THE COLOR TABLE BELOW.
*
*        IC Value   |   Color
*      ----------------------------
*           0       |   Black
*           1       |   Red
*           2       |   Blue
*           3       |   Violet (Magenta)
*           4       |   Green
*           5       |   Yellow
*           6       |   Cyan
*           7       |   White
*           8       |   Coral (Orange)
*           9       |   Red Magenta (Orange Red)
*          10       |   Green Cyan
*          11       |   Blue Cyan                
*
*************************************************************************/

void xvst_plot_colour_(ic)
   int *ic;
{
   int ic1;
   int icode = 3;

   if (xvst_rep_.istore == 1) xvst_store_(&icode, ic);

/************************************************************************
*    SET THE FOREGROUND COLOR AND CHECK IF IC IS IN THE DEFINED RANGE  
*************************************************************************/

   ic1 = Min(Max(0, *ic), 11);

   XSetForeground(dpy, ggc, icmap[ic1]);
   if (zoom) XSetForeground(dpy, zgc, icmap[ic1]);
/* XSetForeground( dpy, pgc, icmap[ic1] ); */
   gcfore = icmap[ic1];
   XFlush(dpy);
}
