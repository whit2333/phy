/* xvst_replay -- replays the window contents from vector storage */

/* Rewritten 20-Nov-91 by F.Jones: now uses fixed-size memory segments */
/* created by xvst_store. */
/* Modified 5-Dec-91 by FWJ: all event reporting is now disabled   */
/* during replay, to prevent deadlock conditions.  A "busy" cursor */
/* is displayed during the replay.                                 */

/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/cursorfont.h>

#ifdef VMS
#define xvst_plot_flush_ xvst_plot_flush
#endif

#define NBMAX 150
#define NWORDS 16000

/* GLOBAL VARIABLES */

int *bank[NBMAX+1];
int nstore, nbank;

Display *dpy;
Window g_window, z_window;
int event_mask;
Cursor xhaircursor, busycursor;


void xvst_replay_()
{
   int irp, iloc, ibank;
   int icode, ia, ib, ic;

   if (nstore < 1) return;

/* FWJ 04-DEC-91:  disable most event reporting during replay */
   XSelectInput(dpy,g_window,StructureNotifyMask);
   XSelectInput(dpy,z_window,StructureNotifyMask);
   XDefineCursor(dpy,g_window,busycursor);
   XDefineCursor(dpy,z_window,busycursor);

   xvst_plot_clear_replay_();

   irp = 0;
   ibank = 1;
   iloc = -1;

L20:
   irp++;
   iloc++;
   if (iloc == NWORDS) {
      ibank++;
      iloc = 0;
   }

   icode = *(bank[ibank]+iloc);

   if (icode == 1) {
      irp++;
      iloc++;
      if (iloc == NWORDS) {
         ibank++;
         iloc = 0;
      }
      ia = *(bank[ibank]+iloc);
      irp++;
      iloc++;
      if (iloc == NWORDS) {
         ibank++;
         iloc = 0;
      }
      ib = *(bank[ibank]+iloc);
      irp++;
      iloc++;
      if (iloc == NWORDS) {
         ibank++;
         iloc = 0;
      }
      ic = *(bank[ibank]+iloc);
      xvst_plot_replay_(&ia, &ib, &ic);
   }
   else if (icode == 2) {
      xvst_plot_flush_();
      irp++;
      iloc++;
      if (iloc == NWORDS) {
         ibank++;
         iloc = 0;
      }
      ia = *(bank[ibank]+iloc);
      xvst_plot_mode_replay_(ia);
   }
   else if (icode == 3) {
      xvst_plot_flush_();
      irp++;
      iloc++;
      if (iloc == NWORDS) {
         ibank++;
         iloc = 0;
      }
      ia = *(bank[ibank]+iloc);
      xvst_plot_colour_replay_(&ia);
   }
   else goto L80;

   if (irp < nstore) goto L20;

L80:
   xvst_plot_flush_();

/* FWJ 04-DEC-91:  resume previous event reporting */
   XSelectInput(dpy,g_window,event_mask);
   XSelectInput(dpy,z_window,event_mask);
   XDefineCursor(dpy,g_window,xhaircursor);
   XDefineCursor(dpy,z_window,xhaircursor);
   XFlush(dpy);

   return;
}
