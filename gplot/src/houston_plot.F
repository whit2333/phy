C======================================================================C
C                                                                      C
C  Plot file driver routines for the               F.W. Jones, TRIUMF  C
C  Houston Instruments DMP-52 MP plotter.                              C
C                                                                      C
C======================================================================C

      SUBROUTINE HOUSTON_PLOTS(LUN)
C======================================================================C
C  Initializes the Houston Instruments plotter driver                  C
C                                                                      C
C  Modified June 15/87 by F. Jones: entry point added to generate      C
C    plot file with only one pen command per record.  This type of     C
C    file can be read by AUTOCAD and converted to an AUTOCAD drawing.  C
C======================================================================C
      COMMON/HOUSTONPLOT/HBUFF,NHB,LUNH,AUTOCAD
      CHARACTER*80 HBUFF
      LOGICAL AUTOCAD

      LOGICAL STATE

      NHB=0      !number of characters in buffer
      LUNH=LUN   !unit for work file
      RETURN

      ENTRY HOUSTON_AUTOCAD(STATE)
      
      AUTOCAD=STATE
      RETURN
      END


      SUBROUTINE HOUSTON_PLOT(X,Y,IPEN)
C======================================================================C
C
C  Houston Instruments plotter driver
C
C    If IPEN=3, move to (X,Y)
C    If IPEN=2, draw to (X,Y)
C
C======================================================================C
      COMMON/HOUSTONPLOT/HBUFF,NHB,LUNH,AUTOCAD
      CHARACTER*80 HBUFF
      LOGICAL AUTOCAD
C
C Convert from cm to plotter units (0.025 mm):
      IX=NINT(400.*X)
      IY=NINT(400.*Y)
      IF(IX.LT.0.OR.IX.GT.99999.OR.IY.LT.0.OR.IY.GT.99999)RETURN

      IF(AUTOCAD)THEN
        IF(IPEN.EQ.2)THEN
          IF(IPENP.EQ.3)THEN
            WRITE(LUNH,3000)IX,IY
          ELSE
            WRITE(LUNH,2000)IX,IY
          ENDIF
        ELSE
          WRITE(LUNH,4000)'U V3  '
          WRITE(LUNH,2000)IX,IY
        ENDIF
        IPENP=IPEN
        RETURN
      ENDIF
2000  FORMAT('A ',I5.5,',',I5.5,'  ')
3000  FORMAT('V2 D A ',I5.5,',',I5.5,'  ')
4000  FORMAT(A)

      IBASE=NHB+1      !base position in buffer
      IF(IPEN.EQ.2)THEN
        HBUFF(IBASE:)='D'
      ELSE
        HBUFF(IBASE:)='U'
      ENDIF
      WRITE(HBUFF(IBASE+1:),1000)IX,IY
1000  FORMAT(I5.5,',',I5.5,',')
      NHB=NHB+13      !update # of characters in buffer
      IF(NHB.GE.78)CALL HOUSTON_FLUSH      !flush buffer if full
      RETURN
      END


      SUBROUTINE HOUSTON_FLUSH
C======================================================================C
C  Flush the Houston Instruments plot buffer to the work file
C======================================================================C
      COMMON/HOUSTONPLOT/HBUFF,NHB,LUNH,AUTOCAD
      CHARACTER*80 HBUFF
      LOGICAL AUTOCAD
C
      IF(AUTOCAD)RETURN

      IF(NHB.GT.0)WRITE(LUNH,1000)HBUFF(1:NHB)
1000  FORMAT(A)
      NHB=0
      RETURN
      END


      SUBROUTINE HOUSTON_NEWPEN(IPEN)
C======================================================================C
C  Select Houston Instruments plotter pen.
C======================================================================C
      COMMON/HOUSTONPLOT/HBUFF,NHB,LUNH,AUTOCAD
      CHARACTER*80 HBUFF
      LOGICAL AUTOCAD
C
      IF(AUTOCAD)RETURN

      IF(IPEN.LT.1.OR.IPEN.GT.14)RETURN
      CALL HOUSTON_FLUSH
      IF(IPEN.LE.7)THEN
        WRITE(LUNH,1000)IPEN
1000    FORMAT('P',I1,',')
      ELSE
        WRITE(LUNH,2000)IPEN-7
2000    FORMAT('P',I1,'+,')
      ENDIF
      RETURN
      END


      SUBROUTINE HOUSTON_SPEED(ISPEED)
C======================================================================C
C  Changes the plotting speed for Houston Instruments plotter.
C  ISPEED is given in cm/sec.
C======================================================================C
      COMMON/HOUSTONPLOT/HBUFF,NHB,LUNH,AUTOCAD
      CHARACTER*80 HBUFF
      LOGICAL AUTOCAD
C
      IF(AUTOCAD)RETURN

      IF(ISPEED.LT.5.OR.ISPEED.GT.40)RETURN
      CALL HOUSTON_FLUSH
      WRITE(LUNH,1000)ISPEED
1000  FORMAT('V',I2.2,',')
      RETURN
      END
