/* Modified 09-MAR-93 by FWJ: calls to signal() removed.    */
/* These calls are unnecessary since the signal handler     */
/* stays established after the first call in xvst_setup().  */
/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#include <stdio.h>
#include <signal.h>
#include <X11/Xlib.h>

/*   GLOBAL VARIABLES   */

Display *dpy;
Window g_window, z_window;
Bool enable_action, replay_zoom;
Bool resize_m, resize_z;

/*************************************************************************
*
*   THIS ROUTINE CHECKS TO SEE IF THE GRAPHICS WINDOW WAS RESIZED
*   OR NOT.  IF IT WAS, IT WILL REPLAY THE GRAPHICS.  FURTHERMORE 
*   IT WILL REARM ITSELF TO BE CALLED AFTER 1 SECOND.  HOWEVER IF
*   IT IS IN THE MAIN EVENT LOOP, IT WILL NOT REARM ITSELF AS THE 
*   LOOP WILL HANDLE RESIZE CHANGES BY ITSELF.
*
*   THE SIGNAL CALL IS A RUN-TIME C SYSTEM LIBRARY CALL WHICH NOTIFIES
*   THE SYSTEM OF THE APPROPRIATE ACTION TO TAKE WHEN IT RECEIVES AN
*   ALARM (SIGALRM).  IN THIS CASE, WHEN IT RECEIVES A SIGALRM, IT IS 
*   TO CALL THE SUBROUTINE EXPOSE_ACTION.  THE TIME INTERVAL IS SET BY 
*   THE ALARM CALL.  THE ARGUMENT OF THE ALARM CALL IS AN INTEGER,
*   THUS THE SHORTEST INTERVAL POSSIBLE BY THIS METHOD IS ONE SECOND.
*
*   FOR A PROGRAM EXAMPLE SEE THE VAX C RUN-TIME LIBRARY REFERENCE
*   MANUAL, CHAPTER 4.3, P 4-7.
*
**************************************************************************/

void resize_action()
{
   XEvent event;

/* If redraw by this routine is inhibited (e.g. by crosshair_r main event */
/* loop), then just set a new alarm and exit.... */
   if ( !enable_action ) {
#ifndef VMS
#ifndef sgi
      alarm( 1 );
#endif
#endif
      return;
   }

/********************************************************************
*   CHECK FOR A RESIZE, THE LOGICALS RESIZE_Z AND RESIZE_M SIGNAL
*   WHETHER IT IS CURRENTLY RESIZING THE ZOOM WINDOW OR THE MAIN
*   GRAPHICS WINDOW.   IF IT IS RESIZING IN ONE OF THESE WINDOWS,
*   DON'T CHECK FOR RESIZE EVENTS IN THE OTHER WINDOW UNTIL THE
*   RESIZE IN THE OTHER WINDOW IS FINISHED.
*********************************************************************/

   if ( !resize_z && XCheckTypedWindowEvent( dpy, g_window,
                                             ConfigureNotify, &event ))
   {
      resize_m = True;
      do_ConfigureNotify( &event );
      resize_m = False;
   }
   else if ( !resize_m && XCheckTypedWindowEvent( dpy, z_window,
                                                  ConfigureNotify, &event ))
   {
      resize_z = True;
      do_zoom_ConfigureNotify( &event );
      resize_z = False;
   }
/* SET A NEW ALARM */
#ifndef VMS
#ifndef sgi
   alarm( 1 );
#endif
#endif
}
