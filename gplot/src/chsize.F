      SUBROUTINE CHSIZE(IOFF,NOLD,NEW,LTYPE,*)
C**********************************************************************
C
C  To assign storage space in the MAIN_STORAGE array
C
C  INPUT:
C
C  IOFF:  The pointer to the existing location (if any)
C
C  NOLD:   The length (in "elements") of the array at the old location.
C          If NOLD=0, we assume this is a NEW array
C
C  NEW:    The length of the new array.  If NEW=0, then free this
C          space but do not get any more
C
C  LTYPE:  The number of bytes/elements (eg LTYPE=4 for REAL*4)
C
C  ERROR RETURN: Various errors; descriptive messages will be printed
C                out.
C  Modified: July 17,1991 Common block XD declared by I4D instead of
C            R8D to make the length the same as in other routines.
C
C      Modified by J. Chuma, March 7, 1997.  Eliminated the "$" from
C      common block and variable names for LINUX g77
C
C**********************************************************************
      COMMON /XD/ I4D     ! Changed from R8D July 17, 1991 by Pui
      COMMON /TABLED/ MAXTABLE,NTABLE,IEXIST,IVEC,IADDRESS,IBYTES
      LOGICAL*1 L1D(1)
      INTEGER I4D(1)
      REAL*8 R8D(1)
      EQUIVALENCE (I4D(1),L1D(1),R8D(1))
C**********************************************************************
C  Check Master Table if IOFF passed with old size NOLD > 0, to verify
C  that a consistent Table entry exists. If NOLD = 0 then a new array
C  is being created and this check is not necessary.
C**********************************************************************
      IF(NOLD.GT.0) THEN
          DO 10 I=1,NTABLE
          K=I
          IF(.NOT.(L1D(IEXIST+I))) GOTO 10
          IF(IOFF .EQ. I4D(IVEC+I)) GOTO 20
   10     CONTINUE
C************************************ Can't find the entry IOFF
          WRITE(6,15) 
   15     FORMAT('0*** Error in CHSIZE: this array is not recorded')
          RETURN 1
C*********************** Found the entry: check size agrees with NOLD
   20     NBYTES_OLD= NOLD*LTYPE
          IF(NBYTES_OLD .NE. I4D(IBYTES+K)) THEN
              WRITE(6,30)
   30         FORMAT('0*** Error in CHSIZE: Table entry, NOLD disagree')
              RETURN 1
          END IF
      END IF
      IF(NOLD-NEW)100,200,300
C**********************************************************************
C  NEW > NOLD
C**********************************************************************
C  If NOLD = 0, this is a NEW entry. 
C  If NOLD > 0, then destroy the old and create a new.
C**********************************************************************
100   IF(NOLD.EQ.0)THEN
          CALL GET_ARRAY(IOFF,LTYPE,NEW,*900)
          RETURN
      ELSE
          CALL GET_ARRAY(IOFF_NEW,LTYPE,NEW,*900)
          IOFF_BYTES= IOFF*LTYPE
          IOFF_NEW_BYTES= IOFF_NEW*LTYPE
          NBYTES= NOLD*LTYPE
          CALL COPY_ARRAY( L1D(IOFF_BYTES+1), L1D(IOFF_NEW_BYTES+1),
     &     NBYTES)
          CALL DEL_ARRAY(IOFF,*900)
          IOFF= IOFF_NEW
          RETURN
      END IF
C**********************************************************************
C  NEW = NOLD  Do nothing
C**********************************************************************
200   RETURN
C**********************************************************************
C  NEW < NOLD
C**********************************************************************
C  If NEW = 0, just destroy
C  If NEW > 0, destroy and create
C**********************************************************************
300   IF(NEW.EQ.0)THEN
          CALL DEL_ARRAY(IOFF,*900)
          RETURN
      ELSE
          CALL GET_ARRAY(IOFF_NEW,LTYPE,NEW,*900)
          IOFF_BYTES= IOFF*LTYPE
          IOFF_NEW_BYTES= IOFF_NEW*LTYPE
          NBYTES= NEW*LTYPE
          CALL COPY_ARRAY( L1D(IOFF_BYTES+1), L1D(IOFF_NEW_BYTES+1),
     &     NBYTES)
          CALL DEL_ARRAY(IOFF,*900)
          IOFF= IOFF_NEW
          RETURN
      END IF
900   RETURN1
      END
