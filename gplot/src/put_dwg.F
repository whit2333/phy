      SUBROUTINE PUT_DWG(X,Y,IPEN,ILIN)
C======================================================================C
C  PUT_DWG                                         F.W. Jones, TRIUMF  C
C  Puts a record to the current DWG file -- interface routine for      C
C    EDGR graphics editor.                                             C
C======================================================================C
      COMMON/CDWG/DWGON,DWGTXT,LDWG,LDWT,IRECG,IRECT,STROKE
      LOGICAL DWGON,DWGTXT,STROKE
      COMMON/DWG_PREVIOUS/XP,YP,IPENP
      COMMON/EDGR_ATT_SET/IFONT_SET,ICOL_SET

#ifdef g77
      COMMON/S_GBUFF/GBUFF_X,GBUFF_Y,GBUFF_IPEN,GBUFF_ICOL,GBUFF_ILIN
      REAL GBUFF_X
      REAL GBUFF_Y
      INTEGER*2 GBUFF_IPEN
      BYTE GBUFF_ICOL
      BYTE GBUFF_ILIN
      CHARACTER*12 GBUFF
      EQUIVALENCE(GBUFF,GBUFF_X)
#elif gfortran
      COMMON/S_GBUFF/GBUFF_X,GBUFF_Y,GBUFF_IPEN,GBUFF_ICOL,GBUFF_ILIN
      REAL GBUFF_X
      REAL GBUFF_Y
      INTEGER*2 GBUFF_IPEN
      BYTE GBUFF_ICOL
      BYTE GBUFF_ILIN
      CHARACTER*12 GBUFF
      EQUIVALENCE(GBUFF,GBUFF_X)
#else
      STRUCTURE/GBUFF/
        REAL X
        REAL Y
        INTEGER*2 IPEN
        BYTE ICOL
        BYTE ILIN
      END STRUCTURE
      RECORD/GBUFF/GBUFF
#endif
      IF(IPEN.NE.2)GO TO 40
      IF(IPENP.NE.2)THEN
#ifdef g77
        GBUFF_X=XP
        GBUFF_Y=YP
        GBUFF_IPEN=3
        GBUFF_ICOL=0
        GBUFF_ILIN=0
#elif gfortran
        GBUFF_X=XP
        GBUFF_Y=YP
        GBUFF_IPEN=3
        GBUFF_ICOL=0
        GBUFF_ILIN=0
#else
        GBUFF.X=XP
        GBUFF.Y=YP
        GBUFF.IPEN=3
        GBUFF.ICOL=0
        GBUFF.ILIN=0
#endif
        WRITE(LDWG,REC=IRECG,ERR=99)GBUFF
        IRECG=IRECG+1
      ENDIF
#ifdef g77
      GBUFF_X=X
      GBUFF_Y=Y
      GBUFF_IPEN=IPEN
      GBUFF_ICOL=ICOL_SET
      GBUFF_ILIN=ILIN
#elif gfortran
      GBUFF_X=X
      GBUFF_Y=Y
      GBUFF_IPEN=IPEN
      GBUFF_ICOL=ICOL_SET
      GBUFF_ILIN=ILIN
#else
      GBUFF.X=X
      GBUFF.Y=Y
      GBUFF.IPEN=IPEN
      GBUFF.ICOL=ICOL_SET
      GBUFF.ILIN=ILIN
#endif
      WRITE(LDWG,REC=IRECG,ERR=99)GBUFF
      IRECG=IRECG+1

40    XP=X
      YP=Y
      IPENP=IPEN

99    RETURN
      END
