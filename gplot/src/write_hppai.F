      SUBROUTINE WRITE_HPPAINT(IOUT,*)
C======================================================================C
C  Writes the HP Paintjet bitmap into the plot file connected to
C  unit IOUT.  The plot file is assumed to be correctly opened by
C  the caller.
C
C  Use RUN-LENGTH-ENCODING
C    We will set the colour pallete as follows
C
C                     palette index number
C    plane#      0   1   2   3   4   5   6   7
C      1         0   1   0   1   0   1   0   1
C      2         0   0   1   1   0   0   1   1
C      3         0   0   0   0   1   1   1   1
C                ^   ^   ^   ^   ^   ^   ^   ^
C                |   |   |   |   |   |   |   | 
C                w   r   g   y   b   m   c   b
C                h   e   r   e   l   a   y   l
C                i   d   e   l   u   g   a   a
C                t       e   l   e   e   n   c
C                e       n   o       n       k
C                            w       t     
C                                    a
C
C Added ESC%8   ESC%@  beginning and end escape sequences to allow
C HP type (PCL) colour inkjet printers and DEC LJ250's to works also
C        C.K.Kost FEb/3/89 
C 01-MAR-89  F. Jones.  Added TRANSPARENCY flag.  If set, the printer
C is set into transparency mode before printing and reset to normal
C mode after.
C
C modified by J.Chuma, 19Mar97 for g77
C   replace LOGICAL*1 with BYTE, and removed intrinsic functions from
C   PARAMETER statements
C======================================================================C

      COMMON/GRAPHICS_BITMAP/HPPAINT,NBYTES_HP,IOFFSET,NXDIM,NYDIM,MAXY
      BYTE HPPAINT(4)    ! dynamic array dimensioned NXDIM,NYDIM
      COMMON/WRITEBITMAPHPPAINT/TRANSPARENCY
      LOGICAL TRANSPARENCY

      BYTE BUFFER(367)  ! modified by J.Chuma, 19Mar97 for g77
      BYTE CW/87/,CV/86/,STAR/42/,SB/98/
      CHARACTER*1 ESC
CCC
      ESC = CHAR(27)
      WRITE(IOUT,200,ERR=999)(ESC,NESC=1,13)
 200  FORMAT(A1,'%8',/,         !set into PCL for LJ250's
     &       A1,'*t180R',/,     !set resolution to 180dpi
     &       A1,'*r3U',/,       !specify 3 colour planes
     &       A1,'*v90A',/,      !NTSC 'red'   for white
     &       A1,'*v88B',/,      !NTSC 'green' for white
     &       A1,'*v85C',/,      !NTSC 'blue'  for white
     &       A1,'*v0I',/,       !Color palette index 0 assigned to white
     &       A1,'*v4A',/,       !NTSC 'red'   for black
     &       A1,'*v4B',/,       !NTSC 'green' for black
     &       A1,'*v6C',/,       !NTSC 'blue'  for black
     &       A1,'*v7I',/,       !Color palette index 7 assigned to black
     &       A1,'*r0A',/,       !start raster graphics
     &       A1,'*b1M')         !set for run length encoding

      IF(TRANSPARENCY)WRITE(IOUT,220,ERR=999)ESC,'&k3W'
220   FORMAT(A1,A)

CC      NBYTES=360                ! 1440/8=180 byte pairs (max)
      NBYTES=2*NXDIM
      BUFFER(1)=ichar(ESC)
      BUFFER(2)=STAR
      BUFFER(3)=SB
      DO 400 J=1,NYDIM                 ! usually 5400 ie. 1800*3 color planes
         CALL ZERO_ARRAY(BUFFER(8),NBYTES)   ! leave first 7 bytes alone
         BUFFER(7)=CV                 ! Set BUFFER to ESC*bnnnV
         IF(J/3*3.EQ.J)BUFFER(7)=CW   ! Set BUFFER to ESC*bnnnW if last plane
C
C Load first byte with repeat count of 1 
C      and second byte with first byte pattern
C
         BUFFER(9)=HPPAINT(1+(J-1)*NXDIM+IOFFSET)
         ILAST=1
         NBYTES=2
         DO 300 I=2,NXDIM
         IF(HPPAINT(I+(J-1)*NXDIM+IOFFSET).NE.
     &              HPPAINT(I-1+(J-1)*NXDIM+IOFFSET))THEN
            NREPEAT=I-ILAST-1
            ILAST=I
            IF(NREPEAT.LE.127)BUFFER(NBYTES+6)=NREPEAT      !Store in byte
            IF(NREPEAT.GT.127)BUFFER(NBYTES+6)=NREPEAT-256  !-128 to 127
            NBYTES=NBYTES+2
C           Load new pattern byte
            BUFFER(NBYTES+1+6)=HPPAINT(I+(J-1)*NXDIM+IOFFSET)
         ENDIF      
 300     CONTINUE
C
C  Take care of case where we run out of loop
C
         NREPEAT=NXDIM-ILAST                            !not 180-ILAST-1
         IF(NREPEAT.LE.127)BUFFER(NBYTES+6)=NREPEAT      !Store in byte
         IF(NREPEAT.GT.127)BUFFER(NBYTES+6)=NREPEAT-256  !-128 to 127
C
C   Load bytes 4,5,6 of BUFFER with number of data bytes (max 360)
C
         N100=NBYTES/100               ! number of hundreds
         N10=(NBYTES-N100*100)/10      ! number of tens
         N1=(NBYTES-N100*100-N10*10)   ! number of ones
         BUFFER(4)=N100+48             ! Character 0 is 48 decimal
         BUFFER(5)=N10+48
         BUFFER(6)=N1+48
C        +7 for control bytes
         WRITE(IOUT,900,ERR=999)(BUFFER(K),K=1,NBYTES+7)
 900     FORMAT(367A1)
 400  CONTINUE

      IF(TRANSPARENCY)WRITE(IOUT,220,ERR=999)ESC,'&k1W'
      WRITE(IOUT,420,ERR=999)ESC,ESC
 420  FORMAT(A1,'*rB',/,                      !end raster graphics
     &       A1,'%@'     )  !Exit HP PCL mode to DEC mode (for LJ250's)
      CLOSE(UNIT=IOUT)
      RETURN
C
999   WRITE(*,*)'Error writing HP Paintjet plot file on unit',IOUT
      IDUM = NARGSI(0)
      CALL PUT_FORMSG
      RETURN 1
      END
