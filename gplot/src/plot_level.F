      SUBROUTINE PLOT_DATA_LEVEL(ILEVEL)
C======================================================================C
C                                                                      C
C   PLOT_DATA_LEVEL: sets the drawing mode on raster or                C
C                      bit-mapped devices                              C
C                                                                      C
C   Written by Arthur Haynes, TRIUMF U.B.C., JULY 6, 1981.             C
C                                                                      C
C   Input  Parameters: ILEVEL (I*4).                                   C
C                                                                      C
C   Parameter Definitions:                                             C
C   --------- -----------                                              C
C                                                                      C
C   ILEVEL:                                                            C
C           = 0: dots turned on (default)                              C
C           = 1: dots turned off                                       C
C           = 2: dots complemented or toggled.                         C
C                                                                      C
C  Modified May 8/84 by F. Jones to support selective erase            C
C  (ILEVEL=1) on the CIT-467 colour terminal.  This involves           C
C  setting the graphics writing colour to black.                       C
C                                                                      C
C  Modified August/84 by F.W. Jones to support the VT241.              C
C  Modified Oct 4/85 by F.W. Jones to support the Plessey PT100.       C
C  Modified Dec 5/85 by F.W. Jones to support the QMS Lasergrafix      C
C  Modified Mar 20/87 by F.W. Jones for the Seiko GR-1105 terminal     C
C  Modified May 11/89 by J. Chuma for the generic terminal             C
C  Modified 19-AUG-93 by FWJ: UIS support removed                      C
C                                                                      C
C======================================================================C
      COMMON /PLOT_LEVEL/ ILEV
      COMMON /PLOTMONITOR/ IMONITOR,IOUTM

      CHARACTER*1 ESC,GS, TMP
      LOGICAL FIRST/.TRUE./

C Default colour for CIT467 (cyan):
      INTEGER ICITCOL_SAVE/6/
      COMMON/CIT_COLOUR/ICITCOL1,ICITCOL2

      BYTE TEK4107_INDEX (16)
C  Modified by J.Chuma, 23Apr97 for Absoft f77  changed X' to Z'
      DATA TEK4107_INDEX /Z'30', Z'31', Z'32', Z'33', Z'34'
     *                   ,Z'35', Z'36', Z'37', Z'38', Z'39'
     *                   ,Z'3A', Z'3B', Z'3C', Z'3D', Z'3E'
     *                   ,Z'3F'/
CCC
      ESC = CHAR(27)  ! modified by J.Chuma, 19Mar97 for g77
      GS = CHAR(29)   !  were inside PARAMETER statements
      IF(ILEVEL .LT. 0 .OR. ILEVEL .GT. 2)RETURN
      ILEV=ILEVEL

      CALL FLUSH_PLOT

C VT640:
      IF(IMONITOR.EQ.1)THEN
        TMP = CHAR( ILEVEL+ICHAR('0') ) ! modified by J.Chuma, 19Mar97 for g77
        WRITE(IOUTM,10)GS,ESC,'/',TMP,'d'
C        WRITE(IOUTM,10)GS,ESC,'/',LEVEL+'0','d'
10      FORMAT(1X,5A1)

C CIT467:
      ELSE IF(IMONITOR.EQ.6)THEN
        IF(ILEVEL.EQ.2)RETURN      !No complement mode
C Make sure the terminal is "armed" for selective erase:
C Go into 4027 mode and draw a dummy vector to initialize.
        IF(FIRST)THEN
          WRITE(IOUTM,1000)ESC,'1',ESC,'O','!VEC 0,0,0,0','!BYE'
C FORMAT modified for Unix compatibility 14-NOV-91 by FWJ
C1000     FORMAT('+',4A1/'+',A/'+',A)
 1000     FORMAT(' ',4A1/' ',A/' ',A)
          FIRST=.FALSE.
        ENDIF
C Selective erase:
        IF(ILEVEL.EQ.1)THEN
          ICITCOL_SAVE=ICITCOL1      !Remember colour setting
C Set writing colour to black:
          WRITE(IOUTM,2000)ESC,'1',ESC,'h',ESC,'2'
C FORMAT modified for Unix compatibility 14-NOV-91 by FWJ
C2000     FORMAT('+',6A1)
 2000     FORMAT(' ',6A1,$)
        ENDIF
C Restore normal writing mode (colour).
C Do not call PLOT_COLOUR since it messes with the second monitor.
        IF(ILEVEL.EQ.0)THEN
          TMP = CHAR( ICHAR('h')+ICITCOL_SAVE )   ! modified by J.Chuma, 19Mar97
          WRITE(IOUTM,2000)ESC,'1',ESC,TMP,ESC,'2'
C          WRITE(IOUTM,2000)ESC,'1',ESC,'h'+ICITCOL_SAVE,ESC,'2'
          ICITCOL1=ICITCOL_SAVE
        ENDIF

C TK4107:
      ELSE IF(IMONITOR.EQ.7)THEN
        IF(ILEVEL.EQ.2)RETURN      !No complement mode
C Selective erase:
        IF(ILEVEL.EQ.1)THEN
          ICITCOL_SAVE=ICITCOL1      !Save colour setting
C Change colour to black:
          WRITE(IOUTM,60100)ESC,'M','L',TEK4107_INDEX(1)
60100     FORMAT (1X, 4A1)
        ENDIF
C Restore normal writing mode (colour).
C Do not call PLOT_COLOUR since it messes with the second monitor.
        IF(ILEVEL.EQ.0)THEN
          WRITE(IOUTM,60100)
     &     ESC,'M','L',TEK4107_INDEX(ICITCOL_SAVE+1)
          ICITCOL1=ICITCOL_SAVE
        ENDIF

C VT241:
      ELSE IF(IMONITOR.EQ.8)THEN
        CALL REGIS_MODE
        IF(ILEVEL.EQ.0)WRITE(IOUTM,3000)'W(V)'
        IF(ILEVEL.EQ.1)WRITE(IOUTM,3000)'W(E)'
        IF(ILEVEL.EQ.2)WRITE(IOUTM,3000)'W(C)'
3000    FORMAT(1X,A)

C PT100G:
      ELSE IF(IMONITOR.EQ.9)THEN
        IF(ILEVEL.EQ.0)WRITE(IOUTM,4000)GS,ESC,'OW `` @@'
        IF(ILEVEL.EQ.1)WRITE(IOUTM,4000)GS,ESC,'OW b` @@'
        IF(ILEVEL.EQ.2)WRITE(IOUTM,4000)GS,ESC,'OW a` @@'
4000    FORMAT(1X,A1,A1,A)

C SEIKO GR1105:
      ELSE IF(IMONITOR.EQ.12)THEN
        IF(ILEVEL.EQ.0)WRITE(IOUTM,5000)ESC,'K ',ESC,'oD '
        IF(ILEVEL.EQ.1)WRITE(IOUTM,5000)ESC,'K ',ESC,'oD!'
5000    FORMAT(1X,A1,A2,A1,A3)

C LN03+:
      ELSE IF(IMONITOR.EQ.16)THEN
        IF(ILEVEL.EQ.0)WRITE(IOUTM,4000)GS,ESC,'OW `` @@'
        IF(ILEVEL.EQ.1)WRITE(IOUTM,4000)GS,ESC,'OW b` @@'
        IF(ILEVEL.EQ.2)WRITE(IOUTM,4000)GS,ESC,'OW a` @@'

C Generic terminal
      ELSE IF( IMONITOR .EQ. 17 )THEN
        CALL GENERIC_PLOT_MODE(ILEVEL)

C X Window display
      ELSE IF(IMONITOR.EQ.18)THEN
        CALL XVST_PLOT_MODE(ILEVEL)
      ENDIF

      RETURN
      END
