/* xvst_alpha: puts keyboard focus into the terminal window    FWJ */
/*                                                                 */
/* Modified 27-Jan-92 by FWJ: only sets focus if enabled by the    */
/* Xdefaults flag TRIUMF.appSetFocus.                              */
/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#include <stdio.h>
#include <X11/Xlib.h>

#ifdef VMS
#define xvst_focus_ xvst_focus
#define plotmonitor_ plotmonitor
#define xvst_alpha_ xvst_alpha
#endif

Display *dpy;
Window t_window;

/* COMMON BLOCK - AUTOFOCUS SWITCH */
struct { Bool appsetfocus; } xvst_focus_;

/* COMMON BLOCK - MONITOR */
struct { int imonitor,ioutm; } plotmonitor_;

void xvst_alpha_()
{
   XWindowAttributes window_attributes;

   if (plotmonitor_.imonitor != 18 || dpy == NULL) return;
   if (!xvst_focus_.appsetfocus) return;

   if (XGetWindowAttributes(dpy,t_window,&window_attributes))
      if (window_attributes.map_state != IsUnmapped) {
         XSetInputFocus(dpy,t_window,RevertToNone,CurrentTime);
         XFlush(dpy);
      }
}
