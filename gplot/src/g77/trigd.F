      REAL*4 FUNCTION COSD( A )
      REAL*4 A
      REAL*4 DTOR /0.017453292519943/
      COSD = COS(A*DTOR)
      RETURN
      END

      REAL*4 FUNCTION SIND( A )
      REAL*4 A
      REAL*4 DTOR /0.017453292519943/
      SIND = SIN(A*DTOR)
      RETURN
      END

      REAL*4 FUNCTION TAND( A )
      REAL*4 A
      REAL*4 DTOR /0.017453292519943/
      TAND = TAN(A*DTOR)
      RETURN
      END

      REAL*4 FUNCTION ACOSD( A )
      REAL*4 A
      REAL*4 RTOD /57.295779513082/
      ACOSD = RTOD * ACOS(A)
      RETURN
      END

      REAL*4 FUNCTION ASIND( A )
      REAL*4 A
      REAL*4 RTOD /57.295779513082/
      ASIND = RTOD*ASIN(A)
      RETURN
      END

      REAL*4 FUNCTION ATAND( A )
      REAL*4 A
      REAL*4 RTOD /57.295779513082/
      ATAND = RTOD*ATAN(A)
      RETURN
      END

      REAL*4 FUNCTION ATAN2D( A,B )
      REAL*4 A,B
      REAL*4 RTOD /57.295779513082/
      ATAN2D =  RTOD*ATAN2(A,B)
      RETURN
      END

      REAL*8 FUNCTION COSD8( A )
      REAL*8 A
      REAL*8 DTOR /0.017453292519943/
      COSD8 = COS(A*DTOR)
      RETURN
      END

      REAL*8 FUNCTION SIND8( A )
      REAL*8 A
      REAL*8 DTOR /0.017453292519943/
      SIND8 = SIN(A*DTOR)
      RETURN
      END

      REAL*8 FUNCTION TAND8( A )
      REAL*8 A
      REAL*8 DTOR /0.017453292519943/
      TAND8 = TAN(A*DTOR)
      RETURN
      END

      REAL*8 FUNCTION ACOSD8( A )
      REAL*8 A
      REAL*8 RTOD /57.295779513082/
      ACOSD8 = RTOD * ACOS(A)
      RETURN
      END

      REAL*8 FUNCTION ASIND8( A )
      REAL*8 A
      REAL*8 RTOD /57.295779513082/
      ASIND8 = RTOD*ASIN(A)
      RETURN
      END

      REAL*8 FUNCTION ATAND8( A )
      REAL*8 A
      REAL*8 RTOD /57.295779513082/
      ATAND8 = RTOD*ATAN(A)
      RETURN
      END

      REAL*8 FUNCTION ATAN2D8( A,B )
      REAL*8 A,B
      REAL*8 RTOD /57.295779513082/
      ATAN2D8 =  RTOD*ATAN2(A,B)
      RETURN
      END
