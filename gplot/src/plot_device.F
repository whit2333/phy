      SUBROUTINE PLOT_DEVICE(IDEVICE,IOUTM)
C
C  reqd. KOSTL: routines - HCOPYRANG,MONITRANG
C
C=================================================================
C=================================================================
C==                                                             ==
C==   PLOT_DEVICE: Sets the plot device to "IDEVICE" and the    ==
C==                screen output unit to "IOUTM".               ==
C==                                                             ==
C==   Written by Arthur Haynes, TRIUMF U.B.C., July 8, 1981.    ==
C==                                                             ==
C==   Input  Parameters: IDEVICE,IOUTM (I*4).                   ==
C==                                                             ==
C==   Parameter Definitions:                                    ==
C==   --------- -----------                                     ==
C==                                                             ==
C==   IDEVICE: Device type. (Default: 1).                       ==
C==                                                             ==
C==        |IDEVICE| = 1: VT640 or Tektronix 4010/12 terminals. ==
C==                       0 <= IX <= 639; 0 <= IY <= 479.       ==
C==                       On the hardcopy: IX is horizontal.    ==
C==        |IDEVICE| = 2: VT640 or Tektronix 4010/12 terminals. ==
C==                       0 <= IX <= 1023; 0 <= IY <= 779.      ==
C==                       On the hardcopy: IX is vertical.      ==
C==        |IDEVICE| = 3: VT640 or Tektronix 4010/12 terminals. ==
C==                       0 <= IX <= 2047; 0 <= IY<= 785.       ==
C==                       On the hardcopy: IX is vertical.      ==
C==        |IDEVICE| = 4: VS11 emulating a VT640 or Tek 4010.   ==
C==                       0 <= IX <= 639; 0 <= IY <= 479.       ==
C==                       On the hardcopy: IX is vertical.      ==
C==         IDEVICE  = 0: Hardcopy only. IX is vertical.        ==
C==                       0 <= IX <= 2047; 0 <= IY <= 785.      ==
C==                                                             ==
C==        If IDEVICE is positive then a screen plot is produced==
C==        and a hardcopy which is a bit map image of the screen==
C==        in printronix format (6 bits per byte) is also       ==
C==        produced.                                            ==
C==        If IDEVICE is negative then only the hardcopy plot is==
C==        done.                                                ==
C==        If IDEVICE is zero then only the hardcopy plot is    ==
C==        produced.                                            ==
C==                                                             ==
C==        When PLOT_DEVICE is called, the hardcopy and screen  ==
C==        are not immediately cleared for that device, but are ==
C==        cleared the next time PLOT_I is called. After a call ==
C==        to PLOT_DEVICE the hardcopy is inaccessible until a  ==
C==        call to PLOT_I is performed.                         ==
C==        Since there is only one hardcopy bit_map_array, only ==
C==        one device can be plotted on at a time, if the user  ==
C==        wants the hardcopy.                                  ==
C==                                                             ==
C==   IOUTM: Output unit for the screen plot. (Default: 6).     ==
C==                                                             ==
C=================================================================
C=================================================================
      COMMON /PLOT_CLEAR/ CLEAR
      LOGICAL CLEAR
      DATA NDEVICE/4/
      REAL XMINH(0:4)/5*0./,XMAXH(0:4)/2047.,479.,1023.,2047.,479./
      REAL YMINH(0:4)/5*0./,YMAXH(0:4)/785.,639.,779.,785.,639./
      REAL XMINHP(0:4)/5*0./,XMAXHP(0:4)/2047.,639.,1023.,2047.,639./
      REAL YMINHP(0:4)/5*0./,YMAXHP(0:4)/785.,479.,779.,785.,479./
      INTEGER IORIENTH(0:4)/1,-1,1,1,-1/,IMONITOR(0:4)/0,1,1,1,3/
      REAL XMINM(0:3)/4*0./,YMINM(0:3)/4*0./
      REAL XMAXM(0:3)/0.,639.,1023.,511./,YMAXM(0:3)/0.,479.,779.,479./
C=================================================================
C==   Check the validity of the plot device number "IDEVICE".   ==
C=================================================================
      IF(IABS(IDEVICE).GT.NDEVICE)THEN
         WRITE(6,10)IDEVICE
10       FORMAT(' ***Error*** in PLOT_DEVICE: Invalid device type: ',
     *          I5)
         RETURN
      ENDIF
      IADEV=IABS(IDEVICE)
      IMON=IMONITOR(IADEV)
      IF(IDEVICE.LT.0)IMON=0
      CALL HARDCOPY_RANGE(XMINH(IADEV),XMAXH(IADEV),YMINH(IADEV),
     * YMAXH(IADEV),XMINHP(IADEV),XMAXHP(IADEV),YMINHP(IADEV),
     * YMAXHP(IADEV),IORIENTH(IADEV))
      CALL MONITORRANGE(IMON,IOUTM,
     *                   XMINH(IADEV),XMAXH(IADEV),
     *                   YMINH(IADEV),YMAXH(IADEV),
     *                   XMINM(IMON),XMAXM(IMON),
     *                   YMINM(IMON),YMAXM(IMON),
     *                   IORIENTH(IADEV))
C=================================================================
C==   Set the clear flag on so that the next time PLOT_I is     ==
C==   called the screen will be cleared.                        ==
C=================================================================
      CLEAR=.TRUE.
      RETURN
      END

