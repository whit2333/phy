      SUBROUTINE FINDST(ARRAY,LEN,STRING,NUMB,ISTART,IFINIS,*,*)
C
C     LIBRARY-ROUTINE
C
C                                                29/JULY/1980
C                                                C.J. KOST SIN
C
C     reqd. routines - NONE
C
C=================================================================
C=================================================================
C==   THIS ROUTINE SEARCHES A GIVEN ARRAY FOR THE FIRST         ==
C==   OCCURRENCE OF A GIVEN CHARACTER STRING. THIS ROUTINE IS   ==
C==   IDENTICAL IN ACTION AND CALLING SEQUENCE TO A ROUTINE     ==
C==   IN THE U.B.C. SYSTEM LIBRARY,  EXCEPT FOR PROVISO         ==
C==   BELOW.                                                    ==
C==                                                             ==
C==   WRITTEN BY LAURENCE TORHSER 7 MAY 1979 FOR TRIUMF.        ==
C==                                                             ==
C==   INPUT PARAMETERS: ARRAY(LEN),STRING(NUMB) (L*1);          ==
C==        LEN,NUMB,ISTART,IFINIS (I*4);                        ==
C==   OUTPUT PARAMETER: IFINIS (I*4);                           ==
C==                                                             ==
C==   'ARRAY' IS THE MEMORY REGION TO BE SEARCHED.              ==
C==   'STRING' IS REGION IN MEMORY CONTAINING THE CHARACTER     ==
C==        STRING TO BE SEARCHED FOR.                           ==
C==   'ISTART' IS THE POSITION IN 'ARRAY' AT WHICH TO BEGIN     ==
C==        SEARCHING.                                           ==
C==   'IFINIS' IS SET, UPON EXIT, TO BE POSITION OF THE         ==
C==        FIRST CHARACTER OF THE FIRST OCCURRENCE OF           ==
C==        'STRING' IN 'ARRAY'.  IF 'STRING' IS NOT FOUND,      ==
C==        'IFINIS' IS SET TO ZERO.                             ==
C==   NOTES: A RETURN 1 IS TAKEN IF THE CHARACTER STRING IS     ==
C==        NOT FOUND.                                           ==
C==        IF 'NUMB'=0 ON THE FIRST CALL, A RETURN 2 WILL BE    ==
C==        TAKEN.  IF, HOWEVER, NUMB=0 ON A SUBSEQUENT CALL,    ==
C==        'NUMB' WILL TAKE ON ITS VALUE FROM THE PRECEDING     ==
C==        CALL.                                                ==
C==        A RETURN 2 WILL ALWAYS BE TAKEN IF 'ISTART'<=0,      ==
C==        'ISTART'>'LEN', OR 'NUMB' < 0.                       ==
C==                                                             ==
C==   PROVISO: FURTHER REMARKS ON THE PRECEDING NOTE. WITH THE  ==
C==        SYSTEM ROUTINE, NOT ONLY IS 'NUMB' SAVED FROM        ==
C==        BUT SO IS THE PREVIOUS VALUE OF 'STRING'.            ==
C==        HENCE, THE APPROPRIATE 'STRING' SHOULD BE SPECIFIED  ==
C==        ON EVERY CALL OF THE PRESENT ROUTINE, CONTRARY TO THE==
C==        SITUATION WITH THE SYSTEM ROUTINE.                   ==

      BYTE ARRAY(LEN), STRING(NUMB),LA(4),LS(4)
      INTEGER NUMBS
      DATA NUMBS /0/
      EQUIVALENCE (IA,LA(1)),(IS,LS(1))
CCC
      IFINIS = 0
      IF( NUMB .NE. 0 )NUMBS = NUMB
      IF( ISTART.LE.0 .OR. ISTART.GT.LEN .OR. NUMBS.LE.0 )RETURN 2
      IEND = LEN-NUMBS+1
      I = ISTART-1
   10 I = I+1
      IF( I .GT. IEND )RETURN 1
      IF( ARRAY(I) .EQ. STRING(1) )THEN
        DO J = 2, NUMBS
          IF( ARRAY(I-1+J) .NE. STRING(J) )GO TO 10
        END DO
        IFINIS = I
        RETURN
      END IF
      GO TO 10
      END
