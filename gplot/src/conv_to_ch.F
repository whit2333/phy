      SUBROUTINE CONVERT_TO_CHAR(BUFF,LEN,CHAR)
C
C     reqd. KOSTL: routines - MOVEC
C
C==================================================================
C== CONVERT_TO_CHAR moves the character string contained in the  ==
C== non-CHARACTER (i.e. LOGICAL, REAL or INTEGER) array BUFF into==
C== the CHARACTER variable CHAR. The length of the character string
C== (in bytes) to be copied is passed in LEN.                    ==
C==================================================================
      LOGICAL*1 BUFF(LEN)
      CHARACTER*(*) CHAR
      CHAR=' '
#ifdef VMS
      CALL MOVEC(LEN,BUFF,%REF(CHAR))
#else
      CALL MOVEC(LEN,BUFF,CHAR)
#endif
      RETURN
      END
