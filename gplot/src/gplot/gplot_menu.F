      SUBROUTINE GPLOT_MENU
C
C    Auxilliary routine for FUNCTION GETNAM
C
      CHARACTER STRING*132, GETLAB*123
C
      REAL*4          TABLE(103)
      COMMON /GPLOTC/ TABLE
C
      LOGICAL*1       DYNAM(103)
      COMMON /GDYNAM/ DYNAM
C
      CHARACTER*8     NAMES(103)
      COMMON /GNAMES/ NNAMES, NAMES
C
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS
C
      CALL TERM_WIDTH(132)
C
      STRING = ' MASK    = *********            | '//
     &          'BOX     = *********            | '//
     &          'CHARSZ  = *********  *********%| '//
     &          'CHARA   = *********  *********% '
      WRITE(STRING(12:20),170,ERR=3)TABLE(10)
3     WRITE(STRING(45:53),170,ERR=4)TABLE(11)
4     IF( DYNAM(12) )THEN
        WRITE(STRING(78:86),170,ERR=5)GETDYN(12)
5       WRITE(STRING(89:97),170,ERR=601)TABLE(12)
      ELSE
        WRITE(STRING(78:86),170,ERR=6)TABLE(12)
6       WRITE(STRING(89:97),170,ERR=601)GETPCNT(12)
      END IF
601   IF( DYNAM(13) )THEN
        WRITE(STRING(111:119),170,ERR=602)GETDYN(13)
602     WRITE(STRING(122:130),170,ERR=7)TABLE(13)
      ELSE
        WRITE(STRING(111:119),170,ERR=603)TABLE(13)
603     STRING(122:130) = '          '
      END IF
7     WRITE(IOUTS,190)STRING
      STRING = ' PMODE   = *********            | '//
     &          'ALIAS   = *********            | '//
     &          'HISTYP  = *********            | '//
     &          'LINTYP  = *********  *********  '
      WRITE(STRING(12:20),170,ERR=8)TABLE(14)
8     WRITE(STRING(45:53),170,ERR=9)TABLE(15)
9     WRITE(STRING(78:86),170,ERR=10)TABLE(16)
10    WRITE(STRING(111:119),170,ERR=11)TABLE(17)
11    WRITE(STRING(122:130),170,ERR=12)TABLE(18)
12    WRITE(IOUTS,190)STRING
      STRING = ' PTYPE   = *********            | '//
     &          'COLOUR  = *********            | '//
     &          'CURSOR  = *********            | '//
     &          'ERRBAR  = *********             '
      WRITE(STRING(12:20),170,ERR=13)TABLE(19)
13    WRITE(STRING(45:53),170,ERR=14)TABLE(20)
14    WRITE(STRING(78:86),170,ERR=15)TABLE(21)
15    WRITE(STRING(111:119),170,ERR=16)TABLE(22)
16    WRITE(IOUTS,190)STRING
      STRING = ' XLWIND  = *********  *********%| '//
     &          'XUWIND  = *********  *********%| '//
     &          'XLAXIS  = *********  *********%| '//
     &          'XUAXIS  = *********  *********% '
      I = TABLE(4)
      IF( DYNAM(I) )THEN
        WRITE(STRING(12:20),170,ERR=17)GETDYN(I)
17      WRITE(STRING(23:31),170,ERR=19)TABLE(I)
      ELSE
        WRITE(STRING(12:20),170,ERR=18)TABLE(I)
18      WRITE(STRING(23:31),170,ERR=19)GETPCNT(I)
      END IF
19    IF( DYNAM(I+1) )THEN
        WRITE(STRING(45:53),170,ERR=20)GETDYN(I+1)
20      WRITE(STRING(56:64),170,ERR=22)TABLE(I+1)
      ELSE
        WRITE(STRING(45:53),170,ERR=21)TABLE(I+1)
21      WRITE(STRING(56:64),170,ERR=22)GETPCNT(I+1)
      END IF
22    IF( DYNAM(I+2) )THEN
        WRITE(STRING(78:86),170,ERR=23)GETDYN(I+2)
23      WRITE(STRING(89:97),170,ERR=25)TABLE(I+2)
      ELSE
        WRITE(STRING(78:86),170,ERR=24)TABLE(I+2)
24      WRITE(STRING(89:97),170,ERR=25)GETPCNT(I+2)
      END IF
25    IF( DYNAM(I+3) )THEN
        WRITE(STRING(111:119),170,ERR=26)GETDYN(I+3)
26      WRITE(STRING(122:130),170,ERR=28)TABLE(I+3)
      ELSE
        WRITE(STRING(111:119),170,ERR=27)TABLE(I+3)
27      WRITE(STRING(122:130),170,ERR=28)GETPCNT(I+3)
      END IF
28    WRITE(IOUTS,190)STRING
      STRING = ' NXDIG   = *********            | '//
     &          'NXDEC   = *********            | '//
     &          'XPOW    = *********            | '//
     &          'XPAUTO  = *********             '
      WRITE(STRING(12:20),170,ERR=29)TABLE(I+4)
29    WRITE(STRING(45:53),170,ERR=30)TABLE(I+5)
30    WRITE(STRING(78:86),170,ERR=31)TABLE(I+6)
31    WRITE(STRING(111:119),170,ERR=32)TABLE(I+7)
32    WRITE(IOUTS,190)STRING
      STRING = ' XNUMSZ  = *********  *********%| '//
     &          'XLABSZ  = *********  *********%| '//
     &          'XTICL   = *********  *********%| '//
     &          'XTICS   = *********  *********% '
      IF( DYNAM(I+8) )THEN
        WRITE(STRING(12:20),170,ERR=33)GETDYN(I+8)
33      WRITE(STRING(23:31),170,ERR=35)TABLE(I+8)
      ELSE
        WRITE(STRING(12:20),170,ERR=34)TABLE(I+8)
34      WRITE(STRING(23:31),170,ERR=35)GETPCNT(I+8)
      END IF
35    IF( DYNAM(I+9) )THEN
        WRITE(STRING(45:53),170,ERR=36)GETDYN(I+9)
36      WRITE(STRING(56:64),170,ERR=38)TABLE(I+9)
      ELSE
        WRITE(STRING(45:53),170,ERR=37)TABLE(I+9)
37      WRITE(STRING(56:64),170,ERR=38)GETPCNT(I+9)
      END IF
38    IF( DYNAM(I+10) )THEN
        WRITE(STRING(78:86),170,ERR=39)GETDYN(I+10)
39      WRITE(STRING(89:97),170,ERR=41)TABLE(I+10)
      ELSE
        WRITE(STRING(78:86),170,ERR=40)TABLE(I+10)
40      WRITE(STRING(89:97),170,ERR=41)GETPCNT(I+10)
      END IF
41    IF( DYNAM(I+11) )THEN
        WRITE(STRING(111:119),170,ERR=42)GETDYN(I+11)
42      WRITE(STRING(122:130),170,ERR=44)TABLE(I+11)
      ELSE
        WRITE(STRING(111:119),170,ERR=43)TABLE(I+11)
43      WRITE(STRING(122:130),170,ERR=44)GETPCNT(I+11)
      END IF
44    WRITE(IOUTS,190)STRING
      STRING = ' XTICA   = *********  *********%| '//
     &          'XCROSS  = *********            | '//
     &          'XMIN    = *********  ********* | '//
     &          'XMAX    = *********  *********  '
      IF( DYNAM(I+12) )THEN
        WRITE(STRING(12:20),170,ERR=45)GETDYN(I+12)
45      WRITE(STRING(23:31),170,ERR=47)TABLE(I+12)
      ELSE
        WRITE(STRING(12:20),170,ERR=46)TABLE(I+12)
46      STRING(23:31) = '          '
      END IF
47    WRITE(STRING(45:53),170,ERR=48)TABLE(I+13)
48    WRITE(STRING(78:86),170,ERR=485)TABLE(I+14)
485   WRITE(STRING(89:97),170,ERR=49)TABLE(I+35)     ! XVMIN
49    WRITE(STRING(111:119),170,ERR=495)TABLE(I+15)
495   WRITE(STRING(122:130),170,ERR=50)TABLE(I+36)   ! XVMAX
50    WRITE(IOUTS,190)STRING
      STRING = ' NLXINC  = *********            | '//
     &          'NSXINC  = *********            | '//
     &          'XAUTO   = *********            | '//
     &          'XITICL  = *********  *********% '
      WRITE(STRING(12:20),170,ERR=51)TABLE(I+16)
51    WRITE(STRING(45:53),170,ERR=52)TABLE(I+17)
52    WRITE(STRING(78:86),170,ERR=53)TABLE(I+18)
53    IF( DYNAM(I+19) )THEN
        WRITE(STRING(111:119),170,ERR=54)GETDYN(I+19)
54      WRITE(STRING(122:130),170,ERR=56)TABLE(I+19)
      ELSE
        WRITE(STRING(111:119),170,ERR=55)TABLE(I+19)
55      WRITE(STRING(122:130),170,ERR=56)GETPCNT(I+19)
      END IF
56    WRITE(IOUTS,190)STRING
      STRING = ' XITICA  = *********  *********%| '//
     &          'XNUMA   = *********  *********%| '//
     &          'XTICTP  = *********            | '//
     &          'BOTTIC  = *********             '
      IF( DYNAM(I+20) )THEN
        WRITE(STRING(12:20),170,ERR=57)GETDYN(I+20)
57      WRITE(STRING(23:31),170,ERR=59)TABLE(I+20)
      ELSE
        WRITE(STRING(12:20),170,ERR=58)TABLE(I+20)
58      STRING(23:31) = '          '
      END IF
59    IF( DYNAM(I+21) )THEN
        WRITE(STRING(45:53),170,ERR=60)GETDYN(I+21)
60      WRITE(STRING(56:64),170,ERR=62)TABLE(I+21)
      ELSE
        WRITE(STRING(45:53),170,ERR=61)TABLE(I+21)
61      STRING(56:64) = '          '
      END IF
62    WRITE(STRING(78:86),170,ERR=63)TABLE(I+22)
63    WRITE(STRING(111:119),170,ERR=64)TABLE(I+23)
64    WRITE(IOUTS,190)STRING
      STRING = ' BOTNUM  = *********            | '//
     &          'TOPTIC  = *********            | '//
     &          'TOPNUM  = *********            | '//
     &          'NXGRID  = *********             '
      WRITE(STRING(12:20),170,ERR=65)TABLE(I+24)
65    WRITE(STRING(45:53),170,ERR=66)TABLE(I+25)
66    WRITE(STRING(78:86),170,ERR=67)TABLE(I+26)
67    WRITE(STRING(111:119),170,ERR=68)TABLE(I+27)
68    WRITE(IOUTS,190)STRING
      STRING = ' XAXIS   = *********            | '//
     &          'XAXISA  = *********            | '//
     &          'XLOG    = *********            | '//
     &          'XZERO   = *********             '
      WRITE(STRING(12:20),170,ERR=69)TABLE(I+28)
69    WRITE(STRING(45:53),170,ERR=70)TABLE(I+29)
70    WRITE(STRING(78:86),170,ERR=71)TABLE(I+30)
71    WRITE(STRING(111:119),170,ERR=72)TABLE(I+31)
72    WRITE(IOUTS,190)STRING
      STRING = ' YLWIND  = *********  *********%| '//
     &          'YUWIND  = *********  *********%| '//
     &          'YLAXIS  = *********  *********%| '//
     &          'YUAXIS  = *********  *********% '
      I = TABLE(6)
      IF( DYNAM(I) )THEN
        WRITE(STRING(12:20),170,ERR=73)GETDYN(I)
73      WRITE(STRING(23:31),170,ERR=75)TABLE(I)
      ELSE
        WRITE(STRING(12:20),170,ERR=74)TABLE(I)
74      WRITE(STRING(23:31),170,ERR=75)GETPCNT(I)
      END IF
75    IF( DYNAM(I+1) )THEN
        WRITE(STRING(45:53),170,ERR=76)GETDYN(I+1)
76      WRITE(STRING(56:64),170,ERR=78)TABLE(I+1)
      ELSE
        WRITE(STRING(45:53),170,ERR=77)TABLE(I+1)
77      WRITE(STRING(56:64),170,ERR=78)GETPCNT(I+1)
      END IF
78    IF( DYNAM(I+2) )THEN
        WRITE(STRING(78:86),170,ERR=79)GETDYN(I+2)
79      WRITE(STRING(89:97),170,ERR=81)TABLE(I+2)
      ELSE
        WRITE(STRING(78:86),170,ERR=80)TABLE(I+2)
80      WRITE(STRING(89:97),170,ERR=81)GETPCNT(I+2)
      END IF
81    IF( DYNAM(I+3) )THEN
        WRITE(STRING(111:119),170,ERR=82)GETDYN(I+3)
82      WRITE(STRING(122:130),170,ERR=84)TABLE(I+3)
      ELSE
        WRITE(STRING(111:119),170,ERR=83)TABLE(I+3)
83      WRITE(STRING(122:130),170,ERR=84)TABLE(I+3)
      END IF
84    WRITE(IOUTS,190)STRING
      STRING = ' NYDIG   = *********            | '//
     &          'NYDEC   = *********            | '//
     &          'YPOW    = *********            | '//
     &          'YPAUTO  = *********             '
      WRITE(STRING(12:20),170,ERR=85)TABLE(I+4)
85    WRITE(STRING(45:53),170,ERR=86)TABLE(I+5)
86    WRITE(STRING(78:86),170,ERR=87)TABLE(I+6)
87    WRITE(STRING(111:119),170,ERR=88)TABLE(I+7)
88    WRITE(IOUTS,190)STRING
      STRING = ' YNUMSZ  = *********  *********%| '//
     &          'YLABSZ  = *********  *********%| '//
     &          'YTICL   = *********  *********%| '//
     &          'YTICS   = *********  *********% '
      IF( DYNAM(I+8) )THEN
        WRITE(STRING(12:20),170,ERR=89)GETDYN(I+8)
89      WRITE(STRING(23:31),170,ERR=91)TABLE(I+8)
      ELSE
        WRITE(STRING(12:20),170,ERR=90)TABLE(I+8)
90      WRITE(STRING(23:31),170,ERR=91)GETPCNT(I+8)
      END IF
91    IF( DYNAM(I+9) )THEN
        WRITE(STRING(45:53),170,ERR=92)GETDYN(I+9)
92      WRITE(STRING(56:64),170,ERR=94)TABLE(I+9)
      ELSE
        WRITE(STRING(45:53),170,ERR=93)TABLE(I+9)
93      WRITE(STRING(56:64),170,ERR=94)GETPCNT(I+9)
      END IF
94    IF( DYNAM(I+10) )THEN
        WRITE(STRING(78:86),170,ERR=95)GETDYN(I+10)
95      WRITE(STRING(89:97),170,ERR=97)TABLE(I+10)
      ELSE
        WRITE(STRING(78:86),170,ERR=96)TABLE(I+10)
96      WRITE(STRING(89:97),170,ERR=97)GETPCNT(I+10)
      END IF
97    IF( DYNAM(I+11) )THEN
        WRITE(STRING(111:119),170,ERR=98)GETDYN(I+11)
98      WRITE(STRING(122:130),170,ERR=100)TABLE(I+11)
      ELSE
        WRITE(STRING(111:119),170,ERR=99)TABLE(I+11)
99      WRITE(STRING(122:130),170,ERR=100)GETPCNT(I+11)
      END IF
100   WRITE(IOUTS,190)STRING
      STRING = ' YTICA   = *********  *********%| '//
     &          'YCROSS  = *********            | '//
     &          'YMIN    = *********  ********* | '//
     &          'YMAX    = *********  *********  '
      IF( DYNAM(I+12) )THEN
        WRITE(STRING(12:20),170,ERR=101)GETDYN(I+12)
101     WRITE(STRING(23:31),170,ERR=103)TABLE(I+12)
      ELSE
        WRITE(STRING(12:20),170,ERR=102)TABLE(I+12)
102     STRING(23:31) = '          '
      END IF
103   WRITE(STRING(45:53),170,ERR=104)TABLE(I+13)
104   WRITE(STRING(78:86),170,ERR=1045)TABLE(I+14)
1045  WRITE(STRING(89:97),170,ERR=105)TABLE(I+35)     ! YVMIN
105   WRITE(STRING(111:119),170,ERR=1055)TABLE(I+15)
1055  WRITE(STRING(122:130),170,ERR=106)TABLE(I+36)   ! YVMAX
106   WRITE(IOUTS,190)STRING
      STRING = ' NLYINC  = *********            | '//
     &          'NSYINC  = *********            | '//
     &          'YAUTO   = *********            | '//
     &          'YITICL  = *********  *********% '
      WRITE(STRING(12:20),170,ERR=107)TABLE(I+16)
107   WRITE(STRING(45:53),170,ERR=108)TABLE(I+17)
108   WRITE(STRING(78:86),170,ERR=109)TABLE(I+18)
109   IF( DYNAM(I+19) )THEN
        WRITE(STRING(111:119),170,ERR=110)GETDYN(I+19)
110     WRITE(STRING(122:130),170,ERR=112)TABLE(I+19)
      ELSE
        WRITE(STRING(111:119),170,ERR=111)TABLE(I+19)
111     WRITE(STRING(122:130),170,ERR=112)GETPCNT(I+19)
      END IF
112   WRITE(IOUTS,190)STRING
      STRING = ' YITICA  = *********  *********%| '//
     &          'YNUMA   = *********  *********%| '//
     &          'YTICTP  = *********            | '//
     &          'LEFTIC  = *********             '
      IF( DYNAM(I+20) )THEN
        WRITE(STRING(12:20),170,ERR=113)GETDYN(I+20)
113     WRITE(STRING(23:31),170,ERR=115)TABLE(I+20)
      ELSE
        WRITE(STRING(12:20),170,ERR=114)TABLE(I+20)
114     STRING(23:31) = '          '
      END IF
115   IF( DYNAM(I+21) )THEN
        WRITE(STRING(45:53),170,ERR=116)GETDYN(I+21)
116     WRITE(STRING(56:64),170,ERR=118)TABLE(I+21)
      ELSE
        WRITE(STRING(45:53),170,ERR=117)TABLE(I+21)
117     STRING(56:64) = '          '
      END IF
118   WRITE(STRING(78:86),170,ERR=119)TABLE(I+22)
119   WRITE(STRING(111:119),170,ERR=120)TABLE(I+23)
120   WRITE(IOUTS,190)STRING
      STRING = ' LEFNUM  = *********            | '//
     &          'RITTIC  = *********            | '//
     &          'RITNUM  = *********            | '//
     &          'NYGRID  = *********             '
      WRITE(STRING(12:20),170,ERR=121)TABLE(I+24)
121   WRITE(STRING(45:53),170,ERR=122)TABLE(I+25)
122   WRITE(STRING(78:86),170,ERR=123)TABLE(I+26)
123   WRITE(STRING(111:119),170,ERR=124)TABLE(I+27)
124   WRITE(IOUTS,190)STRING
      STRING = ' YAXIS   = *********            | '//
     &          'YAXISA  = *********            | '//
     &          'YLOG    = *********            | '//
     &          'YZERO   = *********             '
      WRITE(STRING(12:20),170,ERR=125)TABLE(I+28)
125   WRITE(STRING(45:53),170,ERR=126)TABLE(I+29)
126   WRITE(STRING(78:86),170,ERR=127)TABLE(I+30)
127   WRITE(STRING(111:119),170,ERR=128)TABLE(I+31)
128   WRITE(IOUTS,190)STRING
      STRING = ' TXTHIT  = *********  *********%| '//
     &          'TXTANG  = *********            | '//
     &          'XLOC    = *********  *********%| '//
     &          'YLOC    = *********  *********% '
      I = TABLE(8)
      IF( DYNAM(I) )THEN
        WRITE(STRING(12:20),170,ERR=129)GETDYN(I)
129     WRITE(STRING(23:31),170,ERR=131)TABLE(I)
      ELSE
        WRITE(STRING(12:20),170,ERR=130)TABLE(I)
130     WRITE(STRING(23:31),170,ERR=131)GETPCNT(I)
      END IF
131   WRITE(STRING(45:53),170,ERR=132)TABLE(I+1)
132   IF( DYNAM(I+2) )THEN
        WRITE(STRING(78:86),170,ERR=133)GETDYN(I+2)
133     WRITE(STRING(89:97),170,ERR=135)TABLE(I+2)
      ELSE
        WRITE(STRING(78:86),170,ERR=134)TABLE(I+2)
134     WRITE(STRING(89:97),170,ERR=135)GETPCNT(I+2)
      END IF
135   IF( DYNAM(I+3) )THEN
        WRITE(STRING(111:119),170,ERR=136)GETDYN(I+3)
136     WRITE(STRING(122:130),170,ERR=138)TABLE(I+3)
      ELSE
        WRITE(STRING(111:119),170,ERR=137)TABLE(I+3)
137     WRITE(STRING(122:130),170,ERR=138)GETPCNT(I+3)
      END IF
138   WRITE(IOUTS,190)STRING
      WRITE(IOUTS,190)'XLABEL = '//GETLAB('XLABEL')
      WRITE(IOUTS,190)'YLABEL = '//GETLAB('YLABEL')
170   FORMAT(F9.3)
190   FORMAT(' ',A)
200   FORMAT(' ',132('-'))
      RETURN
      END
