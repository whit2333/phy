      SUBROUTINE GPLOT_TXT( FLAG, TLINE )
C
      CHARACTER*(*) TLINE
      LOGICAL*4     FLAG

      REAL*4 TABLE(103)
      COMMON /GPLOTC/ TABLE

      INTEGER*4 ILEV
      COMMON /PLOT_LEVEL/ ILEV

      INTEGER*4 ICOLR1, ICOLR2
      COMMON /PLOT_COLOURS/ ICOLR1, ICOLR2

      LOGICAL*4 BOLD
      COMMON /GPLOT_BOLDING/ BOLD

      LOGICAL*4 ITALICS
      REAL*4    ANGL, ANGL_OLD
      COMMON /GPLOT_PSALPH/ ITALICS, ANGL, ANGL_OLD

      INTEGER*4 NHATCH(2)
      COMMON /PSYM_HATCHING/ NHATCH

      LOGICAL*4 ASIS
      COMMON /MODE/ ASIS

      INTEGER*4 IINS
      COMMON /PLOT_INPUT_UNIT/  IINS

      INTEGER*4 IOUTS
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS

      LOGICAL*4 DEFAULTS
      COMMON /GPLOT_TEXT/ DEFAULTS

      CHARACTER*2 CTRL
      COMMON /GPLOT_CONTROLS/ CTRL

      LOGICAL*4 Q_TYPED
      COMMON /TEXT_Q_TYPED/ Q_TYPED

      CHARACTER*60  FONT, FONT_SAVE, GETLAB
      CHARACTER*4   REV, NORM
      CHARACTER*2   JUST
      BYTE          CODE  ! modified by J.Chuma 21Mar97 for g77
      LOGICAL*4     FLAG1

      CHARACTER ESC, CCODE
CCC
      ESC = CHAR(27)
      IF( TABLE(1) .EQ. 0.0 )CALL GPLOTI
      REV = ESC//'[7m'
      NORM = ESC//'[0m'
      FLAG1 = ASIS
      ASIS = FLAG
      YLWIND = GETNAM('YLWIND')
      YUWIND = GETNAM('YUWIND')
      XLWIND = GETNAM('XLWIND')
      XUWIND = GETNAM('XUWIND')
      TXTHIT = GETNAM('TXTHIT')
      TXTANG = GETNAM('TXTANG')
      LENGTH = LENSIG(TLINE)
      FONT_SAVE = GETLAB('FONT')
      LENFONT_SAVE = LENSIG(FONT_SAVE)
      FONT = FONT_SAVE
      LENFONT = LENFONT_SAVE
      TXTHIT_SAVE = TXTHIT
      ICOL_SAVE = ICOLR1
      IPEN_SAVE = ICOLR2
      CURSOR = FLOAT(IFIX(GETNAM('CURSOR')))
      IF( CURSOR .LE. 0.0 )THEN ! Use XLOC, YLOC to position text string
        XTXT = GETNAM('XLOC')
        YTXT = GETNAM('YLOC')
      ELSE                      ! Use crosshair to position text string
        XLOC = GETNAM('XLOC')
        YLOC = GETNAM('YLOC')
        XLAST = XLOC
        YLAST = YLOC
        CALL TRANSPARENT_MODE(0)
        WRITE(IOUTS,20)REV//' M '//NORM//'Menu'//
     &   '   '//REV//' Q '//NORM//'Quit'//
     &   '   '//REV//' / '//NORM//'Clear alpha-numeric screen'
        Q_TYPED = .FALSE.
10      CALL CROSSHAIR_R(XTXT,YTXT,CODE,XLAST,YLAST)
        CCODE = CHAR(CODE)
        IF( (CCODE.EQ.'Q') .OR. (CCODE.EQ.'q') )THEN ! don't plot, quit
          TLINE = ' '
          Q_TYPED = .TRUE.
          CALL TRANSPARENT_MODE(0)
          GO TO 999
        ELSE IF( CCODE .EQ. '/' )THEN
          CALL TRANSPARENT_MODE(0)
          CALL CLTRANS
          XLAST = XTXT
          YLAST = YTXT
          GO TO 10
        ELSE IF( (CCODE.EQ.'L') .OR. (CCODE.EQ.'l') )THEN
          CURSOR = 1.
        ELSE IF( (CCODE.EQ.'C') .OR. (CCODE.EQ.'c') )THEN
          CURSOR = 2.
        ELSE IF( (CCODE.EQ.'R') .OR. (CCODE.EQ.'r') )THEN
          CURSOR = 3.
        ELSE IF( (CCODE.EQ.'U') .OR. (CCODE.EQ.'u') )THEN
          CURSOR = 4.
        ELSE IF( (CCODE.EQ.'D') .OR. (CCODE.EQ.'d') )THEN
          CURSOR = 5.
        ELSE IF( (CCODE.EQ.'V') .OR. (CCODE.EQ.'v') )THEN
          CURSOR = 6.
        ELSE IF( (CCODE.EQ.'J') .OR. (CCODE.EQ.'j') )THEN
          CALL TRANSPARENT_MODE(0)
          CALL XVST_ALPHA
14        WRITE(IOUTS,20)REV//' UL '//NORM//'Upper  left'
          WRITE(IOUTS,20)REV//' CL '//NORM//'Centre left'
          WRITE(IOUTS,20)REV//' LL '//NORM//'Lower  left'
          WRITE(IOUTS,20)REV//' UC '//NORM//'Upper  centre'
          WRITE(IOUTS,20)REV//' CC '//NORM//'Centre centre'
          WRITE(IOUTS,20)REV//' LC '//NORM//'Lower  centre'
          WRITE(IOUTS,20)REV//' UR '//NORM//'Upper  right'
          WRITE(IOUTS,20)REV//' CR '//NORM//'Centre right'
          WRITE(IOUTS,20)REV//' LR '//NORM//'Lower  right'
          WRITE(IOUTS,20)REV//' LU '//NORM//'Lower left   at  90 deg'
          WRITE(IOUTS,20)REV//' LD '//NORM//'Lower left   at 270 deg'
          WRITE(IOUTS,20)REV//' CV '//NORM//'Lower centre at  90 deg'
141       WRITE(IOUTS,15)
15        FORMAT(' Enter two letter justification code >> ',$)
          READ(IINS,16,END=999,ERR=14)JUST
16        FORMAT(A2)
          CALL UPRCASE(JUST,JUST)

C    |CURSOR| = 1.   ==>   left justify the text string
C    |CURSOR| = 2.   ==>   center the text string
C    |CURSOR| = 3.   ==>   right justify the text string
C    |CURSOR| = 4.   ==>   write the string at 90 degrees
C    |CURSOR| = 5.   ==>   write the string at 270 degrees
C    |CURSOR| = 6.   ==>   write the string at 90 degrees and centred
C    |CURSOR| = 7.   ==>   left centre
C    |CURSOR| = 8.   ==>   left upper
C    |CURSOR| = 9.   ==>   centre centre
C    |CURSOR| = 10   ==>   centre upper
C    |CURSOR| = 11.  ==>   right centre
C    |CURSOR| = 12.  ==>   right upper

          IF( JUST .EQ. 'LL' )THEN
            CURSOR = 1.
          ELSE IF( JUST .EQ. 'CL' )THEN
            CURSOR = 7.
          ELSE IF( JUST .EQ. 'UL' )THEN
            CURSOR = 8.
          ELSE IF( JUST .EQ. 'LC' )THEN
            CURSOR = 2.
          ELSE IF( JUST .EQ. 'CC' )THEN
            CURSOR = 9.
          ELSE IF( JUST .EQ. 'UC' )THEN
            CURSOR = 10.
          ELSE IF( JUST .EQ. 'LR' )THEN
            CURSOR = 3.
          ELSE IF( JUST .EQ. 'CR' )THEN
            CURSOR = 11.
          ELSE IF( JUST .EQ. 'UR' )THEN
            CURSOR = 12.
          ELSE IF( JUST .EQ. 'LU' )THEN
            CURSOR = 4.
          ELSE IF( JUST .EQ. 'LD' )THEN
            CURSOR = 5.
          ELSE IF( JUST .EQ. 'CV' )THEN
            CURSOR = 6.
          ELSE
            GO TO 141
          END IF
        ELSE IF( (CCODE.EQ.'X') .OR. (CCODE.EQ.'x') )THEN
          XTXT = XLOC
          CURSOR = 2.
        ELSE IF( (CCODE.EQ.'Y') .OR. (CCODE.EQ.'y') )THEN
          YTXT = YLOC
          CURSOR = 1.
        ELSE IF( (CCODE.EQ.'M') .OR. (CCODE.EQ.'m') )THEN
          CALL TRANSPARENT_MODE(0)
17        WRITE(IOUTS,20)REV//' M '//NORM//'Menu'
          WRITE(IOUTS,20)REV//' Q '//NORM//'Quit'
          WRITE(IOUTS,20)REV//' / '//NORM//'Clear alpha-numeric screen'
          WRITE(IOUTS,20)REV//' L '//NORM//'Left  justify'
          WRITE(IOUTS,20)REV//' R '//NORM//'Right justify'
          WRITE(IOUTS,20)REV//' C '//NORM//'Centre'
          WRITE(IOUTS,20)REV//' U '//NORM//'Left justify at  90 deg'
          WRITE(IOUTS,20)REV//' D '//NORM//'Left justify at 270 deg'
          WRITE(IOUTS,20)REV//' V '//NORM//'Centre       at  90 deg'
          WRITE(IOUTS,20)REV//' J '//NORM//'Set justification'
          WRITE(IOUTS,20)REV//' X '//NORM//'Left justify and use XLOC'
          WRITE(IOUTS,20)REV//' Y '//NORM//'Left justify and use YLOC'
20        FORMAT(' ',A)
          XLAST = XTXT
          YLAST = YTXT
          GO TO 10
        END IF
        CALL SETNAM('CURSOR',CURSOR)     ! update CURSOR, XLOC and YLOC
        XLOC = (XTXT-XLWIND)/(XUWIND-XLWIND)*100.
        YLOC = (YTXT-YLWIND)/(YUWIND-YLWIND)*100.
        CALL SETNAM('%XLOC',XLOC)
        CALL SETNAM('%YLOC',YLOC)
      END IF
      ICURSOR = IFIX(ABS(CURSOR))
      CALL GPLOT_LINE(XTXT,YTXT,TXTHIT,TXTANG,TLINE,LENGTH,ICURSOR
     & ,FONT,LENFONT,*100)
      GO TO 200
100   DEFAULTS = .TRUE.
200   IF( DEFAULTS )THEN
        TEMP = TXTHIT_SAVE/(YUWIND-YLWIND)*100.
        CALL SETNAM('%TXTHIT',TEMP)
        CALL PFONT(FONT_SAVE(1:LENFONT_SAVE),0)
        BOLD = .FALSE.
        NHATCH(1) = 0
        NHATCH(2) = 0
        ITALICS = .FALSE.
        CTRL = '<>'
        ASIS = FLAG1
        IF( ((ICOL_SAVE.NE.ICOLR1).OR.(IPEN_SAVE.NE.ICOLR2))
     &    .AND. (ILEV.NE.1) )CALL PLOT_COLOR(ICOL_SAVE,IPEN_SAVE)
      ELSE
        TEMP = TXTHIT/(YUWIND-YLWIND)*100.
        CALL SETNAM('%TXTHIT',TEMP)
        CALL PFONT(FONT(1:LENFONT),0)
      END IF
999   CALL FLUSH_PLOT
      IF( CURSOR .GT. 0.0 )CALL XVST_ALPHA
      RETURN
      END
