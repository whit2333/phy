      SUBROUTINE GPLOT_CONTROL(PROMPT_IN,*)

      CHARACTER*(*) PROMPT_IN

      CHARACTER*255 CLINE, STRINGS(3), STR(2), PROMPT, GETLAB
      CHARACTER*60  COMMAND
      REAL*4        REALS(3), RLS(2)
      INTEGER*4     ITYPE(3), ITP(2)

C   LIB contains the font names
C   NFONT is the number of font names 
C   LIBFONT is the current font name

      PARAMETER (NFONTS=43)
      CHARACTER*20 LIB(NFONTS), LIBFONT
      COMMON /PALPHA_FONTS/ LIB, LIBFONT

      INTEGER*4 IFONT
      COMMON /FONT_NUMBER/ IFONT

      INTEGER*4 IINS
      COMMON /PLOT_INPUT_UNIT/  IINS

      INTEGER*4 IOUTS
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS

      DATA NPAGES / 1 /
CCC
      SAVE

      PROMPT = ' '
      PROMPT = PROMPT_IN
      LENP   = LENSIG(PROMPT)
      IF( LENP .EQ. 0 )THEN
        PROMPT = 'GPLOT CONTROL:'
        LENP   = 14
      END IF
      CALL TRANSPARENT_MODE(0)
      GO TO 20
   2  CALL CLTRANS
#ifdef VMS
      WRITE(IOUTS,10)' $ operating system command'
#else
      WRITE(IOUTS,10)' % operating system command'
#endif
      WRITE(IOUTS,10)' EDGR OPEN { file } | FRAME | CLOSE'
      WRITE(IOUTS,10)' SET { name { value }}'
      WRITE(IOUTS,10)' SAVE file'
      WRITE(IOUTS,10)' RESTORE file'
      WRITE(IOUTS,10)' TEXT string'
      WRITE(IOUTS,10)' XLABEL string'
      WRITE(IOUTS,10)' YLABEL string'
      WRITE(IOUTS,10)' PCHAR pch1 { pch2 }'
      WRITE(IOUTS,10)' MENU         SMENU        LMENU'
      WRITE(IOUTS,10)' HARDCOPY     CLEAR        ACLEAR'
      WRITE(IOUTS,10)' GPLOTI       FONT         REPLOT'
  10  FORMAT(A)
  20  WRITE(IOUTS,30)' '//PROMPT(1:LENP)//' '
  30  FORMAT(A,$)
      READ(IINS,10,END=1000,ERR=20)CLINE
      NLINE = LENSIG(CLINE)
      IF( NLINE .EQ. 0 )GO TO 1000
#ifdef VMS
      IF( CLINE(1:1) .EQ. '$' )THEN
        CALL DCL_CMD(CLINE(2:NLINE))
        GO TO 20
      END IF
#else
      IF( CLINE(1:1) .EQ. '%' )THEN
        CALL SYSTEM(CLINE(2:NLINE))
        GO TO 20
      END IF
#endif
      NFLD = 3
      CALL GPLOT_READLINE(CLINE,STRINGS,REALS,ITYPE,NFLD)
      IF( NFLD .EQ. 0 )GO TO 1000
      LENS = LENSIG(STRINGS(1))
      CALL UPRCASE(STRINGS(1)(1:LENS),COMMAND(1:LENS))
      IF( INDEX('SET',COMMAND(1:LENS)) .EQ. 1 )THEN
        CALL SETNAM_INTERACTIVE(STRINGS,REALS,ITYPE,NFLD)
      ELSE IF( INDEX('FONT',COMMAND(1:LENS)) .EQ. 1 )THEN
        IF( NFLD .EQ. 1 )THEN
          DO I = 1, 37, 4
            DO J = 1, 4
              IJ = I + J - 1
              WRITE(CLINE(1+(J-1)*20:J*20),301)IJ,LIB(IJ)
 301          FORMAT('(',I2,')',A16)
            END DO
            WRITE(IOUTS,10)' '//CLINE(1:80)
          END DO
          DO J = 1, 3
            IJ = 41 + J - 1
            WRITE(CLINE(1+(J-1)*20:J*20),301)IJ,LIB(IJ)
          END DO
          WRITE(IOUTS,10)' '//CLINE(1:80)
  31      WRITE(IOUTS,302)IFONT,LIBFONT
 302      FORMAT(' Current font: (',I2,')',A,
     &     '  Enter font number or name >> ',$)
          READ(IINS,10,END=20,ERR=31)CLINE
          NF = 1
          CALL GPLOT_READLINE(CLINE,STR,RLS,ITP,NF)
          ITYPE(2)   = ITP(1)
          STRINGS(2) = STR(1)
          REALS(2)   = RLS(1)
          NFLD       = NF + 1
        END IF
        IF( NFLD .EQ. 1 )GO TO 20
        IF( ITYPE(2) .EQ. 2 )THEN
          DO I = 1, NFONTS
            IF( I .EQ. IFIX(REALS(2)) )THEN
              STRINGS(2) = LIB(I)
              ITYPE(2)   = 1
              GO TO 303
            END IF
          END DO
        END IF
 303    LENS2 = LENSIG(STRINGS(2))
        IF( LENS2 .NE. 0 )THEN
          CALL UPRCASE(STRINGS(2)(1:LENS2),STRINGS(2)(1:LENS2))
          CALL PFONT(STRINGS(2)(1:LENS2),0)
        END IF
      ELSE IF( INDEX('TEXT',COMMAND(1:LENS)) .EQ. 1 )THEN
        IF( ITYPE(2) .NE. 1 )THEN
  32      WRITE(IOUTS,30)' string >> '
          READ(IINS,10,END=20,ERR=32)CLINE
          STRINGS(2) = CLINE
          ITYPE(2)   = 1
        END IF
        LENS2 = LENSIG(STRINGS(2))
        IF( LENS2 .NE. 0 )CALL SETLAB('TEXT',STRINGS(2)(1:LENS2))
        CALL TRANSPARENT_MODE(0)
      ELSE IF( INDEX('XLABEL',COMMAND(1:LENS)) .EQ. 1 )THEN
        IF( ITYPE(2) .NE. 1 )THEN
          CLINE = ' '
          CLINE = GETLAB('XLABEL')
          LENSC = LENSIG(CLINE)
  34      WRITE(IOUTS,30)' Current xlabel: '//CLINE(1:LENSC)//
     &                   ' new xlabel >> '
          READ(IINS,10,END=20,ERR=34)CLINE
          STRINGS(2) = CLINE
          ITYPE(2)   = 1
        END IF
        LENS2 = LENSIG(STRINGS(2))
        IF( LENS2 .EQ. 0 )THEN
          CALL SETLAB('XLABEL',' ')
        ELSE
          CALL SETLAB('XLABEL',STRINGS(2)(1:LENS2))
        END IF
      ELSE IF( INDEX('YLABEL',COMMAND(1:LENS)) .EQ. 1 )THEN
        IF( ITYPE(2) .NE. 1 )THEN
          CLINE = ' '
          CLINE = GETLAB('YLABEL')
          LENSC = LENSIG(CLINE)
  36      WRITE(IOUTS,30)' Current ylabel: '//CLINE(1:LENSC)//
     &                   ' new ylabel >> '
          READ(IINS,10,END=20,ERR=36)CLINE
          STRINGS(2) = CLINE
          ITYPE(2)   = 1
        END IF
        LENS2 = LENSIG(STRINGS(2))
        IF( LENS2 .EQ. 0 )THEN
          CALL SETLAB('YLABEL',' ')
        ELSE
          CALL SETLAB('YLABEL',STRINGS(2)(1:LENS2))
        END IF
      ELSE IF( INDEX('PCHAR',COMMAND(1:LENS)) .EQ. 1 )THEN
        IF( ITYPE(2) .NE. 2 )THEN
          CLINE = ' '
          CLINE = GETLAB('CHAR')
          IP1 = ICHAR(CLINE(1:1))
          IP2 = ICHAR(CLINE(2:2))
  38      WRITE(IOUTS,39)' Current plot characters: ',IP1,', ',IP2,
     &                   ', new pch1 { pch2 } >> '
  39      FORMAT(A,I2,A,I2,A,$)
          READ(IINS,10,END=20,ERR=38)CLINE
          NF = 2
          CALL GPLOT_READLINE(CLINE,STR,RLS,ITP,NF)
          IF( ITP(1) .EQ. 2 )THEN
            REALS(2) = RLS(1)
            ITYPE(2) = 2
            IF( ITP(2) .EQ. 2 )THEN
              REALS(3) = RLS(2)
              ITYPE(3) = 2
            END IF
          END IF
        END IF
        IF( ITYPE(2) .EQ. 2 )
     &   CALL SETLAB('CHAR',CHAR(IFIX(REALS(2)))//CHAR(IFIX(REALS(3))))
      ELSE IF( INDEX('GPLOTI',COMMAND(1:LENS)) .EQ. 1 )THEN
        CALL GPLOTI
      ELSE IF( (INDEX('MENU',COMMAND(1:LENS)) .EQ. 1) .OR.
     &         (INDEX('HELP',COMMAND(1:LENS)) .EQ. 1) )THEN
        GO TO 2
      ELSE IF( INDEX('ACLEAR',COMMAND(1:LENS)) .EQ. 1 )THEN
        CALL CLTRANS
      ELSE IF( INDEX('CLEAR',COMMAND(1:LENS)) .EQ. 1 )THEN
        CALL CLEAR_PLOT
        CALL TRANSPARENT_MODE(0)
      ELSE IF( INDEX('HARDCOPY',COMMAND(1:LENS)) .EQ. 1 )THEN
        IDUM = NARGSI(1)
        CALL GRAPHICS_HARDCOPY(0)
      ELSE IF( INDEX('EDGRAPH',COMMAND(1:LENS)) .EQ. 1 )THEN
        IF( ITYPE(2) .NE. 1 )THEN
  40      WRITE(IOUTS,30)
     &  ' Enter: OPEN { file } or FRAME or CLOSE >> '
          READ(IINS,10,END=20,ERR=40)CLINE
          NF = 2
          CALL GPLOT_READLINE(CLINE,STR,RLS,ITP,NF)
          STRINGS(2) = STR(1)
          ITYPE(2)   = ITP(1)
          STRINGS(3) = STR(2)
          ITYPE(3)   = ITP(2)
        END IF
        LENS2 = LENSIG(STRINGS(2))
        CALL UPRCASE(STRINGS(2)(1:LENS2),STRINGS(2)(1:LENS2))
        IF( INDEX('OPEN',STRINGS(2)(1:LENS2)) .EQ. 1 )THEN
          IF( ITYPE(3) .EQ. 1 )THEN
            CALL DWG_BATCH(STRINGS(3),IERR)
            IF( IERR .NE. 0 )
     &       WRITE(IOUTS,10)' *** ERROR opening file: '//STRINGS(3)
          ELSE
            CALL DWG
          END IF
          CALL DWG_FORMAT('LANDSCAPE',NPAGES)

        ELSE IF(INDEX('FRAME',STRINGS(2)(1:LENS2)) .EQ. 1)THEN
          CALL DWG_NEXT
          CALL DWG_FORMAT('LANDSCAPE',NPAGES)

        ELSE IF(INDEX('CLOSE',STRINGS(2)(1:LENS2)) .EQ. 1)THEN
          CALL DWG_CLOSE

        ELSE
          WRITE(IOUTS,10)' *** expecting OPEN, FRAME or CLOSE ***'
        END IF
      ELSE IF( INDEX('SAVE',COMMAND(1:LENS)) .EQ. 1 )THEN
        IF( ITYPE(2) .NE. 1 )THEN
  60      WRITE(IOUTS,30)' filename >> '
          READ(IINS,10,END=20,ERR=60)CLINE
          NF = 1
          CALL GPLOT_READLINE(CLINE,STR,RLS,ITP,NF)
          STRINGS(2) = STR(1)
          ITYPE(2)   = ITP(1)
        END IF
        LENS2 = LENSIG(STRINGS(2))
        IF( LENS2 .NE. 0 )CALL GPLOT_SAVE_FILE(STRINGS(2)(1:LENS2))
      ELSE IF( INDEX('REPLOT',COMMAND(1:LENS)) .EQ. 1 )THEN
        RETURN 1
      ELSE IF( INDEX('RESTORE',COMMAND(1:LENS)) .EQ. 1 )THEN
        IF( ITYPE(2) .NE. 1 )THEN
  70      WRITE(IOUTS,30)' filename >> '
          READ(IINS,10,END=20,ERR=70)CLINE
          NF = 1
          CALL GPLOT_READLINE(CLINE,STR,RLS,ITP,NF)
          STRINGS(2) = STR(1)
          ITYPE(2)   = ITP(1)
        END IF
        LENS2 = LENSIG(STRINGS(2))
        IF( LENS2 .NE. 0 )CALL GPLOT_RESTORE_FILE(STRINGS(2)(1:LENS2))
      ELSE IF( INDEX('SMENU',COMMAND(1:LENS)) .EQ. 1 )THEN
        XDUM = GETNAM('MENUS')
      ELSE IF( INDEX('LMENU',COMMAND(1:LENS)) .EQ. 1 )THEN
        XDUM = GETNAM('MENU')
      ELSE
        WRITE(IOUTS,10)' *** ERROR: unknown command'
        WRITE(IOUTS,10)'     enter MENU to list the valid commands'
      END IF
      GO TO 20
1000  RETURN      
      END
CCC
      SUBROUTINE SETNAM_INTERACTIVE(STRINGS,REALS,ITYPE,NFLD)

      CHARACTER*255 STRINGS(3), CLINE, STRS(2)
      CHARACTER*60  NAME
      CHARACTER*15  TEMP
      REAL*4        RLS(3), REALS(3)
      INTEGER*4     ITYPE(3), ITPS(2)

      INTEGER*4 IINS
      COMMON /PLOT_INPUT_UNIT/ IINS

      INTEGER*4 IOUTS
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS
CCC
      SAVE

      ITEST = 0
      IF( NFLD .EQ. 1 )ITEST = 1
      IF( (NFLD .NE. 1) .AND. (ITYPE(2) .EQ. 1) )THEN
        NAME = STRINGS(2)
        GO TO 32
      END IF
  10  WRITE(IOUTS,40)'Enter:  name  { value } >> '
      READ(IINS,30,END=92,ERR=10)CLINE
  30  FORMAT(A)
      NLINE = LENSIG(CLINE)
      IF( NLINE .EQ. 0 )GO TO 92
      NF = 2
      CALL GPLOT_READLINE(CLINE,STRS,RLS,ITPS,NF)
      IF( ITPS(1) .NE. 1 )GO TO 10
      NAME = STRS(1)
      NFLD = NF+1
      REALS(3) = RLS(2)
      ITYPE(3) = ITPS(2)
  32  LENAME = LENSIG(NAME)
      CALL UPRCASE(NAME(1:LENAME),NAME(1:LENAME))
      IF( ITYPE(3) .NE. 2 )THEN
        VAL = GETNAM(NAME)
        IF( VAL .EQ. 9.99E-37 )GO TO 10   ! Invalid name
        IF( (NAME(1:LENAME) .EQ. 'MENU' ) .OR. 
     &      (NAME(1:LENAME) .EQ. 'MENUS') )GO TO 10
        CALL CONVERT_REAL_TO_CHAR(VAL,13,.FALSE.,TEMP,LENS)
  39    WRITE(IOUTS,40)
     &   'current value: '//TEMP(1:LENS)//', new value >> '
  40    FORMAT(' ',A,$)
        READ(IINS,30,END=43,ERR=39)CLINE
        NLINE = LENSIG(CLINE)
        IF( NLINE .EQ. 0 )THEN
  42      IF( ITEST .EQ. 0 )GO TO 92
          GO TO 10
        END IF
        NF = 1
        CALL GPLOT_READLINE(CLINE,STRS,RLS,ITPS,NF)
        IF( ITPS(1) .NE. 2 )GO TO 10
        VAL = RLS(1)
      ELSE
        VAL = REALS(3)
      END IF
      CALL SETNAM(NAME,VAL)
      IF( ITEST .EQ. 1 )GO TO 10
  43  IF( ITEST .EQ. 0 )GO TO 92
      GO TO 10
  92  RETURN
      END
CCC
      SUBROUTINE GPLOT_READLINE(LINE,STRINGS,REALS,ITYPE,NFIELD)
C
C    This routine scans an input LINE and determines the type and number
C    of fields in that line; returning strings in STRINGS, reals in
C    REALS, and types in ITYPE. ITYPE(i) is 1 if field i is a string,
C    ITYPE(i) is 2 if field i is a real number, and ITYPE(i) 0 if field
C    i is a null.  The maximum number of fields that will be scanned for
C    in LINE is the NFIELD that is passed.  NFIELD should not be passed
C    as a constant, as it will be changed if the number of fields in the
C    line is less than the NFIELD that is passed.
C
      CHARACTER*(*)  LINE
      CHARACTER*255  STRINGS(1)
      REAL*4         REALS(1)
      INTEGER*4      ITYPE(1)
      LOGICAL        NULL

      CHARACTER      SQT, DQT
CCC
      SAVE
      SQT = CHAR(39)  ! modified by J.Chuma 21Mar97 for g77
      DQT = CHAR(34)

      IF( NFIELD .LE. 0 )RETURN
      NFMAX = NFIELD
      DO I = 1, NFMAX
        REALS(I)   = 0.0
        ITYPE(I)   = 0
        STRINGS(I) = ' '
      END DO
      NFIELD = 0
      LENGTH = LEN(LINE)            ! find the significant length of LINE
      IF( LENGTH .EQ. 0 )RETURN
      DO WHILE ( (LINE(LENGTH:LENGTH).EQ.' ') .OR.
     &          (ICHAR(LINE(LENGTH:LENGTH)).EQ.9) )
        LENGTH = LENGTH-1
        IF( LENGTH .EQ. 0 )RETURN
      END DO
      IFIND = 0
      NULL = .TRUE.
  10  IFIND = IFIND + 1

C    Scan line for the next non-blank, non-tab character
C    the line can't end on a blank or tab

      DO WHILE ( (LINE(IFIND:IFIND).EQ.' ') .OR.
     &         (ICHAR(LINE(IFIND:IFIND)).EQ.9) )
        IFIND = IFIND+1
      END DO
      IF( LINE(IFIND:IFIND) .EQ. ',' )THEN
        IF( NULL )THEN
          NFIELD = NFIELD + 1
          ITYPE(NFIELD) = 0
        ELSE
          NULL = .TRUE.
        END IF
      ELSE                                    ! Field entry found
        NULL = .FALSE.                        ! Field is not null
        NFIELD = NFIELD + 1
        IF( LINE(IFIND:IFIND) .EQ. SQT )THEN
          IFIND2 = INDEX(LINE(IFIND+1:),SQT)+IFIND
          IF( IFIND2 .EQ. IFIND )IFIND2 = LENGTH+1    ! no closing quote
  20      IF( IFIND2 .GE. LENGTH )GO TO 200           ! finished with line
          IF( LINE(IFIND2+1:IFIND2+1) .EQ. SQT )THEN  ! double quotes
            LENGTH = LENGTH - 1                       ! eliminate second quote
            IF( IFIND2 .EQ. LENGTH )THEN              ! at end of line
              IFIND2 = IFIND2+1                       ! pretend a second quote
              GO TO 200
            END IF
            DO II = IFIND2, LENGTH                    ! take out quote
              LINE(II:II) = LINE(II+1:II+1)
            END DO
            IF( INDEX(LINE(IFIND2+1:),SQT) .EQ. 0 )THEN ! no closing quote
              IFIND2 = LENGTH+1                       ! pretend a second quote
            ELSE
              IFIND2 = INDEX(LINE(IFIND2+1:),SQT)+IFIND2 ! find second quote
            END IF
            GO TO 20
          END IF
          GO TO 200
        ELSE IF( LINE(IFIND:IFIND) .EQ. DQT )THEN
          IFIND2 = INDEX(LINE(IFIND+1:),DQT)+IFIND
          IF( IFIND2 .EQ. IFIND )IFIND2 = LENGTH+1
  30      IF( IFIND2 .GE. LENGTH )GO TO 200
          IF( LINE(IFIND2+1:IFIND2+1) .EQ. DQT )THEN
            LENGTH = LENGTH - 1
            IF( IFIND2 .EQ. LENGTH )THEN
              IFIND2 = IFIND2+1
              GO TO 200
            END IF
            DO II = IFIND2, LENGTH
              LINE(II:II) = LINE(II+1:II+1)
            END DO
            IF( INDEX(LINE(IFIND2+1:),DQT) .EQ. 0 )THEN
              IFIND2 = LENGTH+1
            ELSE
              IFIND2 = INDEX(LINE(IFIND2+1:),DQT)+IFIND2
            END IF
            GO TO 30
          END IF
          GO TO 200
        END IF

C    Now scan for delimiter of current field

        IFIND2 = IFIND+1
        DO WHILE ( IFIND2 .LE. LENGTH )
          IF( (LINE(IFIND2:IFIND2).EQ.' ') .OR.
     &        (ICHAR(LINE(IFIND2:IFIND2)).EQ.9) .OR.
     &        (LINE(IFIND2:IFIND2).EQ.',') )GO TO 40
          IFIND2 = IFIND2+1
        END DO

C    CHREAL tries to interpret the string as a number
C    LINE(IFIND:IFIND2-1) is the string in current field

#ifdef g77
  40    CALL CHREAL(LINE(IFIND:IFIND2-1),IFIND2-IFIND,FPOINT,1,*200)
#elif gfortran
  40    CALL CHREAL(LINE(IFIND:IFIND2-1),IFIND2-IFIND,FPOINT,1,*200)
#else
  40    CALL CHREAL(
     &   %REF(LINE(IFIND:IFIND2-1)),IFIND2-IFIND,FPOINT,1,*200)
#endif
        REALS(NFIELD) = FPOINT
        ITYPE(NFIELD) = 2
        GO TO 300

C    Interpret as a string

 200    NCHAR = IFIND2 - IFIND
        IF( NCHAR .GT. 255 )NCHAR = 255
        STRINGS(NFIELD) = ' '
        IF( (LINE(IFIND:IFIND).EQ.SQT) .OR.  
     &      (LINE(IFIND:IFIND).EQ.DQT) )THEN  ! don't include
          NCHAR = NCHAR - 1                   !  opening/closing quotes
          IFIND2 = IFIND2 + 1
          IFIND  = IFIND + 1
        END IF
        STRINGS(NFIELD)(1:) = LINE(IFIND:NCHAR+IFIND-1)
        ITYPE(NFIELD) = 1
 300    IFIND = IFIND2 - 1
      END IF
      IF( NFIELD .EQ. NFMAX )RETURN
      IF( IFIND .LT. LENGTH )GO TO 10

C    LINE has been completely scanned

      IF( NULL )THEN
        NFIELD = NFIELD + 1
        ITYPE(NFIELD) = 0
      END IF
      RETURN
      END
