      SUBROUTINE DO_COMMAND(X,Y,HEIGHT,ANGLE,COMM,LENT,IDCNT,IUCNT
     &             ,JUSTIFY,PLEN,COSX,SINX,CLASSES,FONT,LENFONT
     &             ,HITMAX,*)
C  modified by J.Chuma 20Mar97 for g77
C
      CHARACTER  COMM*(*), COMM2*255, COMM1*1, CTRL*2,FONT*60,FONT2*14
     &          ,GETLAB*2, CHARL*2, HEXCODE*2
      INTEGER*4  NHATCH(2)
      integer*2  CLASSES(0:127)
      LOGICAL    FIRST, JUSTIFY, DEFAULTS
      BYTE       ACHARI
C
      COMMON /PLOT_LEVEL/ ILEV
C
      LOGICAL ASIS, ASIS_OLD
      COMMON /MODE/ ASIS
C
      INTEGER*4 COLOURS(0:11,0:20)
      COMMON /GPLOT_COLOURS/ COLOURS, ICOL
      COMMON /PLOT_COLOURS/  ICOLR1, ICOLR2
C
      LOGICAL BOLD
      COMMON /GPLOT_BOLDING/   BOLD
C
      LOGICAL ITALICS
      COMMON /GPLOT_PSALPH/ ITALICS, ANGL, ANGL_OLD
C
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS
      COMMON /PSYM_HATCHING/   NHATCH 
      COMMON /GPLOT_CONTROLS/  CTRL
      COMMON /GPLOT_TEXT/      DEFAULTS
      COMMON /PLOTMONITOR/    IMONITOR,  IOUTM
      COMMON /PLOTMONITOR2/   IMONITOR2, IOUTM2

C  Modified by J.L.Chuma, 08-Apr-1997 to elimate SIND, COSD for g77
      REAL*4 DTOR /0.017453292519943/
C
      DATA DH       / 0.6 /
      DATA FIRST    / .TRUE. /
      DATA LTRS_OLD / -32768 /

      LENFONT = ABS(LENFONT)
      YLWIND  = GETNAM('YLWIND')
      YUWIND  = GETNAM('YUWIND')
      XLWIND  = GETNAM('XLWIND')
      XUWIND  = GETNAM('XUWIND')
      I1  = 1
2     IND = INDEX(COMM(I1:LENT),',')
      IF( IND .GT. 0 )THEN
        LEN  = IND - 1
      ELSE
        LEN = LENT - I1 + 1
      END IF
      I2 = I1 + LEN - 1
C
C   First check to see if the command is a special name
C
      CALL UPRCASE(COMM(I1:I2),COMM2(1:LEN))
      COMM1 = COMM2(1:1)
      CALL NAME_IN_HEXCODE(COMM(I1:I2),HEXCODE,FONT2,LF)
      IF(HEXCODE .NE. '  ')THEN
        ASIS_OLD = ASIS
        IF( FONT2(1:LF) .NE. FONT(1:LENFONT) )
     &    CALL PFONT(FONT2(1:LF),0)
        ASIS = .TRUE.
        CALL GLINE_SUB(HEXCODE,2,JUSTIFY,PLEN,X,Y,HEIGHT,ANGLE)
        ASIS = ASIS_OLD
        IF( FONT2(1:LF) .NE. FONT(1:LENFONT) )
     &    CALL PFONT(FONT(1:LENFONT),0)
        GO TO 10
      END IF
      IF( INDEX(COMM2(1:LEN),'DEF') .EQ. 1 )THEN
        DEFAULTS = .TRUE.
      ELSE IF( INDEX(COMM2(1:LEN),'NOD') .EQ. 1 )THEN
        DEFAULTS = .FALSE.
      ELSE IF( COMM1 .EQ. 'X' )THEN
C
C   Hexadecimal input flag
C
        ASIS = .NOT. ASIS
      ELSE IF( (COMM1 .EQ. 'I') .OR. (COMM2(1:LEN) .EQ. 'EM') )THEN
C
C   Italics flag
C
        IF( ITALICS )THEN
          ITALICS = .FALSE.
          ANGL    = ANGL_OLD
        ELSE
          ITALICS = .TRUE.
          ANGL    = 20.0
        END IF
        CALL PSALPH('ANGL',ANGL,IDUMMY,*30)
      ELSE IF(COMM1 .EQ. 'F')THEN
C
C   Set the font
C
        FONT(1:LEN-1) = COMM2(2:LEN)
        LENFONT       = LEN - 1
        CALL PFONT(FONT(1:LENFONT),0)
        CALL PSALPH('ANGL',ANGL,IDUMMY,*30)
      ELSE IF(COMM1 .EQ. 'H')THEN
C
C   Set the height
C
        IF( COMM2(LEN:LEN) .EQ. '%' )THEN
#ifdef g77
          CALL CHREAL(COMM2(2:LEN-1),LEN-2,XHEIGHT,1,*20)
#elif gfortran
          CALL CHREAL(COMM2(2:LEN-1),LEN-2,XHEIGHT,1,*20)
#else
          CALL CHREAL(%REF(COMM2(2:LEN-1)),LEN-2,XHEIGHT,1,*20)
#endif
          HEIGHT = XHEIGHT * (YUWIND - YLWIND) / 100.
        ELSE
#ifdef g77
          CALL CHREAL(COMM2(2:LEN),LEN-1,XHEIGHT,1,*20)
#elif gfortran
          CALL CHREAL(COMM2(2:LEN),LEN-1,XHEIGHT,1,*20)
#else
          CALL CHREAL(%REF(COMM2(2:LEN)),LEN-1,XHEIGHT,1,*20)
#endif
          HEIGHT = XHEIGHT
        END IF
        IF( HEIGHT .GT. HITMAX )HITMAX = HEIGHT
      ELSE IF(COMM1 .EQ. 'B')THEN
C
C   Bolding flag
C
        IF( JUSTIFY )GO TO 10
        IF(LEN .LT. 2)THEN
          BOLD      = .FALSE.
          NHATCH(1) = 0
          NHATCH(2) = 0
        ELSE
          BOLD = .TRUE.
          INDX = INDEX(COMM2(2:LEN),':')
          IF(INDX .GT. 0)THEN
            LENB = INDX
#ifdef g77
            CALL CHREAL(COMM2(INDX+2:LEN),LEN-INDX-1,XBOLD,1,*20)
#elif gfortran
            CALL CHREAL(COMM2(INDX+2:LEN),LEN-INDX-1,XBOLD,1,*20)
#else
            CALL CHREAL(%REF(COMM2(INDX+2:LEN)),LEN-INDX-1,XBOLD,1,*20)
#endif
            NHATCH(2) = IFIX(XBOLD)
          ELSE
            NHATCH(2) = 0
            LENB      = LEN
          END IF
#ifdef g77
          CALL CHREAL(COMM2(2:LENB),LENB-1,XBOLD,1,*20)
#elif gfortran
          CALL CHREAL(COMM2(2:LENB),LENB-1,XBOLD,1,*20)
#else
          CALL CHREAL(%REF(COMM2(2:LENB)),LENB-1,XBOLD,1,*20)
#endif
          NHATCH(1) = IFIX(XBOLD)
        END IF
      ELSE IF(COMM1 .EQ. 'Z')THEN
C
C   Include a horizontal space
C
        IF( COMM2(LEN:LEN) .EQ. '%' )THEN
#ifdef g77
          CALL CHREAL(COMM2(2:LEN-1),LEN-2,HORIZ_SPACE,1,*20)
#elif gfortran
          CALL CHREAL(COMM2(2:LEN-1),LEN-2,HORIZ_SPACE,1,*20)
#else
          CALL CHREAL(%REF(COMM2(2:LEN-1)),LEN-2,HORIZ_SPACE,1,*20)
#endif
          HORIZ_SPACE = HORIZ_SPACE * (XUWIND - XLWIND) / 100.
        ELSE
#ifdef g77
          CALL CHREAL(COMM2(2:LEN),LEN-1,HORIZ_SPACE,1,*20)
#elif gfortran
          CALL CHREAL(COMM2(2:LEN),LEN-1,HORIZ_SPACE,1,*20)
#else
          CALL CHREAL(%REF(COMM2(2:LEN)),LEN-1,HORIZ_SPACE,1,*20)
#endif
        END IF
        X = X + COSX*HORIZ_SPACE
        Y = Y + SINX*HORIZ_SPACE
        IF( JUSTIFY )PLEN = PLEN + HORIZ_SPACE
      ELSE IF(COMM1 .EQ. 'V')THEN
C
C   Include a vertical space
C
        IF( COMM2(LEN:LEN) .EQ. '%' )THEN
#ifdef g77
          CALL CHREAL(COMM2(2:LEN-1),LEN-2,VERT_SPACE,1,*20)
#elif gfortran
          CALL CHREAL(COMM2(2:LEN-1),LEN-2,VERT_SPACE,1,*20)
#else
          CALL CHREAL(%REF(COMM2(2:LEN-1)),LEN-2,VERT_SPACE,1,*20)
#endif
          VERT_SPACE = VERT_SPACE * (YUWIND - YLWIND) / 100.
        ELSE
#ifdef g77
          CALL CHREAL(COMM2(2:LEN),LEN-1,VERT_SPACE,1,*20)
#elif gfortran
          CALL CHREAL(COMM2(2:LEN),LEN-1,VERT_SPACE,1,*20)
#else
          CALL CHREAL(%REF(COMM2(2:LEN)),LEN-1,VERT_SPACE,1,*20)
#endif
        END IF
        X = X - SINX*VERT_SPACE
        Y = Y + COSX*VERT_SPACE
      ELSE IF( (COMM1 .EQ. 'U') .OR. (COMM1 .EQ. '^') )THEN
C
C   Super-script flag
C
        IF(IDCNT .GT. 0)THEN
          IDCNT  = IDCNT - 1
          X      = X - SINX*HEIGHT
          Y      = Y + COSX*HEIGHT
          HEIGHT = HEIGHT / DH
        ELSE
          IUCNT  = IUCNT + 1
          HEIGHT = HEIGHT * DH
          X      = X - SINX*HEIGHT
          Y      = Y + COSX*HEIGHT
        END IF
      ELSE IF( (COMM1 .EQ. 'D') .OR. (COMM1 .EQ. '_') )THEN
C
C   Sub-script flag
C
        IF(IUCNT .GT. 0)THEN
          IUCNT  = IUCNT - 1
          Y      = Y - COSX*HEIGHT
          X      = X + SINX*HEIGHT
          HEIGHT = HEIGHT / DH
        ELSE
          IDCNT  = IDCNT + 1
          HEIGHT = HEIGHT * DH
          Y      = Y - COSX*HEIGHT
          X      = X + SINX*HEIGHT
        END IF
CC      ELSE IF(COMM1 .EQ. 'Q')THEN
C
C   Set the control characters
C
CC        CLASSES(ICHAR(CTRL(1:1))) = 3
CC        CLASSES(ICHAR(CTRL(2:2))) = 3
CC        IF(LEN .EQ. 2)THEN
CC          CTRL = COMM(I1+1:I1+1)//COMM(I1+1:I1+1)
CC        ELSE IF(LEN .EQ. 3)THEN
CC          CTRL = COMM(I1+1:I1+2)
CC        ELSE
CC          GO TO 40
CC        END IF
CC        CLASSES(ICHAR(CTRL(1:1))) = 1
CC        CLASSES(ICHAR(CTRL(2:2))) = 2
      ELSE IF(COMM1 .EQ. 'C')THEN
C
C   Set the terminal colour and the pen number
C
        IF( JUSTIFY .OR. ( ILEV .EQ. 1) )GO TO 10
#ifdef g77
        CALL CHREAL(COMM2(2:LEN),LEN-1,XCOLOR,1,*20)
#elif gfortran
        CALL CHREAL(COMM2(2:LEN),LEN-1,XCOLOR,1,*20)
#else
        CALL CHREAL(%REF(COMM2(2:LEN)),LEN-1,XCOLOR,1,*20)
#endif
        ICOL = MAX(0,MIN(11,IFIX(XCOLOR)))
        CALL PLOT_COLOR(COLOURS(ICOL,IMONITOR),COLOURS(ICOL,IMONITOR2))
      ELSE IF(COMM1 .EQ. 'M')THEN
C
C   Insert a plotting symbol Marker into the text string
C
        CHARSZ = GETNAM('CHARSZ')
        PLEN   = PLEN + CHARSZ
        IF( .NOT.JUSTIFY )THEN
          CHARA  = GETNAM('CHARA')
#ifdef g77
          CALL CHREAL(COMM2(2:LEN),LEN-1,XMARKER,1,*20)
#elif gfortran
          CALL CHREAL(COMM2(2:LEN),LEN-1,XMARKER,1,*20)
#else
          CALL CHREAL(%REF(COMM2(2:LEN)),LEN-1,XMARKER,1,*20)
#endif
          IMARKER = IFIX(XMARKER)
          IF( IMARKER .EQ. 0 )THEN
            CHARL   = GETLAB('CHAR')
            IMARKER = ICHAR(CHARL(1:1))
          END IF
          IF( (IMARKER .LT. 32) .AND. (IMARKER .GT. 0) )THEN
            COSANG = COS(CHARA*DTOR)
            SINANG = SIN(CHARA*DTOR)
            NFILL  = IFIX(500*CHARSZ/ABS(YUWIND-YLWIND))+1
            NFILL  = MAX(10,NFILL)
            XP = X + COSX*0.5*CHARSZ
            YP = Y + (1.+SINX)*0.5*CHARSZ
            CALL GPLOT_SYMBOL(XP,YP,CHARSZ,COSANG,SINANG,NFILL,IMARKER)
          ELSE IF( (IMARKER .GT. 32) .AND. (IMARKER .LT. 97) )THEN
            ACHARI = IMARKER
            CALL SYMBOL(X,Y,CHARSZ,ACHARI,CHARA,1)
          END IF
        END IF
        X = X + COSX*CHARSZ
        Y = Y + SINX*CHARSZ
      ELSE IF(COMM1 .EQ. '?')THEN
        IF( JUSTIFY )GO TO 10
        CALL TRANSPARENT_MODE(0)
        WRITE(IOUTS,*)'B bolding             C  colour'
        WRITE(IOUTS,*)'_ sub-script          ^  super-script'
        WRITE(IOUTS,*)'H height              EM italics'
        WRITE(IOUTS,*)'F font                M  plotting symbol marker'
        WRITE(IOUTS,*)'Z horizontal space    V  vertical space'
        WRITE(IOUTS,*)'X hexadecimal input'
        WRITE(IOUTS,*)' '
        WRITE(IOUTS,*)'DEF   reset the defaults'
        WRITE(IOUTS,*)'NODEF do not reset the defaults'
      ELSE 
        GO TO 40
      END IF
10    IF( IND .GT. 0 )THEN
        I1 = I1 + IND
        GO TO 2
      END IF
      RETURN
20    CALL TRANSPARENT_MODE(0)
      WRITE(IOUTS,21)CTRL(1:1),COMM(I1:I2),CTRL(2:2)
21    FORMAT(' *** ERROR converting ',3A,' to real number(s) ***')
      RETURN1
30    CALL TRANSPARENT_MODE(0)
      WRITE(IOUTS,31)CTRL(1:1),COMM(I1:I2),CTRL(2:2)
31    FORMAT(' *** ERROR in calling PSALPH with ',3A,' ***')
      RETURN1
40    CALL TRANSPARENT_MODE(0)
      WRITE(IOUTS,41)CTRL(1:1),COMM(I1:I2),CTRL(2:2)
41    FORMAT(' *** ERROR: Incorrect text-format command ',3A,' ***')
      RETURN1
      END
