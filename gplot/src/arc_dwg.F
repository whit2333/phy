      SUBROUTINE ARC_DWG(XC,YC,X1,Y1,X2,Y2,AINC,ILIN,IMAT,TMAT)
C======================================================================C
C  Encode an arc into the current drawing file.
C======================================================================C
      COMMON/CDWG/DWGON,DWGTXT,LDWG,LDWT,IRECG,IRECT,STROKE
      LOGICAL DWGON,DWGTXT,STROKE
      COMMON/DWG_PREVIOUS/XP,YP,IPENP
      COMMON/EDGR_ATT_SET/IFONT_SET,ICOL_SET

      REAL TMAT(6)
      INTEGER*2 IPEN/-1001/
      BYTE ICOL,ILIN       ! modified by J.Chuma, 7Mar97, for LINUX g77
C                            changed from LOGICAL*1 to BYTE 
      ICOL=ICOL_SET
      WRITE(LDWG,REC=IRECG,ERR=99)XC,YC,IPEN,ICOL,ILIN
      IRECG=IRECG+1
      WRITE(LDWG,REC=IRECG,ERR=99)X1,Y1,AINC
      IRECG=IRECG+1
      IMAT_OUT=0
C Write out the transformation matrix:
      IF(IMAT.NE.0)THEN
C Skip reserved values:
        IF(IRECT.EQ.2)THEN
          WRITE(LDWT,REC=2,ERR=99)
          WRITE(LDWT,REC=3,ERR=99)
          IRECT=4
        ELSE IF(IRECT.EQ.3)THEN
          WRITE(LDWT,REC=3,ERR=99)
          IRECT=4
        ELSE IF(IRECT.EQ.99)THEN
          WRITE(LDWT,REC=99,ERR=99)
          IRECT=100
        ENDIF
        IMAT_OUT=IRECT
        WRITE(LDWT,REC=IRECT,ERR=99)TMAT
        IRECT=IRECT+1
      ENDIF
      WRITE(LDWG,REC=IRECG,ERR=99)X2,Y2,IMAT_OUT
      IRECG=IRECG+1

      IPENP=0
      XP=X2
      YP=Y2
      IF(IMAT.EQ.0)THEN
        XP=X2
        YP=Y2
      ELSE
        XP=TMAT(1)*X2+TMAT(2)*Y2+TMAT(3)
        YP=TMAT(4)*X2+TMAT(5)*Y2+TMAT(6)
      ENDIF

99    RETURN

      END
