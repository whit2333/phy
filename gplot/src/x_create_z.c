#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

/***********************************************************************
*   THIS ROUTINE IS CALLED TO CREATE THE ZOOM WINDOW.  ACTUALLY THE 
*   ZOOM WINDOW IS CREATED IN SETUP.  THIS ROUTINE MERELY HINTS TO
*   THE WINDOW MANAGER OF THE DESIRED SIZE AND LOCATION OF THE ZOOM
*   WINDOW BEFORE IT IS MAPPED.  THE SIZE AND LOCATION IS CHOSEN TO 
*   BE THE SAME AS THAT OF THE CURRENT MAIN WINDOW'S SIZE AND LOCATION.
*
*   Modified July 1991 by F.Jones: zoom window code revised
*   and expanded.  Support added for graphics input, odometer,
*   long crosshairs, and control by applications.
************************************************************************/

/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#ifdef VMS
#define xvst_zoom_window_ xvst_zoom_window
#endif

/* GLOBAL VARIABLES */

Display *dpy;
Screen *screen;
Window z_window, g_window;
GC fgc;
Bool zoom, zoom_box;
int event_mask;

/* COMMON BLOCK FOR ZOOM WINDOW */
struct {Bool zopen,zact; float zw,zh,zx1,zx2,zy1,zy2;} xvst_zoom_window_;

xvst_create_zoom_()
{
   XEvent TheEvent;

/********************************************************************
*  MAP WINDOW AND WAIT FOR THE FIRST EXPOSE EVENT TO ARRIVE 
*  BEFORE GRAPHING.  UNFORTUNATELY THE WINDOW MANAGER IS NOT
*  CONSISTENT IN SENDING EXPOSE EVENTS EVERY TIME THE WINDOW IS MAPPED.
*  THUS WE WILL HAVE TO RESORT TO WAITING FOR A VISIBILITY EVENT!
*  Problem: this may hang if zoom window is partially off-screen (FWJ)
*********************************************************************/
   XSelectInput( dpy, z_window, VisibilityChangeMask );
   XMapWindow( dpy, z_window );
   XFlush( dpy );
   while (1) {
      XWindowEvent( dpy, z_window, VisibilityChangeMask, &TheEvent );
      if ( TheEvent.xvisibility.state == VisibilityUnobscured ) break;
   }
   XClearWindow( dpy, z_window );

   XSelectInput( dpy, z_window, event_mask );

/* Dummy call as in xvst_crosshair.c */
/*   xvst_draw_fsc_(dpy,screen,z_window,0,0,fgc,0,0,0);
*/
   XFlush( dpy );
}
