/**************************************************************************
*  xvst_zoom_                                                        FWJ  *
*                                                                         *
*  This routine sets up a zoom window bounded by coordinates x1,y1,x2,y2. *
*  These are pixel coordinates in the main graphics window.               *
*                                                                         *
*  This allows an application (such as EDGR) to open or control the zoom  *
*  window and is an alternative to the user specifying the zoom window    *
*  with a "drag box" in the main window (compare do_ButtonRelease).       *
*                                                                         *
***************************************************************************/

/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#include <X11/Xlib.h>

#ifdef VMS
#define xvst_zoom_window_ xvst_zoom_window
#define xvst_zoom_ xvst_zoom
#endif

#define Max(a,b)  ((a) > (b) ? (a) : (b))
#define Min(a,b)  ((a) < (b) ? (a) : (b))
#define Abs(a)    ((a) <  0  ? -(a) : (a))

/*   GLOBAL VARIABLES   */

Display *dpy;
Screen *screen;
Window g_window, z_window;
GC fgc;
Bool zoom, zoom_box, replay_zoom;
Bool enable_action;
int zwidth, zheight;
int corner1_x, corner1_y, corner2_x, corner2_y;
float scalx,scaly;
int offset_x, offset_y;

/* COMMON BLOCK FOR ZOOM WINDOW */
struct {Bool zopen,zact; float zw,zh,zx1,zx2,zy1,zy2;} xvst_zoom_window_;

void xvst_zoom_(ix1,iy1,ix2,iy2)
   int *ix1,*iy1,*ix2,*iy2;
{
   float dx,dy;

/*   printf("ix1,iy1,ix2,iy2 %d %d %d %d\n",*ix1,*iy1,*ix2,*iy2);
*/
   enable_action = False;

   XFlush(dpy);

/* Erase old zoom box */
   if (zoom_box) xvst_draw_zb_(dpy,screen,g_window,fgc,corner1_x,corner1_y,
                               corner2_x, corner2_y);

/* Update the parameters */

   corner1_x = *ix1;
   corner2_x = *ix2;
   corner1_y = *iy1;
   corner2_y = *iy2;

   offset_x = Min(corner1_x, corner2_x);
   offset_y = Min(corner1_y, corner2_y);
   dx = Abs(corner2_x-corner1_x);
   dy = Abs(corner2_y-corner1_y);
   scalx = dx/(float)zwidth;
   scaly = dy/(float)zheight;

/*   printf("scalx,scaly %f %f\n",scalx,scaly);
*/
/* CREATE THE ZOOM WINDOW */
   if (!zoom) xvst_create_zoom_();
   zoom = True;

/* DRAW ZOOM BOX */
   xvst_draw_zb_(dpy, screen, g_window, fgc, corner1_x, corner1_y,
                 corner2_x, corner2_y);
   zoom_box = True;

/* SET COMMON BLOCK VARIABLES */
   xvst_zoom_window_.zopen = True;
   xvst_zoom_window_.zw = zwidth;
   xvst_zoom_window_.zh = zheight;
/* Lower left zoom corner */
   xvst_zoom_window_.zx1 = Min(corner1_x,corner2_x);
   xvst_zoom_window_.zy1 = Max(corner1_y,corner2_y);
/* Upper right zoom corner */
   xvst_zoom_window_.zx2 = Max(corner1_x,corner2_x);
   xvst_zoom_window_.zy2 = Min(corner1_y,corner2_y);

/* REPLAY THE GRAPHICS */
   replay_zoom = True;
   xvst_replay_(0);
   replay_zoom = False;

   enable_action = True;
}
