      SUBROUTINE ARC(XC,YC,X1,Y1,X2,Y2,AINC,ILINE,IMAT,TMAT)
C======================================================================C
C                                                                      C
C  ARC                                             F.W. Jones, TRIUMF  C
C                                                                      C
C  Routine for drawing circles, circular arcs, and affine              C
C  transformations thereof, such as ellipses and elliptical arcs.      C
C                                                                      C
C  Draws an arc centered at (XC,YC), starting at (X1,Y1)               C
C  and ending at (X2,Y2).  The arc is approximated using chords        C
C  of angle AINC.  If AINC>0 the arc is drawn counterclockwise;        C
C  if AINC<0 it is drawn clockwise.  The arc will be drawn with        C
C  line type determined by ILINE.                                      C
C                                                                      C
C  If (X2,Y2) is not on the arc determined by (XC,YC) and (X1,Y1),     C
C  the arc will end where it intersects the segment                    C
C  (XC,YC) --> (X2,Y2).                                                C
C                                                                      C
C  If (X2,Y2)=(X1,Y1) a full circle or close arc will be drawn.        C
C  Regular polygons can be drawn by setting AINC=15,30,45,60, etc.     C
C                                                                      C
C  Optionally, a 2d transformation involving scaling, rotation,        C
C  translation, etc., may be applied to the arc before it is drawn.    C
C  This can be used to produce ellipses and other affine images.       C
C  The array TMAT is used only if the flag IMAT is nonzero; otherwise  C
C  TMAT need not be passed.  TMAT contains the significant portion of 
C  the standard 3x3 transformation matrix.  
C  If TMAT={T1,T2,T3,T4,T5,T6} then the transformation is given by:    C
C                                                                      C
C      X' = X*T1 + Y*T2 + T3                                           C
C      Y' = X*T4 + Y*T5 + T6                                           C
C                                                                      C
C  Modified Dec 20/85 by F. Jones:                                     C
C    |AINC| is constrained to be >= 1.E-4 and < 180.                   C
C                                                                      C
C======================================================================C
C
      REAL TMAT(6)
C
      COMMON/CDWG/DWGON,DWGTXT,LDWG,LDWT,IRECG,IRECT,STROKE
      LOGICAL DWGON,DWGTXT,STROKE
C
      LOGICAL DWGON_SAVE

C  Modified by J.L.Chuma, 08-Apr-1997 for g77
C    to elimate SIND, COSD, and ATAN2D
      REAL*4 DTOR /0.017453292519943/
      REAL*4 RTOD /57.295779513082/
CCC
      DX=X1-XC
      DY=Y1-YC
      R=SQRT(DX**2 + DY**2)
      IF(R.EQ.0.)RETURN
      IF(ABS(AINC).LT.1.E-4.OR.ABS(AINC).GE.180.)RETURN
      DWGON_SAVE=DWGON
C
      A1=RTOD*ATAN2(DY,DX)
      A2=RTOD*ATAN2(Y2-YC,X2-XC)
      IF((A2-A1)*AINC.LE.0.)A2=A2+SIGN(360.,AINC)
C
C==Encode arc to drawing file if open:
      IF(DWGON.AND..NOT.STROKE)THEN
        X2T=XC+R*COS(A2*DTOR)      !correct endpoint
        Y2T=YC+R*SIN(A2*DTOR)
        IF(X2.EQ.X1.AND.Y2.EQ.Y1)THEN  !ensure exactness for closed arcs
          X2T=X2
          Y2T=Y2
        ENDIF
        CALL ARC_DWG(XC,YC,X1,Y1,X2T,Y2T,AINC,ILINE,IMAT,TMAT)
        DWGON=.FALSE.
      ENDIF
C
      IF(IMAT.NE.0)THEN
        T1=TMAT(1)
        T2=TMAT(2)
        T3=TMAT(3)
        T4=TMAT(4)
        T5=TMAT(5)
        T6=TMAT(6)
      ENDIF
C
      IF(IMAT.EQ.0)THEN
        CALL PLOT_R(X1,Y1,3)
      ELSE
        CALL PLOT_R(T1*X1+T2*Y1+T3,T4*X1+T5*Y1+T6,3)
      ENDIF
      A=A1
      IEND=0
C
20    A=A+AINC
      IF((A-A2)*AINC.GE.0.)THEN
        A=A2
        IEND=2
      ENDIF
      X=XC+R*COS(A*DTOR)
      Y=YC+R*SIN(A*DTOR)
      IF(IMAT.NE.0)THEN
        XT=X
        YT=Y
        X=T1*XT+T2*YT+T3
        Y=T4*XT+T5*YT+T6
      ENDIF
      IF(ILINE.LE.1)THEN
        CALL PLOT_R(X,Y,2)
      ELSE
        CALL DLINE(X,Y,ILINE,IEND)
      ENDIF
      IF(A.NE.A2)GO TO 20
C
      DWGON=DWGON_SAVE
      RETURN
      END
