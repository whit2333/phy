/*  
 * FORTRAN-callable I/O routine TCSETA
 * Modified 05-JUL-93 by FWJ for OSF/1 and 64-bit compatibility:
 * Changed longs to ints.  Added error diagnostics.
 */  
#include <sys/types.h>
#include <sys/ioctl.h>
#include <termio.h>

int tcseta_(d, argp)
   int *d;
   int argp[];
{
   struct termio argp_local;
   int d_local, i, rc;

   d_local = *d;
   rc = ioctl( d_local, TCGETA, &argp_local ); 
   if (rc == -1) printf("TCSETA: error in ioctl (TCGETA)\n");
   argp_local.c_lflag     = argp[ 0];
   argp_local.c_cc[VMIN]  = (char)argp[ 1];
   argp_local.c_cc[VTIME] = (char)argp[ 2];
   rc = ioctl( d_local, TCSETA, &argp_local );
   if (rc == -1) printf("TCSETA: error in ioctl (TCSETA)\n");
   return( rc );
}
