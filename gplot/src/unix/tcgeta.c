/*  
 * FORTRAN-callable I/O routine TCGETA
 * Modified 05-JUL-93 by FWJ for OSF/1 and 64-bit compatibility:
 * Changed longs to ints.  Now returns ECHO and ICANON masks.
 * Added error diagnostic.
 */  

#include <sys/types.h>
#include <sys/ioctl.h>
#include <termio.h>
#ifdef __osf__
#include <termios.h>
#endif

int tcgeta_(d, argp, echo_mask, canon_mask)
   int *d, *echo_mask, *canon_mask;
   int argp[];
{
   struct termio argp_local;
   int d_local, i, rc;

   d_local = *d;
   rc = ioctl( d_local, TCGETA, &argp_local ); 
   if (rc == -1) printf("TCGETA: error in ioctl\n");
   argp[ 0] = argp_local.c_lflag ;
   argp[ 1] = argp_local.c_cc[VMIN];
   argp[ 2] = argp_local.c_cc[VTIME];
   *echo_mask = ECHO;
   *canon_mask = ICANON;
   return( rc );
}
