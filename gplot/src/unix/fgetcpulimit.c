/*  fgetcpulimit_                    F.W. Jones, TRIUMF            */
/*  Returns cpu time limit in seconds.                             */
/*  Returns 0 if the cpu limit is infinite or if an error occurs.  */

/*  Modified 24-MAR-95 by FWJ for HP-UX: the man page for getrlimit
 *  indicates that no cpu limit resources are supported, so this
 *  routine returns an infinite cpu limit for now.
 */

#include <sys/time.h>
#include <sys/resource.h>

extern int getrlimit();

void
fgetcpulimit_(limit)
int *limit;
{
   struct rlimit rlp;
   int ret;

#ifdef __hpux
   *limit = 0;
#else
   ret = getrlimit(RLIMIT_CPU,&rlp);
   if (ret==0) {
      *limit = rlp.rlim_cur;
      if (*limit==RLIM_INFINITY) *limit=0;
   }
   else {
      *limit = 0;
   }
#endif
}
