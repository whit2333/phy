      SUBROUTINE FINDC(ARRAY,LEN,CHAR,NUMB,ISTART,IFINIS,ICFIND,*,*)
C
C                                                29/JULY/1980
C                                                C.J. KOST SIN
C==   THIS ROUTINE SEARCHES A GIVEN ARRAY FOR ANY ONE           ==
C==   OF A SET OF CHARACTERS. IDENTICAL EXCEPT FOR PROVISO      ==
C==   BELOW TO A ROUTINE OF THE SAME NAME IN THE U.B.C.         ==
C==   SYSTEM LIBRARY.                                           ==
C==                                                             ==
C==   DEVELOPED IN FORTRAN BY LAURENCE TORHSER 9 MAY 1979.      ==
C==                                                             ==
C==   INPUT PARAMETERS: ARRAY(LEN),CHAR(NUMB) (L*1); LEN,CHAR,  ==
C==        ISTART (I*4);                                        ==
C==   OUTPUT PARAMETERS: IFINIS,ICFIND (I*4);                   ==
C==                                                             ==
C==   'ARRAY' IS THE LOGICAL*1 ARRAY TO BE SEARCHED.            ==
C==   'CHAR' IS THE LOGICAL*1 ARRAY TO BE SEARCHED FOR          ==
C==   'ISTART' IS THE PLACE IN 'ARRAY' WHERE SCANNING IS        ==
C==        TO START.                                            ==
C==   'IFINIS' IS THE POSITION IN 'ARRAY' IN WHICH A            ==
C==        CHARACTER OF 'CHAR' WAS FOUND. IF A CHARACTER        ==
C==        OF 'CHAR' IS NOT FOUND, 'IFINIS' WILL BE SET         ==
C==        TO 0.                                                ==
C==   'ICFIND' WILL BE THE POSITION IN 'CHAR' OF THE            ==
C==        CHARACTER FOUND. IF NO CHARACTER OF 'CHAR' IS        ==
C==        FOUND, 'ICFIND' WILL BE SET TO ZERO AND WILL RETURN 1==
C==                                                             ==
C==   NOTE:IF NUMB=O ON THE FIRST CALL IN A PROGRAM, A          ==
C==        RETURN2 WILL BE EXECUTED. IF 'NUMB'=0 ON             ==
C==        SUBSEQUENT CALLS, IT WILL TAKE ON THE VALUE          ==
C==        OF 'NUMB' DURING THE CALL PRECEDING.                 ==
C==                                                             ==
C==   PROVISO: WHEREAS IN THE VERSION IN THE U.B.C.             ==
C==        SYSTEM LIBRARY, 'NUMB'=0 ON SUBSEQUENT               ==
C==        CALLS CAUSES TO VALUE OF 'NUMB' AND THE              ==
C==        STRING 'CHAR' OF THE PRECEDING CALL TO               ==
C==        BE USED, THE PRESENT ROUTINE ONLY CAUSES             ==
C==        THE VALUE OF 'NUMB' FROM THE PRECEDING CALL          ==
C==        TO BE USED, AND NOT THE PRECEDING VALUE OF 'CHAR'.   ==
C==                                                             ==
C   modified by J.Chuma, 20Mar97 for g77
C     simplified it enormously

      BYTE ARRAY(LEN), CHAR(NUMB)
      INTEGER NUMBS
      DATA NUMBS/0/
CCC
      IF( NUMB .NE. 0 )NUMBS = NUMB
      IFINIS = 0
      ICFIND = 0
      IF( ISTART.LE.0 .OR. ISTART.GT.LEN .OR. NUMBS.LE.0 )
     & RETURN 2
      DO I = ISTART, LEN
        DO J = 1, NUMBS
          IF( ARRAY(I) .EQ. CHAR(J) )THEN
            IFINIS = I
            ICFIND = J
            RETURN
          END IF
        END DO
      END DO
      RETURN 1
      END
