/* X synchronization for TRIUMF Graphics      FWJ 31-Oct-96 */

#include <X11/Xlib.h>

Display *dpy;

void xvst_sync_()
{
   XSync(dpy, True);
   printf("SYNC\n");
}
