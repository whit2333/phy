      SUBROUTINE WRITE_HP300C(IOUT,LASER,*)
C======================================================================C
C                                                                      C
C  WRITE_HP300C                                    F.W. Jones, TRIUMF  C
C                                                  C.J. Kost,  TRIUMF  C
C  Writes the bitmap stored in the dynamic array HPPAINT on unit       C
C  IOUT in HP Laserjet run length encoded compression format for       C
C  300 dpi density.                                                    C
C                                                                      C
C  If LASER=1 non-TeX inclusion                                        C
C  If LASER=2 the plot file will be made for TeX inclusion.            C
C  If LASER=3 the plot file will be for TeX inclusion and all white    C
C  space will be trimmed from the top and left side of the image       C
C  so that the significant portion of the image will start at the      C
C  current writing position on the TeX page.                           C
C  If LASER=4 non-TeX inclusion appended                               C
C                                                                      C
C  Modified 08-MAR-94 by FWJ: removed VMS IOFAST dependency, since     C
C  IOFAST is not available on ALPHA VMS.                               C
C                                                                      C
C  Modified 31-Jan-95 by J.Chuma:  allow appended plot files           C
C                                                                      C
C======================================================================C

      BYTE HPPAINT(4)    ! dynamic array dimensioned NXDIM,NYDIM
      COMMON/GRAPHICS_BITMAP/HPPAINT,NBYTES_HP,IOFFSET,NXDIM,NYDIM,MAXY

      COMMON /HARDCOPYRANGE2/ XMINH2,XMAXH2,YMINH2,YMAXH2

      CHARACTER*1 ESC,FF

      BYTE LESC, CW, STAR, SB
      DATA LESC /27/, CW /87/, STAR /42/, SB /98/

C Output buffer:

      BYTE BUFFER(607)   ! 2400/8*2+ 7 control bytes
      CHARACTER*607 CBUFFER
      EQUIVALENCE (BUFFER,CBUFFER)
CCC
      ESC=CHAR(27)
      FF=CHAR(12)
      IBITS=INT(YMAXH2)+1   ! Find extent of bitmap data...
      JBITS=INT(XMAXH2)+1
      IEND_MAX=(IBITS+7)/8

C Hplaserjet max # bits across page is 2400.
C That is 2400/8=300 bytes. Run length encoding worst case is
C 300*2=600 bytes

      IEND_MAX=MIN(IEND_MAX,600)      !To avoid buffer overflow
      JEND_MAX=MIN(JBITS,NYDIM)
      DO JEND=JEND_MAX,1,-1 ! Determine last non-zero scan line (JEND)
        DO I=1,IEND_MAX
          IF( HPPAINT(I+(JEND-1)*NXDIM+IOFFSET).NE.0 )GO TO 50
        END DO
      END DO
      JEND=1
   50 IBEG=1 ! Normal start of bitmap data
      JBEG=1

C Reset, set density, set left margin, start raster graphics, set to
C run-length encoding compression mode.

      IF ( LASER.EQ.1 )THEN           ! non-TeX, first page
        WRITE(IOUT,75,ERR=999)ESC//'E'//ESC//'*t300R'//ESC//'&a4C'//
     &    ESC//'*r1A'//ESC//'*b1M'
   75   FORMAT(A)
      ELSE IF( LASER.EQ.2 )THEN       ! TeX output
        WRITE(IOUT,75,ERR=999)ESC//'*t300R'//ESC//'*r1A'//ESC//'*b1M'
      ELSE IF( LASER.EQ.3 )THEN       ! justified TeX output
        IBEG=IEND_MAX                 ! Find starting cropping limits
        JBEG=0
        DO 60 J=1,JEND
          DO I=1,IEND_MAX
            IF( HPPAINT(I+(J-1)*NXDIM+IOFFSET).NE.0 )THEN 
              IF( JBEG.EQ.0 )JBEG=J   ! non-zero scan line (first time)
              IBEG=MIN(IBEG,I)        ! Update leftmost pixel location
              GO TO 60
            END IF
          END DO
   60   CONTINUE
        IF( JBEG.EQ.0 )JBEG=1
        WRITE(IOUT,75,ERR=999)ESC//'*t300R'//ESC//'*r1A'//ESC//'*b1M'
      ELSE IF( LASER.EQ.4 )THEN       ! non-TeX, append plot
        WRITE(IOUT,75,ERR=999)FF//ESC//'*t300R'//ESC//'&a4C'//
     &    ESC//'*r1A'//ESC//'*b1M'
      END IF
      BUFFER(1)=LESC   ! Header for pixel scan-line buffer
      BUFFER(2)=STAR
      BUFFER(3)=SB
      NBYTES=2*NXDIM
      DO J=JBEG,JEND
        CALL ZERO_ARRAY(BUFFER(8),NBYTES)    ! leave first 7 bytes alone
        BUFFER(7)=CW                         ! set BUFFER to ESC*bnnnW

C   Load first byte of a pair of bytes with repeat count.
C   A zero means the pattern isn't repeated,
C      that is it occured only once.
C    The second of the byte pairs contains the byte pattern.

        BUFFER(9)=HPPAINT(IBEG-1+1+(J-1)*NXDIM+IOFFSET)
        ILAST=IBEG
        NBYTES=2

C Find number of bytes to get from current scan line:

        DO IEND=IEND_MAX,IBEG,-1
          IF( HPPAINT(IEND+(J-1)*NXDIM+IOFFSET).NE.0 )GO TO 80
        END DO
        IEND=IBEG  ! Empty line
   80   NPUT=IEND-IBEG+1
        DO I=IBEG+1,IEND
          IF( (HPPAINT(I+(J-1)*NXDIM+IOFFSET)) .NE.
     &       (HPPAINT(I-1+(J-1)*NXDIM+IOFFSET)) )THEN  ! Pattern changed
            NREPEAT=I-ILAST-1
            ILAST=I
            IF( NREPEAT.LE.255 )THEN
              CBUFFER(NBYTES+6:NBYTES+6)=CHAR(NREPEAT)
              NBYTES=NBYTES+2
            ELSE
              CBUFFER(NBYTES+6:NBYTES+6)=CHAR(255)
              CBUFFER(NBYTES+8:NBYTES+8)=CHAR(NREPEAT-255-1)
              BUFFER(NBYTES+9)=BUFFER(NBYTES+7)
              NBYTES=NBYTES+4
            END IF

C   Load new pattern (into location 11,13,15 etc)

            BUFFER(NBYTES+1+6)=HPPAINT(I+(J-1)*NXDIM+IOFFSET)   
          END IF
        END DO

C   Take care of case where we run out of loop (or exit it because
C   scan line was empty).

        NREPEAT=IEND-ILAST
        IF( NREPEAT.LE.255 )THEN
          CBUFFER(NBYTES+6:NBYTES+6)=CHAR(NREPEAT)
        ELSE
          CBUFFER(NBYTES+6:NBYTES+6)=CHAR(255)
          CBUFFER(NBYTES+8:NBYTES+8)=CHAR(NREPEAT-255-1)
          BUFFER(NBYTES+9)=BUFFER(NBYTES+7)
          NBYTES=NBYTES+2
        END IF

C   Load bytes 4,5,6 of BUFFER with number of data bytes (from NBYTES)

        N100=NBYTES/100               ! number of hundreds
        N10=(NBYTES-N100*100)/10      ! number of tens
        N1=(NBYTES-N100*100-N10*10)   ! number of ones
        BUFFER(4)=N100+48             ! Character 0 is 48 decimal
        BUFFER(5)=N10+48
        BUFFER(6)=N1+48
        WRITE(IOUT,75,ERR=999)CBUFFER(1:NBYTES+7)  !+7 for control bytes
      END DO

C End raster graphics

      WRITE(IOUT,75,ERR=999)ESC//'*b0M'    ! end compression mode
      WRITE(IOUT,75,ERR=999)ESC//'*rB'
      IF( (LASER.EQ.2) .OR. (LASER.EQ.3) )
     & WRITE(IOUT,75,ERR=999)ESC//'*rB'  ! fix line-eating bug in DVIHP
      RETURN
  999 WRITE(*,*)'Error writing bitmap file on unit',IOUT
      CALL FORMSG
      RETURN 1
      END
