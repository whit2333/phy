      SUBROUTINE PFONT(NAME,WHAT,*)
C
C  This is a replacement for PALPHA with character font names
C  and a few other improvements.  FWJ 28-NOV-91
C  For backward compatibility, PALPHA is now a shell routine that
C  calls PFONT.
C
C  Modified 09-MAR-93 by FWJ: high-order-bit test of SWITC fails on
C  ALPHA/OSF1.  This was replaced by a fullword test since only the
C  high order bit of this parameter is used.
C
C  Modified 27-MAR-95 by FWJ: HP-UX requires record lengths in bytes.
C  Modified by J.Chuma, 20Mar97 for g77
C    OPEN doesn't allow READONLY, SHARED
C  Modified by J.Chuma, 08-Apr-1997 for g77
C    could not read direct access font file, now we have a special
C    linux vaxfont.dat file
C  Modified by J.Chuma, 14-Nov-1997 for g77
C    now you should use the standard vaxfont.dat file since the
C    open and read now seem to work OK
C
C  F.W.Jones 03-JUN-98: added SAVE and OPENs for Linux absoft.
C

#ifdef absoft
      SAVE
#endif
      CHARACTER*(*) NAME
      INTEGER*4 WHAT

C  Documentation for PALPHA follows...
C================================================================
C==                                                            ==
C==   The following documentation is adapted from UBC PLOT     ==
C==   (April 1981).                                            ==
C==                                                            ==
C==  PURPOSE:                                                  ==
C==                                                            ==
C==     PALPHA  changes  the  character  set  used by PSYM. In ==
C==     addition  to  the  standard  character  set,   several ==
C==     library  sets are provided, and user-defined character ==
C==     set are permitted.                                     ==
C==                                                            ==
C==  HOW TO USE:                                               ==
C==                                                            ==
C==     The calling sequence for PALPHA is                     ==
C==                                                            ==
C==           CALL PALPHA(NAME,WHAT,&RC4)                      ==
C==                                                            ==
C==     where                                                  ==
C==                                                            ==
C==        NAME          is  the  name of the character set to ==
C==                      be used, if the set is a library  set ==
C==                      or    the   standard   one.   For   a ==
C==                      user-defined set,  the  name  of  the ==
C==                      file   containing  the  character-set ==
C==                      definition should be given.  In  both ==
C==                      cases, the name must be terminated by ==
C==                      a  blank.  The  name  of the standard ==
C==                      character set is STANDARD.            ==
C==                                                            ==
C==        WHAT          is  a  fullword  integer  (INTEGER*4) ==
C==                      which  is zero if NAME is the name of ==
C==                      a  library  character  set  (or   the ==
C==                      standard one), and nonzero if it is a ==
C==                      file name for a user-defined set.     ==
C==                                                            ==
C==        &RC4          is  the  exit  taken if the character ==
C==                      set has not  been  found,  or  if  an ==
C==                      error occurred while reading it.      ==
C==                                                            ==
C==  METHOD:                                                   ==
C==                                                            ==
C==     Calling PALPHA causes all symbols drawn via PSYM to be ==
C==     drawn with the specified character set.                ==
C==                                                            ==
C==     If  a  library  character set is specified, it is read ==
C==     from  TRIUMF$FONTS:VAXFONT.DAT  on VAX/VMS systems     ==
C==     For the UNIX/ULTRIX systems the data is read from      ==
C==     from  TRIUMF_FONTS/vaxfont.dat where TRIUMF_FONTS      ==
C==     is an environment variable.                            ==
C==     (eg. setenv TRIUMF_FONTS /decu01/usr/users/chau/gplot  ==
C==     If   a   user-defined   set  is                        ==
C==     specified,  it is read from the file named in NAME. If ==
C==     the character set  is  read  successfully,  succeeding ==
C==     calls to PSYM use the new set.                         ==
C==                                                            ==
C==     Each character set occupies three lines in a file. The ==
C==     first  line  contains  the  control  block,  which  is ==
C==     described below. The second line is  a  directory  for ==
C==     the  character  information  on  the  third  line. The ==
C==     directory consists of 256 halfwords; halfword "m" (0<= ==
C==     m <= 255) is the displacement of the information for   ==
C==     character  "m"  in  the  third  line.  The  third line ==
C==     contains  the  actual  character   definitions.   Each ==
C==     character is defined by a sequence of points on an n*n ==
C==     grid.  The  coordinates  of  the lower-left corner are ==
C==     (0,0);  of  the  lower-right   corner   (n-1,0).   The ==
C==     character  is  drawn by drawing straight lines between ==
C==     successive points. The point (15,15) (or (255,255)  if ==
C==     n>16)  means move to the next point without drawing (a ==
C==     pen up operation). To draw to the point  (15,15)  when ==
C==     n <= 16, two successive (15,15)'s are used. A pen up is==
C==     not necessary before the first point. If n <= 16 each  ==
C==     point is represented in the character information by a ==
C==     single byte. The high-order 4 bits of the byte contain ==
C==     the    hexadecimal   representation   of   the   first ==
C==     coordinate, and  the  low-order  4  bits  contain  the ==
C==     second  coordinate. If n>16, each point is represented ==
C==     by  two  bytes,  the  first   containing   the   first ==
C==     coordinate,  and  the  second  containing  the  second ==
C==     coordinate. The format of  the  information  for  each ==
C==     character is:                                          ==
C==                                                            ==
C==     1 byte number of points (k)                            ==
C==     1 byte letterspacing (see below)                       ==
C==     k (or 2k for n>16) bytes points.                       ==
C==                                                            ==
C==     The  character definitions may be packed end-to-end in ==
C==     the   character   information.   For   example,    the ==
C==     information for a lowercase h on a 16x16 grid might be ==
C==     (in hex):                                              ==
C==                                                            ==
C==     0809333FFF3A4B8B9A93                                   ==
C==                                                            ==
C==     The  structure of the control block is shown below. In ==
C==     the  following  table,  H  means  a  halfword  integer ==
C==     (INTEGER*2), and E means a fullword real (REAL*4). The ==
C==     displacement is in decimal.                            ==
C==                                                            ==
C==                                                            ==
C Disp Type Name             Description                       ==
C                                                              ==
C  0   H   SIZE   is the total length of character information,==
C                 in bytes.                                    ==
C                                                              ==
C  2   H   GRID   is the grid size (GRID*GRID).                ==
C                                                              ==
C  4   H   HSPAC  is the number of grid units between adjacent ==
C                 characters on the same line.  It must satisfy==
C                 HSPAC > -GRID.                               ==
C                                                              ==
C  6   H   VSPAC  is the number of grid units between lines of ==
C                 characters.  It must satisfy VSPAC > -GRID.  ==
C                                                              ==
C  8  2H   PSUP   are the coordinates of a grid point to be    ==
C                 used as a reference point for superscripts   ==
C                 (halfword x coordinate, halfword y           ==
C                 coordinate).                                 ==
C                                                              ==
C 12  2H   PSUB   are the coordinates of a grid point to be    ==
C                 used as a reference point for subscripts     ==
C                 (halfword x coordinate, halfword y           ==
C                 coordinate).                                 ==
C                                                              ==
C 16   E   SHGT   is (height of subscripts and superscripts) / ==
C                 character height.                            ==
C                                                              ==
C 20   E   SCAL   are the grid proportions (horizontal/vertical==
C                                                              ==
C 24   E   SSCAL  is the height scale.  Heights given to PSYM  ==
C                 are multiplied by this value.                ==
C                                                              ==
C 28   E   ANGL   is the angle of the Y grid axis from the     ==
C                 vertical in degrees (<0 corresponds to left- ==
C                 slanting letters).  It must satisfy          ==
C                 -90.<ANGL<90.                                ==
C                                                              ==
C 32  2H   REF    are the coordinates of a grid point which is ==
C                 the reference point for characters.  For the ==
C                 first character of a string drawn by PSYM,   ==
C                 this grid point corresponds to (X,Y).        ==
C                                                              ==
C 36   H   SWITCH Only the high-order bit of SWITCH is currently=
C                 used.  If it is 1, letterspacing is enabled; ==
C                 if 0, letterspacing is disabled.             ==
C                                                              ==
C 38   H   LSPAC  is the constant horizontal spacing (similar  ==
C                 to HSPAC) used when letterspacing is enabled.==
C                                                              ==
C 40  CL16 NAME   is the 16-character alphabet name, padded    ==
C                 with blanks if necessary.                    ==
C==                                                            ==
C==                                                            ==
C==  Variable character spacing, or letterspacing, may be used ==
C==  in  character  sets.  In a set with variable spacing, the ==
C==  definition of each character includes the  width  of  the ==
C==  character  so  that the space allotted that character can ==
C==  be shortened (which will  allow  the  next  character  to ==
C==  appear  closer  to  it). Specifically, the second byte of ==
C==  the character definition contains (in hex) the coordinate ==
C==  of  the  column  which  is  considered  the  end  of  the ==
C==  character.                                                ==
C==                                                            ==
C==        WRITTEN BY RICHARD T. LEE,  TRIUMF,  UBC.           ==
C==                                     JULY 3, 1981.          ==
C==                                                            ==
C======================================================================C
C                                                                      C
C     Modified by Alan Carruthers, October 28, 1983                    C
C        -- three fonts incorrectly spelled:                           C
C              GOTHIC.ENGLISH                                          C
C              ROMAN.2A                                                C
C              GOTHIC.ITALIAN                                          C
C                                                                      C
C  Modified May 4/84 by F.W. Jones:                                    C
C                                                                      C
C  The fonts are now read from TRIUMF$FONTS:VAXFONT.DAT (VMS only)     C
C  using the LOK_IOFAST                                                C
C  routines rather than FORTRAN sequential I/O.  This improves the     C
C  speed when loading new fonts, particularly the widely-used          C
C  TRIUMF.2 font, which is at the end of all the other fonts.          C
C  "Pseudo-random-access" to TRIUMF$FONTS:VAXFONT.DAT is provided by   C
C  LOK_IOFAST$GETM which uses the record addresses stored in the       C
C  array MARK to locate each font.  The addresses in MARK were         C
C  obtained using LOK_IOFAST$MARK.  New addresses may have to be       C
C  calculated if changes are made to TRIUMF$FONTS:VAXFONT.DAT !        C
C
C  For ULTRIX/UNIX systems normal indexed reads are used together with C
C  TRIUMF_FONTS/vaxfont.dat                                            C
C                                                                      C
C  Modified May 4/84 by F.W. Jones:                                    C
C  A "font number" is stored in COMMON/EDGR_ATT_SET/ to allow the      C
C  support of fonts in the Graphics Editor.                            C
C                                                                      C
C  Modified Jun28/88 by R. Lee:                                        C
C  A new font called TRIUMF.OUTLINE is constructed and added into      C
C  the library. Address used by LOK_IOFAST$GETM is calculated for this C
C  font.                                                               C
C                                                                      C
C  Modified Apr18/91 by C. Kost:                                       C
C    Caching up to NCACHE fonts coded.                                 C
C                                                                      C
C  Modified by J.L.Chuma, 08-Apr-1997  for g77 compatability           C
C                                                                      C
C======================================================================C

      PARAMETER (NCACHE=5)

      BYTE      LINE1(56), LINE3(25000)
      INTEGER*2 LINE2(256)
      INTEGER*4 MGRID
      LOGICAL*4 GRID16, LISPON
      COMMON /FONT/ LINE1, LINE2, LINE3, GRID16, LISPON, MGRID

      REAL*4    XC, YC
      LOGICAL*4 PALPCD, PSYMCD
      COMMON /CALLED/ PALPCD, PSYMCD, XC, YC

C Graphics Editor attributes:

      INTEGER*4 IFONT_SET, ICOL_SET
      COMMON /EDGR_ATT_SET/ IFONT_SET, ICOL_SET

      INTEGER*4 IFONT_NUMBER
      COMMON /FONT_NUMBER/ IFONT_NUMBER

      REAL*4 LMAT(3,3)
      COMMON /LOTRAN/ LMAT

      CHARACTER*2 CLINE2(256)
      EQUIVALENCE (CLINE2,LINE2)

cc      BYTE      LINEC1(56,NCACHE), LINEC3(25000,NCACHE), NAMM(16)
      BYTE      LINEC1(56,NCACHE), LINEC3(25000,NCACHE)
      character*16 NAMM
      INTEGER*2 LINEC2(256,NCACHE)
      INTEGER*2 SIZE,GRID,HSPAC,VSPAC,PSUPX,PSUPY,PSUBX,PSUBY,
     &          REFX,REFY,SWITC,LSPAC

      integer switc4
      equivalence (switc,switc4)

      REAL*4    SHGT,SCAL,SSCAL,ANGL
      EQUIVALENCE (LINE1( 1),    SIZE),
     &            (LINE1( 3),    GRID),
     &            (LINE1( 5),   HSPAC),
     &            (LINE1( 7),   VSPAC),
     &            (LINE1( 9),   PSUPX),
     &            (LINE1(11),   PSUPY),
     &            (LINE1(13),   PSUBX),
     &            (LINE1(15),   PSUBY),
     &            (LINE1(17),    SHGT),
     &            (LINE1(21),    SCAL),
     &            (LINE1(25),   SSCAL),
     &            (LINE1(29),    ANGL),
     &            (LINE1(33),    REFX),
     &            (LINE1(35),    REFY),
     &            (LINE1(37),   SWITC),
     &            (LINE1(39),   LSPAC),
     &            (LINE1(41),    NAMM)
cc     &            (LINE1(41), NAMM(1))
      INTEGER*2 SWITON
C  Modified by J.Chuma, 23Apr97 for Absoft f77 -- changed X' to Z'
cc      DATA SWITON /Z'8000'/

      integer iswiton
      equivalence (switon,iswiton)
      data iswiton /Z'8000'/

C   LIB contains the font names
C   NFONTS is the number of font names
C   LIBFONT is the current font name

      PARAMETER (NFONTS=43)
      CHARACTER*20 LIB(NFONTS), LIBFONT
      COMMON /PALPHA_FONTS/ LIB, LIBFONT
      DATA LIBFONT /'STANDARD'/
      DATA LIB
     X /'STANDARD',
     X  'HELVETICA.1',
     X  'GOTHIC.ENGLISH',
     X  'ROMAN.3',
     X  'ITALIC.3',
     X  'GREEK.2A',
     X  'ITALIC.2A',
     X  'ROMAN.2A',
     X  'SANSERIF.2',
     X  'GREEK.1',
     X  'SANSERIF.1',
     X  'SCRIPT.1',
     X  'GOTHIC.FRAKTUR',
     X  'GOTHIC.ITALIAN',
     X  'SCRIPT.2',
     X  'ROMAN.2',
     X  'ITALIC.2',
     X  'GREEK.2',
     X  'CYRILLIC.2',
     X  'SANSERIF.CART',
     X  'GREEK.CART',
     X  'HIRAGANA',
     X  'KATAKANA',
     X  'KANJI1',
     X  'KANJI2',
     X  'KANJI3',
     X  'KANJI4',
     X  'KANJI5',
     X  'OLDALPH',
     X  'TRIUMF.1',
     X  'TRIUMF.2',
     X  'TSAN',
     X  'ROMAN.FUTURA',
     X  'ROMAN.SERIF',
     X  'ROMAN.FASHON',
     X  'ROMAN.LOGO1',
     X  'ROMAN.SWISSL',
     X  'ROMAN.SWISSM',
     X  'ROMAN.SWISSB',
     X  'SPECIAL',
     X  'MATH',
     X  'HEBREW',
     X  'TRIUMF.OUTLINE'/

#ifdef VMS
      CHARACTER*256 VAXFILENAME
#else
      CHARACTER*6000 BUFF1, BUFF2, BUFF3, BUFF4
      COMMON /PALPHA_BUFFS/ BUFF1, BUFF2, BUFF3, BUFF4

      CHARACTER*255 FNAME
      INTEGER*2     IBUFF1(3000)
      BYTE          BBUFF(24000)
      EQUIVALENCE (IBUFF1,BUFF1,BBUFF)
#ifdef BEND
      INTEGER*2     IDUM2(2)
      BYTE          BDUM(4)
      EQUIVALENCE (IDUM2(1),BDUM(1))
#endif
#endif
      REAL*4 RADE
      DATA RADE /0.1745329E-1/

      INTEGER*4 MARKC(43), MARKI(NCACHE), IREPLACE
      DATA MARKC /43*0/, MARKI /NCACHE*0/

      LOGICAL*4 FIRST
      DATA FIRST /.TRUE./

C      character*180 ctmp
CCC
      IF( WHAT .NE. 0 )THEN
        CALL TRANSPARENT_MODE(0)
        WRITE(*,*)' PFONT error: user defined font files not supported'
        RETURN 1
      END IF
#ifdef VMS
      IF( FIRST )THEN
        CALL FIND_UNIT(LUFL)
        OPEN(UNIT=LUFL,FILE='TRIUMF$FONTS:VAXFONT.DAT'
     &   ,STATUS='OLD',READONLY,SHARED,RECL=25000,ERR=990)
      END IF
#else
      IF( FIRST )THEN
         CALL GETENV('TRIUMF_FONTS',FNAME)
         IF( LENSIG(FNAME).GT.0 )THEN
#ifdef __hpux
           OPEN(19,FILE=FNAME(1:LENSIG(FNAME))//'/vaxfont.dat',
     &       STATUS='OLD',READONLY,SHARED,RECL=12000,
     &       ACCESS='DIRECT',
     &       FORM='UNFORMATTED',ERR=990)
#elif _AIX
           OPEN(19,FILE=FNAME(1:LENSIG(FNAME))//'/vaxfont.dat',
     &       STATUS='OLD',RECL=12000,ACTION='READ',
     &       ACCESS='DIRECT',
     &       FORM='UNFORMATTED',ERR=990)
#elif g77
           OPEN(
     &          UNIT=19
     &         ,FILE=FNAME(1:LENSIG(FNAME))//'/vaxfont.dat'
     &         ,ACCESS='DIRECT'
     &         ,STATUS='OLD'
     &         ,FORM='UNFORMATTED'
     &         ,RECL=12000
     &         ,ERR=990
     &         )
#elif gfortran
           OPEN(
     &          UNIT=19
     &         ,FILE=FNAME(1:LENSIG(FNAME))//'/vaxfont.dat'
     &         ,ACCESS='DIRECT'
     &         ,STATUS='OLD'
     &         ,FORM='UNFORMATTED'
     &         ,RECL=12000
     &         ,ERR=990
     &         )
#elif absoft
           OPEN(19,FILE=FNAME(1:LENSIG(FNAME))//'/vaxfont.dat',
     &       STATUS='OLD',READONLY,SHARED,RECL=12000,
     &       ACCESS='DIRECT',
     &       FORM='UNFORMATTED',ERR=990)
#else
           OPEN(19,FILE=FNAME(1:LENSIG(FNAME))//'/vaxfont.dat',
     &       STATUS='OLD',READONLY,SHARED,RECL=3000,
     &       ACCESS='DIRECT',
     &       FORM='UNFORMATTED',ERR=990)
#endif
         ELSE
#ifdef __hpux
           OPEN(19,FILE='/usr/local/bin/vaxfont.dat',
     &       STATUS='OLD',READONLY,SHARED,RECL=12000,
     &       ACCESS='DIRECT',
     &       FORM='UNFORMATTED',ERR=990)
#elif _AIX
           OPEN(19,FILE='/usr/local/bin/vaxfont.dat',
     &       STATUS='OLD',RECL=12000,ACTION='READ',
     &       ACCESS='DIRECT',
     &       FORM='UNFORMATTED',ERR=990)
#elif g77
           OPEN(19,FILE='/usr/local/bin/vaxfont.dat',
     &       STATUS='OLD',FORM='UNFORMATTED',ERR=990)
#elif gfortran
           OPEN(19,FILE='/usr/local/bin/vaxfont.dat',
     &       STATUS='OLD',FORM='UNFORMATTED',ERR=990)
#elif absoft
           OPEN(19,FILE='/usr/local/bin/vaxfont.dat',
     &       STATUS='OLD',READONLY,SHARED,RECL=12000,
     &       ACCESS='DIRECT',
     &       FORM='UNFORMATTED',ERR=990)
#else
           OPEN(19,FILE='/usr/local/bin/vaxfont.dat',
     &       STATUS='OLD',READONLY,SHARED,RECL=3000,
     &       ACCESS='DIRECT',
     &       FORM='UNFORMATTED',ERR=990)
#endif
         END IF
      END IF
#endif

C   The character set is to be found in the library

cc      write(*,*)'nfonts=',nfonts
cc      write(*,*)'name=',name

      DO I = 1, NFONTS              ! Set the current font name for

cc        write(*,*)'i=',i,', lib(i)=',lib(i)

        IF( NAME .EQ. LIB(I) )THEN  ! the common block PALPHA_FONTS
          LIBFONT = NAME
          IFOUND = I
          GO TO 200
        END IF
      END DO

C   The character set was not found in the library

      CALL TRANSPARENT_MODE(0)
      WRITE(*,*)' PFONT: no such font ',NAME
      RETURN 1
  200 I = IFOUND

C Set font # in EDGR:

      IFONT_SET = I
      IFONT_NUMBER = I
      NUMREC = 2*(I-1)+1

C  Check to see if font # I is in cache

      IF( MARKC(I) .EQ. 0 )GO TO 253

C  Font # I is in cache

      CALL COPY_ARRAY(LINEC1(1,MARKC(I)),LINE1,56)
      CALL COPY_ARRAY(LINEC2(1,MARKC(I)),LINE2,512)
      ISIZE = SIZE
      CALL COPY_ARRAY(LINEC3(1,MARKC(I)),LINE3,ISIZE)
      GO TO 300

C Font not in cache....

#ifdef VMS
  253 REWIND( LUFL )
      DO N = 1, 3*(I-1)
        READ(LUFL,*)
      END DO

C  Read in elements of first record to define LINE1
C  Read in elements of second record to define LINE2
C  Read in elements of third record to define LINE3

      READ(LUFL,254,ERR=900)SIZE,GRID,HSPAC,VSPAC,PSUPX,PSUPY,PSUBX,
     & PSUBY,SHGT,SCAL,SSCAL,ANGL,REFX,REFY,SWITC,LSPAC,NAMM
  254 FORMAT(8I8,4F16.6,4I8,4X,16A1)
      READ(LUFL,256,ERR=900)LINE2
  256 FORMAT(256A2)
      READ(LUFL,255,ERR=900)(LINE3(N),N=1,SIZE)
  255 FORMAT(25000A1)
      FIRST = .FALSE.

#else 

C    Read the double record into the 4 buffs BUFF1 BUFF2 BUFF3 BUFF4

  253 READ(19,REC=NUMREC,ERR=910)BUFF1,BUFF2
      NUMREC=NUMREC+1
      READ(19,REC=NUMREC,ERR=911)BUFF3,BUFF4

C Read the first record:

      READ(BUFF1(1:180),254,ERR=900)SIZE,GRID,HSPAC,VSPAC,PSUPX,PSUPY,
     & PSUBX,PSUBY,SHGT,SCAL,SSCAL,ANGL,REFX,REFY,SWITC4,LSPAC,NAMM
  254 FORMAT(8I8,4F16.6,4I8,4X,16A1)

C      read(ctmp(1:8),401,err=900)size
C 401  format(i8)
C      write(*,*)'size=',size
C      read(ctmp(9:16),401,err=900)grid
C      write(*,*)'grid=',grid
C      read(ctmp(17:24),401,err=900)hspac
C      write(*,*)'hspac=',hspac
C      read(ctmp(25:32),401,err=900)vspac
C      write(*,*)'vspac=',vspac
C      read(ctmp(33:40),401,err=900)psupx
C      write(*,*)'psupx=',psupx
C      read(ctmp(41:48),401,err=900)psupy
C      write(*,*)'psupy=',psupy
C      read(ctmp(49:56),401,err=900)psubx
C      write(*,*)'psubx=',psubx
C      read(ctmp(57:64),401,err=900)psuby
C      write(*,*)'psuby=',psuby
C      read(ctmp(65:80),402,err=900)shgt
C 402  format(f16.6)
C      write(*,*)'shgt=',shgt
C      read(ctmp(81:96),402,err=900)scal
C      write(*,*)'scal=',scal
C      read(ctmp(97:112),402,err=900)sscal
C      write(*,*)'sscal=',sscal
C      read(ctmp(113:128),402,err=900)angl
C      write(*,*)'angl=',angl
C      read(ctmp(129:136),401,err=900)refx
C      write(*,*)'refx=',refx
C      read(ctmp(137:144),401,err=900)refy
C      write(*,*)'refy=',refy
C      read(ctmp(145:152),401,err=900)switc4
C      write(*,*)'switc=',switc
C      read(ctmp(153:160),401,err=900)lspac
C      write(*,*)'lspac=',lspac
C
C      namm = ctmp(165:180)

C Read the second record:

      DO II = 1, 256
#ifdef BEND
        IDUM2(1) = IBUFF1(90+II)
        BDUM(4) = BDUM(1)
        BDUM(3) = BDUM(2)
        LINE2(II) = IDUM2(2)
#else
        LINE2(II) = IBUFF1(90+II)
#endif
      END DO

C   Read the third record

      NSIZE = SIZE
      DO II = 1, NSIZE
       III = II+692
       LINE3(II) = BBUFF(III)
      END DO
  290 FIRST = .FALSE.
#endif

C Cache this font for later use

      IREPLACE = IREPLACE+1

C  Ensure IREPLACE in range 1 to NCACHE

      IREPLACE = MOD(IREPLACE-1,NCACHE)+1
      IF( MARKI(IREPLACE).NE.0 )MARKC(MARKI(IREPLACE))=0

C  Store font # in MARKI (length NCACHE)

      MARKI(IREPLACE) = I

C  Store LINEC*'th index where font I is cached in location I of MARKC

      MARKC(I) = IREPLACE

C  Store the LINE* stuff in IREPLACE'th cache.

      CALL COPY_ARRAY(LINE1,LINEC1(1,IREPLACE),56)
      CALL COPY_ARRAY(LINE2,LINEC2(1,IREPLACE),512)
      CALL COPY_ARRAY(LINE3,LINEC3(1,IREPLACE),25000)

C  Common section to font cached or not...

  300 PALPCD = .TRUE.
      GRID16 = .TRUE.
      MGRID = 1
      IF( GRID .GT. 16 )GRID16 = .FALSE.
      IF( GRID .GT. 16 )MGRID = 2

C   This is an entry point to update some of the internal parameters

      ENTRY LOCTRA

      LISPON = (SWITC.EQ.SWITON)

C   Calculate the local transformation

      SD = SSCAL/GRID
      SL11 = SD*SCAL
      LMAT(1,1) = SL11
      LMAT(1,2) = 0.
      LMAT(1,3) = 0.
      LMAT(2,1) = SL11*TAN(ANGL*RADE)
      LMAT(2,2) = SD
      LMAT(2,3) = 0.
      LMAT(3,1) = SL11*(-REFX)
      LMAT(3,2) = SD*(-REFY)
      LMAT(3,3) = 1.
      RETURN

  900 CALL TRANSPARENT_MODE(0)
      WRITE(*,*)' PFONT: error reading font data'
      RETURN 1

#ifdef VMS
  990 CALL TRANSPARENT_MODE(0)
      IDUM = NARGSI(1)
      CALL PUT_SYSMSG(ISTAT)
      ISTAT = LIB$SYS_TRNLOG('TRIUMF$FONTS',,VAXFILENAME)
      WRITE(*,1099)'PFONT error: Cant find a file associated with '//
     & VAXFILENAME(1:LENSIG(VAXFILENAME))//' VAXFONT.DAT'
 1099 FORMAT(' ',A)
#endif
#ifdef unix
  910 CALL TRANSPARENT_MODE(0)
      WRITE(*,*)' FONT FILE READ ERROR, NUMREC=',NUMREC
      RETURN 1
  911 CALL TRANSPARENT_MODE(0)
      WRITE(*,*)' 2nd FONT FILE READ ERROR, NUMREC=',NUMREC
      RETURN 1

  990 CALL TRANSPARENT_MODE(0)
      WRITE(*,1099)
 1099 FORMAT(' vaxfont.dat file not found.',/,
     & ' Check environment variable TRIUMF_FONTS')
#endif
      STOP
      END
