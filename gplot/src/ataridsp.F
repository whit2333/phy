C*************************************************************************
C                                                                        *
C  Modified 27-JUL-92 by FWJ: ported to ULTRIX.                          *
C                                                                        *
C    25-aug-87  Both ST1D and ST2D were extensively modified.  Format    *
C  of passing the data to the terminal has now changed - these routines  *
C  only work on version 0.9j or later of ST640.  The old routines will   *
C  not work on version 0.9j or later!!!                                  *
C    Basic changes are:                                                  *
C         a) you now specify a window that you want the plot to fit      *
C            into.  You still must specify the corners of the plot       *
C            itself.  This allows me to position the labels in a simple  *
C            manner and allows selective erasing of parts of the screen. *
C         b) The y-axis on a 1-d plot can now be linear, sqrt, or log.   *
C         c) You can choose whether grids are displayed over the plot.   *
C         d) Choice of dashed or solid lines on 1-d plots (for           *
C            overlaying two plots.                                       *
C                                                                        *
C*************************************************************************
C
C*************************************************************************
C    11-dec-87  Finally really implemented the logarithmic and square    *
C               root y-axis types.  IMPORTANT: To get reasonable         *
C               labelling of the y-axis, make sure at least one decade   *
C               is displayed.                                            *
C                                                                        *
C*************************************************************************
C
C*************************************************************************
C    5-mar-88  Implemented checking for input errors to these subroutines*
C              since input errors can crash the ST640 program - easier to*
C              put error messages here than in the ST640 program.  I will*
C              probably also put checks for errors in the ST640 program, *
C              but no error messages                                     *
C                The error numbers are listed below.  When an error is   *
C              found you get a detailed list of your input paramters and *
C              an error number code.  You must consult the list below to *
C              determine exactly what I am complaining about             *
C                                                                        *
C                Also changed things so that passing ST2D a spectrum     *
C              where ZMIN=ZMAX (most often an empty spectrum) will       *
C              display an empty screen rather than a full (black) screen *
C              as it did before                                          *
C                                                                        *
C                The strings XLABEL, YLABEL, and TITLE no longer have    *
C              to be zero terminated nor 80 characters long.             *
C                                                                        *
C*************************************************************************
C
C  ERROR CODES PRINTED OUT BY ST1D AND ST2D:
C
C IERR = 0  No error was found
C        1  IWXMIN outside valid 4010 limits
C        2  IWXMAX outside valid 4010 limits
C        3  IWYMIN outside valid 4010 limits
C        4  IWYMAX outside valid 4010 limits
C        5  IDXMIN outside valid 4010 limits 
C        6  IDXMAX outside valid 4010 limits 
C        7  IDYMIN outside valid 4010 limits 
C        8  IDYMAX outside valid 4010 limits
C        9  IWXMIN >= IWXMAX
C       10  IWYMIN >= IWYMAX
C       11  IDXMIN >= IDXMAX
C       12  IDYMIN >= IDYMAX
C            ; next few are for ST1D only
C       21  NDAT > 32767
C       22  XRED <= 0
C       23  YMIN > YMAX
C            ; next few are for ST2D only
C       31  NXDIM > 32767
C       32  NYDIM > 32767
C       33  IXSTRT > NXDIM
C       34  IXSTOP > NXDIM
C       35  IYSTRT > NYDIM
C       36  IYSTOP > NYDIM
C       37  IXSTRT > IXSTOP
C       38  IYSTRT > IYSTOP
C       39  XRED <= 0
C       40  YRED <= 0
C
C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
C
      SUBROUTINE  ST1D( 
     +            IWXMIN, IWYMIN, IWXMAX, IWYMAX,
     +            IDXMIN, IDYMIN, IDXMAX, IDYMAX,
     +            DAT, NDAT, XMIN, XRED, YMIN, YMAX, CLR_IT, CLR_ANSI,
     +            CLR_WINDOW, GRIDS, YSQRT, YLOG, DASHED,
     +            XLABEL, YLABEL, TITLE )
C
C   This routine passes the required data to an Atari ST terminal for
C   display of a histogram. This should be slightly faster than the
C   usual histogram displays since (a) I know that I want to display
C   things as histogram bars so I only pass the height of the bars,
C   (b) I will be using a compression technique where each channel
C   requires one and a quarter bytes to transmit it.
C   and (c) the tics on the axes and the labels for the tics are all
C   done by the Atari - you just pass the limits for the plot.
C     The following parameters are required:
C
C   IWXMIN, IWYMIN  -  Tektronix screen coords [(0,0) - (1023,779)] of
C                      where you want the lower left corner of the
C                      display window to be.  All elements of the
C                      display, data points, labels, etc will be con-
C                      fined to this window.
C   IWXMAX, IWYMAX  -  Tektronix screen coords of where you want the 
C                      upper right corner of the display window to be
C                      (see notes on display window under IWXMIN, IWXMAX)
C   IDXMIN, IDYMIN  -  Tektronix screen coords [(0,0) - (1023,779)] of
C                      where you want the lower left corner of the plot
C                      to be.  MUST BE GREATER THAN (15,10)
C   IDXMAX, IDYMAX  -  Tektronix screen coords of where you want the upper
C                      right corner of the plot to be.  MUST BE LESS THAN
C                      (1008,769) so tics can be drawn.
C   DAT             -  array containing the data to be displayed. At 
C                      present this is REAL*4 since that is what LISA uses.
C   NDAT            -  number of data points to display, i.e. display 
C                      from DAT(1) to DAT(NDAT)
C   XMIN            -  ordinate value corresponding to the first bin in DAT
C   XRED            -  reduction factor for DAT spectrum, i.e. the DAT(1)
C                      element contains counts for x-values in the range
C                      XMIN to XMIN+XRED, etc.
C   YMIN, YMAX      -  min and max values for the y-axis on the plot.
C                      IMPORTANT: see note under YLOG!
C   CLR_IT          -  if TRUE then the entire screen will be cleared 
C                      before the histogram is plotted. (see also 
C                      CLR_WINDOW)
C   CLR_ANSI        -  if true the ANSI screen will also be cleared.
C   CLR_WINDOW      -  if TRUE, then the region specified by (IWXMIN,
C                      IWYMIN) -> (IWXMAX,IWYMAX) will be cleared before
C                      the graph is displayed.
C   GRIDS           -  if TRUE then a grid will be displayed over the
C                      plot.  This grid will appear the the tic marks
C                      selected by ST640 and will be drawn with a dotted
C                      line
C   YSQRT           -  if TRUE and YLOG=FALSE then the y-axis will be
C                      the square root of the coordinate.
C   YLOG            -  if TRUE the y-axis will be logrithmic.
C   DASHED          -  if TRUE then the lines making up the histogram
C                      display will be dashed rather than solid.
C   XLABEL, YLABEL, TITLE
C                   -  character arrays for labelling the axes and giving 
C                      a title.  Max characters for any string is 80.  Note 
C                      that for convenience I put the labels right at the 
C                      edge of the window - you can do your own labelling
C                      (remember that normal T4010 commands still work with
C                      the emulator.)
C
      INTEGER NDAT, IDXMIN, IDXMAX, IDYMIN, IDYMAX
      REAL DAT(NDAT), YMIN, YMAX, XMIN, XRED
      INTEGER IWXMIN, IWYMIN, IWXMAX, IWYMAX
      LOGICAL CLR_IT, CLR_ANSI, CLR_WINDOW, GRIDS, YSQRT, YLOG, DASHED
      CHARACTER*(*) XLABEL, YLABEL, TITLE
C
C  check the input for obvious errors
C
      IERR = 0
      IF ( (IWXMIN.LT.0) .OR. (IWXMIN.GE.1024) ) IERR = 1
      IF ( (IWYMIN.LT.0) .OR. (IWYMIN.GE.1024) ) IERR = 2
      IF ( (IWXMAX.LT.0) .OR. (IWXMAX.GE.1024) ) IERR = 3
      IF ( (IWYMAX.LT.0) .OR. (IWYMAX.GE.1024) ) IERR = 4
      IF ( (IDXMIN.LT.0) .OR. (IDXMIN.GE.1024) ) IERR = 5
      IF ( (IDYMIN.LT.0) .OR. (IDYMIN.GE.1024) ) IERR = 6
      IF ( (IDXMAX.LT.0) .OR. (IDXMAX.GE.1024) ) IERR = 7
      IF ( (IDYMAX.LT.0) .OR. (IDYMAX.GE.1024) ) IERR = 8
      IF ( IWXMIN .GE. IWXMAX ) IERR = 9
      IF ( IWYMIN .GE. IWYMAX ) IERR = 10
      IF ( IDXMIN .GE. IDXMAX ) IERR = 11
      IF ( IDYMIN .GE. IDYMAX ) IERR = 12
C
      IF ( NDAT .GT. 32767 ) IERR = 21
      IF ( XRED .LE. 0 ) IERR = 22
      IF ( YMIN .GT. YMAX ) IERR = 23
      IF ( IERR .NE. 0 ) GOTO 902
C
C  do some error checking
C
      IF ( YLOG .AND. ( (YMIN.LE.0.0) .OR. (YMAX.LE.0.0) ) ) GOTO 900
      IF ( YSQRT .AND. ( (YMIN.LT.0.0) .OR. (YMAX.LT.0.0) ) ) GOTO 901
C
C  initialize the TIPRULL routines and clear screens if requested.
C
      CALL TICTL(1)
      IF ( CLR_ANSI ) THEN
        CALL TISTOR( 24 )   ! CAN
        CALL TISTOR( 27 )   ! ESC [ 2 J
        CALL TISTOR( 91 )
        CALL TISTOR( 50 )
        CALL TISTOR( 74 )
        CALL TISTOR( 27 )   ! ESC [ H
        CALL TISTOR( 91 )
        CALL TISTOR( 72 )
        CALL TISTOR( 31 )   ! US - enter alpha mode
       ENDIF
      IF ( CLR_IT ) CALL TICTL(2)
C
C  enter user mode # 1
C
      CALL TISTOR( 27 )    ! ESC " 1 g
      CALL TISTOR( 34 )
      CALL TISTOR( 49 )
      CALL TISTOR( 103 )
C
C  send the options word 
C
      CALL SENDINT( IOPTIONS( CLR_WINDOW, GRIDS, YSQRT, YLOG, DASHED) )
C
C  send the limits of the display window
C
      CALL SENDINT( IWXMIN )    ! lower left corner
      CALL SENDINT( IWYMIN )
      CALL SENDINT( IWXMAX )    ! upper left corner
      CALL SENDINT( IWYMAX )
C
C  send the limits for the graph window
C
      CALL SENDINT( IDXMIN )     ! lower left corner
      CALL SENDINT( IDYMIN )
      CALL SENDINT( IDXMAX )     ! upper right corner
      CALL SENDINT( IDYMAX )
      DHT = IDYMAX - IDYMIN
      DYMIN = IDYMIN
C
C  send the real world limits for the plot axes
C
      CALL SENDREAL( XMIN )
      CALL SENDREAL( XMIN + XRED*NDAT )
      CALL SENDREAL( YMIN )
      CALL SENDREAL( YMAX )
C
C  send the data itself - data is packed into 1.25 bytes per channel
C  and a series of adjacent channels with no counts is sent as a 0
C  followed by a count of the number of adjacent zero count channels.
C
      CALL SENDINT( NDAT )   ! number of bins
      CALL INIPCK             ! clear the packed card array
C
C  'limits' for the y-axis depend on the type of axis
C
      IF ( YLOG ) THEN
        YYMIN = ALOG10(YMIN)
        YYMAX = ALOG10(YMAX)
      ELSE IF ( YSQRT ) THEN
        YYMIN = SQRT(YMIN)
        YYMAX = SQRT(YMAX)
      ELSE
        YYMIN = YMIN
        YYMAX = YMAX
      ENDIF
      DELTAY = YYMAX - YYMIN
      IZEROS = 0

      DO 200 I = 1, NDAT
C
        IF ( .NOT. (YSQRT .OR. YLOG) ) THEN
          YDAT = DAT(I)
        ELSE IF ( YLOG ) THEN
          IF ( DAT(I) .LE. 0.0 ) THEN
            YDAT = -99
           ELSE
            YDAT = ALOG10(DAT(I))
           ENDIF
         ELSE
          IF ( DAT(I) .LT. 0.0 ) THEN
            YDAT = -1
          ELSE
            YDAT = SQRT(DAT(I))
          ENDIF
         ENDIF
C
        IF ( YDAT .LT. YYMIN ) THEN
          YDAT = YYMIN
        ELSE IF ( YDAT .GT. YYMAX ) THEN
          YDAT = YYMAX
        ENDIF
C        ; scale the data to Tektronix coordinates
        NY = NINT( (YDAT-YYMIN)/DELTAY*DHT )
        IF ( NY .EQ. 0 ) THEN
          IZEROS = IZEROS + 1
          IF ( IZEROS .EQ. 1023 ) THEN ! SENDPCARD can not handle # > 1023
            CALL SENDPCARD( 0 )
            CALL SENDPCARD( IZEROS )
            IZEROS = 0
           ENDIF
         ELSE  ! need to send data - get rid of accumulated zeros first
          IF ( IZEROS .NE. 0 ) THEN
            CALL SENDPCARD( 0 )
            CALL SENDPCARD( IZEROS )
            IZEROS = 0
           ENDIF
          CALL SENDPCARD( NY + IDYMIN )
        ENDIF
  200 CONTINUE
C
C  get rid of the rest of the zeros
C
      IF ( IZEROS .NE. 0 ) THEN
        CALL SENDPCARD( 0 )
        CALL SENDPCARD( IZEROS )
       ENDIF
      CALL CLRPCK             ! flush the packed card array buffer
C
C  send the strings for the labels
C
      CALL SENDSTR( XLABEL )
      CALL SENDSTR( YLABEL )
      CALL SENDSTR( TITLE )
C
C  clear the buffer and return
C
      CALL TICTL(5)
      RETURN
C
C  error messages
C
  900 CONTINUE  ! ylog and one or both of the limits are not > 0
        WRITE(*,1000) 'logrithmic', YMIN, YMAX
        RETURN
  901 CONTINUE  ! ylog and one or both of the limits are not > 0
        WRITE(*,1000) 'square root', YMIN, YMAX
        RETURN
  902 CONTINUE  ! input error found - dump parameters
        WRITE(*,1001) IWXMIN, IWYMIN, IWXMAX, IWYMAX, IDXMIN,
     +     IDYMIN, IDXMAX, IDYMAX, NDAT, XMIN, XRED, YMIN, YMAX,
     +     IERR
        RETURN
C
C  format statements
C
 1000 FORMAT( ' *** ERROR IN ST1D **** ', /
     +1x,'One or both of the limits passed to ST1D was inconsistent',/
     +1x,'with a ', A, ' y-axis.  The passed limits are YMIN =', G13.6,/
     +1x,'YMAX =',G13.6,'  This is an error in your display package.',/)
 1001 FORMAT( ' *** ERROR IN ST1D **** ', /
     +1X,'Parameters passed to ST1D are incorrect.  Note the number',/
     +1x,'after IERR= below and report it to the person who program-',/
     +1x,'med your graphics package.  Other numbers below would be',/
     +1x,'useful to copy also. ',//,
     +1x,'IWXMIN,IWYMIN,IWXMAX,IWYMAX=',4I10,/
     +1x,'IDXMIN,IDYMIN,IDXMAX,IDYMAX=',4I10,/
     +1X,'NDAT=', I,/,
     +1X,'XMIN,XRED=', 2G15.6,/
     +1X,'YMIN,YMAX=', 2G15.6,//
     +1X,'   *** IERR = ', I, / )
      END

C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
C
      SUBROUTINE  ST2D( 
     +            IWXMIN, IWYMIN, IWXMAX, IWYMAX,
     +            IDXMIN, IDYMIN, IDXMAX, IDYMAX,
     +            DAT, NXDIM, NYDIM,
     +            IXSTRT, IXSTOP, IYSTRT, IYSTOP,
     +            XMIN, XRED, YMIN, YRED,
     +            ZMIN, ZMAX,
     +            CLR_IT, CLR_ANSI, CLR_WINDOW, GRIDS,
     +            XLABEL, YLABEL, TITLE )
C
C   This routine passes the required data to an Atari ST terminal for
C   display of a 2D histogram. This should be slightly faster than the
C   usual histogram displays since (a) I know that I want to display
C   things as boxes so I only pass the number of counts scaled to 0-15,
C   (b) I will be using a compression technique where each bin
C   requires half a byte to transmit it.
C   and (c) the tics on the axes and the labels for the tics are all
C   done by the Atari - you just pass the limits for the plot.
C     The following parameters are required:
C
C   IWXMIN, IWYMIN  -  Tektronix screen coords [(0,0) - (1023,779)] of
C                      where you want the lower left corner of the
C                      display window to be.  All elements of the
C                      display, data points, labels, etc will be con-
C                      fined to this window.
C   IWXMAX, IWYMAX  -  Tektronix screen coords of where you want the 
C                      upper right corner of the display window to be
C                      (see notes on display window under IWXMIN, IWXMAX)
C   IDXMIN, IDYMIN  -  Tektronix screen coords [(0,0) - (1023,779)] of
C                      where you want the lower left corner of the plot
C                      to be.  MUST BE GREATER THAN (15,10)
C   IDXMAX, IDYMAX  -  Tektronix screen coords of where you want the upper
C                      right corner of the plot to be.  MUST BE LESS THAN
C                      (1008,769) so tics can be drawn.
C   DAT             -  array containing the data to be displayed. At 
C                      present this is REAL*4 since that is what LISA uses.
C   NXDIM, NYDIM    -  dimensions of the array DAT, declared as 
C                      DAT(NXDIM,NYDIM)
C   IXSTRT, IXSTOP, IYSTRT, IYSTOP
C                   -  starting and stopping indices in the array for the
C                      subregion to be plotted
C   XMIN            -  ordinate value corresponding to DAT(IXSTRT,IYSTRT)
C   XRED            -  reduction factor for DAT spectrum, i.e. the DAT(1,1)
C                      element contains counts for x-values in the range
C                      XMIN to XMIN+XRED, etc.
C   YMIN            -  coordinate value corresponding to DAT(IXSTRT,IYSTRT)
C   YRED            -  reduction factor for DAT spectrum, i.e. the DAT(1,1)
C                      element contains counts for y-values in the range
C                      YMIN to YMIN+YRED, etc.
C   ZMIN, ZMAX      -  bins with counts less than or equal to ZMIN will not
C                      be displayed on the plot.  Bins with counts greater
C                      than or equal to ZMAX will be displayed as full size
C                      boxes on the plot (size of box depends on number of
C                      bins).  Bins with counts between ZMIN and ZMAX will
C                      be displayed as boxes of size proportional to the
C                      number of counts in the bin.
C   CLR_IT          -  if TRUE then the entire screen will be cleared 
C                      before the histogram is plotted. (see also 
C                      CLR_WINDOW)
C   CLR_ANSI        -  if true the ANSI screen will also be cleared.
C   CLR_WINDOW      -  if TRUE, then the region specified by (IWXMIN,
C                      IWYMIN) -> (IWXMAX,IWYMAX) will be cleared before
C                      the graph is displayed.
C   GRIDS           -  if TRUE then a grid will be displayed over the
C                      plot.  This grid will appear the the tic marks
C                      selected by ST640 and will be drawn with a dotted
C                      line
C   XLABEL, YLABEL, TITLE
C                   -  character arrays for labelling the axes and giving 
C                      a title.  Max characters for any string is 80.  Note 
C                      that for convenience I put the labels right at the 
C                      edge of the window - you can do your own labelling
C                      (remember that normal T4010 commands still work with
C                      the emulator.)
C
      REAL DAT(NXDIM,NYDIM), XMIN, XRED, YMIN, YRED, ZMIN, ZMAX
      INTEGER IWXMIN, IWYMIN, IWXMAX, IWYMAX
      INTEGER IDXMIN, IDXMAX, IDYMIN, IDYMAX
      INTEGER IXSTRT, IXSTOP, IYSTRT, IYSTOP
      LOGICAL CLR_IT, CLR_ANSI, CLR_WINDOW, GRIDS
      CHARACTER*(*) XLABEL, YLABEL, TITLE
      BYTE IZ, BZEROS(2)
      EQUIVALENCE (IZEROS,BZEROS)
C
C  check the input for obvious errors
C
      IERR = 0
      IF ( (IWXMIN.LT.0) .OR. (IWXMIN.GE.1024) ) IERR = 1
      IF ( (IWYMIN.LT.0) .OR. (IWYMIN.GE.1024) ) IERR = 2
      IF ( (IWXMAX.LT.0) .OR. (IWXMAX.GE.1024) ) IERR = 3
      IF ( (IWYMAX.LT.0) .OR. (IWYMAX.GE.1024) ) IERR = 4
      IF ( (IDXMIN.LT.0) .OR. (IDXMIN.GE.1024) ) IERR = 5
      IF ( (IDYMIN.LT.0) .OR. (IDYMIN.GE.1024) ) IERR = 6
      IF ( (IDXMAX.LT.0) .OR. (IDXMAX.GE.1024) ) IERR = 7
      IF ( (IDYMAX.LT.0) .OR. (IDYMAX.GE.1024) ) IERR = 8
      IF ( IWXMIN .GE. IWXMAX ) IERR = 9
      IF ( IWYMIN .GE. IWYMAX ) IERR = 10
      IF ( IDXMIN .GE. IDXMAX ) IERR = 11
      IF ( IDYMIN .GE. IDYMAX ) IERR = 12
C
      IF ( NXDIM .GT. 32767 ) IERR = 31
      IF ( NYDIM .GT. 32767 ) IERR = 32
      IF ( IXSTRT .GT. NXDIM ) IERR = 33
      IF ( IXSTOP .GT. NXDIM ) IERR = 34
      IF ( IYSTRT .GT. NYDIM ) IERR = 35
      IF ( IYSTOP .GT. NYDIM ) IERR = 36
      IF ( IXSTRT .GT. IXSTOP ) IERR = 37
      IF ( IYSTRT .GT. IYSTOP ) IERR = 38
      IF ( XRED .LE. 0.0 ) IERR = 39
      IF ( YRED .LE. 0.0 ) IERR = 40
      IF ( IERR .NE. 0 ) GOTO 900
C
C  initialize the TIPRULL routines and clear the screens if requested
C
      CALL TICTL(1)
      IF ( CLR_ANSI ) THEN
        CALL TISTOR( 24 )   ! CAN
        CALL TISTOR( 27 )   ! ESC [ 2 J
        CALL TISTOR( 91 )
        CALL TISTOR( 50 )
        CALL TISTOR( 74 )
        CALL TISTOR( 27 )   ! ESC [ H
        CALL TISTOR( 91 )
        CALL TISTOR( 72 )
        CALL TISTOR( 31 )   ! US - enter alpha mode
       ENDIF
      IF ( CLR_IT ) CALL TICTL(2)
C
C  enter user mode # 2
C
      CALL TISTOR( 27 )    ! ESC " 2 g
      CALL TISTOR( 34 )
      CALL TISTOR( 50 )
      CALL TISTOR( 103 )
C
C  send the options word 
C
      CALL SENDINT( IOPTIONS(CLR_WINDOW,GRIDS,.FALSE.,.FALSE.,.FALSE.))
C
C  send the limits of the display window
C
      CALL SENDINT( IWXMIN )    ! lower left corner
      CALL SENDINT( IWYMIN )
      CALL SENDINT( IWXMAX )    ! upper left corner
      CALL SENDINT( IWYMAX )
C
C  send the limits for the graph window
C
      CALL SENDINT( IDXMIN )     ! lower left corner
      CALL SENDINT( IDYMIN )
      CALL SENDINT( IDXMAX )     ! upper right corner
      CALL SENDINT( IDYMAX )
      DHT = IDYMAX - IDYMIN
      DYMIN = IDYMIN
C
C  send the number of bins in both x and y direction
C
      CALL SENDINT( IXSTOP-IXSTRT+1 )
      CALL SENDINT( IYSTOP-IYSTRT+1 )
C
C  send the real world coord limits for the plot axes
C
      CALL SENDREAL( XMIN )
      CALL SENDREAL( XMIN + XRED*(IXSTOP-IXSTRT+1) )
      CALL SENDREAL( YMIN )
      CALL SENDREAL( YMIN + YRED*(IYSTOP-IYSTRT+1) )
C
C  initialize the routine that packs the data for transmission
C
      CALL INIT2D
C
C  send the data itself
C
      DELTAZ = ZMAX - ZMIN
      IZEROS = 0
      DO 200 IY = IYSTRT, IYSTOP
        DO 100 IX = IXSTRT, IXSTOP
          ZDAT = DAT(IX,IY)
          IF ( ZDAT .LE. ZMIN ) THEN
            IZ = 0
          ELSE IF ( ZDAT .GE. ZMAX ) THEN
            IZ = 15
          ELSE
            IZ = (ZDAT-ZMIN) / DELTAZ * 15.9
          ENDIF
C
          CALL SEND2D( IZ )
  100   CONTINUE
  200 CONTINUE
C
C  get rid of anything left to be sent
C
      CALL EMPTY2D
C
C  send the strings for the labels
C
      CALL SENDSTR( XLABEL )
      CALL SENDSTR( YLABEL )
      CALL SENDSTR( TITLE )
C
C  clear the buffer and return
C
      CALL TICTL(5)
      RETURN
C
C  ERROR MESSAGES
C
  900 CONTINUE  ! error in input
        WRITE(*,1000) IWXMIN, IWYMIN, IWXMAX, IWYMAX, IDXMIN,
     +     IDYMIN, IDXMAX, IDYMAX, NXDIM, NYDIM, IXSTRT, IXSTOP,
     +     XMIN, XRED, IYSTRT, IYSTOP, YMIN, YRED,
     +     IERR
        RETURN
C
C  FORMAT STATMENTS
C
 1000 FORMAT( ' *** ERROR IN ST2D **** ', /
     +1X,'Parameters passed to ST2D are incorrect.  Note the number',/
     +1x,'after IERR= below and report it to the person who program-',/
     +1x,'med your graphics package.  Other numbers below would be',/
     +1x,'useful to copy also. ',//,
     +1x,'IWXMIN,IWYMIN,IWXMAX,IWYMAX=',4I10,/
     +1x,'IDXMIN,IDYMIN,IDXMAX,IDYMAX=',4I10,/
     +1X,'NXDIM,NYDIM=', 2I,/,
     +1X,'IXSTRT,IXSTOP,XMIN,XRED=', 2I8,2G15.6,/
     +1X,'IYSTRT,IYSTOP,YMIN,YRED=', 2I8,2G15.6,//
     +1X,'   *** IERR = ', I, / )
      END

C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
C
      FUNCTION IOPTIONS( CLR_WINDOW, GRIDS, YSQRT, YLOG, DASHED)
C
C   take the passed logical variables and pack them into an INTEGER
C   by setting a bit if the variable is TRUE.  The following bits
C   are assigned to each variable (0 is the lsb):
C
C        0 - CLR_WINDOW
C        1 - GRIDS
C        2 - YSQRT
C        3 - YLOG
C        4 - DASHED
C
      LOGICAL CLR_WINDOW, GRIDS, YSQRT, YLOG, DASHED
C
      ITEMP = 0
      IF ( CLR_WINDOW ) ITEMP = IBSET( ITEMP, 0 )
      IF ( GRIDS      ) ITEMP = IBSET( ITEMP, 1 )
      IF ( YSQRT      ) ITEMP = IBSET( ITEMP, 2 )
      IF ( YLOG       ) ITEMP = IBSET( ITEMP, 3 )
      IF ( DASHED     ) ITEMP = IBSET( ITEMP, 4 )
      IOPTIONS = ITEMP
      RETURN
      END

C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      SUBROUTINE SENDINT( I )
C
      INTEGER*2 ITMP
      BYTE BTMP(2)
      EQUIVALENCE (ITMP,BTMP)
C
      ITMP = I
      CALL TISTOR( BTMP(2) )
      CALL TISTOR( BTMP(1) )
CC      WRITE(*,1000) I
 1000 FORMAT( ' INT  SENT = ', I10 )
      RETURN
      END

C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      SUBROUTINE SENDREAL( X )
C
C  Here we hope that the VAX representation of a single precision
C  number is the same as that in the Atari under TDI Modula-2 which
C  uses the IEEE standard.
C
      REAL XTMP
      INTEGER*4 ITMP
      BYTE BTMP(4)
      EQUIVALENCE (XTMP,BTMP), (XTMP,ITMP)
C
      XTMP = X
C
C  mantissa in TDI Modula-2/ST has to be two less than that on VAX
C
#ifdef unix
C Convert from IEEE to VAX format
      CALL FITOF(XTMP,ISTAT)
C The following error check currently does not work.
C Routine itof() does not return 0 on success as stated in man page.
C     IF(ISTAT.NE.0)
C    #  WRITE(*,*)'SENDREAL: error in floating-point conversion'
#endif
      IF ( X .NE. 0.0 ) THEN
        mantissa = iand( itmp, '0000077600'o )
        mantissa = mantissa - '0000000400'o 
        itmp = iand(itmp, '37777700177'o )
        itmp = ior( itmp, mantissa )
      ENDIF
C
C  now send the thing
C
      CALL TISTOR( BTMP(2) )
      CALL TISTOR( BTMP(1) )
      CALL TISTOR( BTMP(4) )
      CALL TISTOR( BTMP(3) )
CC      WRITE(*,1000) X
 1000 FORMAT( ' REAL  SENT = ', G13.6 )
      RETURN
      END

C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
C
      SUBROUTINE INIPCK
C
C  initialize the routine that outputs packed cardinal numbers
C
      BYTE BCHR(4)
      COMMON /PACK97/ NPACK, IHIGH, BCHR
C
      NPACK = 0
      IHIGH = 0
      DO 100 I = 1, 4
        BCHR(I) = 0
  100 CONTINUE
      RETURN
      END

C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
C
      SUBROUTINE SENDPCARD( I )
C
C  send the integer I (0 <= I <= 1023) out to the terminal in packed
C  format - four cardinals in five characters.  First four characters
C  are just the lower bytes of the four cardinals.  The last byte has
C  the two high bits for each cardinal packed into it.  Bits 7,6 are
C  for the first word, bits 5,4 are for the second cardinal, etc.
C
      INTEGER*2 ITMP
      BYTE BTMP(2), BCHR(4)
      EQUIVALENCE (ITMP,BTMP)
      COMMON /PACK97/ NPACK, IHIGH, BCHR
C
      NPACK = NPACK + 1
      ITMP = I
      BCHR(NPACK) = BTMP(1)
      IHIGH = 4*IHIGH + MOD(I/256,4)
CC      WRITE(*,1000) I, NPACK
 1000 FORMAT( '  SEND PACKED CARD = ', I8, '    NPACK = ', I3 )
      IF ( NPACK .EQ. 4 ) CALL CLRPCK
      RETURN
      END

C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
C
      SUBROUTINE CLRPCK
C
C   send the buffer with the packed cardinals to the terminal
C
      INTEGER*2 ITMP
      BYTE BCHR(4), BTMP(2)
      EQUIVALENCE (ITMP,BTMP)
      COMMON /PACK97/ NPACK, IHIGH, BCHR
C
      IF ( NPACK .EQ. 0 ) RETURN
      IF ( NPACK .EQ. 1 ) THEN
        IHIGH = 64*IHIGH
      ELSE IF ( NPACK .EQ. 2 ) THEN
        IHIGH = 16*IHIGH
      ELSE IF ( NPACK .EQ. 3 ) THEN
        IHIGH = 4*IHIGH
      ENDIF
C
      ITMP = IHIGH
      DO 100 I = 1, 4
        CALL TISTOR( BCHR(I) )
  100 CONTINUE
      CALL TISTOR( BTMP(1) )
CC      WRITE(*,1000) (BCHR(I),I=1,4), BTMP(1)
 1000 FORMAT( 5X, 5I6 )
C
      NPACK = 0
      IHIGH = 0
      RETURN
      END

C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
C
      SUBROUTINE SENDSTR( STRING )
C
C  Send the string STRING to the terminal until we find end of string
C  or until 80 characters have been sent
C   5-mar-88 no longer assume that string is 80 characters long or that
C  it is null terminated.  Ignore all trailing blanks in the string.
C
      CHARACTER*(*) STRING
      BYTE TEMP
C
      ILEN = LEN( STRING )
      DO 50 JLEN = ILEN, 1, -1
        IF ( (STRING(JLEN:JLEN).NE.' ').OR.(JLEN.EQ.1) ) GOTO 60
   50 CONTINUE
   60 CONTINUE  ! JLEN points to last element in string
      JLEN = MIN( 79, JLEN )
C --- send the string
      DO 100 I = 1, JLEN
        TEMP = ICHAR(STRING(I:I))
        CALL TISTOR( TEMP )
  100 CONTINUE
C  --- make sure string  is zero terminated
      IF ( ICHAR(STRING(JLEN:JLEN)) .NE. 0 ) CALL TISTOR( 0 )
C
      RETURN
      END

C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
C
      SUBROUTINE  INIT2D
C
C  Set up for the routine SEND2D
C
      LOGICAL FIRST, GETZEROS
      INTEGER ZCOUNT, SAVE
      COMMON /PACK98/ FIRST, GETZEROS, SAVE, ZCOUNT
C
      FIRST = .TRUE.
      GETZEROS = .FALSE.
      ZCOUNT = 0
      RETURN
      END

C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
C
      SUBROUTINE SEND2D( NEW )
C
C  Take the byte NEW and prepare it for transmission to the Atari.  A
C  number of cases can exist:
C
C    1) FIRST = TRUE, then save NEW in SAVE, set FIRST FALSE and return
C       since we are packing two array locations per byte sent to the
C       Atari
C
C    2) FIRST = FALSE and GETZEROS = FALSE.  Find the next byte to be
C       sent to the Atari using SAVE = 16*SAVE + NEW.  Now, if SAVE is
C       non-zero then send the byte using TISTOR and set FIRST TRUE.
C       BUT if it is zero, then set GETZEROS TRUE and ZCOUNT to 2.
C
C    3) FIRST = FALSE and GETZEROS = TRUE.  If NEW is zero then increment
C       ZCOUNT and RETURN (If ZCOUNT >= 250 then send a zero followed by
C       the count and set FIRST to TRUE and GETZEROS to FALSE).  If NEW
C       is nonzero then send a zero followed by ZCOUNT, then set SAVE
C       to NEW and set GETZEROS to FALSE.
C
      LOGICAL FIRST, GETZEROS
      INTEGER ZCOUNT, SAVE
      BYTE NEW, BCOUNT, BSAVE
      COMMON /PACK98/ FIRST, GETZEROS, SAVE, ZCOUNT
      EQUIVALENCE (BCOUNT,ZCOUNT)
      EQUIVALENCE (BSAVE,SAVE)
C
      IF ( FIRST ) THEN
        SAVE = NEW
        FIRST = .FALSE.
      ELSE IF ( GETZEROS ) THEN
        IF ( NEW .EQ. 0 ) THEN
          ZCOUNT = ZCOUNT + 1
          IF ( ZCOUNT .GE. 250 ) THEN
            CALL TISTOR( 0 )
            CALL TISTOR( BCOUNT )
            GETZEROS = .FALSE.
            FIRST = .TRUE.
           ENDIF
        ELSE
         CALL TISTOR( 0 )
         CALL TISTOR( BCOUNT )
         GETZEROS = .FALSE.
         SAVE = NEW
        ENDIF
      ELSE
        SAVE = 16 * SAVE + NEW
        IF ( SAVE .EQ. 0 ) THEN
          GETZEROS = .TRUE.
          ZCOUNT = 2
        ELSE
          CALL TISTOR( BSAVE )
          FIRST = .TRUE.
        ENDIF
      ENDIF
C
      RETURN
      END

C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
C
      SUBROUTINE  EMPTY2D
C
C  flush any zeros that remain to be sent from the SEND2D routine
C
      LOGICAL FIRST, GETZEROS
      INTEGER ZCOUNT, SAVE
      BYTE BCOUNT, BSAVE
      COMMON /PACK98/ FIRST, GETZEROS, SAVE, ZCOUNT
      EQUIVALENCE (BCOUNT,ZCOUNT)
      EQUIVALENCE (BSAVE,SAVE)
C
      IF ( FIRST ) RETURN
C else we have more to send
      IF ( GETZEROS ) THEN
        CALL TISTOR( 0 )
        CALL TISTOR( BCOUNT )
      ELSE
        SAVE = 16 * SAVE
        IF ( SAVE .EQ. 0 ) THEN
          CALL TISTOR( 0 )
          CALL TISTOR( 1 )
        ELSE
          CALL TISTOR( BSAVE )
        ENDIF
      ENDIF
      RETURN
      END
