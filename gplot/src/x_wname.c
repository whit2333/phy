/***********************************************************************
*  xvst_wname_                                     F.W. Jones, TRIUMF  *
*  Sets the window names for the graphics window and the zoom          *
*  window.  The window manager usually displays this name in the       *
*  window banner and icon label.  XVST_SETUP must be called prior to   *
*  this routine.                                                       *
*  NOTE: the strings wname and zname must be null-terminated!          *
***********************************************************************/

/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#include <stdio.h>
#include <X11/Xlib.h>

#ifdef VMS
#include <descrip.h>
#define xvst_wname_ xvst_wname
#endif

/*   GLOBAL VARIABLES   */

Display *dpy;
Window g_window, z_window;
Bool zoom;

void xvst_wname_(mname,zname)
#ifdef VMS
   struct dsc$descriptor_s *mname,*zname;
#else
   char *mname,*zname;
#endif
{
   if (dpy == NULL) return;
#ifdef VMS
   XStoreName(dpy,g_window,mname->dsc$a_pointer);
   XStoreName(dpy,z_window,zname->dsc$a_pointer);
#else
   XStoreName(dpy,g_window,mname);
   XStoreName(dpy,z_window,zname);
#endif
   XFlush(dpy);
   return;
}
