      SUBROUTINE ERASE_RECTANGLE(XL,YL,XU,YU,LPCNT)
C
C  This routine erases a rectangle: lower left  corner = (XL,YL)
C                                   upper right corner = (XU,YU)
C
C  Input:  
C      XL, YL, XU, YU     are REAL*4
C          are the rectangle's opposite corner coordinates
C          these should be expressed either in world coordinates
C          or in percentages of the world
C      LPCNT              is LOGICAL*1
C          is the flag denoting whether XL, YL, XU, YU are in
C          world coordinates (.FALSE.) or in percentages of the 
C          world (.TRUE.)
C
C  Note:  erasing only works on a terminal screen and on a bitmap
C         it will not work on vector plotters
C
      REAL*4    XL, XU, YL, YU
      LOGICAL*1 LPCNT
C
      COMMON /PLOTMONITOR/   IMONITOR,  IOUTM
      COMMON /PLOTMONITOR2/  IMONITOR2, IOUTM2
      COMMON /HARDCOPYRANGE/ XMINHP, XMAXHP, YMINHP, YMAXHP, IORIENTH
C
      LOGICAL WELL, WELL_SAVE
      COMMON /TO_BIT_OR_NOT/ WELL
C
      WELL_SAVE      = WELL
      IMONITOR_SAVE  = IMONITOR
      IMONITOR2_SAVE = IMONITOR2
C
      IF( LPCNT )THEN  ! convert from percentages to world coordinates
        XL1 = XL * (XMAXHP-XMINHP) / 100. + XMINHP
        XU1 = XU * (XMAXHP-XMINHP) / 100. + XMINHP
        YL1 = YL * (YMAXHP-YMINHP) / 100. + YMINHP
        YU1 = YU * (YMAXHP-YMINHP) / 100. + YMINHP
      ELSE             ! do not convert
        XL1 = XL
        XU1 = XU
        YL1 = YL
        YU1 = YU
      END IF
      CALL PIXEL_EXTENT(XL1,XU1,YL1,YU1
     & ,XINC, YINC, NXMIN, NXMAX, NYMIN, NYMAX
     & ,XINCB,YINCB,NXMINB,NXMAXB,NYMINB,NYMAXB)
C
C  First erase the terminal screen
C
      WELL = .FALSE.
      IMONITOR2 = 0
      CALL DWG_OUTPUT('OFF')
      CALL PLOT_DATA_LEVEL(1)
      XLO = XMINHP+NXMIN*XINC
      XUP = XMINHP+NXMAX*XINC
      DO I = NYMIN, NYMAX
        YY = YMINHP+(I-1)*YINC
        CALL PLOT_R(XLO,YY,3)
        CALL PLOT_R(XUP,YY,2)
      END DO
      CALL FLUSH_PLOT
      WELL = WELL_SAVE
      IF( WELL )THEN
C
C  Now, erase the bitmap, if it is on
C
        IMONITOR = 0
        XLO = XMINHP+NXMINB*XINCB
        XUP = XMINHP+NXMAXB*XINCB
        DO I = NYMINB, NYMAXB
          YY = YMINHP+(I-1)*YINCB
          CALL PLOT_R(XLO,YY,3)
          CALL PLOT_R(XUP,YY,2)
        END DO
        CALL FLUSH_PLOT
        IMONITOR = IMONITOR_SAVE
      END IF
      IMONITOR2 = IMONITOR2_SAVE
      CALL DWG_OUTPUT('ON')
      CALL PLOT_DATA_LEVEL(0)
      RETURN
      END
