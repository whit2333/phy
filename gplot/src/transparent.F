      SUBROUTINE TRANSPARENT_MODE(ICLEAR)
C======================================================================C
C                                                                      C
C  TRANSPARENT_MODE: puts monitor 1 into transparent mode              C
C  (i.e. VT100 alpha-numeric mode as opposed to graphics alpha-numeric C
C  mode).  If "ICLEAR" = 0 then the screen is not cleared before       C
C  being put into transparent mode.  If "ICLEAR" .ne. 0 then the       C
C  screen is cleared before being put into transparent mode.           C
C                                                                      C
C  *This routine must be maintained in parallel with TRANSPARENT_MODE2 C
C                                                                      C
C  Written by Arthur Haynes, TRIUMF U.B.C., July 6, 1981.              C
C                                                                      C
C  Input  Parameters: ICLEAR (I*4).                                    C
C                                                                      C
C  Modified February 6/84 by F. Jones.  The VT640 is put in graphics   C
C    mode before sending the transparent mode character CAN, since     C
C    CAN is interpreted as a checkerboard character if the terminal    C
C    is already in transparent mode.                                   C
C  Modified by F.W. Jones, August/84, to include the VT241.            C
C  Modified by F.W. Jones, Oct 7/85, to include the PT100G.            C
C  Modified by F.W. Jones, Mar 20/87, for Seiko GR-1105 terminal.      C
C  Mofified by F.W. Jones, Nov 27/87.  The code has been restructured  C
C    for clarity and easier maintenance.  All escape sequences are     C
C    now in the same form and are written without line-skipping.       C
C  Mofified by J. Chuma, May 11/89  support generic terminal           C
C                                                                      C
C======================================================================C
      COMMON /PLOTMONITOR/ IMONITOR,IOUTM
      COMMON /PLOTMONITOR2/ IMONITOR2,IOUTM2

      CHARACTER*1 CAN,ESC,GS,BACKSLASH

      ENTRY TRANSPARENT(ICLEAR)

      CAN=CHAR(24)     ! modified by J.Chuma, 19Mar97 for g77
      ESC=CHAR(27)
      GS=CHAR(29)
      BACKSLASH=CHAR(92)

      IF( ICLEAR .NE. 0 )CALL CLEAR_PLOT     !Clear the screen

C FORMAT modified for Unix compatibility 14-NOV-91 by FWJ
CC1000  FORMAT('+',A,$)
1000  FORMAT(' ',A,$)

C VT640, PT100G:
      IF( (IMONITOR .EQ. 1) .OR. (IMONITOR .EQ. 9) )THEN
        WRITE(IOUTM,1000)GS//CAN

C CIT467:
      ELSE IF( IMONITOR .EQ. 6 )THEN
        WRITE(IOUTM,1000)ESC//'2'

C TK4107:  Terminate TEK mode.
      ELSE IF( IMONITOR .EQ. 7 )THEN
        WRITE(IOUTM,1000)ESC//'%!1'

C VT241:  Terminate Regis mode.
      ELSE IF( IMONITOR .EQ. 8 )THEN
        WRITE(IOUTM,1000)ESC//BACKSLASH

C GR1105:
      ELSE IF( IMONITOR .EQ. 12 )THEN
        WRITE(IOUTM,1000)ESC//'K!'

C generic terminal
      ELSE IF( IMONITOR .EQ. 17 )THEN
        CALL GENERIC_TRANSPARENT_MODE

      ENDIF
C
      RETURN

      ENTRY TRANSPARENT_MODE2(ICLEAR)
C======================================================================C
C                                                                      C
C  TRANSPARENT_MODE2: puts monitor 2 into transparent mode             C
C======================================================================C
C
      IF(ICLEAR.NE.0)CALL CLEAR_PLOT     !Clear the screen

C VT640, PT100G:
      IF(IMONITOR2.EQ.1.OR.IMONITOR2.EQ.9)THEN
        WRITE(IOUTM2,1000)GS//CAN

C CIT467:
      ELSE IF(IMONITOR2.EQ.6)THEN
        WRITE(IOUTM2,1000)ESC//'2'

C TK4107:  Terminate TEK mode.
      ELSE IF( IMONITOR2 .EQ. 7 )THEN
        WRITE(IOUTM2,1000)ESC//'%!1'

C VT241:  Terminate Regis mode.
      ELSE IF(IMONITOR2.EQ.8)THEN
        WRITE(IOUTM2,1000)ESC//BACKSLASH

C GR1105:
      ELSE IF(IMONITOR2.EQ.12)THEN
        WRITE(IOUTM2,1000)ESC//'K!'
      ENDIF
C
      RETURN

      END
