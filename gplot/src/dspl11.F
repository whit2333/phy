      SUBROUTINE DSPL11(S,X,Y,N,XX,YY,M,YP,W,*,*,*)
C
C DSPL11: interpolates points through function values using a      
C         spline under tension.  For tensions close to zero,       
C         eg. 0.001, the interpolation is close to a cubic spline. 
C         For large tensions, eg. 100., the interpolation is      
C         basically polygonal.  The routine is in double precision.
C
C  input
C       S:  is the normalized tension factor.  It is non-zero and
C           corresponds to the curviness desired. The sign of S
C           is ignored.  If S is close to zero, eg. 0.001, the
C           resulting curve is approximately a cubic spline.  If
C           S is large, eg. 50., the resulting curve is nearly
C           a polygonal line.  A standard value for S is 1.
C       X:  array of N increasing abcissae of the functional values
C       Y:  array of the N ordinates of the values, ie.
C           Y(K) is the functional value corresponding to X(K)
C       N:  number of values to be interpolated, 2 <= N
C      XX:  array of the abcissae of the M output points.  
C           The values do not have to be in increasing order
C       M:  number of output points, 1 <= M
C      YP:  work array (length = N)
C       W:  work array (length = N)
C
C  output
C    YY:  array of the M ordinate values of the interpolated points
C
C  RETURN 1:  input data is not monotonically increasing
C  RETURN 2:  N out of bounds
C  RETURN 3:  M out of bounds
C
C  Originally written by CJ Kost, July 29, 1980 @SIN
C  Extensively modified by JL Chuma, March 29, 1994  eliminating entry points
C
      IMPLICIT NONE

      INTEGER*4 N, M
      REAL*8    S, X(1), Y(1), XX(1), YY(1), YP(1), W(1)

C  local variables

      REAL*8    XT, C1, C2, C3, EXPMAX, SIGMA, SIGMAP, DEL1, DEL2, DELS
     &         ,DIAG1, DIAGIN, SPDIAG, DX1, DX2, DIAG2
     &         ,DELX1, DELX2, DELX12, SLPP1, DELN, DELNM1, DELNN, SLPPN
      INTEGER*4 I, J, II
CCC
      SAVE

      IF( N .LT. 2 )RETURN 2
      IF( M .LE. 0 )RETURN 3
      IF( (X(2)-X(1)) .LE. 0.0D0 )RETURN 1

      IF( N .EQ. 2 )THEN  ! use straight line for curve
        DO J = 1, M
          XT = XX(J)
          IF( XT .LT. X(1) )XT = X(1)
          IF( XT .GT. X(2) )XT = X(2)
          YY(J) = (Y(2)*(XT-X(1))+Y(1)*(X(2)-XT))/(X(2)-X(1))
        END DO
        RETURN
      END IF

C  use second order polynomial interpolation
C  on input data for values at endpoints

      DELX1 = X(2)-X(1)
      DELX2 = X(3)-X(2)
      DELX12 = X(3)-X(1)
      C1 = -(DELX12+DELX1)/DELX12/DELX1
      C2 = DELX12/DELX1/DELX2
      C3 = -DELX1/DELX12/DELX2
      SLPP1 = C1*Y(1)+C2*Y(2)+C3*Y(3)
      DELN = X(N)-X(N-1)
      DELNM1 = X(N-1)-X(N-2)
      DELNN = X(N)-X(N-2)
      C1 = (DELNN+DELN)/DELNN/DELN
      C2 = -DELNN/DELN/DELNM1
      C3 = DELN/DELNN/DELNM1
      SLPPN = C3*Y(N-2)+C2*Y(N-1)+C1*Y(N)

C  set up right hand side and tridiagonal system for yp and perform
C  forwards elimination

      EXPMAX = 80.0D0
      SIGMA = MAX( 1.0D-4, ABS(S) )
      SIGMAP = SIGMA*FLOAT(N-1)/(X(N)-X(1)) ! denormalized tension

      DELS = SIGMAP*DELX1
      DIAG1 = (DELS/DTANH(DELS)-1.0D0)/DELX1
      DIAGIN = 1.0D0/DIAG1
      DX1 = (Y(2)-Y(1))/DELX1
      YP(1) = DIAGIN * (DX1-SLPP1)
      IF( DELS .GT. EXPMAX )THEN
        SPDIAG = 1.0D0/DELX1
      ELSE
        SPDIAG = (1.0D0-DELS/DSINH(DELS))/DELX1
      END IF
      W(1) = DIAGIN*SPDIAG
      DO I = 2, N-1
        DELX2 = X(I+1)-X(I)
        IF( DELX2 .LE. 0.0D0 )RETURN 1
        DX2 = (Y(I+1)-Y(I))/DELX2
        DELS = SIGMAP*DELX2
        DIAG2 = (DELS/DTANH(DELS)-1.0D0)/DELX2
        DIAGIN = 1.0D0/(DIAG1+DIAG2-SPDIAG*W(I-1))
        YP(I) = DIAGIN*(DX2-DX1-SPDIAG*YP(I-1))
        IF( DELS .GT. EXPMAX )THEN
          SPDIAG = 1.0D0/DELX2
        ELSE
          SPDIAG = (1.0D0-DELS/DSINH(DELS))/DELX2
        END IF
        W(I) = DIAGIN*SPDIAG
        DX1 = DX2
        DIAG1 = DIAG2
      END DO
      DIAGIN = 1.0D0/(DIAG1-SPDIAG*W(N-1))
      YP(N) = DIAGIN*(SLPPN-DX2-SPDIAG*YP(N-1))

C  perform back substitution

      DO I = 2, N
        YP(N+1-I) = YP(N+1-I)-W(N+1-I)*YP(N+2-I)
      END DO
      DO J = 1, M
        XT = XX(J)
        IF( XT .LT. X(1) )XT = X(1)
        IF( XT .GT. X(N) )XT = X(N)
        DO II = 2, N
          I = II
          IF( X(II) .GE. XT )GO TO 88
        END DO
   88   DEL1 = XT-X(I-1)
        DEL2 = X(I)-XT
        DELS = X(I)-X(I-1)
        IF( SIGMAP*DELS .GT. EXPMAX )THEN
          YY(J) = ((Y(I)-YP(I))*DEL1+(Y(I-1)-YP(I-1))*DEL2)/DELS
        ELSE
          YY(J) = (YP(I)*DSINH(SIGMAP*DEL1)+YP(I-1)*DSINH(SIGMAP*DEL2))
     &     /DSINH(SIGMAP*DELS)+((Y(I)-YP(I))*DEL1+(Y(I-1)-YP(I-1))*DEL2)
     &     /DELS
        END IF
      END DO
      RETURN
      END
CCC
      SUBROUTINE DSPL1A(S,X,Y,N,YP,W,*,*)
C
C  DSPL1A: calculates spline fit, but does not perform interpolation.
C
C  input
C         S:  is the normalized tension factor.  It is non-zero and
C             corresponds to the curviness desired. The sign of S
C             is ignored.  If S is close to zero, eg. 0.001, the
C             resulting curve is approximately a cubic spline.  If
C             S is large, eg. 50., the resulting curve is nearly
C             a polygonal line.  A standard value for S is 1.
C       X:  array of N increasing abcissae of the functional values
C       Y:  array of the N ordinates of the values, ie.
C             Y(K) is the functional value corresponding to X(K)
C       N:  number of values to be interpolated, 2 <= N
C        YP:  work array (length = N)
C         W:  work array (length = N)
C
C    This routine can be called to calculate the spline function  
C    parameters without calculating actual interpolated points.   
C    After DSPL1A has been called, DSPL1B or DSPL1C may be called 
C    to evaluate interpolated points, derivatives, integrals, etc.
C    
C    RETURN 1:  input data is not monotonically increasing
C    RETURN 2:  N out of bounds
C 
      IMPLICIT NONE

      INTEGER*4 N
      REAL*8    S, X(1), Y(1), YP(1), W(1)

C  local variables

      REAL*8    EXPMAX, SIGMA, SIGMAP, DELX1, DELX2, DELX12
     &         ,C1, C2, C3, SLPP1, DELN, DELNM1, DELNN, SLPPN
     &         ,DELS, DIAG1, DIAG2, DIAGIN, DX1, DX2, SPDIAG
      INTEGER*4 I
CCC
      SAVE
      EXPMAX = 80.0D0

      IF( N .LT. 2 )RETURN 2

      IF( X(2)-X(1) .LE. 0.0D0 )RETURN 1

      EXPMAX = 80.0D0
      SIGMA = MAX( 1.0D-4, ABS(S) )
      SIGMAP = SIGMA*FLOAT(N-1)/(X(N)-X(1)) ! denormalized tension

      IF( N .EQ. 2 )THEN  ! use straight line for curve 
        YP(1) = 0.D0
        YP(2) = 0.D0
        RETURN
      END IF

C  if no derivatives are given use second order polynomial
C  interpolation on input data for values at endpoints

      DELX1 = X(2)-X(1)
      DELX2 = X(3)-X(2)
      DELX12 = X(3)-X(1)
      C1 = -(DELX12+DELX1)/DELX12/DELX1
      C2 = DELX12/DELX1/DELX2
      C3 = -DELX1/DELX12/DELX2
      SLPP1 = C1*Y(1)+C2*Y(2)+C3*Y(3)
      DELN = X(N)-X(N-1)
      DELNM1 = X(N-1)-X(N-2)
      DELNN = X(N)-X(N-2)
      C1 = (DELNN+DELN)/DELNN/DELN
      C2 = -DELNN/DELN/DELNM1
      C3 = DELN/DELNN/DELNM1
      SLPPN = C3*Y(N-2)+C2*Y(N-1)+C1*Y(N)

C  set up right hand side and tridiagonal system for YP and
C  perform forwards elimination

      DELS = SIGMAP*DELX1
      DIAG1 = (DELS/DTANH(DELS)-1.0D0)/DELX1
      DIAGIN = 1.0D0/DIAG1
      DX1 = (Y(2)-Y(1))/(X(2)-X(1))
      YP(1) = DIAGIN*(DX1-SLPP1)
      IF( DELS .GT. EXPMAX )THEN
        SPDIAG = 1.0D0/DELX1
      ELSE
        SPDIAG = (1.0D0-DELS/DSINH(DELS))/DELX1
      END IF
      W(1) = DIAGIN*SPDIAG
      DO I = 2, N-1
        DELX2 = X(I+1)-X(I)
        IF( DELX2 .LE. 0.0D0 )RETURN 1
        DX2 = (Y(I+1)-Y(I))/DELX2
        DELS = SIGMAP*DELX2
        DIAG2 = (DELS/DTANH(DELS)-1.0D0)/DELX2
        DIAGIN = 1.0D0/(DIAG1+DIAG2-SPDIAG*W(I-1))
        YP(I) = DIAGIN*(DX2-DX1-SPDIAG*YP(I-1))
        IF( DELS .GT. EXPMAX )THEN
          SPDIAG = 1.0D0/DELX2
        ELSE
          SPDIAG = (1.0D0-DELS/DSINH(DELS))/DELX2
        END IF
        W(I) = DIAGIN*SPDIAG
        DX1 = DX2
        DIAG1 = DIAG2
      END DO
      DIAGIN = 1.0D0/(DIAG1-SPDIAG*W(N-1))
      YP(N) = DIAGIN*(SLPPN-DX2-SPDIAG*YP(N-1))

C  perform back substitution

      DO I = 2, N
        YP(N+1-I) = YP(N+1-I)-W(N+1-I)*YP(N+2-I)
      END DO
      RETURN
      END
CCC
      SUBROUTINE DSPL1B(S,X,Y,N,IDERIV,XX,YY,M,YP,W,*)
C
C  DSPL1B: calculates first derivative, second derivative, inter-
C          polation, or intergral at specified abcissae. 
C          DSPL1A must be called prior to calling DSPL1B.
C    NOTE: this routine does not calculate the spline function      
C          parameters.  It calculates the integral, interpolation,  
C          first derivative, or second derivative, depending on the 
C          input value of ideriv.  DSPL1A must be called first to
C          calculate the spline function parameters.
C          
C  input
C    IDERIV=-1: integral from X(1) to XX(J) is returned in YY(J)
C          = 0: interpolated ordinate of XX(J) is returned in YY(J)
C          = 1: first derivative at XX(J) is returned in YY (J)
C          = 2: second derivative at XX(J) is returned in YY(J)
C     XX:     is an array of the abcissae of the M output points.    
C               The values do not have to be in increasing order
C     M:     the number of points to be evaluated,  1 <= M
C        YP:    work array (length = N)
C         W:    work array (length = N)
C
C  output
C     YY:     array of M values to be returned
C
      IMPLICIT NONE

      INTEGER*4 N, IDERIV, M
      REAL*8    S, X(1), Y(1), XX(1), YY(1), YP(1), W(1)

C  local variables

      REAL*8    EXPMAX, SIGMA, SIGMAP, DS, DIS, DISM, DELC, XT
     &         ,DEL1, DEL2, DELS
      INTEGER*4 I, II, J
CCC
      SAVE

      IF( M .LE. 0 )RETURN 1

C  fill interpolated arrays (XX,YY)

      EXPMAX = 80.0D0
      SIGMA = MAX( 1.0D-4, ABS(S) )
      SIGMAP = SIGMA*FLOAT(N-1)/(X(N)-X(1)) ! denormalized tension

      IF( IDERIV .LT. 0 )THEN
        DS = X(2)-X(1)
        IF( SIGMAP*DS .GT. EXPMAX )THEN
          W(1) = YP(1)/DTANH(SIGMAP*DS)/SIGMAP
     &     +(Y(2)-YP(2))*X(1)*X(1)/2.0D0/DS
     &     -(Y(1)-YP(1))*(X(2)-X(1)/2.0D0)*X(1)/DS
        ELSE 
          W(1) = -(YP(2)-YP(1)*DCOSH(SIGMAP*DS))/SIGMAP/DSINH(SIGMAP*DS)
     &     +(Y(2)-YP(2))*X(1)*X(1)/2.0D0/DS
     &     -(Y(1)-YP(1))*(X(2)-X(1)/2.0D0)*X(1)/DS
        END IF
        DO I = 2, N-1
          DISM = X(I)-X(I-1)
          DIS = X(I+1)-X(I)
          IF( SIGMAP*DISM.GT.EXPMAX .OR. SIGMAP*DIS.GT.EXPMAX )THEN
            DELC = -YP(I-1)*X(I)*X(I)/DISM/2.0D0
     &       -YP(1+1)*X(I)*X(I)/DIS/2.0D0
     &       +YP(I)*(1.0D0/DTANH(SIGMAP*DISM)+1.0D0/DTANH(SIGMAP*DIS))
     &       /SIGMAP-YP(I)*((.5D0*X(I)-X(I-1))*X(I)/DISM-(X(I+1)
     &       -.5D0*X(I))*X(I)/DIS)
     &       +(Y(I+1)*X(I)*X(I)/2.D0-Y(I)*(X(I+1)-X(I)/2.D0)*
     &       X(I))/DIS+(Y(I)*(X(I)/2.D0-X(I-1))*X(I)+Y(I-1)*
     &       X(I)*X(I)/2.D0)/DISM
          ELSE 
            DELC = -YP(I-1)*(1.D0/SIGMAP/DSINH(SIGMAP*DISM)+X(I)*X(I)/
     &       DISM/2.D0)-YP(I+1)*(1.D0/SIGMAP/DSINH(SIGMAP*DIS)+X(I)*X(I)
     &       /DIS/2.D0)+YP(I)*(1.0D0/DTANH(SIGMAP*DISM)
     &       +1.0D0/DTANH(SIGMAP*DIS))/SIGMAP-YP(I)*((.5D0*X(I)-X(I-1))
     &       *X(I)/DISM-(X(I+1)-.5D0*X(I))*X(I)/DIS)
     &       +(Y(I+1)*X(I)*X(I)/2.D0-Y(I)*(X(I+1)-X(I)/2.D0)*
     &       X(I))/DIS+(Y(I)*(X(I)/2.D0-X(I-1))*X(I)+Y(I-1)*
     &       X(I)*X(I)/2.D0)/DISM
          END IF
          W(I) = W(I-1)+DELC
        END DO
      END IF
      DO J = 1, M
        XT = XX(J)
        IF( XT .LT. X(1) )XT = X(1)
        IF( XT .GT. X(N) )XT = X(N)
        DO II = 2, N
          I = II
          IF( X(II) .GE. XT )GO TO 88
        END DO
   88   DEL1 = XT-X(I-1)
        DEL2 = X(I)-XT
        DELS = X(I)-X(I-1)

        IF( IDERIV .EQ. -1 )THEN  ! integral curve
          IF( SIGMAP*DELS .GT. EXPMAX )THEN
            YY(J) = (Y(I)-YP(I))*(XT/2.D0-X(I-1))*XT/DELS
     &       +(Y(I-1)-YP(I-1))*(X(I)-XT/2.D0)*XT/DELS+W(I-1)
          ELSE
            YY(J) = (YP(I)*DCOSH(SIGMAP*DEL1)
     &       -YP(I-1)*DCOSH(SIGMAP*DEL2))/
     &       SIGMAP/DSINH(SIGMAP*DELS)
     &       +(Y(I)-YP(I))*(XT/2.D0-X(I-1))*XT/DELS
     &       +(Y(I-1)-YP(I-1))*(X(I)-XT/2.D0)*XT/DELS+W(I-1)
          END IF 
        ELSE IF( IDERIV .EQ. 0 )THEN  ! normal interpolation
          IF( SIGMAP*DELS .GT. EXPMAX )THEN
            YY(J) = ((Y(I)-YP(I))*DEL1+(Y(I-1)-YP(I-1))*DEL2)/DELS
          ELSE
            YY(J) = (YP(I)*DSINH(SIGMAP*DEL1)
     &       +YP(I-1)*DSINH(SIGMAP*DEL2))
     &       /DSINH(SIGMAP*DELS)+((Y(I)-YP(I))*DEL1
     &       +(Y(I-1)-YP(I-1))*DEL2)
     &       /DELS
          END IF
        ELSE IF( IDERIV .EQ. 1 )THEN  ! first derivative 
          IF( SIGMAP*DELS .GT. EXPMAX )THEN
            YY(J) = (Y(I)-YP(I)-Y(I-1)+YP(I-1))/DELS
          ELSE
            YY(J) = (YP(I)*DCOSH(SIGMAP*DEL1)
     &       -YP(I-1)*DCOSH(SIGMAP*DEL2))
     &       *SIGMAP/DSINH(SIGMAP*DELS)+(Y(I)-YP(I)-Y(I-1)+YP(I-1))/DELS
          END IF 
        ELSE IF( IDERIV .EQ. 2 )THEN  ! second derivative
          IF( SIGMAP*DELS .GT. EXPMAX )THEN
            YY(J) = 0.0D0
          ELSE
            YY(J) = (YP(I)*DSINH(SIGMAP*DEL1)
     &       +YP(I-1)*DSINH(SIGMAP*DEL2))
     &       *SIGMAP*SIGMAP/DSINH(SIGMAP*DELS)
          END IF 
        END IF
      END DO
      RETURN
      END
CCC
      SUBROUTINE DSPL1C(S,X,Y,N,XLOW,XHI,YTG,YP,W,*)
C
C  DSPL1C: calculates the definite integral between two specified
C          abcissae. DSPL1A must be called prior to calling DSPL1C. 
C
C    This routine evaluates the definite integral of the interpolated curve
C    between two input valued. The second value does not have to be greater
C    than the first.  DSPL1A must be called prior to DSPL1C.  The
C    two abcissae must be within the range of X(1) and X(N).
C
C  input
C      XLOW:  low  x value
C       XHI:  high x value
C        YP:  work array (length = N)
C         W:  work array (length = N)
C  output
C     YTG:  definite integral value
C
C   RETURN 1:  XLOW or XHI < X(1)  or  > X(N)
C
C    this routine can be called to calculate the spline function
C    parameters without calculating actual interpolated points. 
C    after dspl1a has been called, dspl1b or dspl1c may be called
C    to evaluate interpolated points, derivatives, integrals, etC.
C    
      IMPLICIT NONE

      INTEGER*4 N
      REAL*8    S, X(1), Y(1), XLOW, XHI, YTG, YP(1), W(1)

C  local variables

      REAL*8    EXPMAX, SIGMA, SIGMAP, DS, DIS, DISM, DELC, DELS
     &         ,YLOW, YHI
      INTEGER*4 I, II, ILOW, IHI
CCC
      SAVE

      IF( (XLOW-X(1))*(XLOW-X(N)) .GT. 0.0D0 )RETURN 1
      IF( (XHI-X(1))*(XHI-X(N)) .GT. 0.0D0 )RETURN 1

      EXPMAX = 80.0D0
      SIGMA = MAX( 1.0D-4, ABS(S) )
      SIGMAP = SIGMA*FLOAT(N-1)/(X(N)-X(1)) ! denormalized tension

C  find which intervals contain XLOW, XHI

      DO II = 2, N
        I = II
        IF( (XLOW-X(II-1))*(XLOW-X(II)) .LE. 0.D0 )GO TO 10
      END DO
   10 ILOW = I
      DO II = 2, N
        I = II
        IF( (XHI-X(II-1))*(XHI-X(II)) .LE. 0.D0 )GO TO 20
      END DO
   20 IHI = I

C  fill the W(I) array of integration constants

      DS = X(2)-X(1)
      IF( SIGMAP*DS .GT. EXPMAX )THEN
        W(1) = YP(1)/DTANH(SIGMAP*DS)/SIGMAP
     &   +(Y(2)-YP(2))*X(1)*X(1)/2.0D0/DS
     &   -(Y(1)-YP(1))*(X(2)-X(1)/2.0D0)*X(1)/DS
      ELSE
        W(1) = -(YP(2)-YP(1)*DCOSH(SIGMAP*DS))/SIGMAP/DSINH(SIGMAP*DS)
     &   +(Y(2)-YP(2))*X(1)*X(1)/2.0D0/DS
     &   -(Y(1)-YP(1))*(X(2)-X(1)/2.0D0)*X(1)/DS
      END IF

      DO I = 2, N-1
        DISM = X(I)-X(I-1)
        DIS = X(I+1)-X(I)
        IF( SIGMAP*DISM.GT.EXPMAX .OR. SIGMAP*DIS.GT.EXPMAX )THEN
          DELC = -YP(I-1)*X(I)*X(I)/DISM/2.0D0
     &     -YP(1+1)*X(I)*X(I)/DIS/2.0D0
     &     +YP(I)*(1.0D0/DTANH(SIGMAP*DISM)+1.0D0/DTANH(SIGMAP*DIS))
     &     /SIGMAP-YP(I)*((.5D0*X(I)-X(I-1))*X(I)/DISM-(X(I+1)
     &     -.5D0*X(I))*X(I)/DIS)
     &     +(Y(I+1)*X(I)*X(I)/2.D0-Y(I)*(X(I+1)-X(I)/2.D0)*
     &     X(I))/DIS+(Y(I)*(X(I)/2.D0-X(I-1))*X(I)+Y(I-1)*
     &     X(I)*X(I)/2.D0)/DISM
        ELSE
          DELC = -YP(I-1)*(1.D0/SIGMAP/DSINH(SIGMAP*DISM)+X(I)*X(I)/
     &    DISM/2.D0)-YP(I+1)*(1.D0/SIGMAP/DSINH(SIGMAP*DIS)+X(I)*X(I)
     &    /DIS/2.D0)+YP(I)*(1.0D0/DTANH(SIGMAP*DISM)
     &    +1.0D0/DTANH(SIGMAP*DIS))/SIGMAP
     &    -YP(I)*((.5D0*X(I)-X(I-1))*X(I)/DISM-(X(I+1)-.5D0
     &    *X(I))*X(I)/DIS)
     &    +(Y(I+1)*X(I)*X(I)/2.D0-Y(I)*(X(I+1)-X(I)/2.D0)*
     &    X(I))/DIS+(Y(I)*(X(I)/2.D0-X(I-1))*X(I)+Y(I-1)*
     &    X(I)*X(I)/2.D0)/DISM
        END IF
        W(I) = W(I-1)+DELC
      END DO

C  find F(LOW) = YLOW

      DELS = X(ILOW)-X(ILOW-1)
      IF( SIGMA*DELS .GT. EXPMAX )THEN
        YLOW = (Y(ILOW)-YP(ILOW))*(XLOW/2.D0-X(ILOW-1))*XLOW
     &   /DELS+(Y(ILOW-1)-YP(ILOW-1))*(X(ILOW)-XLOW/2.D0)*XLOW/DELS
     &   +W(ILOW-1)
      ELSE
        YLOW = (YP(ILOW)*DCOSH(SIGMAP*(XLOW-X(ILOW-1)))-YP(ILOW-1)
     &   *DCOSH(SIGMAP*(X(ILOW)-XLOW)))/SIGMAP/DSINH(SIGMAP*DELS)
     &   +(Y(ILOW)-YP(ILOW))*(XLOW/2.D0-X(ILOW-1))*XLOW/DELS
     &   +(Y(ILOW-1)-YP(ILOW-1))*(X(ILOW)-XLOW/2.D0)*XLOW/DELS
     &   +W(ILOW-1)
      END IF

C  find F(XHI) = YHI

      DELS = X(IHI)-X(IHI-1)
      IF( SIGMA*DELS .GT. EXPMAX )THEN
        YHI = (Y(IHI)-YP(IHI))*(XHI/2.D0-X(IHI-1))*XHI
     &   /DELS+(Y(IHI-1)-YP(IHI-1))*(X(IHI)-XHI/2.D0)*XHI/DELS
     &   +W(IHI-1)
      ELSE
        YHI = (YP(IHI)*DCOSH(SIGMAP*(XHI-X(IHI-1)))-YP(IHI-1)
     &   *DCOSH(SIGMAP*(X(IHI)-XHI)))/SIGMAP/DSINH(SIGMAP*DELS)
     &   +(Y(IHI)-YP(IHI))*(XHI/2.D0-X(IHI-1))*XHI/DELS
     &   +(Y(IHI-1)-YP(IHI-1))*(X(IHI)-XHI/2.D0)*XHI/DELS+W(IHI-1)
      END IF

      YTG = YHI-YLOW  ! return definite integral in YTG
      RETURN
      END
