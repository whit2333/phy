      SUBROUTINE DWG_LIST(DSPEC)
C======================================================================C
C  DWG_LIST                                        F.W. Jones, TRIUMF  C
C  Generates a list of drawing files    (Graphics Editor routine)      C
C======================================================================C
      COMMON/DWGLIST/LLIST,LFILE
      CHARACTER*80 LFILE

      CHARACTER*80 DSPEC
#ifdef VMS
      EXTERNAL RMS$_NMF,RMS$_FNF
      LOGICAL FIRST
      CHARACTER*80 FNAME
#endif

#ifdef VMS
C Make sure there is no extension on the file name:
      IBRACK=INDEX(DSPEC,']')
      IF(INDEX(DSPEC(IBRACK+1:),'.').NE.0)THEN
        WRITE(*,*)'Drawing name cannot include a file type.'
        RETURN
      ENDIF
#endif

      LS=LENSIG(DSPEC)

#ifdef VMS
      IF(LS.GT.IBRACK)THEN
        DSPEC=DSPEC(1:LS)//'.DWG'
      ELSE        
        DSPEC=DSPEC(1:LS)//'*.DWG'
      ENDIF
#endif
#ifdef unix
      DSPEC=DSPEC(1:LS)//'.dwg'
      LS=LS+4
#endif

#ifdef VMS
      FIRST=.TRUE.
      ICONTEXT=0
 
20    ISTAT=LIB$FIND_FILE(DSPEC,FNAME,ICONTEXT)
      IF(ISTAT.EQ.%LOC(RMS$_NMF))GO TO 99      !no more files
      IF(ISTAT.EQ.%LOC(RMS$_FNF))THEN
        IF(FIRST)WRITE(*,*)'No drawing files found.'
        GO TO 99
      ELSE IF(.NOT.ISTAT)THEN      !something wrong with filespec
        WRITE(*,*)'Error opening ',FNAME(1:LENSIG(FNAME))
        IDUM = NARGSI(1)
        CALL PUT_SYSMSG(ISTAT)
        GO TO 20
      ENDIF
      IF(FIRST)WRITE(*,*)'List of drawing files'
      IEND=INDEX(FNAME,'.DWG;')-1
      WRITE(*,1000)FNAME(1:IEND)
1000  FORMAT(6X,A)
      IF(LLIST.GT.0)WRITE(LLIST,2000)FNAME(1:IEND)
2000  FORMAT(A)
      FIRST=.FALSE.
      GO TO 20
#endif

#ifdef unix
      WRITE(*,*)'List of drawing files:'
      LSL=LENSIG(LFILE)
      IF(LSL.EQ.0)THEN
        CALL SYSTEM('ls '//DSPEC(1:LS)//' |sed s/.dwg//')
      ELSE
        CALL SYSTEM('ls '//DSPEC(1:LS)//' |sed s/.dwg// |tee '//
     &    LFILE(1:LSL))
      ENDIF
#endif

99    RETURN
      END !DWG_LIST
