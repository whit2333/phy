      FUNCTION LIB_OUT(TEXT,IFLAG,IUNIT,KEY)
C======================================================================
C==   LIB_OUT: For use when calling LBR$GET_HELP for a help library. ==
C==            If LIB_OUT and IUNIT are given in the argument list   ==
C==            in the call to LBR$GET_HELP (see documentation on     ==
C==            LBR$GET_HELP for details); then the help text will be ==
C==            printed on unit IUNIT.  (normally it goes to          ==
C==            SYS$OUTPUT).                                          ==
C==								     ==
C==   reqd. routines - NONE 					     ==
C======================================================================
      CHARACTER*(*) TEXT
      WRITE(IUNIT,20)TEXT
20    FORMAT(1X,A)
      LIB_OUT=1
      RETURN
      END
