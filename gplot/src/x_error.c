/* X Error Handler for TRIUMF Graphics      FWJ 06-Apr-92 */

#include <stdio.h>
#include <X11/Xlib.h>

#define BUFSIZE 80

int xvst_error( dpy, errevent )
   Display *dpy;
   XErrorEvent *errevent;
{
   char msg[BUFSIZE+1];
   XGetErrorText(dpy, errevent->error_code, msg, BUFSIZE);

   fprintf(stderr, "X Error on display %s\n", DisplayString(dpy));

   fprintf(stderr, "%s\n", msg);

   fprintf(stderr, "Error code %d  Serial %d  Op code %d.%d\n",
           errevent->error_code,
           errevent->serial,
           errevent->request_code,
           errevent->minor_code);
   return(0);
}
