      FUNCTION GET_USERNAME(LENGTH)
#ifdef VMS
C
C     reqd. routines - none
C=========================================================================
C==   Alan Carruthers                                                   ==
C==   TRIUMF, UBC                                                       ==
C==   January 6, 1983                                                   ==
C==                                                                     ==
C==                                                                     ==
C==  GET_USERNAME                                                       ==
C==                                                                     ==
C==  This returns the user name or the process from which it was        ==
C==  called, as well as the length of the user name (LENGTH).           ==
C==                                                                     ==
C==  NOTES:   1)  See VAX/VMS Sysmtem Services Reference Manual         ==
C==               in the $GETJPI section for more information.          ==
C=========================================================================
C
C
      CHARACTER*12 GET_USERNAME
      INTEGER*4 LENGTH, ITMLST(4)
C
C
      ITMLST(1) = X'0202FFFF' .AND. X'FFFF000C'
C                  ----                     -
C                  item code                size of username buffer
C                  for user name
C
      ITMLST(2) = %LOC(GET_USERNAME)
      ITMLST(3) = 0
      ITMLST(4) = 0
      ISTAT = SYS$GETJPI(,,,ITMLST,,,)
C
C========================================================================
C==   Find length of user name.                                        ==
C========================================================================
      LENGTH = INDEX(GET_USERNAME,' ') - 1
      IF(LENGTH .LT. 0) LENGTH = 12
      GET_USERNAME = GET_USERNAME(1:LENGTH)
      RETURN
#endif
#ifdef unix
C
C    Returns the name of the user (using the environment variable
C    USER) and it's length as the argument
C
      CHARACTER*12 USERNAME,GET_USERNAME
      INTEGER*4 LENGTH
      CALL GETENV('USER',USERNAME)
      LENGTH=LENSIG(USERNAME)
      GET_USERNAME = USERNAME(1:LENGTH)
      RETURN
#endif
      END
