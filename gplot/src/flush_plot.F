      SUBROUTINE FLUSH_PLOT
C======================================================================C
C                                                                      C
C   FLUSH_PLOT: flushes out the remainder of the plot buffer.          C
C                                                                      C
C  The plot buffer is initialized but the plot is not cleared.         C
C  The next call to PLOT_R will then continue the plot. The user       C
C  should NOT use this routine with the ZETA plotter so as to try      C
C  to "finish" the plot. To terminate ZETA plots the user must         C
C  call END_PLOT explicitly or implicitly.                             C
C                                                                      C
C   Written by Arthur Haynes, TRIUMF U.B.C., July 6, 1981.             C
C                                                                      C
C   Modified by Alan Carruthers, August 10, 1983                       C
C     -- added reference to /PLOT_MONITOR_DOPLOT/ to enforce           C
C        transmission of graphics primitives to plot buffer            C
C        when PEN_UP is called.  This is to circumvent problems        C
C        when FLUSH_PLOT is called twice in succession.                C
C                                                                      C
C  Modified by F.W. Jones, June 25/84:                                 C
C    The plot buffers were not being properly initialized after        C
C    flushing, since the call to PEN_UP does not guarantee that        C
C    anything goes into the buffer.  Now, the buffers are flushed      C
C    using entry points to CHECK_BUFFER and CHECK_BUFFER2, so          C
C    that the buffers are always initialized in the same way           C
C    whenever they are printed.                                        C
C                                                                      C
C    Modified by Bernard Henin, U.VIC, July 1984 to include the        C
C    TEKTRONIX-4107. Only a few comments added.                        C
C                                                                      C
C  Modified Dec 5/85 by F. Jones for the QMS Lasergrafix.              C
C  Modified Aug 11/86 by F. Jones for the Houston Instruments plotter. C
C  Modified Mar 20/87 by F. Jones for Seiko GR-1105 terminal support   C
C  Modified Nov 14/88 by J. Chuma for the LN03+                        C
C  Modified May 11/89 by J. Chuma for the generic terminal             C
C  Modified Sep 05/89 by F. Jones for X Window support                 C
C  Modified Nov 09/89 by F. Jones for GKS Metafile                     C
C  Modified May 02/91 by F. Jones: VS11 removed                        C
C  Modified 19-AUG-93 by FWJ: UIS support removed                      C
C                                                                      C
C======================================================================C
      COMMON /PLOT_CLEAR/ CLEAR
      LOGICAL CLEAR
      COMMON /PLOTMONITOR/ IMONITOR,IOUTM
      COMMON /PLOTMONITOR2/ IMONITOR2,IOUTM2
      COMMON /PLOT_PREVIOUS/ XP,YP,IPENP
      LOGICAL MUST_DO_PLOT
      COMMON /PLOT_MONITOR_DOPLOT/MUST_DO_PLOT

      IF(CLEAR)CALL CLEAR_PLOT

C======================================================================C
C  Monitor 1 section:
C======================================================================C

C Tektronix compatible:
      IF(IMONITOR.EQ.1.OR.IMONITOR.EQ.2.OR.IMONITOR.EQ.6.OR.
     &   IMONITOR.EQ.7.OR.IMONITOR.EQ.8.OR.IMONITOR.EQ.9)THEN
         CALL FLUSH_BUFFER      !Entry to CHECK_BUFFER

C Seiko GR-1105:
      ELSE IF(IMONITOR.EQ.12)THEN
         CALL SEIKO_FLUSH

C LN03+
      ELSE IF(IMONITOR.EQ.16)THEN
         CALL FLUSH_BUFFER      !Entry to CHECK_BUFFER

C generic terminal 
      ELSE IF(IMONITOR.EQ.17)THEN
         CALL FLUSH_BUFFER

C X Window
      ELSE IF(IMONITOR.EQ.18)THEN
         CALL XVST_PLOT_FLUSH
      ENDIF

C======================================================================C
C  Monitor 2 section:
C======================================================================C

C Tektronix compatible:
      IF(IMONITOR2.EQ.1.OR.IMONITOR2.EQ.2.OR.IMONITOR2.EQ.6.OR.
     &   IMONITOR2.EQ.7.OR.IMONITOR2.EQ.8.OR.IMONITOR2.EQ.9)THEN
         CALL FLUSH_BUFFER2      !Entry to CHECK_BUFFER2

C Seiko GR-1105:
      ELSE IF(IMONITOR2.EQ.12)THEN
         CALL SEIKO_FLUSH2

C LN03+
      ELSE IF(IMONITOR2.EQ.16)THEN
         CALL FLUSH_BUFFER2      !Entry to CHECK_BUFFER2

C GKS Metafile
      ELSE IF(IMONITOR2.EQ.19)THEN
         CALL GKS_FLUSH

      END IF

      RETURN
      END
