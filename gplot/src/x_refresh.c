/**************************************************************************
*  xvst_refresh_                                                     FWJ  *
*                                                                         *
*  Allows applications to initiate a replay of the main and zoom windows  *
*  without having to resize them.                                         *
*                                                                         *
***************************************************************************/

/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#include <X11/Xlib.h>

#ifdef VMS
#define xvst_refresh_ xvst_refresh
#endif

/* GLOBAL VARIABLES */

Display *dpy;
Screen *screen;
Window g_window;
GC fgc;
Bool zoom_box, fsc_exists, enable_action;
int corner1_x, corner1_y, corner2_x, corner2_y;

void xvst_refresh_()
{
   enable_action = False;
   xvst_replay_(0);
   if (zoom_box) xvst_draw_zb_(dpy,screen,g_window,fgc,corner1_x,corner1_y,
                               corner2_x, corner2_y);
   fsc_exists = False;
   enable_action = True;
}
