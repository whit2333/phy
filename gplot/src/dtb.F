      SUBROUTINE DTB(SOURCE,INT,NC,ND,FILL,*)
C
C     LIBRARY-ROUTINE
C
C                                                29/JULY/1980
C                                                C.J. KOST SIN
C
C     reqd. routines - NONE
C
C===================================================================
C==                                                               ==
C==THIS SUBROUTINE PERFORMS IDENTICALLY AND HAS AN IDENTICAL      ==
C==CALLING SEQUENCE TO THE SUBROUTINE DTB IN THE UBC SYSTEM       ==
C==LIBRARY.                                                       ==
C==                                                               ==
C==WRITTEN IN FORTRAN BY BRUCE BALDEN, TRIUMF,U.B.C. 8 MAY 1979   ==
C==                                                               ==
C==INPUT PARAMETERS:                                              ==
C==     SOURCE(NC),FILL (L*1); NC (I*4);                          ==
C==OUTPUT PARAMETERS:                                             ==
C==     INT,NC,ND (I*4).                                          ==
C==THIS ROUTINE CONVERTS A STRING OF NUMERIC CHARACTERS INTO A    ==
C==FORTRAN INTEGER NUMBER.  THE NUMERIC CHARACTER STRING IS       ==
C==INPUT TO THE SUBROUTINE IN 'SOURCE' WHICH MUST BE A LOGICAL*1  ==
C==ARRAY OR HOLLERITH LITERAL OF LENGTH AT LEAST 'NC'. 'INT'      ==
C==IS A FORTRAN INTEGER*4 VARIABLE IN WHICH THE RESULTING INTEGER ==
C==WILL BE PLACED. 'ND' IS A FORTRAN INTEGER VARIABLE IN WHICH THE==
C==NUMBER OF SIGNIFICANT DIGITS FOUND WILL BE PLACED. 'FILL' IS A ==
C==LOGICAL*1 VARIABLE OR HOLLERITH LITERAL CONTAINING A FILL      ==
C==CHARACTER.  THIS CHARACTER WILL BE IGNORED IF IT PRECEDES THE  ==
C==NUMERIC CHARACTERS IN THE INPUT STRING 'SOURCE'.               ==
C==UPON EXIT, 'NC' IS BE SET TO THE NUMBER OF CHARACTERS SCANNED. ==
C==A RETURN 1 IS EXECUTED IF MULTIPLE SIGNS ARE ENCOUNTERED, IF   ==
C==THE CONVERTED NUMBER IS TO LARGE TO HOLD IN A FORTRAN          ==
C==FULLWORD INTEGER, IF INVALID CHARACTERS ARE ENCOUNTERED,       ==
C==OR IF, ON ENTRY, 'NC'<=0.                                      ==
C==                                                               ==
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      INTEGER INT,NC,ND,SIGN
      DOUBLE PRECISION X,MININT,MAXINT
      LOGICAL*1 SOURCE(NC),CHAR(2),FILLER(2),FILL,NMRC
      INTEGER*2 ICHAR,PLUS,MINUS,ZERO,NINE,IFILL,K
      DATA ICHAR,PLUS,MINUS,ZERO,NINE,IFILL
     1     /'  ','+ ','- ','0 ','9 ','  '/
      DATA MININT,MAXINT/-2.147483648D9,2.147483647D9/
      EQUIVALENCE (CHAR(1),ICHAR),(IFILL,FILLER(1))
      NMRC(K)=(K.GE.ZERO).AND.(K.LE.NINE)
C==                                                               ==
C==INITIALIZE                                                     ==
C==                                                               ==
      ND=0
      INT=0
C==                                                               ==
C== ESTABLISH FILL CHARACTER                                      ==
C==                                                               ==
#ifdef BEND
      FILLER(1)=FILL
#else
      FILLER(2)=FILL
#endif
C==                                                               ==
C== LOOK FOR FIRST NON-FILL CHARACTER                             ==
C==                                                               ==
      IF(NC.LE.0) GOTO 180
      I=1
10    IF (I.GT. NC) GOTO 210
#ifdef BEND
      CHAR(2)=SOURCE(I)
#else
      CHAR(1)=SOURCE(I)
#endif
      IF(ICHAR.NE.IFILL) GOTO 20
      I=I+1
      GOTO 10
20    CONTINUE
C==                                                               ==
C== CHECK FOR SIGN CHARACTER                                      ==
C==                                                               ==
      IF(ICHAR.EQ.MINUS)GOTO 30
      IF(ICHAR.EQ.PLUS)I=I+1
      SIGN=1
      GOTO 40
30    SIGN=-1
      I=I+1
40    CONTINUE
      IF(I.GT.NC) GOTO 160
#ifdef BEND
      CHAR(2)=SOURCE(I)
#else
      CHAR(1)=SOURCE(I)
#endif
C==                                                               ==
C==SKIP OVER REMAINING FILL CHARACTERS                            ==
C==                                                               ==
      IF(ICHAR.NE.IFILL) GOTO 60
      I=I+1
      GOTO 40
C==                                                               ==
C== SAVE POSITION OF FIRST DIGIT                                  ==
C==                                                               ==
60    I0=I
      I1=I0+8
      LIM=I1
      IF(NC.LT.LIM)LIM=NC
C==                                                               ==
C== BEGIN CONVERSION                                              ==
C==                                                               ==
      N=0
70    CONTINUE
      IF(.NOT.(NMRC(ICHAR)))GOTO 160
75    CONTINUE
      N=10*N+(ICHAR-ZERO)
      IF(I.GE.LIM) GOTO 80
      I=I+1
#ifdef BEND
      CHAR(2)=SOURCE(I)
#else
      CHAR(1)=SOURCE(I)
#endif
      IF(.NOT.NMRC(ICHAR)) GOTO 90
      GOTO 75
80    CONTINUE
      IF(I.LT.NC) GOTO 100
      I=I+1
90    CONTINUE
      NC=I-1
      ND=I-I0
      INT=N*SIGN
      GOTO 210
C==                                                               ==
C== CASE OF 10 OR MORE DIGITS                                     ==
C==                                                               ==
100   CONTINUE
      I=I+1
#ifdef BEND
      CHAR(2)=SOURCE(I)
#else
      CHAR(1)=SOURCE(I)
#endif
      IF(.NOT.NMRC(ICHAR)) GOTO 90
      X=N
      X=10*X+(ICHAR-ZERO)
      IF(I.GE.NC) GOTO 105
      I=I+1
#ifdef BEND
      CHAR(2)=SOURCE(I)
#else
      CHAR(1)=SOURCE(I)
#endif
      IF(NMRC(ICHAR))GOTO 120
      I=I-1
105   CONTINUE
      X=X*SIGN
      IF((X.LT.MININT).OR.(X.GT.MAXINT)) GOTO 110
      INT=X
      NC=I
      ND=10
      GOTO 210
110   CONTINUE
      INT=0
      NC=I
      ND=10
      GOTO 200
120   CONTINUE
      IF(I.EQ.NC) GOTO 140
      I=I+1
#ifdef BEND
      CHAR(2)=SOURCE(I)
#else
      CHAR(1)=SOURCE(I)
#endif
      IF(.NOT.NMRC(ICHAR)) GOTO 130
      GOTO 120
130   CONTINUE
      I=I-1
140   CONTINUE
      INT=0
      ND=I-I0+1
      NC=I
      GOTO 200
160   I=I-1
      NC=I
      INT=0
      ND=0
      GOTO 200
180   CONTINUE
C==                                                               ==
C== ON INPUT, NC .LE. 0                                           ==
C==                                                               ==
      INT=0
      ND=0
200   RETURN 1
210   RETURN 0
      END
