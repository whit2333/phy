      SUBROUTINE LIST_FILENAMES(STRING)
#ifdef VMS
C
C   This routine displays all the filenames in the current or the
C   specified directory.  It is equivalent to entering the DCL command
C
C                     $DIRECTORY STRING
C
      CHARACTER STRING*(*), RESULT*132
CC     &         ,ESC*1, REVERSE*4, NORMAL*4, UNDER*4
      LOGICAL   FIRST
C
      COMMON   /PLOT_OUTPUT_UNIT/ IOUTS
C
      DATA IADDR / 0 /, IOUTS / 6 /
C      DATA ESC / 27 /
C
      EXTERNAL RMS$_NORMAL, RMS$_NMF, RMS$_FNF, RMS$_PRV
C
CC      REVERSE = ESC//'[7m'
CC      NORMAL  = ESC//'[0m'
CC      UNDER   = ESC//'[4m'
      FIRST   = .TRUE.
C
      ISTAT   = LIB$FIND_FILE_END(%REF(IADDR))
10    ISTAT   = LIB$FIND_FILE(STRING,RESULT,%REF(IADDR),,,,)
      LENR    = LENSIG(RESULT)
      IF( FIRST )THEN
        FIRST = .FALSE.
        IF( ISTAT .EQ. %LOC(RMS$_NORMAL) )THEN
          IDIR  = INDEX(RESULT,']')
          WRITE(IOUTS,20)'List of files located in '//RESULT(1:IDIR)
        ELSE IF( ISTAT .EQ. %LOC(RMS$_FNF) )THEN
          WRITE(IOUTS,20)'No files found'
          GO TO 10
        ELSE IF( ISTAT .EQ. %LOC(RMS$_NMF) )THEN
          WRITE(IOUTS,20)'No files found'
          RETURN
        ELSE 
          IDUM = NARGSI(2)
          CALL PUT_SYSMSG(ISTAT,IOUTS)
          WRITE(IOUTS,20)'Error opening '//RESULT(1:LENR)
          GO TO 10
        END IF
      END IF
      IF( ISTAT .EQ. %LOC(RMS$_NORMAL) )THEN
        IDIR2  = INDEX(RESULT,']')
        IF( IDIR2 .NE. IDIR )THEN
          IDIR = IDIR2
          WRITE(IOUTS,20)'List of files located in '//RESULT(1:IDIR)
        END IF
        WRITE(IOUTS,20)'     '//RESULT(IDIR+1:LENR)
      ELSE IF( ISTAT .EQ. %LOC(RMS$_FNF) )THEN
        WRITE(IOUTS,20)'No files found'
      ELSE IF( ISTAT .EQ. %LOC(RMS$_NMF) )THEN
        RETURN
      ELSE 
        IDUM = NARGSI(2)
        CALL PUT_SYSMSG(ISTAT,IOUTS)
        WRITE(IOUTS,20)'Error opening '//RESULT(1:LENR)
      END IF
      GO TO 10
20    FORMAT(' ',A)
#endif
#ifdef unix
C
C   This routine displays all the filenames in the current or the
C   specified directory.  It is equivalent to entering the ls command.
C
      CHARACTER*(*) STRING
      CHARACTER*255 STRING2
      STRING2='ls '//STRING(1:LENSIG(STRING))//' | more'
      CALL SYSTEM(STRING2(1:LENSIG(STRING2)))
      RETURN
#endif
      END
