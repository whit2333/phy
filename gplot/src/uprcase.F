      SUBROUTINE UPRCASE(LINE,UPLINE)
C  
C     reqd. routines - NONE
C================================================================
C================================================================
C==                                                            ==
C==   UPCASE:     Converts lower case ASCII characters in      ==
C==               "LINE" into upper case ASCII characters. All ==
C==               other characters remain the same. The result ==
C==               is returned in UPLINE.                       ==
C==               Both LINE and UPLINE are variable length     ==
C==               character strings, i.e.                      ==
C==               their length attribute is determined in the  ==
C==               calling routine. The lengths of "LINE" and   ==
C==               "UPLINE" need not be the same.     If "LINE" ==
C==               is longer than "UPLINE" then "LINE" will     ==
C==               be truncated and stored in "UPLINE"; if      ==
C==               "UPLINE" is longer than "LINE" then it       ==
C==               will be padded with blanks.                  ==
C==                                                            ==
C==   Adapted from UPPER_CASE.FOR       C.J.K  May/91          ==
C==                                                            ==
C==   Input  Parameters: LINE (CHARACTER*(N)).                 ==
C==                                                            ==
C==   Output Parameters: UPLINE  (CHARACTER*(N)).              ==
C==                                                            ==
C================================================================
C================================================================
      CHARACTER*(*) LINE,UPLINE
      CHARACTER TABLE(0:255)
      LOGICAL*1 NFIRST/.FALSE./
      SAVE
      IF(NFIRST)GO TO 100
C================================================================
C==   First time through this subroutine.                      ==
C==   Set up the character translation table to translate all  ==
C==   lower case ASCII characters to upper case.               ==
C================================================================
      NFIRST=.TRUE.
      DO 10 I=0,255
      TABLE(I)=CHAR(I)            ! Most characters are unchanged.
10    CONTINUE
      DO 20 I=97,122
      TABLE(I)=CHAR(I-32)         ! 0 parity lower case --> upper case.
      TABLE(I+128)=CHAR(I+128-32) ! 1 parity lower case --> upper case.
20    CONTINUE
100   LEN1=LEN(LINE)
      LEN2=LEN(UPLINE)
      DO 30 I=1,MIN(LEN1,LEN2)
      UPLINE(I:I)=TABLE(ICHAR(LINE(I:I))) ! Translate line.
30    CONTINUE
      IF(LEN2.GT.LEN1)UPLINE(LEN1+1:LEN2)=' '
      RETURN
      END

