/* RAN.C                                            F.W. Jones, TRIUMF
 *
 * Replacement for DEC Fortran intrinsic RAN which currently (<=Version3.1)
 * has the problem of occasionally returning numbers slightly greater
 * than 1.0.
 *
 * Usage:
 *
 *        EXTERNAL RAN
 *        ISEED=1234567
 *        X=RAN(ISEED)
 *        X=RAN(ISEED)
 *          ...etc.
 *
 * Note: to pick up this routine you must declare EXTERNAL RAN in each
 * calling routine!
 *
 * Note: unlike the standard RAN, this routine does not alter ISEED.
 * On the first call, ISEED is used as the seed and its value is recorded.
 * On subsequent calls, ISEED is ignored if it is the same as the recorded
 * value, and the random sequence continues unaltered.  If ISEED is found
 * to differ from the recorded value, then it is used as a new seed, its
 * value is recorded, and a new random sequence begins.
 * This means that to RE-START the sequence with the SAME seed as the 
 * previous one, the following construct is required:      
 *
 *       EXTERNAL RAN
 *       ISEED=1234567
 * C Generate random sequence:
 *       X=RAN(ISEED)      !seed with ISEED
 *       X=RAN(ISEED)      !ISEED ignored
 *       X=RAN(ISEED)      !ISEED ignored
 *           ...etc.          ...
 * C Generate the same random sequence:
 *       DUM=RAN(0)        !seed with 0
 *       X=RAN(ISEED)      !seed with ISEED
 *       X=RAN(ISEED)      !ISEED ignored
 *       X=RAN(ISEED)      !ISEED ignored
 *           ...etc.          ...
 */

extern double drand48();
extern void srand48();

float ran_(iseed)
long *iseed;
{
  static long iseed_save=0;
  float x;

  if (*iseed!=iseed_save) {
/*  printf("seeding with %d\n",*iseed); */
    srand48(*iseed);
    iseed_save = *iseed;
  }
  x = (float)drand48();

  return(x);
}

