/* AIX Fortran seems to have no GETC utility function */

#include <stdio.h>

int getc_()
{
   return getc(stdin);
}
