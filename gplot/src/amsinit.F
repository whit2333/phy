      SUBROUTINE AMSINIT(IENTRY,*)
C
C  ARINIT:  To initialize the Array Management System
C
C                    INPUT:
C
C  IENTRY:  The maximum number of arrays that may be simultaneously
C           assigned by AMS
C
C  Note:    This system is the analogy of the AMS system that exists
C           on MTS.
C
C  N.B. It is ESSENTIAL that the common block XD be aligned at least
C       on a double-word boundary. This may be ensured by setting up 
C       an options file to be included at LINK time with the AMS 
C       package. This file should contain a command to the linker
C       specifying the desired alignment, i.e.
C
C       PSECT_ATTR=XD,PAGE
C     
C       ensures that XD is PAGE aligned.
C
C  *** Altered by J. Chuma, Oct. 30, 1987:: added LOGICAL FIRST
C      so that this routine can be called by library routines, but
C      will only be executed once.
C
C      Altered by W. Pui, July 17,1991 to change declaration of
C      common block XD from R8D to I4D in order to agree in length
C      with other routines.
C
C      Modified by J. Chuma, March 7, 1997.  Eliminated the "$" from
C      common block and variable names for LINUX g77
C
      LOGICAL*1 L1D(1), FIRST
      INTEGER*4 I4D(1)
      REAL*8 R8D(1)
C
      DATA FIRST /.TRUE./
C
      EQUIVALENCE (L1D(1),I4D(1),R8D(1))
C
      COMMON /XD/ I4D     ! Changed from R8D July 17,1991 by Pui
      COMMON /IADDRD/ IADDR
      COMMON /TABLED/ MAXTABLE,NTABLE,IEXIST,IVEC,IADDRESS,IBYTES
C
      IF( .NOT.FIRST )RETURN
      FIRST = .FALSE.
C
      IF( IENTRY .LE. 0 )THEN
        WRITE(6,10)
10      FORMAT('0*** Error in ARINIT: IENTRY must be > 0')
        RETURN 1
      END IF
C
C  Get IADDR, the base address from which all the assigned arrays
C  will be assigned offset values
C
#ifdef _AIX
      IADDR = LOC(R8D(1))
#else
      IADDR = LOC(R8D(1))
#endif
C
C  Now get a block of space for the MASTER TABLE, a table which will
C  contain the the addresses of the assigned arrays.
C  We need IENTRY*1 words for the table of logical flags, and
C  3*(IENTRY*4) words for the table of offsets, addresses and lengths;
C  for a total of IENTRY*13 words.
C
      NBYTES   = IENTRY*13
C      WRITE(6,*)' Inside amsinit: before get_space. NBYTES=',NBYTES
      CALL GET_SPACE(NBYTES,KADDR,*900)
C      WRITE(6,*)' after get_space. NBYTES, KADDR= ',NBYTES,KADDR
      IEXIST   = KADDR-IADDR
      IVEC     = (IEXIST+IENTRY)/4
      IADDRESS = IVEC+IENTRY
      IBYTES   = IADDRESS+IENTRY
      NTABLE   = 0
      MAXTABLE = IENTRY
C
C  Initialize the "Exist" Table to .FALSE.
C
      DO I = 1, MAXTABLE
        L1D(IEXIST+I) = .FALSE.
      END DO
      RETURN
900   RETURN 1
C
C  Entry point AMSTABLE writes the current table status on unit 6
C
      ENTRY AMSTABLE
      NFILL = 0
      WRITE(6,200)
200   FORMAT('0***************  CURRENT AMS TABLE STORAGE ************')
      IF( NTABLE .LE. 0 ) GOTO 250
      WRITE(6,230)
230   FORMAT(1X,'  # EXIST   OFFSET  LENGTH'/)
      DO I = 1, NTABLE
        WRITE(6,220) I,L1D(IEXIST+I),I4D(IVEC+I),I4D(IBYTES+I)
220     FORMAT(1X,I3,3X,L1,3X,I8,2X,I6)
        IF( L1D(IEXIST+I) ) NFILL=NFILL+1 !COUNT THE VALID TABLE ENTRIES
      END DO
250   WRITE(6,260) NTABLE,NFILL
260   FORMAT(' ******** TOTAL OF ',I3,' ENTRIES, ',I3,' ARE ACTIVE ***')
      RETURN
      END
